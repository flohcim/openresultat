<?php
/**
 * Ce fichier est destine a permettre la surcharge de certaines methodes de
 * la classe om_formulaire pour des besoins specifiques de l'application
 *
 * @package openresultat
 * @version SVN : $Id$
 */

/**
 *
 */
require_once PATH_OPENMAIRIE."om_formulaire.class.php";

/**
 *
 */
class om_formulaire extends formulaire {

    /**
     * WIDGET_FORM - widget__election_workflow.
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     *
     * @return void
     */
    function widget__election_workflow($champ, $validation, $DEBUG = false) {
        // On appelle le widget de formulaire hidden en changeant l'attribut
        $tmp_type = $this->type[$champ];
        $this->type[$champ] = "hidden";
        $this->hidden($champ, $validation, $DEBUG);
        $this->type[$champ] = $tmp_type;
        // On affiche un bloc d'information sur le workflow de l'élection
        printf(
            '<div class="workflow-display-status">
                <span class="%1$s">Paramétrage</span>
                >
                <span class="%2$s">Simulation</span>
                >
                <span class="%3$s">Saisie</span>
                >
                <span class="%4$s">Finalisation</span>
                >
                <span class="%5$s">Archivage</span>
            </div>',
            ($this->val[$champ] === "" || $this->val[$champ] === "Paramétrage" ? "workflow-selected" : ""),
            ($this->val[$champ] === "Simulation" ? "workflow-selected" : ""),
            ($this->val[$champ] === "Saisie" ? "workflow-selected" : ""),
            ($this->val[$champ] === "Finalisation" ? "workflow-selected" : ""),
            ($this->val[$champ] === "Archivage" ? "workflow-selected" : "")
        );
    }

	function afficheall($champ, $validation, $DEBUG = false) {

        //----------------------------------------------------------
        $newWidth =570;
        $height=800;
        $heightcsv=400;
        //-----------------------------------------------------------
        // type autorise : .gif;.jpg;.jpeg;.png;.txt;.pdf;.csv
        if($this->val[$champ]!=""){
			$filename = $this->val[$champ];
			$scan_pdf = $this->f->storage->getPath($filename);
			$mimetype = $this->f->storage->getMimetype($filename);
			$info = $this->f->storage->getInfo($filename);
		}
        if($mimetype == 'application/pdf'){
            echo "<object data='".$scan_pdf."' name='".$champ."' value=\"".
            $scan_pdf."\" type=\"".$mimetype."\" width='100%' height='". $height."px'>";
            echo "</object>";
        }
        if(substr($mimetype,0,6) == 'image/'){
			//mo-------------------------------------
			$x_y = getimagesize($scan_pdf);
			// $x_y[0]."widht";
			// $x_y[1] ."height";
			$newHeight=($x_y[1]/ $x_y[0])*$newWidth;
			//echo "<img src=\"". $scan_pdf."\" width='".$newWidth."' height='". $$newHeight."' >";
			echo "<object data='".$scan_pdf."' name='".$champ."' value=\"".
				$scan_pdf."\" type='.$mimetype.' width='".$newWidth."' height='". $newHeight."''>";
				echo "</object>";
        }
        if($mimetype == 'text/plain'){
            $fichier=fopen($scan_pdf, "r");
            $contenu = fread($fichier, 10000);
            echo "<textarea name='".$champ."' rows='10' cols='50' class=\"champFormulaire\" >".
                $contenu."</textarea>";
        }
        if($mimetype == 'application/csv'){
            $fichier=fopen($scan_pdf, "r");
            $contenu = fread($fichier, 10000);
            $flag_separateur=0;
            // chr(9)-> tabulation : si le texte contient des tab -> c est le separateur
            // chr(59)-> ; : si le texte ne contient pas de tab -> c est le separeateur
            //          | : si le texte ne contient pas de tab -> c est le separeateur
            // chr(44)-> , : si on ne trouve pas de ";" "tab" ou "|" et que le texte contient des ','
            // pas de séparateur si aucun des éléments ci dessus est trouvé -> affichage des lignes
            $separateur="";
            if(strstr($contenu, chr(9))){
                $separateur=chr(9);
                $flag_separateur=1;
            }else{
                if(strstr($contenu, chr(59))){
                    $separateur=chr(59);
                    $flag_separateur=1;
                }
                if(strstr($contenu, "|")){
                    $separateur="|";
                    $flag_separateur=1;
                }
                if($flag_separateur==0 and strstr($contenu, chr(44))){
                    $separateur=chr(44);
                    $flag_separateur=1;
                }
            }
            if($flag_separateur==1){
                    $flagentete=1;
                    $tmp=explode("\n",$contenu);
                    echo "<div style='width:".$newWidth."px;height:". $heightcsv."px;overflow:auto;'>";
                    echo "<table id='table-csv'   cellpadding='0' cellspacing ='0' border='1px solid #000000;'><tr>";
                    foreach ($tmp as &$value) {
                        $tmp1=explode($separateur,$value);
                        echo "<tr>";
                        foreach ($tmp1 as &$value1) {
                            if ($flagentete==1){
                                echo "<td align='center' id='entete-table-csv' >".$value1."</td>";
                            }else{
                                echo "<td id='td-table-csv' >".$value1."</td>";
                            }
                        }
                        echo "</tr>";
                        $flagentete=0;
                        }
                        echo "</table>";
                        echo "</div>";
                }else{
                    echo "<textarea name='".$champ."' rows='10' cols='50' class=\"champFormulaire\" >".
                    $contenu."</textarea>";
                }
		}

	}

    function html_bible($champ, $validation, $DEBUG = false) {
        if(!isset($this->select[$champ]['class'])) {
            $this->select[$champ]['class'] = "";
        }
        if (!$this->correct) {
            $this->select[$champ]['class'] .= " html_bible";
            $this->textarea($champ, $validation, $DEBUG);
        } else {
            $this->htmlstatic($champ, $validation, $DEBUG);
        }
    }

/*
    function html_note($champ, $validation, $DEBUG = false) {
        if(!isset($this->select[$champ]['class'])) {
            $this->select[$champ]['class'] = "";
        }
        if (!$this->correct) {
            $this->select[$champ]['class'] .= " html_note";
            $this->textarea($champ, $validation, $DEBUG);
        } else {
            $this->htmlstatic($champ, $validation, $DEBUG);
        }
    }
*/
    function html_conservation($champ, $validation, $DEBUG = false) {
        if(!isset($this->select[$champ]['class'])) {
            $this->select[$champ]['class'] = "";
        }
        if (!$this->correct) {
            $this->select[$champ]['class'] .= " html_conservation";
            $this->textarea($champ, $validation, $DEBUG);
        } else {
            $this->htmlstatic($champ, $validation, $DEBUG);
        }
    }

    function html_controle_acces($champ, $validation, $DEBUG = false) {
        if(!isset($this->select[$champ]['class'])) {
            $this->select[$champ]['class'] = "";
        }
        if (!$this->correct) {
            $this->select[$champ]['class'] .= " html_controle_acces";
            $this->textarea($champ, $validation, $DEBUG);
        } else {
            $this->htmlstatic($champ, $validation, $DEBUG);
        }
    }

    function html_tracabilite($champ, $validation, $DEBUG = false) {
        if(!isset($this->select[$champ]['class'])) {
            $this->select[$champ]['class'] = "";
        }
        if (!$this->correct) {
            $this->select[$champ]['class'] .= " html_tracabilite";
            $this->textarea($champ, $validation, $DEBUG);
        } else {
            $this->htmlstatic($champ, $validation, $DEBUG);
        }
    }

    function html_protection($champ, $validation, $DEBUG = false) {
        if(!isset($this->select[$champ]['class'])) {
            $this->select[$champ]['class'] = "";
        }
        if (!$this->correct) {
            $this->select[$champ]['class'] .= " html_protection";
            $this->textarea($champ, $validation, $DEBUG);
        } else {
            $this->htmlstatic($champ, $validation, $DEBUG);
        }
    }

    function html_sauvegarde($champ, $validation, $DEBUG = false) {
        if(!isset($this->select[$champ]['class'])) {
            $this->select[$champ]['class'] = "";
        }
        if (!$this->correct) {
            $this->select[$champ]['class'] .= " html_sauvegarde";
            $this->textarea($champ, $validation, $DEBUG);
        } else {
            $this->htmlstatic($champ, $validation, $DEBUG);
        }
    }

    function html_chiffrement($champ, $validation, $DEBUG = false) {
        if(!isset($this->select[$champ]['class'])) {
            $this->select[$champ]['class'] = "";
        }
        if (!$this->correct) {
            $this->select[$champ]['class'] .= " html_chiffrement";
            $this->textarea($champ, $validation, $DEBUG);
        } else {
            $this->htmlstatic($champ, $validation, $DEBUG);
        }
    }

    function html_securite_technique($champ, $validation, $DEBUG = false) {
        if(!isset($this->select[$champ]['class'])) {
            $this->select[$champ]['class'] = "";
        }
        if (!$this->correct) {
            $this->select[$champ]['class'] .= " html_securite_technique";
            $this->textarea($champ, $validation, $DEBUG);
        } else {
            $this->htmlstatic($champ, $validation, $DEBUG);
        }
    }

    function html_securite_organisationnelle($champ, $validation, $DEBUG = false) {
        if(!isset($this->select[$champ]['class'])) {
            $this->select[$champ]['class'] = "";
        }
        if (!$this->correct) {
            $this->select[$champ]['class'] .= " html_securite_organisationnelle";
            $this->textarea($champ, $validation, $DEBUG);
        } else {
            $this->htmlstatic($champ, $validation, $DEBUG);
        }
    }

    /**
     * Renvoie les valeurs des options possibles d'un select en fonction de la valeur
     * d'un autre select.
     *
     * Si un validateur existe, il sera utilisé pour valider la valeur récupérée.
     * Si la valeur récupérée n'est pas valide, un code d'erreur 404 est renvoyé.
     *
     * /!\ : Cette fonction ne prend aucun paramètre, pour autant elle a besoin
     * des paramètres suivant issus de l'URL :
     * - "idx" : integer, la valeur du select utilisé pour filtrer les données
     *                    du select courant
     * - "tableName" : string, le nom du champ du champ select dont on veut filtrer les valeurs
     * - "linkedField" : string, le nom du champ du champ select à partir duquel on va filtrer les valeurs
     * - "formCible" : string, le nom du formulaire auxquels apartiennent les deux selects
     *
     * @return void
     */
    protected function snippet__filterselect() {
        // Il est nécessaire de désactiver les logs car si les modes VERBOSE, EXTRA VERBOSE, etc sont
        // activé, les logs sont affichés en HTML ce qui casse le format json attendu en sortie.
        $this->f->disableLog();

        // Définition des paramètres d'URL requis
        $required = array('idx', 'tableName', 'linkedField', 'formCible');

        // Récupération des paramètres requis provenant de l'URL (404 si indéfini ou vide)
        foreach ($required as $parameter) {
            if (! isset($_GET[$parameter]) || empty(${$parameter} = $this->f->get_submitted_get_value($parameter))) {
                header('HTTP/1.1 404 Not Found');
                return;
            }
        }

        // On cherche à récupérer les valeurs des options du select. Pour cela on a deux possibilités :
        //      1 - ecrire une requête sql pour récupérer ces valeurs
        //      2 - ce select correspondant à une classe/objet, utiliser la méthode associée au setSelect() de cette classe
        // La première est simple à mettre en place mais elle duplique le code de la méthode
        // de l'objet. On risque donc d'avoir des problèmes de maintenabilité car, en cas de modification,
        // le code devra être changé en plusieurs endroits. De plus, si la méthode snippet__filterselect()
        // utilise la méthode de l'objet, elle reste générique et peut être utilisée pour n'importe quel objet.
        // Pour éviter la duplication de code, on utilise donc la méthode de l'objet.
        //
        // Les valeurs des options du select ne sont accessibles que
        // depuis une méthode protégée de l'objet (dont on veut récupérer les valeurs).
        // Cependant cette méthode est accessible via le setSelect() de l'objet.
        // On cherche donc a recréér le contexte de l'appel de cette
        // méthode pour quelle produise le résulat désiré.

        // Premièrement, on a besoin d'un formulaire quelconque pour utiliser la méthode
        // setSelect() de l'objet dont on veut récupérer les valeurs (pour alimenter les
        // options du select).
        $form = $this->f->get_inst__om_formulaire(array());

        // Deuxièmement, on a besoin de l'objet pour pouvoir utiliser sa
        // méthode setSelect et recréer le contexte d'utilisation de la méthode servant à
        // récupérer les valeurs des options du select
        $object = $this->f->get_inst__om_dbform(array("obj" => $formCible, "idx" => "]"));

        // Afin d'éviter les erreurs et les failles de sécurité, on valide la valeur à partir
        // de laquelle on va filtre les données (si un validateur existe)
        $methodName = "validate_$linkedField";
        if (method_exists($object, $methodName)) {
            if (! $object->$methodName($idx)) {
                // code retour 404 car la valeur du champ du select est invalide donc on ne peut
                // trouver aucun objet avec
                header('HTTP/1.1 404 Not Found');
                return;
            }
        }
        // Création du contexte prérequis pour l'appel setSelect()
        $object->setParameter($linkedField, $idx);
        // Remplissage des valeurs des options du select en fonction du champ $linkedField
        $object->setSelect($form, 0);

        // Pour finir, on forme le json contenant les valeurs des options du select.
        // Pour cela, on récupère le select du formulaire.
        echo json_encode($form->select[$tableName]);
    }

    /**
     * WIDGET_FORM - select.
     *
     * Affiche le champ. Si un tableau extra_attr est fourni, son contenu
     * est ajouté en tant qu'attribut dans le code html du champ.
     * L'ajout se fait en utilisant les couples clés valeurs de cette manière :
     *   - clé => nom de l'attribut
     *   - valeur => valeur de l'attribut
     *
     * Exemple : afficher un select caché par défaut
     * select_custom('test1', 1, false, array(
     *   'style' => 'display:none;'
     * ))
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     * @param array $extra_attr tableau des attributs à ajouter
     *
     * @return void
     */
    protected function select_custom($champ, $validation, $DEBUG = false, $extra_attr = array()) {
        //code html des attributs
        $attr = array(
            'name' => $champ,
            'id' => $champ,
            'size' => 1,
            'class' => 'champFormulaire'
        );

        $attr = array_merge($attr, $extra_attr);

        if (!$this->correct) {
            if (! empty($this->onchange[$champ])) {
                $attr['onchange'] = $this->onchange[$champ];
            }
        } else {
            $attr['disabled'] = 'disabled';
        }

        $htmlAttributs = array_map(function ($key, $value) {
            return $key.'="'.$value.'"';
        }, array_keys($attr), $attr);

        // code html des options
        $htmlOptions = '';
        if (isset($this->select[$champ])) {
            $optKV = $this->select[$champ];
            if (is_array($optKV)) {
                if (count($optKV) == 2 && isset($optKV[0]) && isset($optKV[1])
                        && count($optKV[1]) == count($optKV[0])) {
                    $keys = $optKV[1];
                    $values = $optKV[0];
                    if (!$this->correct) {
                        foreach ($keys as $index => $key) {
                            $value = isset($values[$index]) ? strval($values[$index]) : '';

                            $attr_option['value'] = $value;
                            // si la valeur issue du formulaire est la même que la
                            // valeur de l'option, c'est qu'il s'agit du champ selectionné
                            if (isset($this->val[$champ]) && !empty($this->val[$champ]) && $this->val[$champ] == $value) {
                                $attr_option['selected'] = 'selected';
                            }

                            //code html des attributs de l'option
                            $htmlOptionAttributs = array_map(function ($key, $value) {
                                return $key.'="'.$value.'"';
                            }, array_keys($attr_option), $attr_option);

                            //ajout du code html de l'option au code html de la liste des options
                            $htmlOptions .= sprintf(
                                '<option%s>%s</option>',
                                (! empty($htmlOptionAttributs) ? ' ':'').implode(' ', $htmlOptionAttributs),
                                $key
                            );
                        }
                    } else {
                        // affiche uniquement l'option selectionnée
                        $selected = ! empty($this->val[$champ])
                            && $key = array_search($this->val[$champ], $values, true) ?
                                $key : '';
                        $htmlOptions .= sprintf(
                            '<option selected="selected value="%s">%s</option>',
                            $values[$selected],
                            $key[$selected]
                        );
                    }
                } else {
                    $this->addToLog('le format du contenu du select n\'est pas valide', DEBUG_MODE);
                }
            } else {
                $this->addToLog('Le contenu du select n\'est pas un tableau.'.
                    'Le traitement ne peut pas être effectué', DEBUG_MODE);
            }
        } else {
            $this->addToLog('le champ : '.$champ.' n\'existe pas', DEBUG_MODE);
        }

        $html = sprintf(
            '<select%s>%s</select>',
            (! empty($htmlAttributs) ? ' ':'').implode(' ', $htmlAttributs),
            $htmlOptions
        );
        echo $html;
    }

    /**
     * Affichage du champ dans le code de la page mais ce champ n'est pas
     * visible par l'utilisateur
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     *
     * @return void
     */
    protected function select_undisplayed($champ, $validation, $DEBUG = false) {
        $this->select_custom($champ, $validation, false, array(
                    'style' => 'display:none;'
                ));
    }

    /**
     * Affichage du champ dans le code de la page avec un attribut
     * style=display
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     *
     * @return void
     */
    protected function select_displayed($champ, $validation, $DEBUG = false) {
        $this->select_custom($champ, $validation, false, array(
                    'style' => 'display'
                ));
    }


    /**
     * WIDGET_FORM - hex.
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     *
     * @return void
     */
    protected function hexa($champ, $validation, $DEBUG = false) {
        $htmlEtat = 'disabled="disabled"';

        if (!$this->correct) {
            $htmlEtat = '';
            if (isset($this->onchange) and $this->onchange[$champ] != "") {
                $htmlEtat .= 'onchange="'.$this->onchange[$champ].'" ';
            }
            if (isset($this->onkeyup) and $this->onkeyup[$champ] != "") {
                $htmlEtat .= 'onkeyup="'.$this->onkeyup[$champ].'" ';
            }
            if (isset($this->onclick) and $this->onclick[$champ] != "") {
                $htmlEtat .= 'onclick="'.$this->onclick[$champ].'" ';
            }
        }

        $hexHtml = sprintf(
            '<input type="text" name="%s" id="%s" value="%s" size="%s"
            maxlength="%s" class="champFormulaire hexa" %s/>
            ',
            $champ,
            $champ,
            $this->val[$champ],
            $this->taille[$champ],
            $this->max[$champ],
            $htmlEtat
        );

        echo $hexHtml;
    }

    /**
     * WIDGET_FORM - checkboxdisabled.
     * Case à cocher qui ne peut pas être modifiée
     * 
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     *
     * @return void
     */
    function checkboxdisabled($champ, $validation, $DEBUG = false) {
        $value = '';
        $checked = '';

        if ($this->val[$champ] == 1 ||
            $this->val[$champ] == "t" ||
            $this->val[$champ] == "Oui"
        ) {
            $value = 'Oui';
            $checked = 'checked';
        }

        printf(
            '<input type="%s" name="%s" id="%s" value="%s" size="%s" maxlength="%s"
            class="champFormulaire" checked ="%s" disabled="disabled">',
            $this->type[$champ],
            $champ,
            $champ,
            $value,
            $this->taille[$champ],
            $this->max[$champ],
            $checked
        );
    }

    /**
     * NOUVELLE METHODE pour la localisation sur plan
     * La modification par rapport à la méthode originale est l'appel à la
     * fonction javascript localisation_plan()
     *
     * localisation
     * - $select['positiony'][0]="plan";// zone plan
     * - $select['positiony'][1]="positionx"; // zone coordonnees X
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     */
    function localisation_plan($champ, $validation, $DEBUG = false) {

        //
        echo "<input";
        echo " type=\"text\"";
        echo " name=\"".$champ."\"";
        echo " id=\"".$champ."\" ";
        echo " value=\"".$this->val[$champ]."\"";
        echo " size=\"".$this->taille[$champ]."\"";
        echo " maxlength=\"".$this->max[$champ]."\"";
        echo " class=\"champFormulaire localisation\"";
        if (!$this->correct) {
            if (isset($this->onchange) and $this->onchange[$champ] != "") {
                echo " onchange=\"".$this->onchange[$champ]."\"";
            }
            if (isset($this->onkeyup) and $this->onkeyup[$champ] != "") {
                echo " onkeyup=\"".$this->onkeyup[$champ]."\"";
            }
            if (isset($this->onclick) and $this->onclick[$champ] != "") {
                echo " onclick=\"".$this->onclick[$champ]."\"";
            }
        } else {
            echo " disabled=\"disabled\"";
        }
        echo " />\n";

        //
        if (!$this->correct) {
            // zone libelle
            $plan = $this->select[$champ][0][0];  // plan
            $positionx = $this->select[$champ][0][1];
            //          
            echo "<a class=\"localisation ui-state-default ui-corner-all\" href=\"javascript:localisation_plan('".$champ."','".$plan."','".$positionx."');\">";
            echo "<span class=\"ui-icon ui-icon-pin-s\" ";
            echo "title=\"".__("Cliquer ici pour positionner l'element")."\">";
            echo __("Localisation");
            echo "</span>";
            echo "</a>";
        }

    }

    /**
     * NOUVELLE METHODE pour la localisation sur plan
     * La modification para rapport à la méthode originale est l'appel à la
     * fonction javascript localisation_plan()
     * 
     * localisation
     * - $select['positiony'][0]="plan";// zone plan
     * - $select['positiony'][1]="positionx"; // zone coordonnees X
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     */
    function localisation_plan_static($champ, $validation, $DEBUG = false) {
        //
        $this->selectstatic($champ, $validation, $DEBUG);
        //
        echo "<a href=\"../app/localisation_plan_view.php?idx=".$this->val["plan_unite"]."&amp;obj=plan_unite&amp;retour=form#camap0\"><span title=\"Visualiser sur plan\" class=\"om-icon om-icon-16 om-icon-fix sig-16\">Plan</span></a>";
    }
}
