<?php
//$Id$ 
//gen openMairie le 05/03/2021 17:08

require_once "../gen/obj/delegation.class.php";

class delegation extends delegation_gen {

    /**
     * Definition des actions disponibles sur la classe
     * Supprime du portlet toutes les actions définies pour les élection unités
     * à part celle du CRUD
     *
     * @return void
     */
    function init_class_actions() {
        parent::init_class_actions();
        $this->class_actions[1]['condition'][0] = 'is_parametrege_saisie_or_simulation';
        $this->class_actions[2]['condition'][0] = 'is_parametrege_saisie_or_simulation';

        $this->class_actions[5] = array(
            "identifier" => "ajouter_multiple",
            "permission_suffix" => "ajouter_delegation_2",
            "crud" => "create",
            "method" => "ajouter_multiple"
        );
    }

    /**
     * Indique si l'étape en cours est celle du paramétrage, de la saisie ou
     * de la simulation
     *
     * @return boolean
     */
    protected function is_parametrege_saisie_or_simulation() {
        $election = $this->f->get_element_by_id('election', $this->getVal('election'));
        return $election->is_etape_saisie() ||
            $election->is_etape_parametrage() ||
            $election->is_etape_simulation();
    }

    /**
     * SETTER FORM - setType.
     *
     * @param formulaire $form Instance formulaire.
     * @param integer $maj Identifant numérique de l'action.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);

        // Formulaire d'ajout simplifié
        if ($maj == 5) {
            $form->setType("delegation", "hidden");
            if ($this->is_in_context_of_foreign_key("election", $this->retourformulaire)) {
                $form->setType("election", "selecthiddenstatic");
            }
            $form->setType('unite', "select_multiple");
            $form->setType('acteur', "select_multiple");
        }
    }

    /**
     * 
     */
    function setOnchange(&$form, $maj) {
        parent::setOnchange($form, $maj);
        // Suppression du 'verifNum' car il empêche la sélection de plusieurs item dans la liste
        if ($maj == 5) {
            $form->setOnchange('unite', '');
            $form->setOnchange('acteur', '');
        }
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_unite() {
        $sql =
            "SELECT
                unite.unite, concat(unite.code_unite, ' ', unite.libelle)
            FROM
                ".DB_PREFIXE."unite
            WHERE
                ((unite.om_validite_debut IS NULL AND
                    (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)) OR
                (unite.om_validite_debut <= CURRENT_DATE AND
                    (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)))
            ORDER BY
                unite.libelle ASC";
        if ($this->is_in_context_of_foreign_key("election", $this->retourformulaire)) {
            $electionId = $this->f->get_submitted_get_value('idxformulaire');
            $sql =
                "SELECT
                    unite.unite, concat(unite.code_unite, ' ', unite.libelle)
                FROM
                    ".DB_PREFIXE."unite
                    INNER JOIN ".DB_PREFIXE."election_unite ON unite.unite = election_unite.unite
                WHERE
                    election_unite.election = ".$electionId."
                ORDER BY
                    unite.libelle ASC";
        }
        return $sql;
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_unite_by_id() {
        return "SELECT
                unite.unite, concat(unite.code_unite, ' ', unite.libelle)
            FROM
                ".DB_PREFIXE."unite
            WHERE
                unite = <idx>";
    }

    /**
     * SETTER FORM - set_form_default_values
     * 
     * @param formulaire $form Instance formulaire.
     * @param integer $maj Identifant numérique de l'action.
     * @param integer $validation Marqueur de validation du formulaire.
     * 
     * @return void
     */
    function set_form_default_values(&$form, $maj, $validation) {
        if ($validation == 0 && $maj == 5) {
            $form->setVal('unite', '', $validation);
            $form->setVal('acteur', '', $validation);
            $form->setVal('delegation', '', $validation);
        }
    }

    /**
     * Récupère les données du formulaire et vérifie si elles sont correct.
     * Si c'est le cas vérifie si une election, des acteurs et des unités ont
     * bien été sélectionnés avant de créer les délégations correspondantes.
     *
     * @param array $val tableau contenant les valeurs issues du formulaire
     * @return boolean
     */
    protected function ajouter_multiple($val = array()) {
        $this->begin_treatment(__METHOD__);
        // Si les verifications precedentes sont correctes, on procede a
        // la modification, sinon on ne fait rien et on affiche un message
        // d'echec
        if ($this->correct) {
            if (! empty($val['acteur']) && ! empty($val['unite']) && ! empty($val['election'])) {
                $acteurs = explode(";", $val['acteur']);
                $unites = explode(";", $val['unite']);
                if (is_array($acteurs) && is_array($unites)) {
                    if (! $this->creer_delegation_multiple($val['election'], $unites, $acteurs)) {
                        $this->addToLog(__METHOD__."(): ERROR", DEBUG_MODE);
                        return $this->end_treatment(__METHOD__, false);
                    }
                }
            }
        } else {
            // Message d'echec (saut d'une ligne supplementaire avant le
            // message pour qu'il soit mis en evidence)
            $this->addToMessage("<br/>".__("SAISIE NON ENREGISTREE")."<br/>");
            // Return
            return $this->end_treatment(__METHOD__, false);
        }
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * Création dans la base de données de toutes les délégations liées à l'élection
     * pour tous les acteurs et toutes les unités fournies.
     *
     * @param integer $idElection identifiant de l'élection
     * @param array $unites  Liste des identifiants des unités
     * @param array $acteurs Liste des identifiants des acteurs
     *
     * @return boolean indique si le traitement à fonctionné
     */
    protected function creer_delegation_multiple($idElection, $unites, $acteurs) {
        $succes = true;
        foreach ($unites as $idUnite) {
            foreach ($acteurs as $idActeur) {
                if (! empty($idActeur) &&
                    ! empty($idUnite) &&
                    ! $this->delegation_existe($idElection, $idActeur, $idUnite)
                ) {
                    $delegation = $this->f->get_element_by_id('delegation', ']');
                    $data = array(
                        "delegation" => null,
                        "election" => $idElection,
                        "unite" => $idUnite,
                        "acteur" => $idActeur
                    );
                    if (! $delegation->ajouter($data)) {
                        $succes = false;
                        break;
                    }
                    $succes = true;
                    // Message informant l'utilisateur des délégation crées
                    $acteur = $this->f->get_element_by_id('acteur', $idActeur);
                    $unite = $this->f->get_element_by_id('unite', $idUnite);
                    $message = sprintf(
                        'delegation : %s -> %s',
                        $acteur->getVal('nom'),
                        $unite->getVal('libelle')
                    );
                    $this->addToMessage($message);
                }
            }
        }
        $message = $succes ? 'Les délégations ont été ajoutés avec succés' :
            'Erreur : la création des délégations à échouée';
        $this->addToMessage($message);
        return $succes;
    }

    /**
     * Requête sql permettant de récupérer la délégation dont l'élection, l'acteur
     * et l'unité correspondent aux paramétres.
     * Si un élement est récupéré, la délégation existe.
     * Renvoie true si la délégation existe et false sinon.
     *
     * @param integer id de l'élection
     * @param integer id de l'acteur
     * @param integer id de l'unité
     * @return boolean
     */
    protected function delegation_existe($idElection, $idActeur, $idUnite) {
        $sql = sprintf(
            'SELECT
                delegation
            FROM
                %sdelegation
            WHERE
                election = %d AND
                acteur = %d AND
                unite = %d',
            DB_PREFIXE,
            $idElection,
            $idActeur,
            $idUnite
        );
        $delegation = $this->f->db->getOne($sql);
        if ($this->f->isDatabaseError($delegation, true)) {
            $this->addToLog(__METHOD__." database error:".$delegation->getDebugInfo().";", DEBUG_MODE);
            $this->addToMessage('Erreur lors de la récupération de la délégation');
        }
        return ! empty($delegation);
    }
}
