<?php
//$Id$ 
//gen openMairie le 23/02/2021 11:02

require_once "../gen/obj/parti_politique.class.php";

class parti_politique extends parti_politique_gen {

    /**
     * SETTER FORM - setType.
     *
     * @param formulaire $form Instance formulaire.
     * @param integer $maj Identifant numérique de l'action.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        $form->setType('couleur', 'hexa');
    }
}
