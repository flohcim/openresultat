<?php
//$Id$
//gen openMairie le 16/12/2020 17:03

require_once "../gen/obj/plan_unite.class.php";

class plan_unite extends plan_unite_gen {

    /**
     * SETTER FORM - setLib
     *
     * @param formulaire $form Instance formulaire.
     * @param integer $maj Identifant numérique de l'action.
     *
     * @return void
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        $form->setLib('img_unite_arrivee', 'unités arrivées');
        $form->setLib('img_unite_non_arrivee', 'unités non arrivées');
        $form->setLib('position_x', '[X]');
        $form->setLib('position_y', '[Y]');
        $form->setLib('largeur_icone', 'largeur des icones');
    }
    
    /**
     * SETTER FORM - setLayout
     *
     * @param formulaire $form Instance formulaire.
     * @param integer $maj Identifant numérique de l'action.
     *
     * @return void
     */
    function setLayout(&$form, $maj) {
        // Fieldset unité contiens dans l'ordre : libellé, type et ordre
        $form->setFieldset('plan', 'D', ' '._('Lien').' ', 'collapsible');
            $form->setBloc('plan', 'D', "", "group");
            $form->setBloc('unite', 'F');
        $form->setFieldset('unite', 'F', '');

        // Fieldset prefecture contiens dans l'ordre : canton et circonscription
        $form->setFieldset('img_unite_arrivee', 'D', ' '._('Affichage icônes').' ', 'collapsible');
            $form->setBloc('img_unite_arrivee', 'D', "", "group");
            $form->setBloc('img_unite_non_arrivee', 'F');
        $form->setFieldset('img_unite_non_arrivee', 'F', '');

        // Fieldset adresse contiens dans l'ordre : adresse1, 2, code postal, ville
        // le geom, les coordonnées, la longitude et la latitude
        $form->setFieldset('position_x', 'D', ' '._('Positionnement').' ', 'collapsible');
            $form->setBloc('position_x', 'D', "", "group");
            $form->setBloc('position_y', 'F');
        $form->setFieldset('position_y', 'F', '');
    }

    /**
     * SETTER FORM - setType.
     *
     * @param formulaire $form Instance formulaire.
     * @param integer $maj Identifant numérique de l'action.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        if ($maj < 2) {
            $form->setType('position_y', 'localisation_plan');
            $form->setType('couleur_texte', 'hexa');
        }

        if ($maj == 3) {
            $form->setType('position_x', 'hidden');
            $form->setType('position_y', 'hidden');
            $form->setType('plan', 'localisation_plan_static');
        }
    }

    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj);
        // MODE AJOUTER & MODE MODIFIER
        if ($maj == 0 || $maj == 1) {
            // Localisation
            $contenu = array();
            $contenu[0] = array('plan', 'position_x');
            $form->setSelect("position_y", $contenu);
        }
    }

    /**
     * SETTER FORM - set_form_default_values
     *
     * Par défaut les position en x et y de l'unité sont mise à 0
     *
     * @param formulaire $form Instance formulaire.
     * @param integer $maj Identifant numérique de l'action.
     * @param integer $validation Marqueur de validation du formulaire.
     *
     * @return void
     */
    function set_form_default_values(&$form, $maj, $validation) {
        if ($validation==0 and $maj==0) {
            $form->setVal('position_x', 0);
            $form->setVal('position_y', 0);
        }
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_unite() {
        return "SELECT
                unite.unite, concat(unite.code_unite, ' ', unite.libelle)
            FROM
                ".DB_PREFIXE."unite
            WHERE
                ((unite.om_validite_debut IS NULL AND
                    (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)) OR
                    (unite.om_validite_debut <= CURRENT_DATE AND
                        (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)))
            ORDER BY
                unite.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_unite_by_id() {
        return "SELECT
                unite.unite, concat(unite.code_unite, ' ', unite.libelle)
            FROM
                ".DB_PREFIXE."unite
            WHERE
                unite = <idx>";
    }
}
