<?php
//$Id$ 
//gen openMairie le 17/09/2020 14:28

require_once "../gen/obj/candidat.class.php";

class candidat extends candidat_gen {

    /**
     * SETTER FORM - setLib
     *
     * @param formulaire $form Instance formulaire.
     * @param integer $maj Identifant numérique de l'action.
     *
     * @return void
     */
    function setLib(&$form, $maj) {
        //libelle des champs pour éviter les termes techniques
        parent::setLib($form, $maj);
        $form->setLib('libelle_liste', 'Liste');
    }

    /**
     * Vérifie la validité des valeurs en mode CREATE & UPDATE.
     *
     * @param array $val Tableau des valeurs brutes.
     * @param null &$dnu1 @deprecated  Ne pas utiliser.
     * @param null $dnu2 @deprecated  Ne pas utiliser.
     *
     * @return void
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        $result = parent::verifier($val);
        // Vérifie si une unité possédant le même nom n'existe pas déjà
        if (! $this->isUnique('libelle', $val['libelle'])) {
            $this->addToMessage('ATTENTION : un candidat portant ce nom existe déjà !');
        }
        return $result;
    }
}
