<?php
//$Id$
//gen openMairie le 10/08/2020 10:07

require_once "../gen/obj/election_unite.class.php";

class delegation_participation extends election_unite_gen {

    protected $_absolute_class_name = "delegation_participation";

    /**
     * Liste des tranches horaires rattachées à l'élection
     *
     * @var array
     */
    protected $tranches;

    /**
     * Constructeur.
     *
     * @param string $id Identifiant de l'objet.
     * @param null &$dnu1 @deprecated  Ne pas utiliser.
     * @param null $dnu2 @deprecated  Ne pas utiliser.
     */
    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
        if ($id!="]") {
            // nombre d'unites a afficher depuis election_unite
            $electionId = $this->getVal("election");
            $election = $this->f->get_element_by_id('election', $electionId);
            $tranches = $election->get_participation_election($electionId);
            $this->tranches = $tranches;
            foreach ($tranches as $tranche) {
                $this->champs[] = $tranche->getVal('participation_election');
            }

            // champs servant à récupérer l'heure d'ouverture du formulaire
            $this->champs[] = 'ouverture';
        }
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_unite() {
        return "SELECT
                unite.unite, concat(unite.code_unite, ' ', unite.libelle)
            FROM
                ".DB_PREFIXE."unite
            WHERE
                ((unite.om_validite_debut IS NULL AND
                    (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)) OR
                    (unite.om_validite_debut <= CURRENT_DATE AND
                        (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)))
            ORDER BY
                unite.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_unite_by_id() {
        return "SELECT
                unite.unite, concat(unite.code_unite, ' ', unite.libelle)
            FROM
                ".DB_PREFIXE."unite
            WHERE
                unite = <idx>";
    }

    /**
     * Definition des actions disponibles sur la classe
     * Supprime du portlet toutes les actions définies pour les élection unités
     * à part celle du CRUD
     *
     * @return void
     */
    function init_class_actions() {
        parent::init_class_actions();
        $this->class_actions[1]['portlet']['libelle'] = 'Saisir la participation';
        $this->class_actions[1]['condition'][1] = 'pas_perimetre';
        $this->class_actions[1]['condition'][2] = 'est_autorise';
        $this->class_actions[1]['condition'][3] = 'is_saisie_or_simulation';
        $this->class_actions[2] = '';
        // Remise à zéro de toutes les actions sauf ajouter, supprimer, modifier
        for ($index = 4; $index < count($this->class_actions); $index++) {
            $this->class_actions[$index] = array();
        }

        // ACTION - 04 - affichage
        $this->class_actions[4] = array(
            "identifier" => "affichage",
            "portlet" => array(
                "type" => "action-direct-with-confirmation",
                "libelle" => _("envoyer à l'animation"),
                "order" => 50,
                "class" => "arrow-right-16",
            ),
            "view" => "formulaire",
            "method" => "envoyer_affichage",
            "button" => "diffuser",
            "permission_suffix" => "publication_part_aff",
            'condition' => array(
                'is_finalisation_saisie_or_simulation'
            )
        );

        // ACTION - 06 - remiseazero
        $this->class_actions[5] = array(
            "identifier" => "desafficher",
            "portlet" => array(
                "type" => "action-direct-with-confirmation",
                "libelle" => _("supprimer de l'animation"),
                "order" => 50,
                "class" => "delete-16",
            ),
            "view" => "formulaire",
            "method" => "supprimer_affichage",
            "permission_suffix" => "publication_part_aff",
            'condition' => array(
                'is_saisie_or_simulation',
                'est_envoye_a_affichage'
            )
        );

        // ACTION - 06 - envoyer au portail web
        $this->class_actions[6] = array(
            "identifier" => "web",
            "portlet" => array(
                "type" => "action-direct-with-confirmation",
                "libelle" => _("envoyer au portail web"),
                "order" => 60,
                "class" => "arrow-right-16",
            ),
            "view" => "formulaire",
            "method" => "envoyer_web",
            "button" => "diffuser",
            "permission_suffix" => "publication_part_web",
            'condition' => array(
                'is_finalisation_saisie_or_simulation'
            )
        );

        // ACTION - 07 - supprimer du portail web
        $this->class_actions[7] = array(
            "identifier" => "depublier_web",
            "portlet" => array(
                "type" => "action-direct-with-confirmation",
                "libelle" => _("supprimer du portail web"),
                "order" => 70,
                "class" => "delete-16",
            ),
            "view" => "formulaire",
            "method" => "supprimer_web",
            "permission_suffix" => "publication_part_web",
            'condition' => array(
                'is_saisie_or_simulation',
                'est_envoye_au_web'
            )
        );
    }

    /**
     * SETTER FORM - setType.
     *
     * @param formulaire $form Instance formulaire.
     * @param integer $maj Identifant numérique de l'action.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        // Affiche uniquement les champs relatif à l'élection et l'unité
        foreach ($this->champs as $value) {
            $form->setType($value, 'hidden');
        }

        $form->setType('unite', "selecthiddenstatic");
        $form->setType('election', "selecthiddenstatic");
        $form->setType($this->clePrimaire, "hiddenstatic");
        // Champs permettant de récupérer l'heure d'ouverture du formulaire.
        $form->setType('ouverture', "hidden");

        if ($maj < 2) {
            foreach ($this->tranches as $tranche) {
                $form->setType($tranche->getVal('participation_election'), 'text');
            }
        }

        // Pour les actions appelée en POST en Ajax, il est nécessaire de
        // qualifier le type de chaque champs (Si le champ n'est pas défini un
        // PHP Notice:  Undefined index dans core/om_formulaire.class.php est
        // levé). On sélectionne donc les actions de portlet de type
        // action-direct ou assimilé et les actions spécifiques avec le même
        // comportement.
        if ($this->get_action_param($maj, "portlet_type") == "action-direct"
            || $this->get_action_param($maj, "portlet_type") == "action-direct-with-confirmation") {
            //
            $form->setType($this->clePrimaire, "hiddenstatic");
        }
    }

    /**
     * SETTER FORM - setOnchange.
     *
     * @param formulaire $form Instance formulaire.
     * @param integer $maj Identifant numérique de l'action.
     *
     * @return void
     */
    function setOnchange(&$form, $maj) {
        parent::setOnchange($form, $maj);
        foreach ($this->tranches as $tranche) {
            $form->setOnchange($tranche->getVal('participation_election'), 'VerifNum(this)');
        }
    }

    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        parent::setTaille($form, $maj);
        foreach ($this->tranches as $tranche) {
            $form->setTaille($tranche->getVal('participation_election'), 11);
        }
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        parent::setMax($form, $maj);
        foreach ($this->tranches as $tranche) {
            $form->setMax($tranche->getVal('participation_election'), 11);
        }
    }

    /**
     * Methode setLib
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        foreach ($this->tranches as $tranche) {
            $horaire = $this->f->get_element_by_id('tranche', $tranche->getVal('tranche'));
            $form->setLib($tranche->getVal('participation_election'), $horaire->getVal('libelle'));
        }
        $form->setLib('ouverture', 'ouverture');
    }

    /**
     * SETTER FORM - set_form_default_values
     *
     * @param formulaire $form Instance formulaire.
     * @param integer $maj Identifant numérique de l'action.
     * @param integer $validation Marqueur de validation du formulaire.
     *
     * @return void
     */
    function set_form_default_values(&$form, $maj, $validation) {
        if (($validation == 0 && $maj == 1) || ($maj != 1)) {
            foreach ($this->tranches as $tranche) {
                $participationUnite = $this->get_participation_unite($tranche->getVal('participation_election'));
                $votant = $participationUnite->getVal('votant');
                $form->setVal($tranche->getVal('participation_election'), $votant, $validation);
            }
        }

        // Récupère le temps à l'ouverture du formulaire d'ajout, de modification et de suppression
        if ($validation == 0) {
            $form->setVal('ouverture', microtime(true));
        }
    }

    /**
     * SETTER FORM - setLayout
     *
     * @param formulaire $form Instance formulaire.
     * @param integer $maj Identifant numérique de l'action.
     *
     * @return void
     */
    function setLayout(&$form, $maj) {
        if ($maj == 3) {
            // Fieldset Election contiens dans l'ordre : election_unite, election, libellé
            $form->setBloc('election_unite', 'D', "", ""); // debut du bloc
                $form->setFieldset('election_unite', 'D', _('Election'), "collapsible");
                $form->setFieldset('unite', 'F', '');
            $form->setBloc('unite', 'F', ''); // fin fieldset election
        }
        if ($maj < 2) {
            // Fieldset Election contiens dans l'ordre : election_unite, election, libellé
            $form->setBloc('election_unite', 'D', "", ""); // debut du bloc
                $form->setFieldset('election_unite', 'D', _('Election'), "collapsible");
                $form->setFieldset('unite', 'F', '');
            $form->setBloc('unite', 'F', ''); // fin fieldset election
            $tranches  = $this->tranches;
            $lastTranche = $tranches[count($tranches) - 1];
            // Fieldset Participation contiens les tranches horaires dans l'ordre
            $form->setBloc($tranches[0]->getVal('participation_election'), 'D', "", "");
                $form->setFieldset($tranches[0]->getVal('participation_election'), 'D', 'Participation', "collapsible");
                $form->setFieldset($lastTranche->getVal('participation_election'), 'F', '');
            $form->setBloc($lastTranche->getVal('participation_election'), 'F', '');
        }
    }

    /**
     * Vérifie la validité des valeurs en mode CREATE & UPDATE.
     *
     * - Vérifie qu'il n'y a pas de conflit lors de la saisie'
     * 
     * @param array $val Tableau des valeurs brutes.
     * @param null &$dnu1 @deprecated  Ne pas utiliser.
     * @param null $dnu2 @deprecated  Ne pas utiliser.
     *
     * @return void
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        parent::verifier($val, $dnu1, $dnu2);
        // Vérification que les participations enregistrées ne risque pas de rentrer en conflit avec
        // d'autre participations
        if (array_key_exists('ouverture', $val)) {
            foreach ($this->tranches as $tranche) {
                $idPartElection = $tranche->getVal('participation_election');
                $participationUnite = $this->get_participation_unite($idPartElection);
                if ($participationUnite->f->conflit_saisie($participationUnite, $val['ouverture'])) {
                    $this->addToMessage('Attention: votre modification est en conflit avec la modification précédente!');
                    $this->correct = false;
                    return;
                }
            }
        }
    }

    /**
     * TRIGGER - triggermodifierapres
     *
     * Met à jour les résultats des périmètres qui contiennent l'unité qui viens
     * d'être modifiée
     *
     * @param string $id
     * @param null &$dnu1 @deprecated  Ne pas utiliser.
     * @param array $val Tableau des valeurs brutes.
     * @param null $dnu2 @deprecated  Ne pas utiliser.
     *
     * @param integer $id identifiant de l'objet
     *
     * @return void
     */
    function triggermodifierapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        $message = '';
        $succes = true;
        foreach ($this->tranches as $tranche) {
            $votant = $val[$tranche->getVal('participation_election')];
            $horaire = $this->f->get_element_by_id('tranche', $tranche->getVal('tranche'));
            $maj = $this->set_votant_participation_unite(
                $votant,
                $tranche->getVal('participation_election'),
                $this->getVal('election_unite')
            );
            $message .= ! $maj ? 'Le nombre de votant enregistré pour '
                .$horaire->getVal('libelle')
                .' n\'a pas été sauvegardé.<br>'
                : '';
            $succes = $succes && $maj;
        }

        // Mise à jour des résultats des périmètres
        $this->maj_participation_perimetre();

        // Si la mise à jour à fonctionnée et que tous les éléments ont été saisies
        // alors les informations sont envoyées à la page web et à l'affichage
        if ($succes && $this->est_saisie()) {
            $this->envoyer_affichage();
            $this->envoyer_web();
        }
        $this->addToMessage($message);
    }

    /**
     * Récupère par une requête sql, la participation de l'unité.
     * La participation par unité est récupèrée pour l'unité et la participation_election
     * (tranche horaire rattachée à l'élection) passée en paramétre
     *
     * @param integer $participationElectionId id de la participation_election (tranche horaire
     * rattachée à l'élection)
     *
     * @return participation_unite ou null en cas d'erreur de bd
     */
    protected function get_participation_unite($participationElectionId) {
        $sql = sprintf(
            'SELECT
                participation_unite
            FROM
                %sparticipation_unite
            WHERE
                election_unite = %d AND
                participation_election = %d',
            DB_PREFIXE,
            $this->getVal('election_unite'),
            $participationElectionId
        );

        $idParticipationUnite = $this->f->db->getOne($sql);
        $this->addToLog(__METHOD__."(): db->getOne(\"".$sql."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($idParticipationUnite, true)) {
            $this->addToLog(__METHOD__." database error:".$idParticipationUnite->getDebugInfo().";", DEBUG_MODE);
            $this->addToMessage('Erreur, la participation par unité n\'a pas été récupérée');
            return null;
        }

        $participationUnite = $this->f->get_element_by_id('participation_unite', $idParticipationUnite);
        return $participationUnite;
    }


    /**
     * Calcul le nombre total de votant enregistré pour la tranche horaire de l'élection
     * donnée (identifiant de participation_election).
     * Le calcul n'est effectué qu'à partir des unités de type bureau de vote pour éviter les
     * doublons.
     * Seul les résultats envoyés à l'affichage sont pris en compte pour le calcul
     *
     * @param string 'aff' ou 'web' type d'envoi
     * @param integer identifiant de la tranche horaire de l'élection
     *
     * @return mixed integer|null nombre de votant si il a pu être récupéré, null sinon
     */
    protected function calcul_participation_envoye_tranche($envoi, $idParticipationElec) {
        $votants = null;
        if ($envoi === 'aff' || $envoi === 'web' && ! empty($idParticipationElec)) {
            $sql = sprintf(
                'SELECT
                    sum(participation_unite.votant)
                FROM
                    %sparticipation_election
                    INNER JOIN %sparticipation_unite
                        ON participation_election.participation_election = participation_unite.participation_election
                    INNER JOIN %selection_unite
                        ON participation_unite.election_unite = election_unite.election_unite
                    INNER JOIN %sunite
                        ON election_unite.unite = unite.unite
                    INNER JOIN %stype_unite
                        ON unite.type_unite = type_unite.type_unite
                WHERE
                    type_unite.bureau_vote = true
                    AND participation_unite.envoi_%s = true
                    AND participation_unite.participation_election = %d',
                DB_PREFIXE,
                DB_PREFIXE,
                DB_PREFIXE,
                DB_PREFIXE,
                DB_PREFIXE,
                $this->f->db->escapeSimple($envoi),
                $idParticipationElec
            );

            $votants = $this->f->db->getOne($sql);
            $this->addToLog(__METHOD__."(): db->getOne(\"".$sql."\");", VERBOSE_MODE);
            if ($this->f->isDatabaseError($votants, true)) {
                $this->addToLog(__METHOD__." database error:".$votants->getDebugInfo().";", DEBUG_MODE);
                $this->addToMessage('Erreur, la participation par unité n\'a pas été récupérée');
                return null;
            }
        }
        return $votants;
    }

    /**
     * Calcul le nombre de votant enregistré pour un périmètre à partir de la
     * participation des unités qu'il contiens
     *
     * @param integer identifiant du périmètre
     * @param integer identifiant de la tranche horaire de l'élection (participation_election)
     *
     * @return mixed integer|null nombre de votant si il a pu être récupéré, null sinon
     */
    protected function calcul_participation_perimetre($idUniteParent, $idParticipationElec) {
        $sql = sprintf(
            'SELECT
                SUM(participation_unite.votant) AS votant
            FROM
                %slien_unite
                INNER JOIN %selection_unite
                    ON lien_unite.unite_enfant = election_unite.unite
                INNER JOIN %sparticipation_unite
                    ON election_unite.election_unite = participation_unite.election_unite
            WHERE
                unite_parent = %d
                AND participation_election = %d',
            DB_PREFIXE,
            DB_PREFIXE,
            DB_PREFIXE,
            $idUniteParent,
            $idParticipationElec
        );
        $votants = $this->f->db->getOne($sql);
        $this->addToLog(__METHOD__."(): db->getOne(\"".$sql."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($votants, true)) {
            $this->addToLog(__METHOD__." database error:".$votants->getDebugInfo().";", DEBUG_MODE);
            $this->addToMessage('Erreur, le nombre de votant n\'a pas été récupéré');
            return null;
        }
        return $votants;
    }
    
    /**
     * Enregistre le nombre de votant donné dans la table
     * base de donnée
     *
     * @param integer votant : nombre de votant
     * @param integer participationElectionId :  identifiant de la tranche horaire de l'élection
     * @return boolean indique si le traitement à fonctionné
     */
    protected function set_votant_participation_unite($votant, $participationElectionId, $electionUniteId){
        $succes = false;

        // Modification du nombre de votant
        $modif = array(
            'votant' => is_numeric($votant) ? $votant : null,
            'date_derniere_modif' => microtime(true),
            'saisie' => is_numeric($votant) ? true : false
        );

        $maj = $this->f->db->autoExecute(
            DB_PREFIXE."participation_unite",
            $modif,
            DB_AUTOQUERY_UPDATE,
            'election_unite = '.$electionUniteId.
            ' AND participation_election = '.$participationElectionId
        );

        // En cas d'erreur annule la modif sinon met à jour les résultats
        if ($this->f->isDatabaseError($maj, true)) {
            $this->addToLog(__METHOD__." database error:".$maj->getDebugInfo().";", DEBUG_MODE);
            $this->addToMessage('Erreur lors de l\'enregistrement de la participation');
            $this->f->db->rollback();
        } else {
            $succes = true;
            $this->f->db->commit();
        }

        return $succes;
    }

    /**
     * Met à jour le nombre de votant des périmètres contenant l'unité.
     * Réalise également l'envoi à l'affichage et au portail web si la saisie
     * est valide pour le périmètre.
     *
     * @return boolean indique si le traitement à fonctionné
     */
    protected function maj_participation_perimetre(){
        // Récupèration de la liste des périmètres contenant l'unité
        // et mise à jour des résultats de chacun de ces périmètres
        $electionUnite = $this->f->get_element_by_id('election_unite', $this->getVal('election_unite'));
        $perimetres = $electionUnite->get_perimetres_election_unite(
            $this->getVal('unite'),
            $this->getVal('election')
        );

        $majParticipation = true;
        foreach ($perimetres as $perimetre) {
            $estSaisi = true;
            foreach ($this->tranches as $tranche) {
                // Calcul de la participation par tranche horaire
                $votant = $this->calcul_participation_perimetre(
                    $perimetre->getVal('unite'),
                    $tranche->getVal('participation_election')
                );
                // Enregistrement des résultats
                $majParticipation = $this->set_votant_participation_unite(
                    $votant,
                    $tranche->getVal('participation_election'),
                    $perimetre->getVal('election_unite')
                );
                if (! $majParticipation) {
                    $this->addToMessage('Erreur lors de l\'enregistrement de la participation des périmètres');
                    return false;
                }
                // Vérifie si la participation du périmètre a été saisie pour chaque tranche horaire
                $estSaisi = $estSaisi + is_numeric($votant);
            }
            // Si la participation est complétement saisie, elle est publiée
            if ($estSaisi) {
                $delegation = $this->f->get_element_by_id(
                    'delegation_participation',
                    $perimetre->getVal('election_unite')
                );
                $delegation->envoyer_affichage();
                $delegation->envoyer_web();
            }
        }
        $this->addToMessage('Participation des périmètres enregistrée');
        return $majParticipation;
    }
    
    

    /**
     * Indique si l'utilisateur à le droit de saisir la participation de cette
     * unité de l'élection.
     *
     * @return booleen true : autorisé, false : pas autorisé
     */
    protected function est_autorise() {
        return $this->f->est_autorise($this->getVal('election'), $this->getVal('unite'));
    }

    /**
     * Indique si la participation enregistrée pour la tranche horaire a été evoyé à l'affichage
     *
     * @return boolean
     */
    protected function est_envoye_a_affichage() {
        $envoiAff = true;
        foreach ($this->tranches as $tranche) {
            $participation = $this->get_participation_unite($tranche->getVal('participation_election'));
            $envoiPart = $this->f->boolean_string_to_boolean($participation->getVal('envoi_aff'));
            $envoiAff = $envoiPart && $envoiAff;
        }
        return $envoiAff;
    }

    /**
     * Indique si la participation enregistrée pour la tranche horaire a été evoyé au
     * portail web
     *
     * @return boolean
     */
    protected function est_envoye_au_web() {
        $envoiWeb = true;
        foreach ($this->tranches as $tranche) {
            $participation = $this->get_participation_unite($tranche->getVal('participation_election'));
            $envoiPart = $this->f->boolean_string_to_boolean($participation->getVal('envoi_web'));
            $envoiWeb = $envoiPart && $envoiWeb;
        }
        return $envoiWeb;
    }

    /**
     * Indique si la participation enregistrée est saisie
     *
     * @return boolean
     */
    protected function est_saisie() {
        $saisie = true;
        foreach ($this->tranches as $tranche) {
            $participation = $this->get_participation_unite($tranche->getVal('participation_election'));
            $saisiPart = $this->f->boolean_string_to_boolean($participation->getVal('saisie'));
            $saisie = $saisiPart && $saisie;
        }
        return $saisie;
    }

    /**
     * Indique si l'unité n'est pas un périmètre
     *
     * @return booleen true : pas perimetre, false : perimetre
     */
    protected function pas_perimetre() {
        //instance de l'unité permettant d'accéder à l'attribut is_perimetre
        $unite = $this->f->get_element_by_id('unite', $this->getVal('unite'));
        return ! $this->f->boolean_string_to_boolean($unite->getVal('perimetre'));
    }

    /**
     * Indique si l'étape en cours est celle de la saisie ou
     * de la simulation
     *
     * @return boolean
     */
    protected function is_saisie_or_simulation() {
        $election = $this->f->get_element_by_id('election', $this->getVal('election'));
        return $election->is_etape_saisie() || $election->is_etape_simulation() ;
    }

    /**
     * Indique si l'étape en cours est celle de la saisie, de la finalisation ou
     * de la simulation
     *
     * @return boolean
     */
    protected function is_finalisation_saisie_or_simulation() {
        $election = $this->f->get_element_by_id('election', $this->getVal('election'));
        return $election->is_etape_saisie() ||
            $election->is_etape_finalisation() ||
            $election->is_etape_simulation();
    }

    /**
     * Met à jour les attributs d'envoi à la page web et la participation
     * dans le repertoire web
     *
     * @return boolean
     */
    protected function envoyer_web() {
        // Maj des attributs d'envoi à la page web
        $succes = true;
        foreach ($this->tranches as $participationElec) {
            $participation = $this->get_participation_unite($participationElec->getVal('participation_election'));
            $modif = $this->f->modifier_element_BD_by_id(
                'participation_unite',
                $participation->getVal('participation_unite'),
                array('envoi_web' => true)
            );
            $succes = $succes && $modif;
        }
        if (! $succes) {
            $this->addToMessage('L\'attribut d\'envoi au portail web n\'a pas été mis à jour');
        }

        // Met à jour le fichier contenant la participation dans le répertoire web
        $succes = $succes && $this->web();
        return $succes;
    }

    /**
     * Met à jour les attributs d'envoi à l'affichage et la participation
     * dans le repertoire aff
     *
     * @return boolean
     */
    protected function envoyer_affichage() {
        $succes = true;
        // Creation du repertoire de l'élection. Permet d'éviter les problèmes d'écriture
        // de fichier pour l'envoi à l'affichage. En effet, si plusieurs repertoire doivent
        // être crées le fichier n'arrive pas à s'écrire. C'est par exemple le cas pour la
        // participation qui est dans le repertoire bpar du repertoire de l'élection.
        if (! file_exists('../aff/res/'.$this->getVal('election'))) {
            if (! mkdir('../aff/res/'.$this->getVal('election'))) {
                $this->addToMessage('repertoire aff de l\'élection non crée');
                $succes = false;
            }
        }
        // Récupération du périmètre
        $election = $this->f->get_element_by_id('election', $this->getVal('election'));
        $election->affichage_perimetre($this->getVal('election'));
        // Maj des attributs d'envoi à l'animation
        foreach ($this->tranches as $participationElec) {
            $participation = $this->get_participation_unite($participationElec->getVal('participation_election'));
            $modif = $this->f->modifier_element_BD_by_id(
                'participation_unite',
                $participation->getVal('participation_unite'),
                array('envoi_aff' => true)
            );
            $succes = $succes && $modif;
        }
        if (! $succes) {
            $this->addToMessage('Les attributs d\'envoi à l\'affichage n\'ont pas été mis à jour');
        }

        // Met à jour le fichier contenant la participation dans le répertoire web
        $succes = $succes && $this->affichage();
        return $succes;
    }

    /**
     * Met à jour les attributs d'envoi à la page web et la participation
     * dans le repertoire web
     *
     * @return boolean
     */
    protected function supprimer_web() {
        // Maj des attributs d'envoi à la page web
        $succes = true;
        foreach ($this->tranches as $participationElec) {
            $participation = $this->get_participation_unite($participationElec->getVal('participation_election'));
            $modif = $this->f->modifier_element_BD_by_id(
                'participation_unite',
                $participation->getVal('participation_unite'),
                array('envoi_web' => false)
            );
            $succes = $succes && $modif;
        }
        if (! $succes) {
            $this->addToMessage('L\'attribut d\'envoi au portail web n\'a pas été mis à jour');
        }

        // Met à jour le fichier contenant la participation dans le répertoire web
        $succes = $succes && $this->web();
        return $succes;
    }

    /**
     * Met à jour les attributs d'envoi à l'affichage et la participation
     * dans le repertoire aff
     *
     * @return boolean
     */
    protected function supprimer_affichage() {
        // Maj des attributs d'envoi à la page web
        $succes = true;
        foreach ($this->tranches as $participationElec) {
            $participation = $this->get_participation_unite($participationElec->getVal('participation_election'));
            $modif = $this->f->modifier_element_BD_by_id(
                'participation_unite',
                $participation->getVal('participation_unite'),
                array('envoi_aff' => false)
            );
            $succes = $succes && $modif;
        }
        if (! $succes) {
            $this->addToMessage('Les attributs d\'envoi à l\'affichage n\'ont pas été mis à jour');
        }

        // Met à jour le fichier contenant la participation dans le répertoire web
        $succes = $succes && $this->affichage();
        return $succes;
    }

    /**
     * Ecris le fichier contenant la participation par unité dans le
     * répertoire de l'affichage.
     * ex : resultat de l'unité 3 de l'élection 1 -> aff/res/1/bpar/b3.json
     *
     * @return boolean
     */
    protected function affichage() {
        // Récupération des informations concernant les résultats de l'unité
        $unite = $this->f->get_element_by_id('unite', $this->getVal('unite'));
        $election = $this->f->get_element_by_id('election', $this->getVal('election'));
        $contenu = array(
            'election' => $election->getVal('libelle'),
            'unite_libelle' => $unite->getVal('code_unite').' '.$unite->getVal('libelle'),
            'inscrit' => $this->getVal('inscrit'),
            'votant' => $this->getVal('votant'),
            'exprime' => $this->getVal('exprime'),
            'blanc' => $this->getVal('blanc'),
            'nul' => $this->getVal('nul'),
            'participations' => array()
        );

        // Récupération de la participation par tranche horaire si elle a été envoyée à l'affichage
        foreach ($this->tranches as $partElection) {
            $tranche = $this->f->get_element_by_id('tranche', $partElection->getVal('tranche'));
            $participation = $this->get_participation_unite($partElection->getVal('participation_election'));
            if ($this->f->boolean_string_to_boolean($participation->getVal('envoi_aff'))) {
                $votant = $participation->getVal('votant');
    
                $tx = ! empty($this->getVal('inscrit')) && is_numeric($votant) ?
                    number_format(round($votant * 100 / $this->getVal('inscrit'), 2), 2)."%" : 0;
    
                $contenu['participations'][$tranche->getVal('libelle')] = array(
                    'heure' => $tranche->getVal('libelle'),
                    'votant' => $votant,
                    'tx' => $tx
                );
            }
        }

        // écriture du fichier contenant la participation
        $contenu = json_encode($contenu);
        $cheminFichier = "../aff/res/".$this->getVal('election')."/bpar/b".$this->getVal('unite').".json";
        $succes = $this->f->write_file($cheminFichier, $contenu);

        $message = $succes ? 'La participation de l\'unité a été envoyé à l\'affichage' :
            'Erreur : la participation n\'a pas été envoyée';
        $this->addToMessage($message);
        return $succes;
    }

    /**
     * Ecris le fichier contenant les informations relatives à la participation
     * dans le répertoire web (participation.inc).
     *
     * @return boolean
     */
    protected function web() {
        // Récupération du nombre d'inscrit de l'élection qui correspond au nombre d'inscrit
        // du périmètre de l'élection
        $electionUnite = $this->f->get_element_by_id('election_unite', $this->getVal('election_unite'));
        $election = $this->f->get_element_by_id('election', $this->getVal('election'));
        $path = $election->get_election_web_path();
        $perimetreElection = $electionUnite->get_instance_election_unite(
            $election->getVal('perimetre'),
            $election->getVal('election')
        );
        $inscrits = $perimetreElection->getVal('inscrit');

        // Remplissage du tableau contenant la participation total par tranche horaire avec le taux
        // de votant pour chaque tranche
        $indexParticipation = 0;
        $tableauParticipation = '';
        foreach ($this->tranches as $partElection) {
            $tranche = $this->f->get_element_by_id('tranche', $partElection->getVal('tranche'));
            $votants = $this->calcul_participation_envoye_tranche(
                'web',
                $partElection->getVal('participation_election')
            );

            $tx = ! empty($inscrits) && is_numeric($votants) ?
                number_format(round($votants * 100 / $inscrits, 2), 2)."%"
                : '0%';
            $tableauParticipation .= sprintf(
                '$participations[%d] = array(
                    "heure" => "%s",
                    "votants" => "%d",
                    "tx" => "%s");
                ',
                $indexParticipation++,
                $tranche->getVal('libelle'),
                $votants,
                $tx
            );
        }

        // Ecriture du fichier participation.inc
        $contenu = sprintf(
            '<?php
                %s
            ?>',
            $tableauParticipation
        );
        $succes = $this->f->write_file($path."/participation.inc", $contenu);

        $message = $succes ? 'La participation a été envoyé à la page web' :
            'Erreur : la participation n\'a pas été envoyée';
        $this->addToMessage($message);
        return $succes;
    }
}
