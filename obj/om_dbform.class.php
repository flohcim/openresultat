<?php
/**
 * Ce script définit la classe 'om_dbform'.
 *
 * @package openresultat
 * @version SVN : $Id$
 */

require_once PATH_OPENMAIRIE."om_dbform.class.php";

/**
 * Définition de la classe 'om_dbform' (om_dbform).
 *
 * Cette classe est destinée à permettre la surcharge de certaines méthodes de
 * la classe 'om_dbform' du framework pour des besoins spécifiques de
 * l'application.
 */
class om_dbform extends dbForm {

    /**
     *
     */
    function get_values_substitution_vars($om_collectivite_idx = null) {
        $values = parent::get_values_substitution_vars($om_collectivite_idx);
        //
        $values["aujourdhui"] = date("d/m/Y");
        $values["aujourdhui_lettre"] = strftime("%d %B %Y");
        $values["maintenant"] = date("G:i");
        //
        return $values;
    }

    /**
     *
     */
    function get_labels_substitution_vars($om_collectivite_idx = null) {
        $labels = parent::get_labels_substitution_vars($om_collectivite_idx);
        //
        $labels["divers"]["aujourdhui"] = __("Date du jour (Format : 14/01/1978)");
        $labels["divers"]["aujourdhui_lettre"] = __("Date du jour (Format : 14 janvier 1978)");
        $labels["divers"]["maintenant"] = __("Heure (Format : 12:12)");
        //
        return $labels;
    }

    /**
     * Accesseur standard à une ressource.
     *
     * Cette méthode permet d'instancier la classe passée en paramètre selon
     * deux logiques différentes :
     *  - Cas n°1 : soit on veut instancier un objet en particulier de manière
     *    ponctuelle alors on passe le paramètre id qui correspond à
     *    l'identifiant de l'objet sur lequel on veut instancier la classe, et
     *    l'instanciation est effectuée et la ressource retournée.
     *  - Cas n°2 : soit on veut instancier un objet lié (clé étrangère) à
     *    l'objet courant et on ne passe donc pas de paramètre id, car il est
     *    récupéré directement sur l'objet courant (on peut éventuellement
     *    indiquer le nom du champ à récupérer par le paramètre field sinon
     *    c'est le nom de la classe qui est utilisé), et l'instanciation est
     *    effectuée et la ressource stockée puis retournée. Attention, si la
     *    ressource a déjà été stockée lors d'un appel précédent alors on la
     *    retourne sans réinstanciation.
     *
     * @param string $class Nom de la classe à instancier.
     * @param string|null $id Identifiant de l'objet à instancier.
     * @param string|null $field Nom du champ ou récupérer l'identifiant de
     *                    l'objet à instancier si différent du nom de la classe.
     *
     * @return resource
     */
    function get_inst_common($class, $id = null, $field = null) {
        //// Gestion du cas n°1 -> Instanciation ponctuelle
        // Si un identifiant est passé en paramètre
        if ($id !== null) {
            // Retour de l'instanciation
            return $this->f->get_inst__om_dbform(array(
                "obj" => $class,
                "idx" => $id,
            ));
        }

        //// Gestion du cas n°2 -> Instanciation liée à l'objet courant
        // On définit le nom de l'attribut dans lequel on va stocker la
        // ressource
        $var_name = "inst_".$class;
        // Si l'attribut n'existe pas ou est initialisé à null
        if (!isset($this->$var_name) || $this->$var_name === null) {
            // Si le paramètre field n'est pas passé en paramètre
            // alors on utilise le nom de la classe
            if ($field === null) {
                $field = $class;
            }
            // Stockage de l'instanciation dans l'attribut de l'objet courant
            $this->$var_name = $this->f->get_inst__om_dbform(array(
                "obj" => $class,
                "idx" => $this->getVal($field),
            ));
        }
        // Retour de l'instanciation
        return $this->$var_name;
    }

    /**
     * MERGE_FIELDS - Liste des classes *and co*.
     * @var array
     */
    var $merge_fields_and_co_obj = array(
    );

    /**
     * MERGE_FIELDS - get_merge_fields_and_co.
     *
     * @return array
     */
    public function get_merge_fields_and_co($type, $avoid_merge_fields_and_co_obj = array()) {
        switch ($type) {
            case 'values':
                $values = array();
                foreach ($this->merge_fields_and_co_obj as $key => $value) {
                    if (in_array($value, $avoid_merge_fields_and_co_obj) === true) {
                        continue;
                    }
                    $elem = $this->get_inst_common($value);
                    if ($elem != null) {
                        $elem_values = $elem->get_merge_fields($type);
                        $values = array_merge($values, $elem_values);
                        $elem_values = $elem->get_merge_fields_and_co($type);
                        $values = array_merge($values, $elem_values);
                    }
                }
                return $values;
                break;
            case 'labels':
                $labels = array();
                foreach ($this->merge_fields_and_co_obj as $key => $value) {
                    if (in_array($value, $avoid_merge_fields_and_co_obj) === true) {
                        continue;
                    }
                    $elem = $this->f->get_inst__om_dbform(array(
                        "obj" => $value,
                        "idx" => 0,
                    ));
                    $elem_labels = $elem->get_merge_fields($type);
                    $labels = array_merge($labels, $elem_labels);
                    $elem_labels = $elem->get_merge_fields_and_co($type);
                    $labels = array_merge($labels, $elem_labels);
                }
                return $labels;
                break;
            default:
                return array();
                break;
        }
    }
}
