<?php
//$Id$ 
//gen openMairie le 05/03/2021 17:09

require_once PATH_OPENMAIRIE."obj/om_utilisateur.class.php";

class om_utilisateur extends om_utilisateur_core {

    /**
     * TRIGGER - triggerajouterapres.
     *
     * Ajoute une ligne dans la table acteur en utilisant le login et
     * le nom de l'utilisateur.
     *
     * @param string $id
     * @param null &$dnu1 @deprecated  Ne pas utiliser.
     * @param array $val Tableau des valeurs brutes.
     * @param null $dnu2 @deprecated  Ne pas utiliser.
     *
     * @return boolean
     */
    function triggerajouterapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        $valActeur = array(
            'acteur' => null,
            'nom' => $val['nom'],
            'login' => $val['login']
        );
        $this->f->ajouter_element_BD('acteur', $valActeur);
    }

    /**
     * TRIGGER - triggerajouterapres.
     *
     * @param string $id
     * @param null &$dnu1 @deprecated  Ne pas utiliser.
     * @param array $val Tableau des valeurs brutes.
     * @param null $dnu2 @deprecated  Ne pas utiliser.
     *
     * @return boolean
     */
    function triggermodifierapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        if ($val['nom'] !== $this->getVal('nom') ||
            $val['login'] !== $this->getVal('login')
        ) {
            $valActeur = array(
                'nom' => $val['nom'],
                'login' => $val['login']
            );
            // /!\ : utilisation de "l'ancien" login car si le login a été modifié l'acteur
            //       ne pourra pas être retrouvé
            $acteur = $this->recupere_acteur_avec_login($this->getVal('login'));
            if ($acteur !== false) {
                $this->f->modifier_element_BD_by_id('acteur', $acteur->getVal('acteur'), $valActeur);
            }
        }
    }

    /**
     * TRIGGER - triggerajouterapres.
     *
     * @param string $id
     * @param null &$dnu1 @deprecated  Ne pas utiliser.
     * @param array $val Tableau des valeurs brutes.
     * @param null $dnu2 @deprecated  Ne pas utiliser.
     *
     * @return boolean
     */
    function triggersupprimer($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        $acteur = $this->recupere_acteur_avec_login($this->getVal('login'));
        if ($acteur !== false) {
            $this->f->supprimer_element_BD($acteur);
        }
    }

    /**
     * Récupère l'identifiant d'un acteur en faisant une requête sur
     * son login.
     * Si l'identifiant de l'acteur a pu être récupéré récupère l'acteur
     * correspondant.
     * Sinon renvoi false.
     *
     * @param string $login : login de l'acteur
     * @return mixed acteur | boolean
     */
    protected function recupere_acteur_avec_login($login) {
        $sql = sprintf(
            "SELECT
                acteur
            FROM
                %sacteur
            WHERE
                login LIKE '%s'",
            DB_PREFIXE,
            $this->f->db->escapeSimple($login)
        );
        $res = $this->f->db->getOne($sql);
        $this->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);

        // Renvoie l'instance si un id a été récupéré et false sinon
        $acteur = false;
        if (! empty($res)) {
            $acteur = $this->f->get_element_by_id('acteur', $res);
        }
        return $acteur;
    }
}
