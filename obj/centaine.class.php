<?php
//$Id$
//gen openMairie le 30/05/2019 16:10

require_once "election.class.php";

class centaine extends election {

    /**
     * Nom de la classe
     *
     * @var string = "centaine"
     */
    protected $_absolute_class_name = "centaine";

    /**
     * Definition des actions disponibles sur la classe
     * Supprime du portlet toutes les actions définies pour l'élection
     * à l'exception des actions d'ajout, de suppression, de consultation
     * et de modification
     *
     * @return void
     */
    function init_class_actions() {
        parent::init_class_actions();
        $this->class_actions[1]["condition"][1] = 'election_etape_parametrage_simulation';
        $this->class_actions[2]["condition"][0] = 'election_etape_parametrage_simulation';

        $actionsASupprimer = array(
            4,  //reset
            5,  //import des inscrits
            11, //export prefecture
            15, // calcul des sièges
            19, // vérification paramétrage
            20, // maj du paramétrage des plans
            21, // transfert du paramétrage des plans
            30, // Passage des étapes du workflow
            31,
            32,
            33,
            34,
            35,
            36,
            37,
            43,
            45
        );
        foreach ($actionsASupprimer as $action) {
            $this->class_actions[$action] = array();
        }
    }

    /**
     * SETTER FORM - setType.
     *
     * @param formulaire $form Instance formulaire.
     * @param integer $maj Identifant numérique de l'action.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        // Cache tous les champs de l'élection par défaut et seul les champs souhaité sont
        // affichés par la suite
        foreach ($this->champs as $champs) {
            $form->setType($champs, "hidden");
        }

        $form->setType("libelle", "hiddenstatic");
        $form->setType("votant_defaut", "hiddenstatic");
        $form->setType('web', 'selecthiddenstatic');

        if ($this->is_in_context_of_foreign_key("election", $this->retourformulaire)) {
            $form->setType("election_reference", "selecthiddenstatic");
        }
        if ($maj < 2) {
            $form->setType("libelle", 'text');
            $form->setType('votant_defaut', 'text');
            $form->setType('web', 'select');
        }
        if ($maj != 0) {
            $form->setType("election", "hiddenstatic");
        }
        if ($maj == 3) {
            $form->setType("election", "static");
        }
    }

    /**
     * SETTER FORM - setLayout
     *
     * @param formulaire $form Instance formulaire.
     * @param integer $maj Identifant numérique de l'action.
     *
     * @return void
     */
    function setLayout(&$form, $maj) {
        // Fieldset Paramétrage centaine contiens dans l'ordre : libellé, élection de réference
        // nombre de votant
        $form->setBloc('election', 'D', "", ""); // debut du bloc
            $form->setFieldset('election', 'D', _('Paramétrage centaine'), "collapsible");
            $form->setFieldset('election_reference', 'F', '');
        $form->setBloc('election_reference', 'F', ''); // fin
        // Fieldset Paramétrage portail web contiens dans l'ordre : web
        $form->setBloc('web', 'D', "", ""); // debut du bloc
            $form->setFieldset('web', 'D', _('Portail web'), "collapsible");
            $form->setFieldset('web', 'F', '');
        $form->setBloc('web', 'F', ''); // fin
    }

    /**
     * SETTER FORM - setLib
     *
     * @param formulaire $form Instance formulaire.
     * @param integer $maj Identifant numérique de l'action.
     *
     * @return void
     */
   function setLib(&$form, $maj) {
        //libelle des champs
        parent::setLib($form, $maj);
        $form->setLib('election', 'centaine');
        $form->setLib('votant_defaut', 'nombre de votant');
        $form->setLib('election_reference', 'élection de référence');
    }

    /**
     * Dans le contexte d'un sous-formulaire ded l'élection set automatiquement
     * l'idetifiant de l'élection de référence en prenant celui de l'élection.
     *
     * Remplis également les valeurs des champs de la centaine en copiant ceux
     * de l'élection de référence. Les valeurs copiées sont :
     *   - le type d'election
     *   - la date
     *   - le perimetre
     *   - les horaires d'ouverture et de fermeture
     *
     */
    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if ($validation == 0) {
            // Copie des valeurs du formulaire de l'élection dans le formulaire de la centaine
            // La délégation de saisie est géré au niveau de l'élection et pas de la centaine
            // les valeurs des options de délégation n'ont donc pas besoin d'être copiée.
            // Idem pour la conservation des résultats après la simulation.
            // Ces options sont toujours inactive pour une centaine.
            if ($this->is_in_context_of_foreign_key('election', $this->retourformulaire)) {
                $form->setVal('election_reference', $idxformulaire);
                $electionRef = $this->f->get_element_by_id('election', $idxformulaire);
                $champsAAffecter = array(
                    'code',
                    'tour',
                    'type_election',
                    'date',
                    'perimetre',
                    'heure_ouverture',
                    'heure_fermeture',
                    'publication_auto',
                    'publication_erreur',
                    'calcul_auto_exprime'
                );
                foreach ($champsAAffecter as $champ) {
                    $form->setVal($champ, $electionRef->getVal($champ));
                }
            }
        }
        $this->set_form_default_values($form, $maj, $validation);
    }

    /**
     * SETTER FORM - set_form_default_values
     *
     * @param formulaire $form Instance formulaire.
     * @param integer $maj Identifant numérique de l'action.
     * @param integer $validation Marqueur de validation du formulaire.
     *
     * @return void
     */
    function set_form_default_values(&$form, $maj, $validation) {
        // Copie les champs de l'élection sur ceux de la centaine
        if ($validation==0 and $maj==0) {
            $form->setVal('is_centaine', 't');
            // A la creation le champ is_centaine est mis à vrai
            // Comme ce champs n'est pas accessible, il ne pourra pas être
            // modifié par la suite et sera donc toujours vrai
            $form->setVal('votant_defaut', 100);
            // Même principe pour le workflow. L'étape de workflow de la centaine
            // est la simulation car elle permet la saisie des résultats (ce qui
            // évite des problèmes de permission lors de la saisie des centaines)
            // mais également car elle permet l'utilisation des éditions, extraction
            // (hors envoi prefecture) et d'accéder à la page web
            // Permet donc d'avoir les actions voulues tout en ayant la saisie
            $form->setVal('workflow', 'Simulation');
            // Options qui ne concerne pas les centaines
            $form->setVal('validation_obligatoire', 'f');
            $form->setVal('delegation_saisie', 'f');
            $form->setVal('delegation_participation', 'f');
            $form->setVal('garder_resultat_simulation', 'f');
        }
    }

    /**
     * TRIGGER - triggerajouterapres.
     *
     * @param string $id
     * @param null &$dnu1 @deprecated  Ne pas utiliser.
     * @param array $val Tableau des valeurs brutes.
     * @param null $dnu2 @deprecated  Ne pas utiliser.
     *
     * @return boolean
     */
    function triggerajouterapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        parent::triggerajouterapres($id, $dnu1, $val, $dnu2);
        $this->copier_candidats_election_reference($val['election_reference'], $id);
        $this->set_votant_et_inscrit_par_unite(
            $id,
            $val['votant_defaut'],
            $val['election_reference']
        );
    }

    /**
     * TRIGGER - triggermodifier.
     *
     * @param string $id
     * @param null &$dnu1 @deprecated  Ne pas utiliser.
     * @param array $val Tableau des valeurs brutes.
     * @param null $dnu2 @deprecated  Ne pas utiliser.
     *
     * @return boolean
     */
    function triggermodifier($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        parent::triggermodifier($id, $dnu1, $val, $dnu2);
        $this->set_votant_et_inscrit_par_unite(
            $id,
            $val['votant_defaut'],
            $val['election_reference']
        );
    }

    /**
     * TRIGGER - triggersupprimer.
     *
     * @param string $id
     * @param null &$dnu1 @deprecated  Ne pas utiliser.
     * @param array $val Tableau des valeurs brutes.
     * @param null $dnu2 @deprecated  Ne pas utiliser.
     *
     * @return boolean
     */
    function triggersupprimer($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        parent::triggersupprimer($id, $dnu1, $val, $dnu2);
    }

    /**
     * Récupère la liste des candidats d'une élection et ajoute
     * les candidats, de la liste, à la centaine.
     * En fin de tratement affiche un message indiquant combien
     * de candidat ont été ajouté
     *
     * @param integer $electionRefId : identifiant de l'élection de référence
     */
    protected function copier_candidats_election_reference($referenceId, $electionId) {
        $succes = true;
        $candidats = $this->get_candidats_election($referenceId);
        // Les photos n'ont pas besoin d'être copiée elles seront récupérées dans le repertoire
        // de l'élection à laquelle est liée la centaine
        $required = array(
            'candidat',
            'ordre',
            'prefecture',
            'creation_resultat',
            'age_moyen',
            'siege',
            'age_moyen_com',
            'siege_com',
            'age_moyen_mep',
            'siege_mep',
            'couleur',
            'parti_politique'
        );

        foreach ($candidats as $candidat) {
            $data = array(
                'election_candidat' => null,
                'photo' => null,
                'election' => $electionId
            );
            foreach ($required as $champs) {
                $data[$champs] = $candidat->getVal($champs);
            }
            $succes = $candidat->ajouter($data);
        }
        return $succes;
    }

    /**
     * Rempli le nombre de votant de chaque unité avec le nombre de votant par
     * défaut
     *
     * @param integer $idCentaine : identifiant de la centaine dont on veut modifier
     * les résultats
     * @param integer $nbVotant : nombre de votant a enregistrer
     */
    protected function set_votant_et_inscrit_par_unite($idCentaine, $nbVotant, $idElectionRef) {
        $unites = $this->get_unites_election($idCentaine);
        // pour chaque unité de la centaine
        foreach ($unites as $unite) {
            // Récupération de l'unité de référence pour avoir le nombre d'inscrit
            $uniteRef = $unite->get_election_unite(
                $idElectionRef,
                $unite->getVal('unite')
            );
            $nbInscrit = ! empty($uniteRef->getVal('inscrit')) ? $uniteRef->getVal('inscrit') : 0;
            $nbVotant = $this->f->validateur_entier_positif($nbVotant) ? $nbVotant : 0;
            $modif = array(
                'votant' => $nbVotant,
                'inscrit' => $nbInscrit
            );
            //Modifie l'unité correspondante qui appartiens à l'élection
            $modification = $this->f->db->autoExecute(
                DB_PREFIXE."election_unite",
                $modif,
                DB_AUTOQUERY_UPDATE,
                "election_unite = ".$unite->getVal('election_unite')
            );
            $this->addToLog(__METHOD__ . "() : db->autoExecute(" . $modification . ")", VERBOSE_MODE);
            if ($this->f->isDatabaseError($modification, true)) {
                $this->addToMessage('Erreur lors du remplissage du nombre de votant et d\'inscrit');
                $this->db->rollback();
                return false;
            }
            $this->db->commit();
        }
        return true;
    }

    /**
     * Surcharge de la méthode de la classe élection pour éviter les erreurs
     *
     * @return void
     */
    function afterSousFormSpecificContent() {
        $maj = $this->getParameter("maj");
        if ($maj==3) {
            $this->tableau_resultat_centaine();
        }
    }

    /**
     * Code html du tableau récapitulant les résultats des bureaux pour la centaine.
     *
     * @return void
     */
    protected function tableau_resultat_centaine() {
        $htmlTableau = '';
        $oddEven = 'odd';
        $unites = $this->get_unites_election($this->getVal('election'));

        // Préparation des colonnes dédiées aux résultats des candidats et
        // au total des voix obtenus par les candidats
        $candidats = $this->get_candidats_election($this->getVal('election'));

        foreach ($unites as $uniteElec) {
            $unite = $this->f->get_element_by_id('unite', $uniteElec->getVal('unite'));
            if ($uniteElec->getVal('unite') === $this->getVal('perimetre')) {
                $perimetreElec = $uniteElec;
            }
            if ($uniteElec->is_bureau()) {
                $htmlResCandidats = '';
                // Préparation des résultats des colonnes dédiées aux candidats
                foreach ($candidats as $candidat) {
                    $resultat = $uniteElec->get_resultat_candidat_unite(
                        $candidat->getVal('election_candidat'),
                        $uniteElec->getVal('election_unite')
                    );
                    $htmlResCandidats .= sprintf(
                        '<td>%d</td>',
                        $resultat
                    );
                }

                $htmlTableau .= sprintf(
                    '<tr class="tab-data %s">
                        <td> %s </td>
                        <td> %d </td>
                        <td> %d </td>
                        <td> %d </td>
                        <td> %d </td>
                        <td> %d </td>
                        %s
                    </tr>',
                    $oddEven,
                    $unite->getVal('code_unite').' '.$unite->getVal('libelle'),
                    $uniteElec->getVal('inscrit'),
                    $uniteElec->getVal('votant'),
                    $uniteElec->getVal('blanc'),
                    $uniteElec->getVal('nul'),
                    $uniteElec->getVal('exprime'),
                    $htmlResCandidats
                );
                // Switch entre odd et even à chaque itération
                $oddEven = $oddEven === 'odd' ? 'even' : 'odd';
            }
        }

        
        // Préparation des titres des colonnes au nom des différents candidats et
        // du total de voix obtenus par ces candidats
        // Le total est récupéré en récupérant les résultats enregistrés sur le
        // périmètre de l'élection
        $htmlColonneCandidats = '';
        $htmlTotalCandidat = '';
        foreach ($candidats as $candidatElec) {
            // Entete des colonnes des résultats candidats
            $candidat = $this->f->get_element_by_id('candidat', $candidatElec->getVal('candidat'));
            $htmlColonneCandidats .= sprintf(
                '<th>%s</th>',
                $candidat->getVal('libelle')
            );

            // Total obtenu pour chaque candidat
            $total = $perimetreElec->get_resultat_candidat_unite(
                $candidatElec->getVal('election_candidat'),
                $perimetreElec->getVal('election_unite')
            );
            $htmlTotalCandidat .= sprintf(
                '<td>%d</td>',
                $total
            );
        }

        // Ligne du total du tableau
        $htmlTableau .= sprintf(
            '<tr class="tab-data %s">
                <td> Total </td>
                <td> %d </td>
                <td> %d </td>
                <td> %d </td>
                <td> %d </td>
                <td> %d </td>
                %s
            </tr>',
            $oddEven,
            $perimetreElec->getVal('inscrit'),
            $perimetreElec->getVal('votant'),
            $perimetreElec->getVal('blanc'),
            $perimetreElec->getVal('nul'),
            $perimetreElec->getVal('exprime'),
            $htmlTotalCandidat
        );

        printf(
            '<div id="affichage_article" class="formEntete ui-corner-all">
                <fieldset class="cadre ui-corner-all ui-widget-content">
                    <legend>Résultats - %s</legend>
                    <table class="tab-tab">
                        <tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">
                            <th class="title">Unités</th>
                            <th class="title">Ins</th>
                            <th class="title">Vot</th>
                            <th class="title">Blancs</th>
                            <th class="title">Nuls</th>
                            <th class="title">Exp</th>
                            %s
                        </tr>
                        %s
                    </table>
                </fieldset>
            </div>',
            $this->getVal('libelle'),
            $htmlColonneCandidats,
            $htmlTableau
        );
    }

    /**
     * Indique si l'élection de réference en est à l'étape de paramétrage
     * ou de simulation.
     *
     * @return boolean
     */
    protected function election_etape_parametrage_simulation() {
        $election = $this->f->get_element_by_id('election', $this->getVal('election_reference'));
        return $election->getVal('workflow') === 'Paramétrage' ||
            $election->getVal('workflow') === 'Simulation';
    }
}
