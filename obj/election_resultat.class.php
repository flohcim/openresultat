<?php
//$Id$
//gen openMairie le 31/05/2019 22:16

require_once "../gen/obj/election_resultat.class.php";

class election_resultat extends election_resultat_gen {
    /**
     * Definition des actions disponibles sur la classe
     *
     * @return void
     */
    function init_class_actions() {
        parent::init_class_actions();
        // Aucune action disponible
        $this->class_actions[0] = null;
        $this->class_actions[1] = null;
        $this->class_actions[2] = null;
    }

    /**
     * SETTER FORM - setLib
     * 
     * @param formulaire $form Instance formulaire.
     * @param integer $maj Identifant numérique de l'action.
     * 
     * @return void
     */
    function setLib(&$form, $maj) {
        //libelle des champs pour éviter les termes techniques
        parent::setLib($form, $maj);
        $form->setLib('election_resultat', 'id');
        $form->setLib('election_unite', 'unite de l\'élection');
        $form->setLib('election_candidat', 'candidat à l\'élection');
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_election_unite() {
        $sql = "SELECT election_unite.election_unite, " ;
        $sql .= "concat(election.libelle,' ', unite.code_unite, ' ',  unite.libelle) as lib ";
        $sql .= " FROM ".DB_PREFIXE."election_unite ";
        $sql .= " inner join ".DB_PREFIXE."unite on election_unite.unite = unite.unite ";
        $sql .= " inner join ".DB_PREFIXE."election on election_unite.election = election.election ";
        $sql .= " ORDER BY election_unite.election ASC ";
        return $sql;
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_election_unite_by_id() {
        $sql = "SELECT election_unite.election_unite, " ;
        $sql .= "concat(election.libelle,' ', unite.code_unite, ' ',  unite.libelle) as lib ";
        $sql .= " FROM ".DB_PREFIXE."election_unite ";
        $sql .= " inner join ".DB_PREFIXE."unite on election_unite.unite = unite.unite ";
        $sql .= " inner join ".DB_PREFIXE."election on election_unite.election = election.election ";
        $sql .= " WHERE election_unite = <idx>";
        return $sql;
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_election_candidat() {
        $sql = "SELECT election_candidat.election_candidat, " ;
        $sql .= "concat(election.libelle,' ', election_candidat.ordre, '-',  candidat.libelle) as lib ";
        $sql .= " FROM ".DB_PREFIXE."election_candidat ";
        $sql .= " inner join ".DB_PREFIXE."candidat on election_candidat.candidat = candidat.candidat ";
        $sql .= " inner join ".DB_PREFIXE."election on election_candidat.election = election.election ";
        $sql .= " ORDER BY election_candidat.election, election_candidat.ordre ASC ";
        return $sql;
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_election_candidat_by_id() {
        $sql = "SELECT election_candidat.election_candidat, " ;
        $sql .= "concat(election.libelle,' ', election_candidat.ordre, '-',  candidat.libelle) as lib ";
        $sql .= " FROM ".DB_PREFIXE."election_candidat ";
        $sql .= " inner join ".DB_PREFIXE."candidat on election_candidat.candidat = candidat.candidat ";
        $sql .= " inner join ".DB_PREFIXE."election on election_candidat.election = election.election ";
        $sql .= " WHERE election_candidat = <idx> ";
        return $sql;
    }

    /**
     *
     * @return string
     */
    protected function get_var_sql_forminc__sql_election() {
        return "SELECT election.election, election.libelle FROM ".DB_PREFIXE.
               "election";
    }

    /**
     *
     * @return string
     */
    protected function get_var_sql_forminc__sql_election_by_id() {
        return "SELECT election.election, election.libelle FROM ".DB_PREFIXE."election WHERE election = <idx>";
    }

    /**
     *
     * @return string
     */
    protected function get_var_sql_forminc__sql_candidat() {
        $sql= "SELECT distinct candidat.candidat, candidat.libelle ";
        $sql .= " FROM ".DB_PREFIXE."candidat ";
        $sql .= " inner join ".DB_PREFIXE."election_candidat on election_candidat.candidat = candidat.candidat ";
        $sql .= " inner join ".DB_PREFIXE."election on election_candidat.election = election.election ";
        $sql .= " ORDER BY candidat.libelle ASC";
        return  $sql;
    }

    /**
     *
     * @return string
     */
    protected function get_var_sql_forminc__sql_candidat_by_id() {
        return "SELECT candidat.candidat, candidat.libelle FROM ".DB_PREFIXE."candidat WHERE candidat = <idx>";
    }

    /**
     *
     * @return string
     */
    protected function get_var_sql_forminc__sql_unite() {
        $sql = " SELECT distinct unite.unite, concat (unite.code_unite,' ',unite.libelle) as lib, unite.ordre ";
        $sql .= " FROM ".DB_PREFIXE."unite ";
        $sql .= " inner join ".DB_PREFIXE."election_unite on election_unite.unite = unite.unite ";
        $sql .= " inner join ".DB_PREFIXE."election on election_unite.election = election.election ";
        $sql .= " ORDER BY unite.ordre ASC ";
        return  $sql  ;
    }

    /**
     *
     * @return string
     */
    protected function get_var_sql_forminc__sql_unite_by_id() {
        return "SELECT unite.unite, concat (unite.code_unite,' ',unite.libelle) FROM ".DB_PREFIXE."unite WHERE unite = <idx>";
    }

    /**
     * SETTER FORM - setSelect.
     * 
     * @param formulaire $form Instance formulaire.
     * @param integer $maj Identifant numérique de l'action.
     * @param null &$dnu1 @deprecated  Ne pas utiliser.
     * @param null $dnu2 @deprecated  Ne pas utiliser.
     *
     * @return void
     */   
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj, $dnu1, $dnu2);
        // candidat
        $this->init_select(
            $form,
            $this->f->db,
            $maj,
            null,
            "candidat",
            $this->get_var_sql_forminc__sql("candidat"),
            $this->get_var_sql_forminc__sql("candidat_by_id"),
            false
        );
        // election
        $this->init_select(
            $form,
            $this->f->db,
            $maj,
            null,
            "election",
            $this->get_var_sql_forminc__sql("election"),
            $this->get_var_sql_forminc__sql("election_by_id"),
            false
        );
        // unite
        $this->init_select(
            $form,
            $this->f->db,
            $maj,
            null,
            "unite",
            $this->get_var_sql_forminc__sql("unite"),
            $this->get_var_sql_forminc__sql("unite_by_id"),
            false
        );
    }
}
