-------------------------------------------------------------------------------
-- Script de mise à jour vers la version v2.0.0-rc3 depuis la version v2.0.0-rc2
--
-- @package openresultat
-- @version SVN : $Id$
-------------------------------------------------------------------------------

--
-- Name: resultat_participation_unite; Type: VIEW; Schema: openresultat; Owner: -
-- Modification de la récupération du nombre de votant pour récupérer uniquement le nombre
-- total de votant et pas la somme des votants.
--
-- Le type de la colonne votant change dans la modification ce qui provoque des erreurs. Pour
-- pouvoir modifier la colonne sans problème il faut supprimer la vue et la recréer.

DROP VIEW resultat_participation_unite;

CREATE VIEW resultat_participation_unite AS
 SELECT election_unite.election_unite,
    every(participation_unite.saisie) AS saisie,
    every(participation_unite.envoi_aff) AS envoi_aff,
    every(participation_unite.envoi_web) AS envoi_web,
    max(participation_unite.votant) AS votant
   FROM (election_unite
     JOIN participation_unite ON ((election_unite.election_unite = participation_unite.election_unite)))
  GROUP BY election_unite.election_unite;
