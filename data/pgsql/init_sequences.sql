--------------------------------------------------------------------------------
-- Script d'initialisation
--
-- Initialisation des séquences pour les tables de paramétrage pour lesquelles
-- les enregistrements dont la clé primaire est inférieure à 100000 n'est pas
-- modifiable via l'interface (Voir is_not_from_configuration()).
--
-- @package openresultat
-- @version SVN : $Id$
--------------------------------------------------------------------------------

SELECT pg_catalog.setval('om_etat_seq', (
    SELECT 
        CASE
            WHEN COALESCE(MAX(om_etat), 1) < 100000
                THEN 100000
            ELSE
                max(om_etat)
        END
    FROM
        om_etat
), true);
SELECT pg_catalog.setval('om_lettretype_seq', (
    SELECT 
        CASE
            WHEN COALESCE(MAX(om_lettretype), 1) < 100000
                THEN 100000
            ELSE
                max(om_lettretype)
        END
    FROM
        om_lettretype
), true);
SELECT pg_catalog.setval('om_logo_seq', (
    SELECT 
        CASE
            WHEN COALESCE(MAX(om_logo), 1) < 100000
                THEN 100000
            ELSE
                max(om_logo)
        END
    FROM
        om_logo
), true);
SELECT pg_catalog.setval('om_requete_seq', (
    SELECT 
        CASE
            WHEN COALESCE(MAX(om_requete), 1) < 100000
                THEN 100000
            ELSE
                max(om_requete)
        END
    FROM
        om_requete
), true);
SELECT pg_catalog.setval('om_sousetat_seq', (
    SELECT 
        CASE
            WHEN COALESCE(MAX(om_sousetat), 1) < 100000
                THEN 100000
            ELSE
                max(om_sousetat)
        END
    FROM
        om_sousetat
), true);
