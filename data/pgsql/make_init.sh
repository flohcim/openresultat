#! /bin/bash
##
# Ce script permet de générer les fichiers sql d'initialisation de la base de
# données pour permettre de publier une nouvelle version facilement
#
# @package openresultat
# @version SVN : $Id$
##

schema="openresultat"
database="openresultat"

# Génération du fichier init_metier.sql
sudo su postgres -c "pg_dump -s -O -n $schema -T $schema.om_* $database" > init_metier.sql

# Suppression du schéma
sed -i "s/^CREATE SCHEMA $schema;/-- CREATE SCHEMA $schema;/g" init*.sql
sed -i "s/^SET/-- SET/g" init*.sql
sed -i "s/^SELECT pg_catalog.set_config/-- SELECT pg_catalog.set_config/g" init*.sql
sed -i "s/$schema\.//g" init*.sql
