*** Settings ***
Documentation     Test de l'affichage web
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Jeu de données
    [Documentation]  L'objet de ce test case est de creer le jeu de données
    ...  qui servira ensuite à tester le fonctionnement du portail web

    Depuis la page d'accueil    admin  admin

    # Ajout d'un logo
    Ajouter le logo  logo  openresultat  openResultat.png  logo openresultat

    # Création d'un modéle pour le portail web
    Set Suite Variable  ${titre}  Test personnalisation portail web
    &{web} =  BuiltIn.Create Dictionary
    ...  libelle=test
    ...  logo=openresultat
    ...  entete=${titre}
    ...  url_collectivite=url_test
    ...  libelle_url=test
    ...  feuille_style=* {color: #ff0000;}
    ...  jscript_stats=document.getElementById("footer").innerHTML = "Hello JavaScript!";
    ...  display_simulation=false
    ${id_modele_web} =  Ajouter web  ${web}
    Set Suite Variable  ${id_modele_web}

    #Création de deux unités, une mairie et deux bureaux à sa charge
    Set Suite Variable  ${ordre_ba}  1
    &{bureau_a} =  BuiltIn.Create Dictionary
    ...  libelle=Bureau d'A
    ...  type_unite=Bureau de vote
    ...  ordre=${ordre_ba}
    ...  code_unite=41
    ${id_bureau_a} =  Ajouter unite  ${bureau_a}
    Set Suite Variable  ${id_bureau_a}

    Set Suite Variable  ${ordre_bb}  2
    &{bureau_b} =  BuiltIn.Create Dictionary
    ...  libelle=Bureau n°B
    ...  type_unite=Bureau de vote
    ...  ordre=${ordre_bb}
    ...  code_unite=42
    ${id_bureau_b} =  Ajouter unite  ${bureau_b}
    Set Suite Variable  ${id_bureau_b}

    Set Suite Variable  ${ordre_bc}  3
    &{bureau_c} =  BuiltIn.Create Dictionary
    ...  libelle=Bûreau C
    ...  type_unite=Bureau de vote
    ...  ordre=${ordre_bc}
    ...  code_unite=43
    ${id_bureau_c} =  Ajouter unite  ${bureau_c}
    Set Suite Variable  ${id_bureau_c}

    Set Suite Variable  ${ordre_mabc}  4
    &{mairie} =  BuiltIn.Create Dictionary
    ...  libelle=Mairie ABC
    ...  type_unite=Mairie
    ...  ordre=${ordre_mabc}
    ...  perimetre=true
    ...  type_unite_contenu=Bureau de vote
    ${id_mairie} =  Ajouter unite  ${mairie}
    Set Suite Variable  ${id_mairie} 

    #lien entre ces unites pour obtenir le perimetre de l'election
    Ajouter lien_unite  Mairie ABC  42 Bureau n°B
    Ajouter lien_unite  Mairie ABC  43 Bûreau C
    Ajouter lien_unite  Mairie ABC  41 Bureau d'A

    # Ajout d'un nouveaux plan et placement des unités sur ce plan
    &{plan} =  BuiltIn.Create Dictionary
    ...  libelle=Mairie ABC
    ...  image_plan=plan_centre.gif
    ...  img_unite_arrivee=img_unite_arrivee.gif
    ...  img_unite_non_arrivee=img_unite_non_arrivee.gif
    ...  par_defaut=true
    ...  largeur_icone=100
    ${id_plan} =  Ajouter plan  ${plan}
    Set Suite Variable  ${id_plan}

    # Coordonnées des points sur le plan conservé pour pouvoir tester
    # le positionnement des unités dans la suite des tests
    Set Suite Variable  ${position_x_A}  430
    Set Suite Variable  ${position_y_A}  290
    Set Suite Variable  ${position_x_B}  100
    Set Suite Variable  ${position_y_B}  50
    Set Suite Variable  ${position_x_C}  200
    Set Suite Variable  ${position_y_C}  50

    &{pos_bureau_a} =  BuiltIn.Create Dictionary
    ...  plan=Mairie ABC
    ...  unite=41 Bureau d'A
    ...  img_unite_arrivee=bureau_a.png
    ...  position_x=${position_x_A}
    ...  position_y=${position_y_A}
    ...  largeur_icone=50
    ${id_plan_unite_a} =  Ajouter plan_unite  ${pos_bureau_a}
    Set Suite Variable  ${id_plan_unite_a}

    &{pos_bureau_b} =  BuiltIn.Create Dictionary
    ...  plan=Mairie ABC
    ...  unite=42 Bureau n°B
    ...  img_unite_non_arrivee=bureau_na.png
    ...  position_x=${position_x_B}
    ...  position_y=${position_y_B}
    ${id_plan_unite_b} =  Ajouter plan_unite  ${pos_bureau_b}
    Set Suite Variable  ${id_plan_unite_b}

    &{pos_bureau_c} =  BuiltIn.Create Dictionary
    ...  plan=Mairie ABC
    ...  unite=43 Bûreau C
    ...  position_x=${position_x_C}
    ...  position_y=${position_y_C}
    ${id_plan_unite_c} =  Ajouter plan_unite  ${pos_bureau_c}
    Set Suite Variable  ${id_plan_unite_c}

    # Creation d'une election avec les options de calcul auto des votes exprimés
    # et d'envoi auto à l'affichage activé
    &{election} =  BuiltIn.Create Dictionary
    ...  libelle=l'election de test de la page web
    ...  code=lyoko
    ...  type_election=Municipales
    ...  date=09/06/1995
    ...  perimetre=Mairie ABC
    ...  heure_ouverture=08:00:00
    ...  heure_fermeture=10:00:00
    ...  calcul_auto_exprime=true
    ${id_election} =  Ajouter election  ${election}
    Set Suite Variable  ${id_election}
    Element Should Contain In Subform  css=div.message  enregistrées

    # Durant l'étae de simulation, la saisie des résultats, l'accés à la page web
    # et le paramétrage de l'élection sont possible. C'est donc plus pratique pour
    # tester l'affichage web
    Passer à l'étape suivante  simulation  Simulation

    # Ajout de candidats
    Set Suite Variable  ${ordre_candidat1}  37
    &{candidat} =  BuiltIn.Create Dictionary
    ...  candidat=David G.
    ...  ordre=${ordre_candidat1}
    Ajouter election_candidat  ${candidat}  ${id_election}

    Set Suite Variable  ${ordre_candidat2}  38
    &{candidat} =  BuiltIn.Create Dictionary
    ...  candidat=Jean Michel B.
    ...  ordre=${ordre_candidat2}
    Ajouter election_candidat  ${candidat}  ${id_election}

    # Création d'une centaine
    &{centaine} =  BuiltIn.Create Dictionary
    ...  libelle=Centaine
    ...  votant_defaut=200
    ${id_centaine} =  Ajouter centaine à l'élection  ${centaine}  ${id_election}
    Set Suite Variable  ${id_centaine}

Envoi à la page web
    [Documentation]  Test de l'envoi des données à la page web pour les résultats,
    ...  la participation et les plans. Pour les résultats, teste l'envoi automatique,
    ...  l'envoi via l'action d'envoi et l'envoi en cochant la case d'envoi.
    ...  Test l'envoi automatique de la participation et l'envoi via l'action d'envoi

    # Vérification de la présence des fichiers envoyés lors de la creation de l'élection
    Le fichier doit exister  ../web/res/00${id_election}-0/plans.json
    # Présence des icones par défauts et des icones par unité
    Le fichier doit exister  ../web/res/00${id_election}-0/plan_${id_plan}/img_unite_arrivee.gif
    Le fichier doit exister  ../web/res/00${id_election}-0/plan_${id_plan}/img_unite_non_arrivee.gif
    Le fichier doit exister  ../web/res/00${id_election}-0/plan_${id_plan}/${id_plan_unite_a}/img_unite_arrivee.png
    Le fichier doit exister  ../web/res/00${id_election}-0/plan_${id_plan}/${id_plan_unite_b}/img_unite_non_arrivee.png
    
    # Vérification de la présence des fichiers envoyés lors de l'activation du modèle web
    Activer modèle web  ${id_modele_web}
    Le fichier doit exister  ../web/dyn/param.ini
    Le fichier doit exister  ../web/img/logo.png

    # Envoi en utilisant la case d'envoi
    Depuis le contexte election  ${id_election}
    Click On Tab  election_unite  unité(s)
    Click On Link   41 Bureau d'A
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  100
    Input Text  votant  97
    Input Text  emargement  96
    Input Text  procuration  7
    Input Text  blanc  3
    Input Text  nul  4
    Input Text  candidat${ordre_candidat1}  90
    Input Text  candidat${ordre_candidat2}  0
    Set Checkbox  envoi_web  true
    Click On Submit Button In Subform
    La page ne doit pas contenir d'erreur

    # Présence des fichiers dans le répertoire web
    Le fichier doit exister  ../web/res/00${id_election}-0/b${ordre_ba}.html
    Le fichier doit exister  ../web/res/00${id_election}-0/unite.inc
    Le fichier doit exister  ../web/res/00${id_election}-0/collectivite.inc
    Le fichier doit exister  ../web/res/00${id_election}-0/election.inc

    # Envoi en utilisant l'action d'envoi
    Click On Back Button In Subform
    Click On Link   42 Bureau n°B
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  150
    Input Text  votant  150
    Input Text  emargement  144
    Input Text  procuration  7
    Input Text  blanc  15
    Input Text  nul  35
    Input Text  candidat${ordre_candidat1}  0
    Input Text  candidat${ordre_candidat2}  100
    Click On Submit Button In Subform
    La page ne doit pas contenir d'erreur
    Click On SubForm Portlet Action  election_unite  web  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer

    # Présence des fichiers dans le répertoire web
    Le fichier doit exister  ../web/res/00${id_election}-0/b${ordre_bb}.html
    
    # envoi automatique
    &{election} =  BuiltIn.Create Dictionary
    ...  publication_auto=true
    Modifier election  ${id_election}  ${election}

    Click On Tab  election_unite  unité(s)
    Click On Link   43 Bûreau C
    Click On SubForm Portlet Action  election_unite  modifier
    # Résultat en erreur : les attribut de publication ne doivent pas être à vrai
    # Rien ne doit être transmis au portail web
    Input Text  inscrit  50
    Input Text  blanc  50
    Click On Submit Button In Subform
    La page ne doit pas contenir d'erreur
    Element Should Contain  css=span#envoi_web  Non
    File Should Not Exist  ../web/res/00${id_election}-0/b${ordre_bc}.html
    File Should Not Exist  ../web/res/00${id_election}-0/b${ordre_mabc}.html

    # Permet de laisser suffisamment de temps pour éviter qu'il y ait un conflit de saisie
    # entre la saisie précédente et celle-ci
    Depuis le contexte election_unite  ${id_election}  43 Bûreau C
    Click On SubForm Portlet Action  election_unite  modifier
    # Résultat non saisis mais envoyés
    Input Text  inscrit  50
    Input Text  blanc  0
    Click On Submit Button In Subform
    La page ne doit pas contenir d'erreur
    # Présence des fichiers dans le répertoire web -> bureau + son perimetre car la publication
    # auto est activée
    Le fichier doit exister  ../web/res/00${id_election}-0/b${ordre_bc}.html
    Le fichier doit exister  ../web/res/00${id_election}-0/b${ordre_mabc}.html

    # Envoi des resultats de la participation de manière automatique et en utilisant
    # l'action d'envoi
    Depuis le contexte election  ${id_election}
    Click On Tab  participation_election  participation(s)
    Click On Link  09:00:00
    Click On SubForm Portlet Action  participation_election  modifier
    # Remplissage du formulaire
    Input Text  unite${id_bureau_a}  97
    Input Text  unite${id_bureau_b}  150
    Input Text  unite${id_bureau_c}  0
    # Permet de laisser le javascript de la participation des périmètre se charger
    Click Element   css=input#unite${id_bureau_a}
    Wait Until Element Contains  css=div.field-type-hiddenstaticnum div.form-content  247
    Click On Submit Button In Subform
    La page ne doit pas contenir d'erreur
    Message Should Contain  La participation a été mise à jour sur la page web

    Depuis le contexte election  ${id_election}
    Click On Tab  participation_election  participation(s)
    Click On Link  10:00:00
    Click On SubForm Portlet Action  participation_election  modifier
    # Remplissage du formulaire
    Input Text  unite${id_bureau_a}  98
    Input Text  unite${id_bureau_b}  160
    Click On Submit Button In Subform
    La page ne doit pas contenir d'erreur

    Click On SubForm Portlet Action  participation_election  web  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    Message Should Contain  La participation a été mise à jour sur la page web
    La page ne doit pas contenir d'erreur

    Le fichier doit exister  ../web/res/00${id_election}-0/participation.inc

    # Le plan ainsi que le fichier election.inc doivent exister pour la centaine
    Le fichier doit exister  ../web/res/00${id_centaine}-0C/plan_${id_plan}/plan.gif
    Le fichier doit exister  ../web/res/00${id_centaine}-0C/plans.json
    Le fichier doit exister  ../web/res/00${id_centaine}-0C/election.inc
    # Saisi et envoi des résultats de la centaine
    Depuis le contexte election_unite  ${id_election}  41 Bureau d'A
    Click On SubForm Portlet Action  election_unite  centaine_${id_centaine}
    La page ne doit pas contenir d'erreur
    Input Text  blanc  3
    Input Text  nul  4
    Input Text  candidat${ordre_candidat1}  90
    Input Text  candidat${ordre_candidat1}  4
    Click On Submit Button In Subform

    Envoyer les résultats de la centaine au portail web  ${id_centaine}  ${id_election}  41 Bureau d'A
    Le fichier doit exister  ../web/res/00${id_centaine}-0C/b${ordre_ba}.html

Affichage web
    [Documentation]  Test servant à vérifier que les résultats envoyés
    ...  et les plans sont bien visible sur la page web. Vérifie aussi
    ...  que les unités sont correctement placées sur le plan

    Depuis le contexte election  ${id_election}
    Go To  http://localhost/openresultat/web/index.php?election=00${id_election}-0
    # Verification des erreurs
    La page ne doit pas contenir d'erreur

    Title Should Be  ${titre}

    # Accés à l'onglet de la participation
    Click Element  css=a[href="#participation"]
    # Colonne : Heure
    Element Should Contain  css=div#participation tbody tr td:nth-child(1)  09:00:00
    # Colonne : Votant
    Element Should Contain  css=div#participation tbody tr td:nth-child(2)  247
    # Colonne : Inscrit
    Element Should Contain  css=div#participation tbody tr td:nth-child(3)  82,33

    # Accés à l'onglet des résultats
    Click Element  css=a[href="#resultats"]
    # Ligne : Inscrit Colonne : nombre
    Table Cell Should Contain  css=div#resultats-collectivite > table  2  2  300
    # Ligne : Abstention  Colonne : Nombre et %inscrits
    Table Cell Should Contain  css=div#resultats-collectivite > table  3  2  53
    Table Cell Should Contain  css=div#resultats-collectivite > table  3  3  17,67
    # Ligne : Votants  Colonne : Nombre et %inscrits
    Table Cell Should Contain  css=div#resultats-collectivite > table  4  2  247
    Table Cell Should Contain  css=div#resultats-collectivite > table  4  3  82,33
    # Ligne : Blancs  Colonne : Nombre, %inscrits, %votants
    Table Cell Should Contain  css=div#resultats-collectivite > table  5  2  18
    Table Cell Should Contain  css=div#resultats-collectivite > table  5  3  6,00
    Table Cell Should Contain  css=div#resultats-collectivite > table  5  4  7,29
    # Ligne : Nuls  Colonne : Nombre, %inscrits, %votants 
    Table Cell Should Contain  css=div#resultats-collectivite > table  6  2  39
    Table Cell Should Contain  css=div#resultats-collectivite > table  6  3  13,00
    Table Cell Should Contain  css=div#resultats-collectivite > table  6  4  15,79
    # Ligne : Exprimés  Colonne : Nombre, %inscrits, %votants 
    Table Cell Should Contain  css=div#resultats-collectivite > table  7  2  190
    Table Cell Should Contain  css=div#resultats-collectivite > table  7  3  63,33
    Table Cell Should Contain  css=div#resultats-collectivite > table  7  4  76,92

    # Pourcentage de vote obtenu par le candidat
    Table Cell Should Contain  css=table.table-resultats  2  3  47,37
    Table Cell Should Contain  css=table.table-resultats  3  3  52,63
    # Accés aux résultats par unités
    Click Element  css=a[href="#resultats-par-unite"]
    # La liste s'affiche dans l'ordre
    Element Should Contain  css=div#resultats-par-unite div.list-group > :nth-child(1)  41 Bureau d'A
    Element Should Contain  css=div#resultats-par-unite div.list-group > :nth-child(2)  42 Bureau n°B
    Element Should Contain  css=div#resultats-par-unite div.list-group > :nth-child(3)  43 Bûreau C
    # La liste des unités s'affiche correctement : les bureaux a et b dont les
    # résultats ont été remplis sont arrivés mais pas le c car les "résultats"
    # n'ont pas été saisie pour ce bureaux
    Element Should Contain  css=a[href="#myModal${ordre_ba}"]   41 Bureau d'A
    Element Should Contain  css=a[href="#myModal${ordre_bb}"]   42 Bureau n°B
    Element Should Contain  css=p.elem-${ordre_bc}   43 Bûreau C
    # Détail des résultats du bureau
    Click Element  css=div#resultats-par-unite a[href="#myModal${ordre_ba}"]
    # Le candidat est présent
    Element Should Contain  css=div#myModal${ordre_ba} div.modal-content td.candidat-liste  DAVID G.
    # Le nombre de voix est correct
    Element Should Contain  css=div#myModal${ordre_ba} div.modal-content td.candidat-liste-voix  90
    # Le nombre de vote exprimé est correct
    Element Should Contain  css=div#myModal${ordre_ba} div.modal-content td.candidat-liste-taux  100
    # Fermeture de la fenêtre des résultats
    Click Element  css=div#myModal${ordre_ba} button.close

    # On clique sur l'onglet du plan et on vérifie que le plan et les bureaux
    # sont bien affiché et que cliquer sur l'icone des bureaux affiche leur résultat
    Accéder à l'onglet de la page web  plan-${id_plan}  plan-${id_plan}
    Element Should Be Visible  css=div#plan-${id_plan}

    # Vérification de la présence et de la position des unités
    Element Should Be Visible  css=img[alt="Résultats de l'unité 41 Bureau d'A"]
    Element Should Be Visible  css=img[alt="Résultats de l'unité 42 Bureau n°B"]
    Element Should Be Visible  css=img[alt="Unité 43 Bûreau C non arrivé"]
    Verifier la position de l'icone  ${position_x_A}  ${position_y_A}  css=div#plan-${id_plan}  css=img[alt="Résultats de l'unité 41 Bureau d'A"]
    Verifier la position de l'icone  ${position_x_B}  ${position_y_B}  css=div#plan-${id_plan}  css=img[alt="Résultats de l'unité 42 Bureau n°B"]
    Verifier la position de l'icone  ${position_x_C}  ${position_y_C}  css=div#plan-${id_plan}  css=img[alt="Unité 43 Bûreau C non arrivé"]

    # Test de l'affichage des résultats qaund on clique sur l'icone
    Click Element  css=img[alt="Résultats de l'unité 41 Bureau d'A"]
    Element Should Be Visible  css=div#myModal${ordre_ba}
    Click Element  css=div#myModal${ordre_ba} button.close

Personnalisation de la page web
    [Documentation]  Test servant à vérifier que la personnalisation de la page web
    ...  est correctement réalisé. Vérifie également que les paramétres sont pris en
    ...  compte, c'est à dire que l'entete, l'url et son libelle ainsi que le logo
    ...  s'affiche correctement.
    ...  Valide aussi l'affichage de la liste des élections et le fonctionnement des
    ...  script saisis.

    Depuis le contexte election  ${id_election}
    Go To  http://localhost/openresultat/web/index.php

    # Présence du logo, de l'url avec le bon intitulé, de l'entete et test du script
    Page Should Contain Element  css=img#logo
    Element Should Contain  css=a[href="url_test"]   test
    Element Should Contain  css=a.navbar-brand   Test personnalisation portail web
    Element Should Contain  css=a.navbar-brand   Test personnalisation portail web
    Element Should Contain  css=div#footer  Hello JavaScript!

    # Affichage de la liste sans les centaines
    Page Should Contain Link  css=a.election-type-00${id_election}
    Page Should Not Contain Link  css=a.election-type-00${id_centaine}

    # Affichage des centaines
    &{web} =  BuiltIn.Create Dictionary
    ...  display_simulation=true
    Modifier web  ${id_modele_web}  ${web}

    # Affichage de la liste des élections avec les centaines
    Go To  http://localhost/openresultat/web/index.php
    Page Should Contain Link  css=a.election-type-00${id_election}
    Page Should Contain Link  css=a.election-type-00${id_centaine}

Annulation des résultats
    [Documentation]  Test de la dépublication des résultats de la page web.
    ...  Teste la dépublication à l'aide de l'action de dépublication et en
    ...  décochant la case d'envoi

    # Utilisation de dépublication
    Depuis le contexte election  ${id_election}
    Click On Tab  election_unite  unité(s)
    Click Element  css=div#sousform-election_unite tbody tr.tab-data a
    Click On SubForm Portlet Action  election_unite  depublier_web  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    # Message de confirmation
    Message Should Contain  ../web/res/00${id_election}-0/b${ordre_ba}.html supprimé
    Message Should Contain  ../web/res/00${id_election}-0/unite.inc envoyé
    Message Should Contain  ../web/res/00${id_election}-0/collectivite.inc envoyé
    Message Should Contain  ../web/res/00${id_election}-0/election.inc envoyé
    Message Should Contain  Les fichiers du répertoire web ont été mis à jour
    File Should Not Exist  ../web/res/00${id_election}-0/b${ordre_ba}.html

    # La publication ne doit plus être automatique pour accéder à la case à cocher
    &{election} =  BuiltIn.Create Dictionary
    ...  publication_auto=false
    Modifier election  ${id_election}  ${election}
    # Décochage de la case d'envoi
    Depuis le contexte election_unite  ${id_election}  42 Bureau n°B
    Click On SubForm Portlet Action  election_unite  modifier
    Set Checkbox  envoi_web  false
    Click On Submit Button In Subform
    La page ne doit pas contenir d'erreur
    Message Should Contain  ../web/res/00${id_election}-0/b${ordre_bb}.html supprimé
    Message Should Contain  ../web/res/00${id_election}-0/unite.inc envoyé
    Message Should Contain  ../web/res/00${id_election}-0/collectivite.inc envoyé
    Message Should Contain  ../web/res/00${id_election}-0/election.inc envoyé
    Message Should Contain  Les fichiers du répertoire web ont été mis à jour
    File Should Not Exist  ../web/res/00${id_election}-0/b${ordre_bb}.html

    # Vérification des résultats affichés
    Go To  http://localhost/openresultat/web/index.php?election=00${id_election}-0
    # Raffraichit la page pour s'assurer que les modifications sont prises en compte
    Reload Page
    # Accés aux résultats par unités de l'onglet des résultats
    Click Element  css=a[href="#resultats"]
    Click Element  css=a[href="#resultats-par-unite"]
    # Le Bureau d'A et le n°B ne sont plus cliquable sur la page des unités
    Element Should Contain  css=p.elem-${ordre_ba}    41 Bureau d'A
    Element Should Contain  css=p.elem-${ordre_bb}    42 Bureau n°B
    # Vérification qu'il est marqué comme non arrivé sur le plan
    Accéder à l'onglet de la page web  plan-${id_plan}  plan-${id_plan}
    Element Should Be Visible  css=img[alt="Unité 41 Bureau d'A non arrivé"]
    Element Should Be Visible  css=img[alt="Unité 42 Bureau n°B non arrivé"]
    Element Should Be Visible  css=img[alt="Unité 43 Bûreau C non arrivé"]

Annulation de la participation
    [Documentation]  Test servant à vérifier que l'annulation de la participation
    ...  supprime bien la participation de la page web

    Depuis le contexte election  ${id_election}
    # Acces au listing de la participation
    Click On Tab  participation_election  participation(s)
    # Acces a la tranche horaire de 8 à 9h
    Click On Link  09:00:00
    # Annulation des résultats
    Click On SubForm Portlet Action  participation_election  depublier_web  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer

    # Message de confirmation
    Message Should Contain  La participation a été mise à jour sur la page web

    # Accés à la pge web
    Go To  http://localhost/openresultat/web/index.php?election=00${id_election}-0
    # Raffraichit la page pour s'assurer que les modifications sont prises en compte
    Reload Page
    # Accés à l'onglet de la participation
    Click Element  css=a[href="#participation"]
    # Pas de participation visible sur la page
    Page Should Not Contain    09:00:00

Mise à jour des plans de l'élection
    [Documentation]  Test servant à vérifier que l'action de ise à jour
    ...  du paramétrage des plans, met bien à joour le paramétrage sur la
    ...  page web, ainsi que les images envoyées

    # Modification du paramétrage du plan et du plan_unite crée dans le jeu
    # de données
    &{plan} =  BuiltIn.Create Dictionary
    ...  libelle=Mairie ABC
    ...  image_plan=candidat1.png
    ...  img_unite_arrivee=bureau_a.png
    ...  img_unite_non_arrivee=bureau_na.png
    Modifier plan  ${id_plan}  ${plan}

    &{plan_unite} =  BuiltIn.Create Dictionary
    ...  plan=Mairie ABC
    ...  unite=41 Bureau d'A
    ...  img_unite_arrivee=
    ...  position_x=0
    ...  position_y=0
    Modifier plan_unite  ${id_plan_unite_a}  ${plan_unite}

    # Utilisation de l'action de mise à jour des plans et vérification
    # que les modifications ont bien été effectuée
    Depuis le contexte plan_election  Mairie ABC  ${id_election}
    Click On SubForm Portlet Action  plan_election  parametrage_election_plan  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    La page ne doit pas contenir d'erreur

    # Pour vérifier que les images ont bien été mis à jour, leur extensions sont utilisées
    # Au départ, c'étaient des .gif et maintenant ce sont des .png
    # Si le fichier .png existe c'est que l'image a été mise à jour dans le repertoire web
    # Si le fichier .gif n'existe pas c'est que les anciennes images ont bien été supprimées
    Le fichier doit exister  ../web/res/00${id_election}-0/plan_${id_plan}/img_unite_arrivee.png
    File Should Not Exist    ../web/res/00${id_election}-0/plan_${id_plan}/img_unite_non_arrivee.gif
    # Pour le plan_unite l'image des unités arrivées a été supprimé, elle ne doit donc plus
    # exister dans le repertoire web
    File Should Not Exist    ../web/res/00${id_election}-0/plan_${id_plan}/${id_plan_unite_a}/img_unite_non_arrivee.png

    Go To  http://localhost/openresultat/web/index.php?election=00${id_election}-0#resultats
    # Accés à l'onglet des plans
    Reload Page
    Accéder à l'onglet de la page web  plan-${id_plan}  plan-${id_plan}
    Verifier la position de l'icone  0  0  css=div#plan-${id_plan}  css=img[alt="Unité 41 Bureau d'A non arrivé"]

    # Le paramétrage des plans des centaines doit également avoir été mis à jour
    Go To  http://localhost/openresultat/web/index.php?election=00${id_centaine}-0C#resultats
    # Accés à l'onglet des plans
    Reload Page
    Accéder à l'onglet de la page web  plan-${id_plan}  plan-${id_plan}
    Verifier la position de l'icone  0  0  css=div#plan-${id_plan}  css=img[alt="Résultats de l'unité 41 Bureau d'A"]

Suppression du plan
    [Documentation]  Test servant à vérifier que la suppression d'un plan
    ...  n'entraine pas d'erreur et fait bien disparaitre le plan de la
    ...  page web

    Supprimer plan_election  Mairie ABC  ${id_election}
    Message Should Contain  Mise à jour du fichier plans.json

    # Le plan doit être supprimer de la page web
    File Should Not Exist  ../web/res/00${id_election}-0/plan_${id_plan}

    # Accés à la pge web
    Go To  http://localhost/openresultat/web/index.php?election=00${id_election}-0
    # Raffraichit la page pour s'assurer que les modifications sont prises en compte
    Reload Page
    Page Should Not Contain  css=a#plan-${id_plan}

Reset des résultats de simulation
    [Documentation]  Test servant à vérifier qu'en sortie de l'étape de simulation
    ...  les résultats du répertoire web sont réinitialisé

    Depuis le contexte election  ${id_election}
    Passer à l'étape suivante  saisie  Saisie
    Go To  http://localhost/openresultat/web/index.php?election=00${id_election}-0
    # Verification des erreurs
    La page ne doit pas contenir d'erreur

    # Accés à l'onglet de la participation
    Click Element  css=a[href="#participation"]
    # Pas de ligne de participation
    Page Should Not Contain  css=div#participation tbody tr td:nth-child(1)

    # Accés à l'onglet des résultats
    Click Element  css=a[href="#resultats"]
    # Ligne : Inscrit Colonne : nombre
    Table Cell Should Contain  css=div#resultats-collectivite > table  2  2  0
    # Ligne : Abstention  Colonne : Nombre et %inscrits
    Table Cell Should Contain  css=div#resultats-collectivite > table  3  2  0
    Table Cell Should Contain  css=div#resultats-collectivite > table  3  3  0,00
    # Ligne : Votants  Colonne : Nombre et %inscrits
    Table Cell Should Contain  css=div#resultats-collectivite > table  4  2  0
    Table Cell Should Contain  css=div#resultats-collectivite > table  4  3  0,00
    # Ligne : Blancs  Colonne : Nombre, %inscrits, %votants
    Table Cell Should Contain  css=div#resultats-collectivite > table  5  2  0
    Table Cell Should Contain  css=div#resultats-collectivite > table  5  3  0,00
    Table Cell Should Contain  css=div#resultats-collectivite > table  5  4  0,00
    # Ligne : Nuls  Colonne : Nombre, %inscrits, %votants 
    Table Cell Should Contain  css=div#resultats-collectivite > table  6  2  0
    Table Cell Should Contain  css=div#resultats-collectivite > table  6  3  0,00
    Table Cell Should Contain  css=div#resultats-collectivite > table  6  4  0,00
    # Ligne : Exprimés  Colonne : Nombre, %inscrits, %votants 
    Table Cell Should Contain  css=div#resultats-collectivite > table  7  2  0
    Table Cell Should Contain  css=div#resultats-collectivite > table  7  3  0,00
    Table Cell Should Contain  css=div#resultats-collectivite > table  7  4  0,00

    # Accés aux résultats par unités
    Click Element  css=a[href="#resultats-par-unite"]
    # La liste s'affiche dans l'ordre
    Page Should Not Contain Element  css=a.list-group-item

Suppression du repertoire web
    [Documentation]  Test servant à vérifier que lors de la suppression de l'élection
    ...  son répertoire web et celui de ses centaines sont supprimés

    Depuis le contexte election  ${id_election}
    Retourner à l'étape précédente  simulation  Simulation
    Depuis le contexte election  ${id_election}
    Retourner à l'étape précédente  parametrage  Paramétrage
    # Les répertoires ne doivent plus exister
    Supprimer election  ${id_election}
    File Should Not Exist  ../web/res/00${id_election}-0
    File Should Not Exist  ../web/res/00${id_centaine}-0C

Compatibilité de l'affichage avec la v1
    [Documentation]  Test servant à vérifier que les résultats web issus de la v1 peuvent
    ...  être affiché sans erreur dans la v2. Vérifie : l'affichage dans le listing des
    ...  élection, l'affichage de la participation, celui des résultats et celui des plans.

    # Récupération d'un répertoire web de la v1
    Copy Directory  ${EXECDIR}${/}binary_files/MUN20-1  ${EXECDIR}${/}..${/}web${/}res
    # Récupération d'un fichier de paramétrage de la v1
    Copy File  ${EXECDIR}${/}binary_files${/}var-v1.inc.php  ${EXECDIR}${/}..${/}web${/}dyn${/}var.inc.php
    # Récupération du plan associé au paramétrage
    Copy File  ${EXECDIR}${/}binary_files${/}plan.gif  ${EXECDIR}${/}..${/}web${/}img${/}plan${/}plan.gif

    # Accès à l'affichage web et vérification que l'élection apparaît dans la liste
    Go To  http://localhost/openresultat/web/index.php
    La page ne doit pas contenir d'erreur
    Element Should Contain  css=a[href="index.php?election=MUN20-1"]  Municipales 2020 1er tour (15/03/2020)

    # Accès à l'élection et vérification des résultats
    Click On Link  css=a[href="index.php?election=MUN20-1"]
    La page ne doit pas contenir d'erreur
    # Inscrits
    Table Cell Should Contain  css=div#resultats-collectivite table.table  2  2  8 588
    # Abstention
    Table Cell Should Contain  css=div#resultats-collectivite table.table  3  2  4 367
    Table Cell Should Contain  css=div#resultats-collectivite table.table  3  3  50,85
    # Votants
    Table Cell Should Contain  css=div#resultats-collectivite table.table  4  2  4 221
    Table Cell Should Contain  css=div#resultats-collectivite table.table  4  3  49,15
    # Blancs
    Table Cell Should Contain  css=div#resultats-collectivite table.table  5  2  45
    Table Cell Should Contain  css=div#resultats-collectivite table.table  5  3  0,52
    Table Cell Should Contain  css=div#resultats-collectivite table.table  5  4  1,07
    # Nuls
    Table Cell Should Contain  css=div#resultats-collectivite table.table  6  2  74
    Table Cell Should Contain  css=div#resultats-collectivite table.table  6  3  0,86
    Table Cell Should Contain  css=div#resultats-collectivite table.table  6  4  1,75
    # Exprimés
    Table Cell Should Contain  css=div#resultats-collectivite table.table  7  2  4 102
    Table Cell Should Contain  css=div#resultats-collectivite table.table  7  3  47,76
    Table Cell Should Contain  css=div#resultats-collectivite table.table  7  4  97,18

    # Vérification des résultats des candidats
    Table Cell Should Contain  css=div#resultats-collectivite table.table-resultats  2  1  CHAUVIN PASCAL
    Table Cell Should Contain  css=div#resultats-collectivite table.table-resultats  2  2  1 660
    Table Cell Should Contain  css=div#resultats-collectivite table.table-resultats  2  3  40,47

    # Résultats par unité
    Click On Link  css=a[href="#resultats-par-unite"]
    Click On Link  css=div#resultats-par-unite a[href="#myModal1"]
    # Inscrits
    Table Cell Should Contain  css=div#myModal1 table.table  2  2  935
    # Abstention
    Table Cell Should Contain  css=div#myModal1 table.table  3  2  498
    Table Cell Should Contain  css=div#myModal1 table.table  3  3  53,26
    # Votants
    Table Cell Should Contain  css=div#myModal1 table.table  4  2  437
    Table Cell Should Contain  css=div#myModal1 table.table  4  3  46,74
    # Blancs
    Table Cell Should Contain  css=div#myModal1 table.table  5  2  5
    Table Cell Should Contain  css=div#myModal1 table.table  5  3  0,53
    Table Cell Should Contain  css=div#myModal1 table.table  5  4  1,14
    # Nuls
    Table Cell Should Contain  css=div#myModal1 table.table  6  2  5
    Table Cell Should Contain  css=div#myModal1 table.table  6  3  0,53
    Table Cell Should Contain  css=div#myModal1 table.table  6  4  1,14
    # Exprimés
    Table Cell Should Contain  css=div#myModal1 table.table  7  2  427
    Table Cell Should Contain  css=div#myModal1 table.table  7  3  45,67
    Table Cell Should Contain  css=div#myModal1 table.table  7  4  97,71

    # Vérification des résultats des candidats
    Table Cell Should Contain  css=div#myModal1 table.table-resultats  3  1  FERAUD JEAN-CLAUDE
    Table Cell Should Contain  css=div#myModal1 table.table-resultats  3  2  107
    Table Cell Should Contain  css=div#myModal1 table.table-resultats  3  3  25,06

    # Vérification que le plan est affiché
    Click Button  css=div.modal-footer button.btn
    Click On Link  css=a#plan-sur-carte
    Page Should Contain Element  css=img[src="img/plan/plan.gif"]
    # Vérifie que le bureau 1 est affiché au bon endroit et cliquable
    Page Should Contain Element  css=span[style="top: 281px; left: 356px; width: 80px;"] a[href="#myModal1"]

    # Vérification de la participation
    Click On Link  css=a[href="#participation"]
    Table Cell Should Contain  css=div#participation table.table  2   1  09:00:00
    Table Cell Should Contain  css=div#participation table.table  2   2  586
    Table Cell Should Contain  css=div#participation table.table  2   3  6,82
    Table Cell Should Contain  css=div#participation table.table  11  1  18:00:00
    Table Cell Should Contain  css=div#participation table.table  11  2  4 240
    Table Cell Should Contain  css=div#participation table.table  11  3  49,37

    # Reinitialisation du fichier de paramétrage de base
    Copy File  ${EXECDIR}${/}binary_files${/}var.inc.php  ${EXECDIR}${/}..${/}web${/}dyn${/}var.inc.php

Passage des unités en fin de validité
    [Documentation]  Pour éviter que les unités créées dans ce test ne provoquent
    ...  des erreurs liées à la vérification que le code unité est unique pour
    ...  les unités valides, on passe toutes les unités en fin de validité

    Passer l'unité en fin de validité  41
    Passer l'unité en fin de validité  42
    Passer l'unité en fin de validité  43