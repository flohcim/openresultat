*** Settings ***
Documentation     Test du parametrage d'une unite
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Constitution du jeu de données
    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.
    Depuis la page d'accueil  admin  admin


Listing des unités
    [Documentation]  L'objet de ce TestCase est de vérifier l'intégration du
    ...  listing des enregistrements de type 'unite'.
    # le lien est présent dans le menu
    Depuis l'écran 'Administration & Paramétrage'
    Element Should Contain  css=#settings a.unite  unité
    # le lien amène sur la page correcte
    Click Element  css=#settings a.unite
    Page Title Should Be  Administration & Paramétrage > Élection > Unité
    La page ne doit pas contenir d'erreur
    # le mot clé raccourci amène sur la page correcte
    Depuis le listing des unités
    Page Title Should Be  Administration & Paramétrage > Élection > Unité


Ajout d'une unité
    [Documentation]  L'objet de ce TestCase est de vérifier l'intégration de
    ...  l'action 'ajouter' depuis le contexte 'unite'.

    Depuis le listing des unités
    Click On Add Button
    Page Title Should Be  Administration & Paramétrage > Élection > Unité
    La page ne doit pas contenir d'erreur

    &{unite} =  Create Dictionary
    ...  libelle=commune
    ...  type_unite=Commune
    ...  perimetre=true
    ...  type_unite_contenu=Bureau de vote
    ...  ordre=99
    ...  code_unite=99
    ${idUnite} =  Ajouter unite  ${unite}
    Set Suite Variable  ${idUnite}
    Element Should Contain In Subform  css=div.message  enregistrées


type_unite_contenu
    [Documentation]  L'objet de ce test case est de verifier que le select,
    ...  du champs type unite contenu, récupère les valeurs désirées, en fonction
    ...  du champ type unite et que lorsqu'un type est sélectionné la case périmètre
    ...  l'est aussi. La vérification et fait pour l'ajout et la modification


    # liste des types pour la vérification
    &{liste_element} =  BuiltIn.Create Dictionary
    ...  Bureau de vote
    ...  Mairie
    ...  Arrondissement
    ...  Circonscription
    ...  Commune
    ...  Canton
    ...  Departement
    ...  Region

    &{liste_vide} =  BuiltIn.Create Dictionary
    ...  Choisir type_unite_contenu=

    Depuis le formulaire d'ajout d'une unité
    # On saisit des valeurs
    Select From List By Label  type_unite  Bureau de vote
    # le bureau de vote ayant la hierarchie la plus faible la liste doit être vide
    Select List Should Be  type_unite_contenu  ${liste_vide}
    La page ne doit pas contenir d'erreur
    # On saisit des valeurs
    Select From List By Label  type_unite  Election
    # la liste doit contenir tous les autres champs
    Select List Should Contain List  type_unite_contenu  ${liste_element}
    La page ne doit pas contenir d'erreur
    # On saisit le type contenu
    Select From List By Label  type_unite_contenu  Bureau de vote
    # On vérifie que la case perimetre est coché
    Checkbox Should Be Selected  perimetre
    # On desaisit le type
    Select From List By Label  type_unite_contenu  Choisir type_unite_contenu
    # On vérifie que la case perimetre est coché
    Checkbox Should Not Be Selected  perimetre

    # Vérification en modification
    Depuis le contexte unite  ${idUnite}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  unite  modifier
    # On saisit des valeurs
    Select From List By Label  type_unite  Bureau de vote
    # le bureau de vote ayant la hierarchie la plus faible la liste doit être vide
    Select List Should Be  type_unite_contenu  ${liste_vide}
    La page ne doit pas contenir d'erreur
    # On saisit des valeurs
    Select From List By Label  type_unite  Election
    # la liste doit contenir tous les autres champs
    Select List Should Contain List  type_unite_contenu  ${liste_element}
    La page ne doit pas contenir d'erreur
    # On saisit le type contenu
    Select From List By Label  type_unite_contenu  Bureau de vote
    # On vérifie que la case perimetre est coché
    Checkbox Should Be Selected  perimetre
    # On desaisit le type
    Select From List By Label  type_unite_contenu  Choisir type_unite_contenu
    # On vérifie que la case perimetre est coché
    Checkbox Should Not Be Selected  perimetre

Avertissement si une unité de même libellé existe déjà
    [Documentation]  L'objet de ce test case est de vérifier qu'un message d'avertissement
    ...  est bien affiché lorsque l'on crée une unité ayant le même nom qu'une unité déjà
    ...  existante

    # creation du nouvelle unité ayant le même nom que l'unité créée dans le premier test
    &{unite} =  BuiltIn.Create Dictionary
    ...  libelle=commune
    ...  type_unite=Commune
    ...  perimetre=true
    ...  type_unite_contenu=Bureau de vote
    ...  ordre=99
    ${idUnite} =  Ajouter unite  ${unite}
    La page ne doit pas contenir d'erreur
    Message Should Contain   ATTENTION : une unité portant ce nom existe déjà !

Vérification en cas de modification des unités
    [Documentation]  L'objet de ce test case est de s'assurer que toutes les vérifications
    ...  s'effectue correctement en cas de modification des unités

    Depuis la page d'accueil  admin  admin
    &{unite} =  BuiltIn.Create Dictionary
    ...  libelle=commune_test_verif
    ...  ordre=1
    ...  type_unite=Commune
    ...  code_unite=60
    ${idUniteEnfant} =  Ajouter unite  ${unite}

    &{unite} =  BuiltIn.Create Dictionary
    ...  libelle=perimetre_test_verif
    ...  ordre=2
    ...  type_unite=Region
    ...  code_unite=61
    ...  type_unite_contenu=Commune
    ${idUniteParent} =  Ajouter unite  ${unite}

    Ajouter lien_unite  perimetre_test_verif  60 commune_test_verif

    # Si l'unité contiens d'autres unité la case périmètre ne doit pas être décochée
    Depuis le contexte unite  ${idUniteParent}
    Click On Form Portlet Action  unite  modifier
    &{unite} =  BuiltIn.Create Dictionary
    ...  perimetre=false
    Saisir unite  ${unite}
    Click On Submit Button
    Message Should Contain  L'unité est un périmètre la case périmètre ne doit pas être décochée

    # Si l'unité contiens d'autres unité le type d'unité contenu ne doit pas être modifiable
    &{unite} =  BuiltIn.Create Dictionary
    ...  perimetre=true
    ...  type_unite_contenu=Bureau de vote
    Saisir unite  ${unite}
    Click On Submit Button
    Message Should Contain  L'unité contiens d'autres unité, son type d'unité contenu ne doit pas être modifié

    # Si l'unité contiens d'autres unité son type d'unité peut être modifié si il respecte la hierarchie
    &{unite} =  BuiltIn.Create Dictionary
    ...  type_unite=Departement
    ...  type_unite_contenu=Commune
    Saisir unite  ${unite}
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.

    # Si l'unité est contenu par d'autres unité son type d'unité ne peut pas être modifié
    Depuis le contexte unite  ${idUniteEnfant}
    Click On Form Portlet Action  unite  modifier
    &{unite} =  BuiltIn.Create Dictionary
    ...  type_unite=Departement
    Saisir unite  ${unite}
    Click On Submit Button
    Message Should Contain  L'unité est contenues par d'autres unité, son type ne doit pas être modifié

    # Si le code de l'unité n'est pas unique
    &{unite} =  BuiltIn.Create Dictionary
    ...  code_unite=61
    Saisir unite  ${unite}
    Click On Submit Button
    Message Should Contain  Le code de l'unité doit être unique

Code unité obligatoire pour les unités de type bureau de vote
    [Documentation]  L'objet de ce test case est de vérifier que si l'unité à un type
    ...  ayant un comportement de bureau de vote alors la saisie d'un code unité est
    ...  obligatoire

    Depuis le formulaire d'ajout d'une unité
    &{unite} =  BuiltIn.Create Dictionary
    ...  libelle=bureau
    ...  ordre=1
    ...  type_unite=Bureau de vote
    Saisir unite  ${unite}
    # Vérifie que le champ code unité est marqué comme obligatoire
    Wait Until Element Contains  lib-code_unite  code unite *
    
    # Modification du type le champ ne doit plus être affiché commme obligatoire
    &{unite} =  BuiltIn.Create Dictionary
    ...  type_unite=Commune
    Saisir unite  ${unite}
    Wait Until Element Contains  lib-code_unite  code unite

    # Si l'unité est de type bureau et que le champ code unité n'est pas remplis
    # un message d'erreur doit être affiché
    &{unite} =  BuiltIn.Create Dictionary
    ...  type_unite=Bureau de vote
    Saisir unite  ${unite}
    Click On Submit Button

    La page ne doit pas contenir d'erreur
    Message Should Contain   Le code unité est obligatoire pour les bureaux de vote !


Import d'unités
    [Documentation]  L'objet de ce TestCase est de vérifier l'intégration de
    ...  l'action 'import-unites' depuis le contexte 'unite'.
    # l'action est présente dans le coin haut gauch du tableau
    Depuis le listing des unités
    Element Should Contain  css=a#action-tab-unite-corner-import-unites  Importer
    # le lien amène sur la page correcte
    Click Element  css=a#action-tab-unite-corner-import-unites
    Page Title Should Be   Administration & Paramétrage > Élection > Unité > Import
    La page ne doit pas contenir d'erreur
    # le mot clé raccourci amène sur la page correcte
    Depuis le formulaire d'import des unités
    Page Title Should Be   Administration & Paramétrage > Élection > Unité > Import


Import d'unités depuis un fichier csv d'ELIRE
    [Documentation]  L'objet de ce test case est de tester que l'import
    ...  des unités de saisie à l'aide d'un fichier csv, issus d'ELIRE,
    ...  récupère et enregistre correctement les données dans la base

    &{commune} =  BuiltIn.Create Dictionary
    ...  libelle=commune 42
    ...  code=13042
    ...  prefecture=42
    ${id_commune} =  Ajouter commune  ${commune}

    &{departement} =  BuiltIn.Create Dictionary
    ...  libelle=departement 14
    ...  code=14
    ...  prefecture=14
    ${id_departement} =  Ajouter departement  ${departement}

    Depuis le formulaire d'import des unités
    Add File    fic1    ListeBureauVoteElire.csv
    Select From List By Label   separateur   , (virgule)
    Select From List By Label   departement   departement 14
    Select From List By Label   commune   commune 42
    Click On Submit Button In Import CSV

    Valid Message Should Contain  5 ligne(s) dans le fichier dont :
    Valid Message Should Contain  - 1 ligne(s) d'entête
    Valid Message Should Contain  - 4 ligne(s) importée(s)

    # Vérification que le canton et la circonscription ont été créés
    Depuis le listing  canton
    Use Simple Search  code  95-17
    Click On Link  95-17
    Element Should Contain  css=span#libelle  Saint-Ouen-l'Aumône

    Depuis le listing  circonscription
    Use Simple Search  code  09502
    Click On Link  09502
    Element Should Contain  css=span#libelle  Circonscription import

    # Vérification que le code préfecture n'est pas rempli
    Depuis le listing  canton
    Use Simple Search  prefecture  0
    Element Should Contain  css=div#tab-canton  Saint-Ouen-l'Aumône

    Depuis le listing  circonscription
    Use Simple Search  prefecture  0
    Element Should Contain  css=div#tab-circonscription  Circonscription import

    # Vérification pour chaque unité qu'elles existent et que les valeur
    # enregistrées sont bien celle attendues
    &{unite} =  BuiltIn.Create Dictionary
    ...  libelle=Mairie Import
    ...  type_unite=Bureau de vote
    ...  ordre=008
    ...  adresse1=42 Avenue du test marseille
    ...  adresse2=
    ...  cp=13014
    ...  perimetre=Non
    ...  code_unite=008
    ...  departement=departement 14
    ...  commune=commune 42
    L'unite a été correctement importée  ${unite}  1507

    &{unite} =  BuiltIn.Create Dictionary
    ...  libelle=Ecole Import
    ...  type_unite=Bureau de vote
    ...  ordre=009
    ...  adresse1=42 Rue du test des imports, batiment a Annecy
    ...  adresse2=
    ...  cp=74000
    ...  perimetre=Non
    ...  code_unite=009
    ...  departement=departement 14
    ...  commune=commune 42
    L'unite a été correctement importée  ${unite}  1508

    &{unite} =  BuiltIn.Create Dictionary
    ...  libelle=Gymnase Import
    ...  type_unite=Bureau de vote
    ...  ordre=010
    ...  adresse1=
    ...  adresse2=
    ...  cp=
    ...  ville=
    ...  perimetre=Non
    ...  code_unite=010
    ...  departement=departement 14
    ...  commune=commune 42
    L'unite a été correctement importée  ${unite}  1509

    &{unite} =  BuiltIn.Create Dictionary
    ...  libelle=Salle des Fêtes Import
    ...  type_unite=Bureau de vote
    ...  ordre=011
    ...  adresse1=
    ...  adresse2=
    ...  cp=
    ...  ville=
    ...  perimetre=Non
    ...  code_unite=011
    ...  departement=departement 14
    ...  commune=commune 42
    L'unite a été correctement importée  ${unite}  1510

    # Passe toutes les unités en fin de validité pour éviter de géner les autres test
    Passer l'unité en fin de validité  008
    Passer l'unité en fin de validité  009
    Passer l'unité en fin de validité  010
    Passer l'unité en fin de validité  011

    # Import d'unité autre que des bureaux de vote
    Depuis le formulaire d'import des unités
    Add File    fic1    ListeUniteREU.csv
    Select From List By Label   separateur   , (virgule)
    Select From List By Label   departement   departement 14
    Select From List By Label   commune   commune 42
    Click On Submit Button In Import CSV

    Valid Message Should Contain  3 ligne(s) dans le fichier dont :
    Valid Message Should Contain  - 1 ligne(s) d'entête
    Valid Message Should Contain  - 2 ligne(s) importée(s)

    &{unite} =  BuiltIn.Create Dictionary
    ...  libelle=Circonscription import
    ...  type_unite=Circonscription
    ...  ordre=008
    ...  adresse1=Circonscription de test des imports
    ...  adresse2=
    ...  cp=
    ...  ville=
    ...  perimetre=Non
    ...  code_unite=008
    ...  departement=departement 14
    ...  commune=commune 42
    L'unite a été correctement importée  ${unite}  ${EMPTY}

    &{unite} =  BuiltIn.Create Dictionary
    ...  libelle=Commune import
    ...  type_unite=Commune
    ...  ordre=009
    ...  adresse1=
    ...  adresse2=
    ...  cp=
    ...  ville=
    ...  perimetre=Non
    ...  code_unite=009
    ...  departement=departement 14
    ...  commune=commune 42
    L'unite a été correctement importée  ${unite}  ${EMPTY}
    # Passe toutes les unités en fin de validité pour éviter de géner les autres test
    Passer l'unité en fin de validité  009
    Passer l'unité en fin de validité  008


Import d'unités depuis un fichier csv d'openElec (via l'api REU)
    [Documentation]  L'objet de ce test case est de tester que l'import
    ...  des unités de saisie à l'aide d'un fichier csv, issus d'openElec,
    ...  récupère et enregistre correctement les données dans la base

    Depuis le formulaire d'import des unités
    Add File    fic1    ListeBureauVoteOpenelec.csv
    Select From List By Label   separateur   ; (point-virgule)
    Click On Submit Button In Import CSV

    Valid Message Should Contain  8 ligne(s) dans le fichier dont :
    Valid Message Should Contain  - 1 ligne(s) d'entête
    Valid Message Should Contain  - 6 ligne(s) importée(s)

    # Vérification que le canton et la circonscription ont été créés
    Depuis le listing  canton
    Use Simple Search  code  13-28
    Click On Link  13-28
    Element Should Contain  css=span#libelle  Trets

    Depuis le listing  circonscription
    Use Simple Search  code  13-10
    Click On Link  13-10
    Element Should Contain  css=span#libelle  13-10

    # Vérification que le code préfecture n'est pas rempli
    Depuis le listing  canton
    Use Simple Search  prefecture  0
    Element Should Contain  css=div#tab-canton  Trets

    Depuis le listing  circonscription
    Use Simple Search  prefecture  0
    Element Should Contain  css=div#tab-circonscription  13-10

    # Vérification pour chaque unité qu'elles existent et que les valeur
    # enregistrées sont bien celle attendues
    &{unite} =  BuiltIn.Create Dictionary
    ...  libelle=SALLE DES MARIAGES
    ...  type_unite=Bureau de vote
    ...  ordre=8
    ...  adresse1=
    ...  adresse2=
    ...  cp=
    ...  perimetre=Non
    ...  code_unite=8
    L'unite a été correctement importée  ${unite}  9959

    &{unite} =  BuiltIn.Create Dictionary
    ...  libelle=SALLE DES FETES JEAN MONNET
    ...  type_unite=Bureau de vote
    ...  ordre=9
    ...  adresse1=
    ...  adresse2=
    ...  cp=
    ...  perimetre=Non
    ...  code_unite=9
    L'unite a été correctement importée  ${unite}  49965

    &{unite} =  BuiltIn.Create Dictionary
    ...  libelle=ECOLE DU CANET
    ...  type_unite=Bureau de vote
    ...  ordre=10
    ...  adresse1=
    ...  adresse2=
    ...  cp=
    ...  ville=
    ...  perimetre=Non
    ...  code_unite=10
    L'unite a été correctement importée  ${unite}  52493

    &{unite} =  BuiltIn.Create Dictionary
    ...  libelle=SALLE DES FETES JEAN MONNET
    ...  type_unite=Bureau de vote
    ...  ordre=11
    ...  adresse1=
    ...  adresse2=
    ...  cp=
    ...  ville=
    ...  perimetre=Non
    ...  code_unite=11
    L'unite a été correctement importée  ${unite}  30945

    &{unite} =  BuiltIn.Create Dictionary
    ...  libelle=LE GYMNASE
    ...  type_unite=Bureau de vote
    ...  ordre=12
    ...  adresse1=
    ...  adresse2=
    ...  cp=
    ...  ville=
    ...  perimetre=Non
    ...  code_unite=12
    L'unite a été correctement importée  ${unite}  35558

    &{unite} =  BuiltIn.Create Dictionary
    ...  libelle=ECOLE DU VILLAGE
    ...  type_unite=Bureau de vote
    ...  ordre=13
    ...  adresse1=
    ...  adresse2=
    ...  cp=
    ...  ville=
    ...  perimetre=Non
    ...  code_unite=13
    L'unite a été correctement importée  ${unite}  24419

    # Passe toutes les unités en fin de validité pour éviter de géner les autres test
    Passer l'unité en fin de validité  8
    Passer l'unité en fin de validité  9
    Passer l'unité en fin de validité  10
    Passer l'unité en fin de validité  11
    Passer l'unité en fin de validité  12
    Passer l'unité en fin de validité  13

    # Import d'unité autre que des bureaux de vote
    Depuis le formulaire d'import des unités
    Add File    fic1    ListeUniteOpenelec.csv
    Select From List By Label   separateur   ; (point-virgule)
    Click On Submit Button In Import CSV

    Valid Message Should Contain  4 ligne(s) dans le fichier dont :
    Valid Message Should Contain  - 1 ligne(s) d'entête
    Valid Message Should Contain  - 2 ligne(s) importée(s)
    Valid Message Should Contain  - 0 ligne(s) rejetée(s)
    Valid Message Should Contain  - 1 ligne(s) vide(s)

    &{unite} =  BuiltIn.Create Dictionary
    ...  libelle=COMMUNE OPENELEC A
    ...  type_unite=Commune
    ...  ordre=8
    ...  adresse1=
    ...  adresse2=
    ...  cp=
    ...  ville=
    ...  perimetre=Non
    ...  code_unite=8
    ...  departement=
    ...  commune=
    L'unite a été correctement importée  ${unite}  ${EMPTY}

    &{unite} =  BuiltIn.Create Dictionary
    ...  libelle=CANTON OPENELEC B
    ...  type_unite=Canton
    ...  ordre=9
    ...  adresse1=
    ...  adresse2=
    ...  cp=
    ...  ville=
    ...  perimetre=Non
    ...  code_unite=9
    ...  departement=
    ...  commune=
    L'unite a été correctement importée  ${unite}  ${EMPTY}

    Passer l'unité en fin de validité  8
    Passer l'unité en fin de validité  9