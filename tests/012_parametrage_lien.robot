*** Settings ***
Documentation     Test du parametrage des liens entre unites
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Constitution du jeu de données
    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.
    Depuis la page d'accueil  admin  admin


Listing des liens entre unités
    [Documentation]  Ce 'Test Case' vérifie l'intégration du listing des
    ...  enregistrements de type 'lien_unite'.
    # le lien n'est pas présent dans le menu
    Depuis l'écran 'Administration & Paramétrage'
    Element Should Not Be Visible  css=#settings a.lien_unite
    # le mot clé raccourci amène sur la page correcte
    Depuis le listing des liens entre unités
    Page Title Should Be  Administration & Paramétrage > Élection > Lien Entre Unités


Ajout d'un lien entre unités
    [Documentation]  Ce 'Test Case' vérifie l'intégration de l'action 'ajouter'
    ...  depuis le contexte 'lien_unite'.

    Depuis le listing des liens entre unités
    Click On Add Button
    Page Title Should Be  Administration & Paramétrage > Élection > Lien Entre Unités
    La page ne doit pas contenir d'erreur

    #creation des unites à lier
    &{unite_parent} =  BuiltIn.Create Dictionary
    ...  libelle=commune_test
    ...  type_unite=Commune
    ...  perimetre=true
    ...  type_unite_contenu=Bureau de vote
    ...  ordre=666
    ${idUnite} =  Ajouter unite  ${unite_parent}
    Set Suite Variable  ${idUnite}

    &{unite_enfant} =  BuiltIn.Create Dictionary
    ...  libelle=bureau
    ...  type_unite=Bureau de vote
    ...  ordre=42
    ...  code_unite=42
    Ajouter unite  ${unite_enfant}

    #ajout d'un lien entre les deux unites
    Ajouter lien_unite  commune_test  42 bureau
    Element Should Contain In Subform  css=div.message  enregistrées


ajout de lien_unite multiple
    [Documentation]  L'objet de ce test case est de verifier que l'ajout
    ...  de lien multiple fonctionne

    #creation des unites à lier
    &{unite_parent} =  BuiltIn.Create Dictionary
    ...  libelle=commune_abc
    ...  type_unite=Commune
    ...  perimetre=true
    ...  type_unite_contenu=Bureau de vote
    ...  ordre=666
    ...  code_unite=666
    ${idUniteParent} =  Ajouter unite  ${unite_parent}
    Set Suite Variable  ${idUnite}

    &{unite_enfant} =  BuiltIn.Create Dictionary
    ...  libelle=bureau_a
    ...  type_unite=Bureau de vote
    ...  ordre=7
    ...  code_unite=7
    Ajouter unite  ${unite_enfant}

    &{unite_enfant} =  BuiltIn.Create Dictionary
    ...  libelle=bureau_b
    ...  type_unite=Bureau de vote
    ...  ordre=8
    ...  code_unite=8
    Ajouter unite  ${unite_enfant}

    &{unite_enfant} =  BuiltIn.Create Dictionary
    ...  libelle=bureau_c
    ...  type_unite=Bureau de vote
    ...  ordre=9
    ...  code_unite=9
    Ajouter unite  ${unite_enfant}

    # ajout d'un lien unité pour tester la non création de doublon
    Ajouter lien_unite  commune_abc  8 bureau_b

    # Création des liens multiples
    @{unites} =   Create List  7 bureau_a  8 bureau_b  9 bureau_c
    Ajouter lien_unite multiple  commune_abc  ${unites}

    # Vérifie que les tous les liens ont été créés sauf celui déjà existant
    Valid Message Should Contain  lien : commune_abc -> bureau_a
    Valid Message Should Contain  lien : commune_abc -> bureau_c
    Element Should Not Contain  css=div.message span.text  lien : commune_abc -> bureau_b


filtrage dans les select
    [Documentation]  L'objet de ce test case est de verifier que
    ...  les unités du select unite_enfant sont bien celles recherchées
    ...  par l'unité_parent et que les unités_parent proposé sont toutes
    ...  des périmètres

    # élements nécessaires pour les tests
    &{non_contenu} =  BuiltIn.Create Dictionary
    ...  libelle=mairieTest
    ...  type_unite=Mairie
    ...  ordre=64
    ...  perimetre=true
    ...  type_unite_contenu=Bureau de vote
    Ajouter unite  ${non_contenu}

    &{liste_perimetre} =  BuiltIn.Create Dictionary
    ...  mairieTest=
    ...  commune_test=

    &{liste_unite_type_voulu} =  BuiltIn.Create Dictionary
    ...  bureau=

    # On accède au formulaire d'ajout des liens et on vérifie que la
    # liste du select des unités parents contiens uniquement les perimetres
    Depuis le formulaire d'ajout d'un lien entre unités
    Select List Should Contain List  unite_parent  ${liste_perimetre}
    # bureau n'est pas un périmètre elle ne doit donc pas être dans les options du select
    Select List Should Not Contain List  unite_parent  ${liste_unite_type_voulu}

    # Les options du select unité enfant dépendent de l'unité parente
    # En choisissant la mairie comme parent, seules les unités de type
    # bureau de vote doivent être présente dans le select.
    # On doit donc avoir bureau et pas mairie ni commune
    &{liste_unite_type_voulu} =  BuiltIn.Create Dictionary
    ...  42 bureau=
    &{liste_perimetre} =  BuiltIn.Create Dictionary
    ...  64 mairieTest=
    ...  666 commune=
    Select From List By Label  unite_parent  mairieTest
    Select List Should Contain List  unite_enfant  ${liste_unite_type_voulu}
    Select List Should Not Contain List  unite_enfant  ${liste_perimetre}

    # Test du select si l'unité n'a pas de type d'unité contenu.
    # Le select doit être vide et ne retourner aucune erreur
    &{non_contenu} =  BuiltIn.Create Dictionary
    ...  libelle=test perimetre sans type voulu
    ...  type_unite=Mairie
    ...  ordre=65
    ...  perimetre=true
    Ajouter unite  ${non_contenu}
    Depuis le formulaire d'ajout d'un lien entre unités
    Select From List By Label  unite_parent  test perimetre sans type voulu
    List Selection Should Be  unite_enfant  Choisir unite_enfant
    La page ne doit pas contenir d'erreur


Lien unique
    [Documentation]  L'objet de ce test case est de vérifier qu'il n'est pas
    ...  possible de lier deux foix une unité parent avec une unité enfant

    # creation du même lien que dans le premier test
    Depuis le formulaire d'ajout d'un lien entre unités
    Saisir lien_unite  commune_test  42 bureau
    Click On Submit Button
    Error Message Should Contain  Les valeurs saisies dans les champs perimetre, unité contenue existent déjà


Avertissement si l'unité enfant à déjà des parents
    [Documentation]  L'objet de ce test case est de vérifier qu'un message d'avertissement
    ...  est bien affiché lorsque l'on crée un lien_unité avec une unité enfant ayant déjà
    ...  des unités parentes

    # creation du nouvelle unité qui va servir de parent à l'unité bureau
    &{unite_parent} =  BuiltIn.Create Dictionary
    ...  libelle=commune2
    ...  type_unite=Commune
    ...  perimetre=true
    ...  type_unite_contenu=Bureau de vote
    ...  ordre=667
    Ajouter unite  ${unite_parent}

    Ajouter lien_unite  commune2  42 bureau
    Message Should Contain   Attention : l'unite bureau a déjà pour parent :\n- commune


Passage des unités en fin de validité
    [Documentation]  Pour éviter que les unités créées dans ce test ne provoquent
    ...  des erreurs liées à la vérification que le code unité est unique pour
    ...  les unités valides, on passe toutes les unités en fin de validité

    # Unités créée dans les tests précédents et qui sers dans ce test donc on le passe
    # en fin de validté dans ce tests
    Passer l'unité en fin de validité  666
    Passer l'unité en fin de validité  42
    Passer l'unité en fin de validité  7
    Passer l'unité en fin de validité  8
    Passer l'unité en fin de validité  9