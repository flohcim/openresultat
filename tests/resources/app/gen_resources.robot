*** Settings ***
Documentation    Ressources de mots-clefs générés
...    @author  generated
...    @package openRésultat
...    @version 10/08/2020 09:08

Resource          animation.robot
Resource          candidat.robot
Resource          canton.robot
Resource          circonscription.robot
Resource          commune.robot
Resource          departement.robot
Resource          election.robot
Resource          election_candidat.robot
Resource          election_resultat.robot
Resource          election_unite.robot
Resource          lien_unite.robot
Resource          participation_election.robot
Resource          participation_unite.robot
Resource          plan.robot
Resource          plan_election.robot
Resource          plan_unite.robot
Resource          tranche.robot
Resource          type_election.robot
Resource          type_unite.robot
Resource          unite.robot
Resource          centaine.robot
Resource          web.robot
Resource          delegation.robot