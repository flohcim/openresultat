*** Settings ***
Documentation    CRUD de la table plan_election
...    @author  generated
...    @package openRésultat
...    @version 17/12/2020 10:12

*** Keywords ***

Depuis le contexte plan_election
    [Documentation]  Accède au formulaire
    [Arguments]  ${plan_election}

    # On accède au tableau
    Go To Tab  plan_election
    # On recherche l'enregistrement
    Use Simple Search  plan_election  ${plan_election}
    # On clique sur le résultat
    Click On Link  ${plan_election}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter plan_election
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  plan_election
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir plan_election  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${plan_election} =  Get Text  css=div.form-content span#plan_election
    # On le retourne
    [Return]  ${plan_election}

Modifier plan_election
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${plan_election}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte plan_election  ${plan_election}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  plan_election  modifier
    # On saisit des valeurs
    Saisir plan_election  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer plan_election
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${plan_election}

    # On accède à l'enregistrement
    Depuis le contexte plan_election  ${plan_election}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  plan_election  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir plan_election
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "plan" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "election" existe dans "${values}" on execute "Select From List By Label" dans le formulaire