*** Settings ***
Documentation    CRUD de la table election_candidat
...    @author  generated
...    @package openRésultat
...    @version 29/01/2021 15:01

*** Keywords ***

Depuis le contexte election_candidat
    [Documentation]  Accède au formulaire
    [Arguments]  ${election_candidat}

    # On accède au tableau
    Go To Tab  election_candidat
    # On recherche l'enregistrement
    Use Simple Search  election_candidat  ${election_candidat}
    # On clique sur le résultat
    Click On Link  ${election_candidat}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter election_candidat
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  election_candidat
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir election_candidat  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${election_candidat} =  Get Text  css=div.form-content span#election_candidat
    # On le retourne
    [Return]  ${election_candidat}

Modifier election_candidat
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${election_candidat}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte election_candidat  ${election_candidat}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  election_candidat  modifier
    # On saisit des valeurs
    Saisir election_candidat  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer election_candidat
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${election_candidat}

    # On accède à l'enregistrement
    Depuis le contexte election_candidat  ${election_candidat}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  election_candidat  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir election_candidat
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "election" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "candidat" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "ordre" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "prefecture" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "age_moyen" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "siege" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "age_moyen_com" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "siege_com" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "couleur" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "photo" existe dans "${values}" on execute "Input Text" dans le formulaire