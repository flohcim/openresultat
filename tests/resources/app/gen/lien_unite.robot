*** Settings ***
Documentation    CRUD de la table lien_unite
...    @author  generated
...    @package openRésultat
...    @version 10/08/2020 09:08

*** Keywords ***

Depuis le contexte lien_unite
    [Documentation]  Accède au formulaire
    [Arguments]  ${lien_unite}

    # On accède au tableau
    Go To Tab  lien_unite
    # On recherche l'enregistrement
    Use Simple Search  lien_unite  ${lien_unite}
    # On clique sur le résultat
    Click On Link  ${lien_unite}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter lien_unite
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  lien_unite
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir lien_unite  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${lien_unite} =  Get Text  css=div.form-content span#lien_unite
    # On le retourne
    [Return]  ${lien_unite}

Modifier lien_unite
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${lien_unite}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte lien_unite  ${lien_unite}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  lien_unite  modifier
    # On saisit des valeurs
    Saisir lien_unite  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer lien_unite
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${lien_unite}

    # On accède à l'enregistrement
    Depuis le contexte lien_unite  ${lien_unite}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  lien_unite  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir lien_unite
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "unite_enfant" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "unite_parent" existe dans "${values}" on execute "Select From List By Label" dans le formulaire