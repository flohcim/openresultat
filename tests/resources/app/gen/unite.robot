*** Settings ***
Documentation    CRUD de la table unite
...    @author  generated
...    @package openRésultat
...    @version 07/05/2021 10:05

*** Keywords ***

Depuis le contexte unite
    [Documentation]  Accède au formulaire
    [Arguments]  ${unite}

    # On accède au tableau
    Go To Tab  unite
    # On recherche l'enregistrement
    Use Simple Search  unite  ${unite}
    # On clique sur le résultat
    Click On Link  ${unite}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter unite
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  unite
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir unite  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${unite} =  Get Text  css=div.form-content span#unite
    # On le retourne
    [Return]  ${unite}

Modifier unite
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${unite}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte unite  ${unite}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  unite  modifier
    # On saisit des valeurs
    Saisir unite  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer unite
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${unite}

    # On accède à l'enregistrement
    Depuis le contexte unite  ${unite}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  unite  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir unite
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "type_unite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "ordre" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "cp" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "ville" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "geom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "coordinates" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "type_unite_contenu" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "perimetre" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "canton" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "circonscription" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "commune" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "departement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "code_unite" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "id_reu" existe dans "${values}" on execute "Input Text" dans le formulaire