*** Settings ***
Documentation    CRUD de la table circonscription
...    @author  generated
...    @package openRésultat
...    @version 25/09/2020 17:09

*** Keywords ***

Depuis le contexte circonscription
    [Documentation]  Accède au formulaire
    [Arguments]  ${circonscription}

    # On accède au tableau
    Go To Tab  circonscription
    # On recherche l'enregistrement
    Use Simple Search  circonscription  ${circonscription}
    # On clique sur le résultat
    Click On Link  ${circonscription}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter circonscription
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  circonscription
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir circonscription  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${circonscription} =  Get Text  css=div.form-content span#circonscription
    # On le retourne
    [Return]  ${circonscription}

Modifier circonscription
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${circonscription}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte circonscription  ${circonscription}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  circonscription  modifier
    # On saisit des valeurs
    Saisir circonscription  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer circonscription
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${circonscription}

    # On accède à l'enregistrement
    Depuis le contexte circonscription  ${circonscription}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  circonscription  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir circonscription
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "prefecture" existe dans "${values}" on execute "Input Text" dans le formulaire