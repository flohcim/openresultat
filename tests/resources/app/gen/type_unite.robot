*** Settings ***
Documentation    CRUD de la table type_unite
...    @author  generated
...    @package openRésultat
...    @version 10/08/2020 09:08

*** Keywords ***

Depuis le contexte type_unite
    [Documentation]  Accède au formulaire
    [Arguments]  ${type_unite}

    # On accède au tableau
    Go To Tab  type_unite
    # On recherche l'enregistrement
    Use Simple Search  type_unite  ${type_unite}
    # On clique sur le résultat
    Click On Link  ${type_unite}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter type_unite
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  type_unite
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir type_unite  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${type_unite} =  Get Text  css=div.form-content span#type_unite
    # On le retourne
    [Return]  ${type_unite}

Modifier type_unite
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${type_unite}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte type_unite  ${type_unite}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  type_unite  modifier
    # On saisit des valeurs
    Saisir type_unite  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer type_unite
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${type_unite}

    # On accède à l'enregistrement
    Depuis le contexte type_unite  ${type_unite}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  type_unite  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir type_unite
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "hierarchie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "bureau_vote" existe dans "${values}" on execute "Set Checkbox" dans le formulaire