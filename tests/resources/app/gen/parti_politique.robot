*** Settings ***
Documentation    CRUD de la table parti_politique
...    @author  generated
...    @package openRésultat
...    @version 23/02/2021 10:02

*** Keywords ***

Depuis le contexte parti_politique
    [Documentation]  Accède au formulaire
    [Arguments]  ${parti_politique}

    # On accède au tableau
    Go To Tab  parti_politique
    # On recherche l'enregistrement
    Use Simple Search  parti_politique  ${parti_politique}
    # On clique sur le résultat
    Click On Link  ${parti_politique}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter parti_politique
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  parti_politique
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir parti_politique  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${parti_politique} =  Get Text  css=div.form-content span#parti_politique
    # On le retourne
    [Return]  ${parti_politique}

Modifier parti_politique
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${parti_politique}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte parti_politique  ${parti_politique}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  parti_politique  modifier
    # On saisit des valeurs
    Saisir parti_politique  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer parti_politique
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${parti_politique}

    # On accède à l'enregistrement
    Depuis le contexte parti_politique  ${parti_politique}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  parti_politique  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir parti_politique
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "couleur" existe dans "${values}" on execute "Input Text" dans le formulaire