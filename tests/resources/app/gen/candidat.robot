*** Settings ***
Documentation    CRUD de la table candidat
...    @author  generated
...    @package openRésultat
...    @version 13/04/2021 16:04

*** Keywords ***

Depuis le contexte candidat
    [Documentation]  Accède au formulaire
    [Arguments]  ${candidat}

    # On accède au tableau
    Go To Tab  candidat
    # On recherche l'enregistrement
    Use Simple Search  candidat  ${candidat}
    # On clique sur le résultat
    Click On Link  ${candidat}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter candidat
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  candidat
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir candidat  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${candidat} =  Get Text  css=div.form-content span#candidat
    # On le retourne
    [Return]  ${candidat}

Modifier candidat
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${candidat}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte candidat  ${candidat}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  candidat  modifier
    # On saisit des valeurs
    Saisir candidat  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer candidat
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${candidat}

    # On accède à l'enregistrement
    Depuis le contexte candidat  ${candidat}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  candidat  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir candidat
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle_liste" existe dans "${values}" on execute "Input Text" dans le formulaire