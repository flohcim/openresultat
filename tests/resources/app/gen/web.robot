*** Settings ***
Documentation    CRUD de la table web
...    @author  generated
...    @package openRésultat
...    @version 19/01/2021 14:01

*** Keywords ***

Depuis le contexte web
    [Documentation]  Accède au formulaire
    [Arguments]  ${web}

    # On accède au tableau
    Go To Tab  web
    # On recherche l'enregistrement
    Use Simple Search  web  ${web}
    # On clique sur le résultat
    Click On Link  ${web}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter web
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  web
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir web  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${web} =  Get Text  css=div.form-content span#web
    # On le retourne
    [Return]  ${web}

Modifier web
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${web}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte web  ${web}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  web  modifier
    # On saisit des valeurs
    Saisir web  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer web
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${web}

    # On accède à l'enregistrement
    Depuis le contexte web  ${web}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  web  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir web
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "logo" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "entete" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "url_collectivite" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle_url" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "feuille_style" existe dans "${values}" on execute "Input Text" dans le formulaire