*** Settings ***
Documentation    CRUD de la table election
...    @author  generated
...    @package openRésultat
...    @version 10/08/2020 16:08

*** Keywords ***

Depuis le contexte election
    [Documentation]  Accède au formulaire
    [Arguments]  ${election}

    Depuis le listing  election
    Input Text  css=div#adv-search-adv-fields input#id    ${election}
    Click On Search Button
    # On clique sur le résultat
    Click On Link  ${election}
    # On vérifie qu'il n'y a pas d'erreur
    La page ne doit pas contenir d'erreur

Ajouter election
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Depuis le listing  election
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir election  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${election} =  Get Text  css=div.form-content span#election
    # On le retourne
    [Return]  ${election}

Modifier election
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${election}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte election  ${election}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  election  modifier
    # On saisit des valeurs
    Saisir election  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer election
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${election}

    # On accède à l'enregistrement
    Depuis le contexte election  ${election}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  election  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir election
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}

    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "tour" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "type_election" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "date" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "perimetre" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "votant_defaut" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "heure_ouverture" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "heure_fermeture" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "envoi_initial" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "publication_auto" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "publication_erreur" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "calcul_auto_exprime" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "garder_resultat_simulation" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "delegation_saisie" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "delegation_participation" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "validation_avant_publication" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "sieges" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "sieges_com" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "sieges_mep" existe dans "${values}" on execute "Input Text" dans le formulaire

Passer à l'étape suivante
    [Documentation]  Utilise l'action de passage à l'étape suivante du workflow
    [Arguments]  ${nom_etape}  ${lib_etape}

    Click On Form Portlet Action  election  ${nom_etape}  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    Wait Until Element Contains  css=div.workflow-display-status span.workflow-selected  ${lib_etape}
    La page ne doit pas contenir d'erreur

Retourner à l'étape précédente
    [Documentation]  Utilise l'action de retour à l'étape précédente du workflow
    [Arguments]  ${nom_etape}  ${lib_etape}

    Click On Form Portlet Action  election  retour_${nom_etape}  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    Wait Until Element Contains  css=div.workflow-display-status span.workflow-selected  ${lib_etape}
    La page ne doit pas contenir d'erreur
