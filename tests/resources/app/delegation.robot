*** Settings ***
Documentation    CRUD de la table delegation
...    @author  generated
...    @package openRésultat
...    @version 05/03/2021 17:03

*** Keywords ***

Depuis le contexte delegation
    [Documentation]  Accède au formulaire
    [Arguments]  ${delegation}  ${election}

    # On accède à l'élection
    Depuis le contexte election  ${election}
    # On accède au tableau
    Click On Tab  delegation  délégation
    # On clique sur le résultat
    Click On Link  ${delegation}
    # On vérifie qu'il n'y a pas d'erreur
    La page ne doit pas contenir d'erreur

Ajouter delegation
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}  ${election}

    # On accède au tableau
    Depuis le contexte election  ${election}
    Click On Tab  delegation  délégation
    La page ne doit pas contenir d'erreur
    # On clique sur le bouton ajouter
    Click On Add Button In SubForm
    # On saisit des valeurs
    Saisir delegation  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${delegation} =  Get Text  css=div.form-content span#delegation
    # On le retourne
    [Return]  ${delegation}

Ajouter delegation multiple
    [Documentation]  Crée plusieurs enregistrement de délégation
    [Arguments]  ${liste_acteur}  ${liste_unite}  ${election}

    # On accède au tableau
    Depuis le contexte election  ${election}
    Click On Tab  delegation  délégation
    La page ne doit pas contenir d'erreur
    # On clique sur le bouton ajouter
    Click Element  css=a#action-soustab-delegation-corner-ajouter_multiple
    # On saisit des valeurs
    Saisir delegation multiple  ${liste_acteur}  ${liste_unite}
    # On valide le formulaire
    Click On Submit Button

Modifier delegation
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${delegation}  ${election}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte delegation  ${delegation}  ${election}
    # On clique sur le bouton modifier
    Click On SubForm Portlet Action  delegation  modifier
    # On saisit des valeurs
    Saisir delegation  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer delegation
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${delegation}  ${election}

    # On accède à l'enregistrement
    Depuis le contexte delegation  ${delegation}  ${election}
    # On clique sur le bouton supprimer
    Click On SubForm Portlet Action  delegation  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir delegation
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "election" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "unite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "acteur" existe dans "${values}" on execute "Select From List By Label" dans le formulaire

Saisir delegation multiple
    [Documentation]  Remplit le formulaire
    [Arguments]  ${liste_acteur}  ${liste_unite}

    Select Multiple By Label  css=div#sousform-container select#acteur  ${liste_acteur}
    Select Multiple By Label  css=div#sousform-container select#unite  ${liste_unite}