*** Settings ***
Documentation    CRUD de la table participation_election
...    @author  generated
...    @package openRésultat
...    @version 25/06/2020 10:06

*** Keywords ***

Depuis le contexte participation_election
    [Documentation]  Accède au formulaire
    [Arguments]  ${election}  ${participation_election}

    # On accède à l'élection
    Depuis le contexte election  ${election}
    # On accède au tableau
    Click On Tab  participation_election  participation(s)
    # On clique sur la tranche horaire ayant le nom recherchée
    Click On Link  ${participation_election}
    # On vérifie qu'il n'y a pas d'erreur
    La page ne doit pas contenir d'erreur

Ajouter participation_election
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Depuis le listing  participation_election
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir participation_election  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${participation_election} =  Get Text  css=div.form-content span#participation_election
    # On le retourne
    [Return]  ${participation_election}

Modifier participation_election
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${participation_election}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte participation_election  ${participation_election}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  participation_election  modifier
    # On saisit des valeurs
    Saisir participation_election  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer participation_election
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${participation_election}

    # On accède à l'enregistrement
    Depuis le contexte participation_election  ${participation_election}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  participation_election  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir participation_election
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}

    ${keys} =  Get Dictionary Keys  ${values}
    :FOR    ${key}    IN    @{keys}
    \    Si "${key}" existe dans "${values}" on execute "Input Text" dans le formulaire

Envoyer la participation à l'affichage
    [Documentation]  Utilise l'action d'envoi de la participation à la page web
    [Arguments]  ${election}  ${tranche_horaire}

    Depuis le contexte participation_election  ${election}  ${tranche_horaire}
    Click On SubForm Portlet Action  participation_election  affichage  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    Message Should Contain    La participation a été mise à jour sur les animations
    La page ne doit pas contenir d'erreur

Envoyer la participation à la page web
    [Documentation]  Utilise l'action d'envoi de la participation à la page web
    [Arguments]  ${election}  ${tranche_horaire}

    Depuis le contexte participation_election  ${election}  ${tranche_horaire}
    Click On SubForm Portlet Action  participation_election  web  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    Message Should Contain  La participation a été mise à jour sur la page web
    La page ne doit pas contenir d'erreur
