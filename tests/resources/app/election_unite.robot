*** Settings ***
Documentation    CRUD de la table election_unite
...    @author  generated
...    @package openRésultat
...    @version 13/08/2020 14:08

*** Keywords ***

Depuis le contexte election_unite
    [Documentation]  Accède au formulaire
    [Arguments]  ${election}  ${election_unite_lib}

    # On accède à l'élection
    Depuis le contexte election  ${election}
    # On accède au tableau
    Click On Tab  election_unite  unité(s)
    # On clique sur l'unité ayant le nom recherchée
    Click On Link  ${election_unite_lib}
    # On vérifie qu'il n'y a pas d'erreur
    La page ne doit pas contenir d'erreur

Ajouter election_unite
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Depuis le listing  election_unite
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir election_unite  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${election_unite} =  Get Text  css=div.form-content span#election_unite
    # On le retourne
    [Return]  ${election_unite}

Modifier election_unite
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${election_unite}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte election_unite  ${election_unite}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  election_unite  modifier
    # On saisit des valeurs
    Saisir election_unite  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer election_unite
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${election_unite}

    # On accède à l'enregistrement
    Depuis le contexte election_unite  ${election_unite}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  election_unite  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir election_unite
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}

    Si "election" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "unite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "inscrit" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "votant" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "blanc" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "nul" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "exprime" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "envoi_aff" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "envoi_web" existe dans "${values}" on execute "Set Checkbox" dans le formulaire

Envoyer les résultats à l'affichage
    [Documentation]  Utilise l'action d'envoi des résultats à l'affichage
    [Arguments]  ${election}  ${election_unite_lib}

    Depuis le contexte election_unite  ${election}  ${election_unite_lib}
    Click On SubForm Portlet Action  election_unite  affichage  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    Message Should Contain  envoi affichage effectué
    La page ne doit pas contenir d'erreur

Envoyer les résultats à la page web
    [Documentation]  Utilise l'action d'envoi des résultats à la page web
    [Arguments]  ${election}  ${election_unite_lib}

    Depuis le contexte election_unite  ${election}  ${election_unite_lib}
    Click On SubForm Portlet Action  election_unite  web  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    Message Should Contain  envoi web effectué
    La page ne doit pas contenir d'erreur

Demander la validation des résultats
    [Documentation]  Utilise l'action de demande de validation

    Click On SubForm Portlet Action  election_unite  demander_validation  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    Valid Message Should Contain  En attente de validation de la saisie
    La page ne doit pas contenir d'erreur

Reprendre la saisie des résultats
    [Documentation]  Utilise l'action de reprise de la saisie

    Click On SubForm Portlet Action  election_unite  reprendre_saisie  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    Valid Message Should Contain  Reprise de la saisie
    La page ne doit pas contenir d'erreur

Valider la saisie des résultats
    [Documentation]  Utilise l'action de validation des résultats

    Click On SubForm Portlet Action  election_unite  valider  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    Valid Message Should Contain  La saisie est validée
    La page ne doit pas contenir d'erreur