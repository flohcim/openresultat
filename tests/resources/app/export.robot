*** Settings ***
Documentation  Keywords des exports de l'application.
Resource  gen_resources.robot

*** Keywords ***
Contenu CSV
    [Arguments]  ${link}
    ${output_dir}  ${output_name} =  Télécharger un fichier  ${SESSION_COOKIE}  ${link}  ${PATH_BIN_FILES}
    ${full_path_to_file} =  Catenate  SEPARATOR=  ${output_dir}  ${output_name}
    ${content_file} =  Get File  ${full_path_to_file}
    [Return]  ${content_file}