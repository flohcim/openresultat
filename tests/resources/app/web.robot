*** Settings ***
Documentation    CRUD de la table web
...    @author  generated
...    @package openRésultat
...    @version 13/01/2021 18:01

*** Keywords ***

Depuis le contexte web
    [Documentation]  Accède au formulaire
    [Arguments]  ${web}

    # On accède au tableau
    Depuis le listing  web
    # On recherche l'enregistrement
    Use Simple Search  web  ${web}
    # On clique sur le résultat
    Click On Link  ${web}
    # On vérifie qu'il n'y a pas d'erreur
    La page ne doit pas contenir d'erreur

Ajouter web
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Depuis le listing  web
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir web  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${web} =  Get Text  css=div.form-content span#web
    # On le retourne
    [Return]  ${web}

Modifier web
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${web}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte web  ${web}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  web  modifier
    # On saisit des valeurs
    Saisir web  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer web
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${web}

    # On accède à l'enregistrement
    Depuis le contexte web  ${web}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  web  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir web
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "logo" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "entete" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "url_collectivite" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle_url" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "feuille_style" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "jscript_stats" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "display_simulation" existe dans "${values}" on execute "Set Checkbox" dans le formulaire

Activer modèle web
    [Documentation]  Rend actif le modèle web
    [Arguments]  ${web}
    
    Depuis le contexte web  ${web}
    Click On Form Portlet Action  web  activer  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    Valid Message Should Contain  Le modèle a été correctement activé.