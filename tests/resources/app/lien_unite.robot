*** Settings ***
Documentation    CRUD de la table lien_unite
...    @author  generated
...    @package openRésultat
...    @version 10/08/2020 09:08

*** Keywords ***
Depuis le listing des liens entre unités
    [Tags]  lien_unite
    [Documentation]  Accède au listing des enregistrements de type 'lien_unite'.
    Depuis le listing  lien_unite


Depuis le formulaire d'ajout d'un lien entre unités
    [Tags]  lien_unite
    [Documentation]  Accède directement via URL au formulaire d'ajout d'un
    ...  enregistrement de type 'lien_unite'.
    Go To  ${PROJECT_URL}${OM_ROUTE_FORM}&obj=lien_unite&action=0&retour=form
    La page ne doit pas contenir d'erreur


Depuis le contexte lien_unite
    [Documentation]  Accède au formulaire
    [Arguments]  ${lien_unite}

    Depuis le listing des liens entre unités
    # On recherche l'enregistrement
    Use Simple Search  lien_unite  ${lien_unite}
    # On clique sur le résultat
    Click On Link  ${lien_unite}
    # On vérifie qu'il n'y a pas d'erreur
    La page ne doit pas contenir d'erreur

Ajouter lien_unite
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${lib_unite_parent}  ${lib_unite_enfant}

    Depuis le formulaire d'ajout d'un lien entre unités
    # On saisit des valeurs
    Saisir lien_unite  ${lib_unite_parent}  ${lib_unite_enfant}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${lien_unite} =  Get Text  css=div.form-content span#lien_unite
    # On le retourne
    [Return]  ${lien_unite}

Ajouter lien_unite multiple
    [Documentation]  Crée plusieurs enregistrement de lien_unite
    [Arguments]  ${lib_unite_parent}  ${liste_lib_unite_enfant}

    Depuis le listing des liens entre unités
    # On clique sur le bouton ajouter
    Click Element  css=a#action-tab-lien_unite-corner-ajouter_multiple
    # On saisit des valeurs
    Saisir lien_unite multiple  ${lib_unite_parent}  ${liste_lib_unite_enfant}
    # On valide le formulaire
    Click On Submit Button

Modifier lien_unite
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${lien_unite}  ${lib_unite_parent}  ${lib_unite_enfant}

    # On accède à l'enregistrement
    Depuis le contexte lien_unite  ${lien_unite}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  lien_unite  modifier
    # On saisit des valeurs
    Saisir lien_unite  ${lib_unite_parent}  ${lib_unite_enfant}
    # On valide le formulaire
    Click On Submit Button

Supprimer lien_unite
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${lien_unite}

    # On accède à l'enregistrement
    Depuis le contexte lien_unite  ${lien_unite}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  lien_unite  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir lien_unite
    [Documentation]  Remplit le formulaire
    [Arguments]  ${lib_unite_parent}  ${lib_unite_enfant}

    Select From List By Label  unite_parent  ${lib_unite_parent}
    Select From List By Label  unite_enfant  ${lib_unite_enfant}

Saisir lien_unite multiple
    [Documentation]  Remplit le formulaire
    [Arguments]  ${lib_unite_parent}  ${liste_lib_unite_enfant}

    Select From List By Label  unite_parent  ${lib_unite_parent}
    Select Multiple By Label   unite_enfant  ${liste_lib_unite_enfant}