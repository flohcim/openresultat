*** Settings ***
Documentation    CRUD de la table election_resultat
...    @author  generated
...    @package openRésultat
...    @version 10/08/2020 09:08

*** Keywords ***

Depuis le contexte election_resultat
    [Documentation]  Accède au formulaire
    [Arguments]  ${election_resultat}

    # On accède au tableau
    Depuis le listing  election_resultat
    # On recherche l'enregistrement
    Use Simple Search  election_resultat  ${election_resultat}
    # On clique sur le résultat
    Click On Link  ${election_resultat}
    # On vérifie qu'il n'y a pas d'erreur
    La page ne doit pas contenir d'erreur

Ajouter election_resultat
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Depuis le listing election_resultat
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir election_resultat  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${election_resultat} =  Get Text  css=div.form-content span#election_resultat
    # On le retourne
    [Return]  ${election_resultat}

Modifier election_resultat
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${election_resultat}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte election_resultat  ${election_resultat}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  election_resultat  modifier
    # On saisit des valeurs
    Saisir election_resultat  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer election_resultat
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${election_resultat}

    # On accède à l'enregistrement
    Depuis le contexte election_resultat  ${election_resultat}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  election_resultat  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir election_resultat
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "election_unite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "election_candidat" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "resultat" existe dans "${values}" on execute "Input Text" dans le formulaire