*** Settings ***
Documentation    CRUD de la table centaine
...    @author  -
...    @package openRésultat
...    @version 10/08/2020 16:08

*** Keywords ***

Depuis le listing centaine
    [Documentation]  Accède au formulaire
    [Arguments]  ${election}

    # On accède à l'élection
    Depuis le contexte election  ${election}
    # On accéde à l'onglet des centaines
    Click On Tab  centaine  centaine(s)

Depuis le contexte centaine
    [Documentation]  Accède au formulaire
    [Arguments]   ${election}  ${centaine}

    # On accède au listing des centaines
    Depuis le listing centaine  ${election}
    # On clique sur la centaine voulu
    Click On Link  ${centaine}
    # On vérifie qu'il n'y a pas d'erreur
    La page ne doit pas contenir d'erreur

Ajouter centaine à l'élection
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}  ${election}

    # On accède au listing des candidats de l'élection
    Depuis le listing centaine  ${election}
    # On clique sur le bouton ajouter
    Click On Add Button In SubForm
    # On saisit des valeurs
    Saisir centaine  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${centaine} =  Get Text  css=div#sousform-container div.form-content span#election
    # On le retourne
    [Return]  ${centaine}

Modifier centaine de l'élection
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${election}  ${centaine}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte centaine  ${election}  ${centaine}
    # On clique sur le bouton modifier
    Click On SubForm Portlet Action  centaine  modifier
    # On saisit des valeurs
    Saisir centaine  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer centaine de l'élection
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${election}  ${centaine}

    # On accède à l'enregistrement
    Depuis le contexte centaine  ${election}  ${centaine}
    # On clique sur le bouton supprimer
    Click On SubForm Portlet Action  centaine  supprimer
    # On valide le formulaire
    Click On Submit Button In SubForm

Saisir centaine
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}

    Si "election_reference" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans "centaine"
    Si "votant_defaut" existe dans "${values}" on execute "Input Text" dans "centaine"
    Si "web" existe dans "${values}" on execute "Select From List By Label" dans "centaine"

Envoyer les résultats de la centaine à l'affichage
    [Documentation]  Utilise l'action d'envoi à l'affichage pour les résultats d'une centaine
    [Arguments]  ${centaine}  ${election}  ${unite}

    # Accéde à l'unité voulu
    Depuis le contexte election_unite  ${election}  ${unite}
    # Accés au formulaire de saisie des centaines
    Click on Subform Portlet Action  election_unite  centaine_${centaine}
    # Utilisation du bouton de retour de la centaine
    Click Element  css=div#sousform-election_unite_centaine div#sousform-container a.retour
    Wait Until Element Is Visible  css=div#sousform-election_unite_centaine div#sousform-container div#portlet-actions
    # Utilisation de l'action d'envoi à l'affichage
    Click on Subform Portlet Action  election_unite_centaine  affichage  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    # Vérification que l'envoi s'est effectué
    Valid Message Should Contain  envoi affichage effectué

Envoyer les résultats de la centaine au portail web
    [Documentation]  Utilise l'action d'envoi au portail web pour les résultats d'une centaine
    [Arguments]  ${centaine}  ${election}  ${unite}

    # Accéde à l'unité voulu
    Depuis le contexte election_unite  ${election}  ${unite}
    # Accés au formulaire de saisie des centaines
    Click on Subform Portlet Action  election_unite  centaine_${centaine}
    # Utilisation du bouton de retour de la centaine
    Click Element  css=div#sousform-election_unite_centaine div#sousform-container a.retour
    Wait Until Element Is Visible  css=div#sousform-election_unite_centaine div#sousform-container div#portlet-actions
    # Utilisation de l'action d'envoi au portail web
    Click on Subform Portlet Action  election_unite_centaine  web  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    # Vérification que l'envoi s'est effectué
    Valid Message Should Contain  envoi web effectué