*** Settings ***
Documentation    CRUD de la table participation_unite
...    @author  generated
...    @package openRésultat
...    @version 10/08/2020 09:08

*** Keywords ***

Depuis le contexte de la participation par unite
    [Documentation]  Accède au formulaire
    [Arguments]  ${election}  ${participation_unite}

    # On accède à l'élection
    Depuis le contexte election  ${election}
    # On accède au tableau
    Click On Tab  delegation_participation  participation(s)
    # On clique sur la tranche horaire ayant le nom recherchée
    Click On Link  ${participation_unite}
    # On vérifie qu'il n'y a pas d'erreur
    La page ne doit pas contenir d'erreur

Modifier la participation par unite
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${participation_unite}  ${values}  ${election}

    # On accède à l'enregistrement
    Depuis le contexte de la participation par unite  ${participation_unite}  ${election}
    # On clique sur le bouton modifier
    Click On SubForm Portlet Action  delegation_participation  modifier
    # On saisit des valeurs
    Saisir la participation par unite  ${values}
    # On valide le formulaire
    Click On Submit Button

Saisir la participation par unite
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    :FOR ${champ}  IN   @{values.keys()}
    \   Input Text  ${champ}  ${values["${champ}"]}

Envoyer la participation de l'unité à l'affichage
    [Documentation]  Utilise l'action d'envoi de la participation à la page web
    [Arguments]  ${election}  ${participation_unite}

    Depuis le contexte de la participation par unite  ${election}  ${participation_unite}
    Click On SubForm Portlet Action  delegation_participation  affichage  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    Message Should Contain    La participation de l'unité a été envoyé à l'affichage
    La page ne doit pas contenir d'erreur

Envoyer la participation de l'unité à la page web
    [Documentation]  Utilise l'action d'envoi de la participation à la page web
    [Arguments]  ${election}  ${participation_unite}

    Depuis le contexte de la participation par unite  ${election}  ${participation_unite}
    Click On SubForm Portlet Action  delegation_participation  web  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    Message Should Contain  La participation a été envoyé à la page web
    La page ne doit pas contenir d'erreur