*** Settings ***
Documentation  Keywords de l'application.
Resource  gen_resources.robot

*** Keywords ***
La page du fichier PDF doit contenir les chaînes de caractères
    [Tags]
    [Documentation]  Change de fenêtre, fait les verifs, et ferme la fenêtre.
    ...  Il faut donc avoir déclenché l'ouverture de la fenêtre avant.
    ...  L'argument window est {name, title, url} de la fenêtre sans le suffixe '.php'
    [Arguments]  ${window}  ${strings_to_find}  ${page_number}=1  ${from_system}=False

    Run Keyword If  ${from_system}==False  Open PDF  ${window}
    PDF Move Page To  ${page_number}
    La page ne doit pas contenir d'erreur
    ${result} =  Get Text  css=#pageContainer${page_number}
    ${result} =  Set Variable  ${result.replace('\n', ' ')}
    :FOR  ${string}  IN  @{strings_to_find}
    \    Should Contain  ${result}  ${string}
    Run Keyword If  ${from_system}==False  Close PDF

Le tableau doit contenir les éléments
    [Tags]
    [Documentation]  Vérifie que le tableau du listing contiens bien tous les
    ...  éléments recherchés
    [Arguments]  ${elements_à_trouver}

    :FOR  ${elements}  IN  @{elements_à_trouver}
    \    Table Should Contain  css=table.tab-tab  ${elements}

Le tableau ne doit pas contenir les éléments
    [Tags]
    [Documentation]  Vérifie que le tableau du listing ne contiens aucun des
    ...  éléments recherchés
    [Arguments]  ${elements_à_chercher}

    :FOR  ${elements}  IN  @{elements_à_chercher}
    \    Element Should Not Contain  css=div.soustab-container  ${elements}

Accéder à l'onglet de la page web
    [Documentation]  Clique sur l'onglet ayant l'id voulu et attend que le contenu s'affiche
    [Arguments]  ${onglet}  ${contenu}

    Click Element Until New Element  css=a#${onglet}  css=div#${contenu}

Déselectionner le fichier
    [Documentation]  Supprime un fichier en cliquant sur l'icone de suppression
    [Arguments]  ${field}

    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=#${field}_upload ~ a.upload > span.ui-icon-closethick

Selectionner un fichier si ${key} existe et n'est pas vide dans ${collection} sinon deselectionner
    [Documentation]  Si un fichier est choisi alors ce fichier est télécharger. Sinon
    ...  si le champ est vide clique sur l'icone de suppression pour s'assurer qu'aucun
    ...  fichier n'est sélectionné

    ${exist} =    Run Keyword And Return Status    Dictionary Should Contain Key    ${collection}    ${key}
    ${empty} =    Run Keyword And Return Status    Should Be empty  ${collection.${key}}
    Run Keyword If  ${exist} == True and ${empty} == False
    ...  Add File  ${key}  ${collection.${key}}
    ...  ELSE  Déselectionner le fichier  ${key}

Le portlet du formulaire doit contenir
    [Documentation]  Vérifie que le portlet d'un formulaire contiens la liste des actions
    ...  recherchées
    [Arguments]  ${objet}  ${actions}

    :FOR  ${action}  IN  @{actions}
    \    Portlet Action Should Be In Form  ${objet}  ${action}

Le portlet du sous formulaire doit contenir
    [Documentation]  Vérifie que le portlet d'un sous-formulaire contiens la liste des
    ...  actions recherchées
    [Arguments]  ${objet}  ${actions}

    :FOR  ${action}  IN  @{actions}
    \    Portlet Action Should Be In SubForm  ${objet}  ${action}

Le portlet du formulaire ne doit pas contenir
    [Documentation]  Vérifie que le portlet d'un formulaire ne contiens aucunes des actions
    ...  recherchées
    [Arguments]  ${objet}  ${actions}

    :FOR  ${action}  IN  @{actions}
    \    Portlet Action Should Not Be In Form  ${objet}  ${action}

Le portlet du sous formulaire ne doit pas contenir
    [Documentation]  Vérifie que le portlet d'un sous-formulaire ne contiens aucunes des
    ...  actions recherchées
    [Arguments]  ${objet}  ${actions}

    :FOR  ${action}  IN  @{actions}
    \    Portlet Action Should Not Be In SubForm  ${objet}  ${action}

L'icone d'ajout ne doit pas être présente dans le sous tableau
    [Documentation]  Vérifie que l'icone permettant d'ajouter des elements
    ...  dans un sous tableau n'est pas présente
    [Arguments]  ${objet}

    Page Should Not Contain Element  css=a#action-soustab-${objet}-corner-ajouter

L'icone d'ajout doit être présente dans le sous tableau
    [Documentation]  Vérifie que l'icone permettant d'ajouter des elements
    ...  dans un sous tableau est présente
    [Arguments]  ${objet}

    Page Should Contain Element  css=a#action-soustab-${objet}-corner-ajouter

Le champ ${champ} doit être ${type}
    [Documentation]  Vérifie que les champs de la liste correspondent
    ...  a des champs du type voulu dans le formulaire

    Page Should Contain Element  css=div.field-type-${type} div.form-content > #${champ}

Vérifier les valeurs des champs
    [Documentation]  Vérifie à partir de la fenêtre de consultation si les valeurs
    ...  des champs correspondent aux valeurs attendues
    [Arguments]  ${values}

    # Pour chacune des valeurs enregistrées vérifie si la valeur du champ est correcte
    ${keys} =  Get Dictionary Keys  ${values}
    :FOR    ${key}    IN    @{keys}
    \    Element Should Contain  css=span#${key}  ${values.${key}}

Click On Add Button In SubForm
    [Tags]
    Click Element Until No More Element    css=div.soustab-container span.add-16
    La page ne doit pas contenir d'erreur

Click On Back Button In Import CSV
    [Tags]  module_import

    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=#form-csv-import form div.formControls a.retour
    Sleep  1
    La page ne doit pas contenir d'erreur


Depuis l'écran 'Administration & Paramétrage'
    [Documentation]
    Go To  ${PROJECT_URL}app/index.php?module=settings
    La page ne doit pas contenir d'erreur
