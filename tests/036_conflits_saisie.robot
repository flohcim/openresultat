*** Settings ***
Documentation     Test de la gestion des conflits de saisie
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Conflit de saisie des résultats
    [Documentation]  L'objet de ce test case est de vérifier que les conflits de saisie
    ...  sont correctement gérés lors de la saisie des résultats.

    # Ouverture d'un premier navigateur
    Open Browser  http://localhost/openresultat/app/index.php  firefox  1
    Depuis la page d'accueil    admin  admin

    # preparation d'une election et ouverture du formulaire de saisie des résultats
    &{election} =  BuiltIn.Create Dictionary
    ...  libelle=Municipales 2020 1er tour
    ...  code=MUN20-1
    ...  tour=1
    ...  principal=true
    ...  type_election=Municipales
    ...  date=15/03/2020
    ...  perimetre=COMMUNE
    ...  heure_ouverture=08:00:00
    ...  heure_fermeture=18:00:00
    ...  calcul_auto_exprime=true
    ${id_election} =  Ajouter election  ${election}
    Set Suite Variable  ${id_election}
    Element Should Contain In Subform  css=div.message  enregistrées

    &{candidat} =  BuiltIn.Create Dictionary
    ...  candidat=David G.
    ...  ordre=1
    ...  prefecture=100
    ...  age_moyen=45
    ...  age_moyen_com=46
    ${id_candidat1} =  Ajouter election_candidat  ${candidat}  ${id_election}

    &{candidat} =  BuiltIn.Create Dictionary
    ...  candidat=Jean Michel B.
    ...  ordre=2
    ...  prefecture=101
    ...  age_moyen=38
    ...  age_moyen_com=41
    ${id_candidat2} =  Ajouter election_candidat  ${candidat}  ${id_election}

    # Passage à l'étape de simulation pour pouvoir accéder au formulaire de saisie
    Depuis le contexte election  ${id_election}
    Passer à l'étape suivante  simulation  Simulation

    # Saisie des résultats
    Depuis le contexte election_unite  ${id_election}  1 Salle des Mariages
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  348
    Input Text  votant  238
    Input Text  emargement  238
    Input Text  procuration  57
    Input Text  blanc  8
    Input Text  nul  0
    Input Text  candidat1  200
    Input Text  candidat2  30

    # Ouverture d'un autre navigateur et accés au formulaire de saisie
    Open Browser  http://localhost/openresultat/app/index.php  firefox  2
    Depuis la page d'accueil    admin  admin

    Depuis le contexte election_unite  ${id_election}  1 Salle des Mariages
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  760
    Input Text  votant  438
    Input Text  emargement  438
    Input Text  procuration  0
    Input Text  blanc  8
    Input Text  nul  1
    Input Text  candidat1  251
    Input Text  candidat2  178
    Click On Submit Button In Subform

    # Retour sur la première fenêtre et validation du formulaire
    Switch Browser  1
    Click On Submit Button In Subform
    Element Should Contain  css=div#sousform-container span.text  Attention: votre modification est en conflit avec la modification précédente!

Conflit de saisie de la participation par tranche horaire
    [Documentation]  L'objet de ce test case est de vérifier que les conflits de saisie
    ...  sont correctement gérés lors de la saisie de la participation par tranche horaire.

    # Saisie des résultats
    Depuis le contexte participation_election  ${id_election}  09:00:00
    Click On SubForm Portlet Action  participation_election  modifier
    Input Text  unite1  36
    Input Text  unite2  50
    Input Text  unite3  100

    # Ouverture d'un autre navigateur et accés au formulaire de saisie
    Switch Browser  2
    Depuis la page d'accueil    admin  admin

    Depuis le contexte participation_election  ${id_election}  09:00:00
    Click On SubForm Portlet Action  participation_election  modifier
    Input Text  unite1  6
    Input Text  unite2  12
    Input Text  unite3  18
    Click On Submit Button In Subform

    # Retour sur la première fenêtre et validation du formulaire
    Switch Browser  1
    Click On Submit Button In Subform
    Element Should Contain  css=div#sousform-container span.text  Attention: votre modification est en conflit avec la modification précédente!

Conflit de saisie de la participation par unite
    [Documentation]  L'objet de ce test case est de vérifier que les conflits de saisie
    ...  sont correctement gérés lors de la saisie de la participation par unite.

    Ajouter l'utilisateur  conflit1  conflit1@conflit1.conflit  conflit1  conflit1  ADMINISTRATEUR
    Ajouter l'utilisateur  conflit2  conflit2@conflit2.conflit  conflit2  conflit2  ADMINISTRATEUR

    &{election} =  BuiltIn.Create Dictionary
    ...  delegation_saisie=true
    ...  delegation_participation=true
    Modifier election  ${id_election}  ${election}

    &{delegation} =  BuiltIn.Create Dictionary
    ...  acteur=conflit1
    ...  unite=1 Salle des Mariages
    Ajouter delegation  ${delegation}  ${id_election}

    &{delegation} =  BuiltIn.Create Dictionary
    ...  acteur=conflit2
    ...  unite=1 Salle des Mariages
    Ajouter delegation  ${delegation}  ${id_election}

    # Saisie des résultats
    Depuis la page d'accueil    conflit1  conflit1

    Depuis le contexte de la participation par unite  ${id_election}  1 Salle des Mariages
    Click On SubForm Portlet Action  delegation_participation  modifier
    Input Text  css=fieldset#fieldset-sousform-delegation_participation-participation div.field:nth-child(1) input  25
    Input Text  css=fieldset#fieldset-sousform-delegation_participation-participation div.field:nth-child(2) input  50
    Input Text  css=fieldset#fieldset-sousform-delegation_participation-participation div.field:nth-child(3) input  100

    # Ouverture d'un autre navigateur et accés au formulaire de saisie
    Switch Browser  2
    Depuis la page d'accueil    conflit2  conflit2

    Depuis le contexte de la participation par unite  ${id_election}  1 Salle des Mariages
    Click On SubForm Portlet Action  delegation_participation  modifier
    Input Text  css=fieldset#fieldset-sousform-delegation_participation-participation div.field:nth-child(1) input  0
    Input Text  css=fieldset#fieldset-sousform-delegation_participation-participation div.field:nth-child(2) input  0
    Input Text  css=fieldset#fieldset-sousform-delegation_participation-participation div.field:nth-child(3) input  0
    Click On Submit Button In Subform

    # Retour sur la première fenêtre et validation du formulaire
    Switch Browser  1
    Click On Submit Button In Subform
    Element Should Contain  css=div#sousform-container span.text  Attention: votre modification est en conflit avec la modification précédente!
