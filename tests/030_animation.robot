*** Settings ***
Documentation     Test des animations
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Jeu de données
    [Documentation]  L'objet de ce test case est de creer le jeu de données
    ...  qui servira ensuite à tester le fonctionnement de l'animation

    Depuis la page d'accueil    admin  admin

    # Les unités ont été volontairement créées dans le désordre afin
    # de vérifier que l'ordre d'affichage dans la sidebar des animations
    # est bien réalisé par numéro d'ordre (code) et pas par identifiant
    &{bureau_2} =  BuiltIn.Create Dictionary
    ...  libelle=bureau_2
    ...  type_unite=Bureau de vote
    ...  ordre=2
    ...  code_unite=002
    ${unite2} =  Ajouter unite  ${bureau_2}
    Set Suite Variable  ${unite2}

    &{bureau_1} =  BuiltIn.Create Dictionary
    ...  libelle=bureau_1
    ...  type_unite=Bureau de vote
    ...  ordre=1
    ...  code_unite=001
    ${unite1} =  Ajouter unite  ${bureau_1}
    Set Suite Variable  ${unite1}

    &{mairie_de_12} =  BuiltIn.Create Dictionary
    ...  libelle=mairie_de_12
    ...  type_unite=Mairie
    ...  ordre=5
    ...  type_unite_contenu=Bureau de vote
    ${unite5} =  Ajouter unite  ${mairie_de_12}
    Set Suite Variable  ${unite5}

    &{bureau_3} =  BuiltIn.Create Dictionary
    ...  libelle=bureau_3
    ...  type_unite=Bureau de vote
    ...  ordre=3
    ...  code_unite=003
    ${unite3} =  Ajouter unite  ${bureau_3}
    Set Suite Variable  ${unite3}

    &{arrondissement_123} =  BuiltIn.Create Dictionary
    ...  libelle=arrondissement_123
    ...  type_unite=Arrondissement
    ...  ordre=7
    ...  type_unite_contenu=Mairie
    ${unite7} =  Ajouter unite  ${arrondissement_123}
    Set Suite Variable  ${unite7}

    &{mairie_de_34} =  BuiltIn.Create Dictionary
    ...  libelle=mairie_de_34
    ...  type_unite=Mairie
    ...  ordre=6
    ...  type_unite_contenu=Bureau de vote
    ${unite6} =  Ajouter unite  ${mairie_de_34}
    Set Suite Variable  ${unite6}

    &{bureau_4} =  BuiltIn.Create Dictionary
    ...  libelle=bureau_4
    ...  type_unite=Bureau de vote
    ...  ordre=4
    ...  code_unite=004
    ${unite4} =  Ajouter unite  ${bureau_4}
    Set Suite Variable  ${unite4}

    # Lien entre ces unites pour creer le perimetre de l'election
    Ajouter lien_unite  mairie_de_12  001 bureau_1
    Ajouter lien_unite  mairie_de_12  002 bureau_2
    Ajouter lien_unite  mairie_de_34  004 bureau_4
    Ajouter lien_unite  mairie_de_34  003 bureau_3
    Ajouter lien_unite  arrondissement_123  mairie_de_12
    Ajouter lien_unite  arrondissement_123  mairie_de_34

    # Creation de deux candidats
    &{candidat1} =  BuiltIn.Create Dictionary
    ...  libelle=koro
    ...  libelle_liste=sensei
    Ajouter candidat  ${candidat1}

    &{candidat2} =  BuiltIn.Create Dictionary
    ...  libelle=goku
    ...  libelle_liste=kakarot
    Ajouter candidat  ${candidat2}

    # Ajout d'un logo
    Ajouter le logo  logo  openresultat  openResultat.png  logo openresultat

    # Saisie du nombre d'inscrit pour chaque bureau


Envoi des informations à l'affichage
    [Documentation]  Test de l'envoi des données à l'affichage pour les résultats,
    ...  la participation, la mise en page de l'animation, le perimetre, les candidats et
    ...  le nombre de siège.
    ...  Teste les envois des résultats de trois façon :
    ...  envoi automatique, envoi en cochant la case d'envoi et envoi via l'action
    ...  d'envoi à l'affichage
    ...  Test l'envoi automatique de la participation ainsi que l'envoi via l'action d'envoi

    # Creation d'une election ayant pour périmètre l'arrondissement
    &{election} =  BuiltIn.Create Dictionary
    ...  libelle=Election test animation
    ...  code=lyoko
    ...  principal=true
    ...  type_election=Municipales
    ...  date=09/06/1995
    ...  perimetre=arrondissement_123
    ...  heure_ouverture=08:00:00
    ...  heure_fermeture=11:00:00
    ...  calcul_auto_exprime=true
    ...  sieges_mep=20
    ${id_election} =  Ajouter election  ${election}
    Element Should Contain In Subform  css=div.message  enregistrées
    Set Suite Variable  ${id_election}

    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  File Should Not Exist  ../aff/res/${id_election}/perimetres.json

    # Ajout des candidats à l'élection
    &{candidat} =  BuiltIn.Create Dictionary
    ...  candidat=koro
    ...  ordre=3
    ...  photo=candidat1.png
    ...  couleur=#e1ff00
    ${id_candidat1} =  Ajouter election_candidat  ${candidat}  ${id_election}

    &{candidat} =  BuiltIn.Create Dictionary
    ...  candidat=goku
    ...  ordre=1
    ...  photo=candidat2.png
    ...  couleur=#ff5900
    ${id_candidat2} =  Ajouter election_candidat  ${candidat}  ${id_election}

    # Création d'une nouvelle animation et vérification que les fichiers
    # de paramétrage, du logo et des candidats sont bien envoyés
    &{animation} =  BuiltIn.Create Dictionary
    ...  libelle=test affichage
    ...  titre=Animation test
    ...  refresh=300
    ...  sous_titre=Résultats provisoires - nb_unite_arrivees unités arrivées sur nb_unite
    ...  couleur_titre=#ffffff
    ...  couleur_bandeau=#000000
    ...  perimetre_aff=arrondissement_123
    ...  type_aff=Bureau de vote
    ...  logo=openresultat
    ...  affichage=[sidebar]\nposition=gauche\ntaille=3\n[bloc1]\ntype=resultat_unite\noffset=3\ndiagramme=camembert\n[bloc2]\ntype=resultat_global\nphoto=oui\ntaille=5
    ...  script=document.getElementById("unite-4").innerHTML = "Hello JavaScript!";
    ...  feuille_style=* {color: #ff0000;}
    ${id_animation} =  Ajouter animation à l'élection  ${animation}  ${id_election}
    Set Suite Variable  ${id_animation}
    Element Should Contain In Subform  css=div.message  enregistrées

    Le fichier doit exister  ../aff/res/${id_election}/animation_${id_animation}/param.ini
    Le fichier doit exister  ../aff/res/${id_election}/animation_${id_animation}/blocs.ini
    Le fichier doit exister  ../aff/res/${id_election}/animation_${id_animation}/logo.png
    Le fichier doit exister  ../aff/res/${id_election}/unites.json
    Le fichier doit exister  ../aff/res/${id_election}/perimetres.json
    Le fichier doit exister  ../aff/res/${id_election}/candidats.json
    Le fichier doit exister  ../aff/res/${id_election}/photo/3.png
    Le fichier doit exister  ../aff/res/${id_election}/photo/1.png

    # Durant l'étape de simulation, la saisie des résultats, l'accés aux animations
    # et le paramétrage de l'élection sont possible, c'est donc l'étape la plus pratique
    # pour ce test
    Depuis le contexte election  ${id_election}
    Passer à l'étape suivante  simulation  Simulation
    Reload Page

    Click On Tab  election_unite  unité(s)
    # Envoi des résultats en cochant la case d'envoi à l'affichage
    Click On Link   001 bureau_1
    Click On SubForm Portlet Action  election_unite  modifier
    Set Checkbox  envoi_aff  true
    Input Text  inscrit  100
    Input Text  votant  97
    Input Text  emargement  96
    Input Text  procuration  7
    Input Text  blanc  3
    Input Text  nul  4
    Input Text  candidat3  40
    Input Text  candidat1  50
    Click On Submit Button In Subform
    La page ne doit pas contenir d'erreur

    Le fichier doit exister  ../aff/res/${id_election}/bres/b${unite1}.json

    # Vérification que l'unité est considéré comme envoyé sur l'animation
    Go To  http://localhost/openresultat/aff/animation.php?idx=${id_election}&identifier=${id_animation}
    Page Should Contain Element  css=li#unite-1 span.glyphicon-ok

    # Envoi des résultats à l'aide de l'action d'envoi
    Depuis le contexte election_unite  ${id_election}  002 bureau_2
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  150
    Input Text  votant  150
    Input Text  emargement  144
    Input Text  procuration  7
    Input Text  blanc  15
    Input Text  nul  35
    Input Text  candidat3  70
    Input Text  candidat1  30
    Click On Submit Button In Subform
    La page ne doit pas contenir d'erreur

    Click On SubForm Portlet Action  election_unite  affichage  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    Message Should Contain  ../aff/res/${id_election}/bres/b${unite2}.json
    Message Should Contain  ../aff/res/${id_election}/unites.json
    Message Should Contain  envoi affichage effectué
    Le fichier doit exister  ../aff/res/${id_election}/bres/b${unite2}.json

    Depuis le contexte election_unite  ${id_election}  mairie_de_12
    Click On SubForm Portlet Action  election_unite  affichage  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    Message Should Contain  ../aff/res/${id_election}/bres/b${unite5}.json
    Message Should Contain  ../aff/res/${id_election}/unites.json
    Message Should Contain  envoi affichage effectué
    Le fichier doit exister  ../aff/res/${id_election}/bres/b${unite5}.json

    # Envoi automatique des résultats
    &{election} =  BuiltIn.Create Dictionary
    ...  publication_auto=true
    Modifier election  ${id_election}  ${election}

    Depuis le contexte election_unite  ${id_election}  003 bureau_3
    Click On SubForm Portlet Action  election_unite  modifier
    # Résultat en erreur : les attribut de publication ne doivent pas être à vrai
    # Rien ne doit être transmis au portail web
    Input Text  inscrit  300
    Input Text  blanc  666
    Click On Submit Button In Subform
    La page ne doit pas contenir d'erreur
    Element Should Contain  css=span#envoi_aff  Non
    File Should Not Exist  ../aff/res/${id_election}/bres/b${unite3}.json
    File Should Not Exist  ../aff/res/${id_election}/bres/b${unite6}.json
    File Should Not Exist  ../aff/res/${id_election}/bres/b${unite7}.json

    Depuis le contexte election_unite  ${id_election}  003 bureau_3
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  300
    Input Text  votant  287
    Input Text  emargement  280
    Input Text  procuration  27
    Input Text  blanc  47
    Input Text  nul  39
    Input Text  candidat3  136
    Input Text  candidat1  65
    Click On Submit Button In Subform
    La page ne doit pas contenir d'erreur

    Message Should Contain  ../aff/res/${id_election}/bres/b${unite3}.json
    Message Should Contain  ../aff/res/${id_election}/unites.json
    Message Should Contain  envoi affichage effectué

    # Les résultats de l'unité et de ses périmètres sont tous envoyés car la publication
    # auto est activée
    Le fichier doit exister  ../aff/res/${id_election}/bres/b${unite3}.json
    Le fichier doit exister  ../aff/res/${id_election}/bres/b${unite6}.json
    Le fichier doit exister  ../aff/res/${id_election}/bres/b${unite7}.json

    # Envoi d'un bureau dont seul le nombre d'inscrit est noté
    # Ce bureau ne doit pas être considéré comme arrivé mais son nombre d'inscrit
    # doit être comptabilisé dans le total
    Depuis le contexte election_unite  ${id_election}  004 bureau_4
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  300
    Click On Submit Button In Subform
    La page ne doit pas contenir d'erreur

    Message Should Contain  ../aff/res/${id_election}/bres/b${unite4}.json
    Message Should Contain  ../aff/res/${id_election}/unites.json
    Message Should Contain  envoi affichage effectué

    # Envoi automatique à l'animation
    Click On Tab  participation_election  participation(s)
    Click On Link  09:00:00
    Click On SubForm Portlet Action  participation_election  modifier
    ${participation} =  BuiltIn.Create Dictionary
    ...  unite${unite1}=34
    ...  unite${unite2}=50
    ...  unite${unite3}=147
    ...  unite${unite4}=0
    Saisir participation_election  ${participation}
    # Vérifie que la participation des périmètres s'affiche bien
    Click Element  css=input#unite${unite1}
    Wait Until Element Contains  xpath://input[@id='unite${unite7}']/../p  231
    Click On Submit Button In Subform
    Message Should Contain  La participation a été mise à jour sur les animations
    Click On Back Button In SubForm

    Click On Link  10:00:00
    Click On SubForm Portlet Action  participation_election  modifier
    ${participation} =  BuiltIn.Create Dictionary
    ...  unite${unite1}=27
    ...  unite${unite2}=50
    Saisir participation_election  ${participation}
    # Vérifie que la participation des périmètres s'affiche bien
    Click Element  css=input#unite${unite3}
    Wait Until Element Contains  xpath://input[@id='unite${unite7}']/../p  77
    Click On Submit Button In Subform
    No Message Should Be  La participation a été mise à jour sur les animations

    Click On Back Button In SubForm
    Click On Link  11:00:00
    Click On SubForm Portlet Action  participation_election  modifier
    ${participation} =  BuiltIn.Create Dictionary
    ...  unite${unite1}=36
    ...  unite${unite2}=50
    ...  unite${unite3}=100
    Saisir participation_election  ${participation}
    # Vérifie que la participation des périmètres s'affiche bien
    Click Element  css=input#unite${unite1}
    Wait Until Element Contains  xpath://input[@id='unite${unite7}']/../p  186
    Click On Submit Button In Subform

    # Envoi des resultats de la participation à l'affichage
	@{horaires}    Create List    10:00:00  11:00:00
    :FOR  ${horaire}  IN  @{horaires}
    \    Envoyer la participation à l'affichage  ${id_election}  ${horaire}

    # Existance des fichiers dans le repertoire de l'affichage
    # le fichier des résultats l'unité 5 n'existe pas car il n'a pas été publié en
    # revanche sa participation a été publiée
    Le fichier doit exister  ../aff/res/${id_election}/bpar/b${unite5}.json
	@{id_unites}    Create List    ${unite1}  ${unite2}  ${unite3}  ${unite6}  ${unite7}
    :FOR  ${id_unite}  IN  @{id_unites}
    \    Le fichier doit exister  ../aff/res/${id_election}/bres/b${id_unite}.json
    \    Le fichier doit exister  ../aff/res/${id_election}/bpar/b${id_unite}.json

    # Envoi à l'affichage des informations concernant le nombre de siège
    Depuis le contexte election  ${id_election}
    Click On Form Portlet Action  election  siege  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    Message Should Contain   ../aff/res/${id_election}/repartition_sieges.json envoyé
    Le fichier doit exister  ../aff/res/${id_election}/repartition_sieges.json


Affichage de l'animation
    [Documentation]  Test vérifiant que les éléments affichés correspondent
    ...  aux éléments paramétrés. Vérifie également le fonctionnement de
    ...  chacun des blocs disponibles pour l'animation

    Depuis le contexte animation de l'election  ${id_election}  ${id_animation}
    Click On SubForm Portlet Action  animation  animation  new_window
    Go To  http://localhost/openresultat/aff/animation.php?idx=${id_election}&identifier=${id_animation}
    La page ne doit pas contenir d'erreur

    # vérification du titre, de la couleur du titre, de la présence du logo et du script
    Element Should Contain  css=span.navbar-brand  ANIMATION TEST
    Page Should Contain Element  css=div[style="background-color:#000000;border-color:#000000;"]
    Element Should Contain  css=li > span[style="color:#ffffff;"]  Résultats provisoires - 3 unités arrivées sur 4
    Page Should Contain Element  css=img.logo
    Element Should Contain  css=li#unite-4  HELLO JAVASCRIPT!

    # Vérification de la présence des trois blocs choisit
	@{blocs}    Create List    sidebar-left  resultats-entite  resultats-du-perimetre
    :FOR  ${bloc}  IN  @{blocs}
    \    Page Should Contain Element  css=div.${bloc}
    \    La page ne doit pas contenir d'erreur

    # Vérification du contenu de chaque bloc
    # En prenant, l'arrondissement comme périmètre et les bureaux
    # en type, la sidebar doit contenir : bureau_1, bureau_2 et bureau_3
	@{unites}    Create List    001 BUREAU_1   002 BUREAU_2    003 BUREAU_3
    :FOR  ${unite}  IN  @{unites}
    \    Element Should Contain  css=div.sidebar-left  ${unite}

    # Vérification des résultats affichés dans le bloc des résultats du périmètre
    Element Should Contain  css=div.resultats-du-perimetre div.panel-body  Total inscrits : 850
    Element Should Contain  css=div.resultats-du-perimetre div.panel-body  Inscrits : 550 / Votants : 534 (97 %)
    Element Should Contain  css=div.resultats-du-perimetre div.panel-body  Blancs : 65 / Nuls : 78 / Exprimés : 391
    # Présence des photos des candidats
    Page Should Contain Element  css=div.resultats-du-perimetre img[src="res/${id_election}/photo/3.png"]
    Page Should Contain Element  css=div.resultats-du-perimetre img[src="res/${id_election}/photo/1.png"]
    # Vérification des résultats dans le tableau et de l'ordre d'affichge des candidats
    Table Cell Should Contain  css=div.resultats-du-perimetre table.resultats-resultats  3  1  koro
    Table Cell Should Contain  css=div.resultats-du-perimetre table.resultats-resultats  3  2  246
    Table Cell Should Contain  css=div.resultats-du-perimetre table.resultats-resultats  3  3  62,92%
    Table Cell Should Contain  css=div.resultats-du-perimetre table.resultats-resultats  2  1  goku
    Table Cell Should Contain  css=div.resultats-du-perimetre table.resultats-resultats  2  2  145
    Table Cell Should Contain  css=div.resultats-du-perimetre table.resultats-resultats  2  3  37,08%

    # Vérification des résultats affichés dans le bloc des résultats par unité
    # Pour cette vérification les résultats du bureau_2 sont testés
    Click Element  css=div.sidebar-left li#unite-2
    Element Should Contain  css=div.resultats-entite div.panel-body  Inscrits : 150 / Votants : 150 (100 %)
    Element Should Contain  css=div.resultats-entite div.panel-body  Blancs : 15 / Nuls : 35 / Exprimés : 100
    # Absence des photos des candidats
    Page Should Not Contain Element  css=div.resultats-entite img[src="res/${id_election}/photo/3.png"]
    Page Should Not Contain Element  css=div.resultats-entite img[src="res/${id_election}/photo/1.png"]
    # Vérification des résultats dans le tableau et de l'ordre d'affichge des candidats
    Table Cell Should Contain  css=div.resultats-entite table.resultats-resultats  3  1  koro
    Table Cell Should Contain  css=div.resultats-entite table.resultats-resultats  3  2  70
    Table Cell Should Contain  css=div.resultats-entite table.resultats-resultats  3  3  70,00%
    Table Cell Should Contain  css=div.resultats-entite table.resultats-resultats  2  1  goku
    Table Cell Should Contain  css=div.resultats-entite table.resultats-resultats  2  2  30
    Table Cell Should Contain  css=div.resultats-entite table.resultats-resultats  2  3  30,00%
    # Vérifie la présence de l'emplacement du diagramme
    Page Should Contain Element  css=div.resultats-entite canvas#chartResultatEntite

    # Test de l'affichage des résultats si aucune valeur n'a été saisi
    # cas de l'unité 4
    # Pour cela, le script est enlevé pour ne pas géner le test et l'unité
    # est cliquable même si elle n'est pas "arrivée"
    &{animation} =  BuiltIn.Create Dictionary
    ...  script=
    ...  affichage=[sidebar]\nposition=gauche\ntaille=3\ncliquable=oui\n[bloc1]\ntype=resultat_unite\noffset=3\ndiagramme=camembert\n[bloc2]\ntype=resultat_global\nphoto=oui\ntaille=5
    Modifier animation de l'élection  ${animation}  ${id_election}  ${id_animation}
    Go To  http://localhost/openresultat/aff/animation.php?idx=${id_election}&identifier=${id_animation}
    Click On Link  css=li#unite-4 > a
    La page ne doit pas contenir d'erreur
    Element Should Contain  css=div.resultats-entite div.panel-body  Inscrits : 300 / Votants : 0 (0 %)\nBlancs : 0 / Nuls : 0 / Exprimés : 0
    # Vérification des résultats dans le tableau  et de l'ordre d'affichge des candidats
    Table Cell Should Contain  css=div.resultats-entite table.resultats-resultats  3  1  koro
    Table Cell Should Contain  css=div.resultats-entite table.resultats-resultats  3  2  0
    Table Cell Should Contain  css=div.resultats-entite table.resultats-resultats  3  3  0,00%
    Table Cell Should Contain  css=div.resultats-entite table.resultats-resultats  2  1  goku
    Table Cell Should Contain  css=div.resultats-entite table.resultats-resultats  2  2  0
    Table Cell Should Contain  css=div.resultats-entite table.resultats-resultats  2  3  0,00%

    # Modification des blocs choisit, du diagramme et aucun logo choisit
    # Permet de vérifier que le logo est bien supprimé de l'affichage et
    # le fonctionnement des blocs et diagramme
    &{animation} =  BuiltIn.Create Dictionary
    ...  logo=Choisir logo
    ...  affichage=[sidebar]\nposition=droite\ncliquable=oui\n[bloc1]\ntype=participation_unite\n[bloc2]\ntype=participation_globale\ndiagramme=histogramme
    Modifier animation de l'élection  ${animation}  ${id_election}  ${id_animation}

    Click On SubForm Portlet Action  animation  animation  new_window
    Go To  http://localhost/openresultat/aff/animation.php?idx=${id_election}&identifier=${id_animation}
    La page ne doit pas contenir d'erreur

    # Vérifie que le logo n'est plus présent ur la page
    Page Should Not Contain Element  css=img.logo

    # Vérification de la présence des éléments dans la sidebar
    # En prenant, l'arrondissement comme périmètre et les bureaux
    # en type, la sidebar doit contenir : bureau_1, bureau_2 et bureau_3
	@{unites}    Create List    001 BUREAU_1   002 BUREAU_2    003 BUREAU_3
    :FOR  ${unite}  IN  @{unites}
    \    Element Should Contain  css=div.sidebar  ${unite}

    # Test du bloc de la participation globale
    # Total d'inscrits
    Element Should Contain  css=div.participation-du-perimetre div.panel-body  Inscrits : 850
    # Vérification de la participation enregistrée dans le tableau
    Table Cell Should Contain  css=div.participation-du-perimetre table  2  1  09:00:00
    Table Cell Should Contain  css=div.participation-du-perimetre table  2  2  231
    Table Cell Should Contain  css=div.participation-du-perimetre table  2  3  27,18%
    Table Cell Should Contain  css=div.participation-du-perimetre table  3  1  10:00:00
    Table Cell Should Contain  css=div.participation-du-perimetre table  3  2  77
    Table Cell Should Contain  css=div.participation-du-perimetre table  3  3  9,06%
    Table Cell Should Contain  css=div.participation-du-perimetre table  4  1  11:00:00
    Table Cell Should Contain  css=div.participation-du-perimetre table  4  2  186
    Table Cell Should Contain  css=div.participation-du-perimetre table  4  3  21,88%
    # Vérifie la présence de l'emplacement du diagramme
    Page Should Contain Element  css=div.participation-du-perimetre canvas#chartParticipationPerimetre

    # Test du bloc de la participation par unité pour le bureau_3
    Click Element  css=div.sidebar li#unite-3
    # Total d'inscrits
    Element Should Contain  css=div.participation-entite div.panel-body  Inscrits : 300
    # Vérification de la participation enregistrée dans le tableau
    Table Cell Should Contain  css=div.participation-entite table  2  1  09:00:00
    Table Cell Should Contain  css=div.participation-entite table  2  2  147
    Table Cell Should Contain  css=div.participation-entite table  2  3  49,00%
    Table Cell Should Contain  css=div.participation-entite table  3  1  10:00:00
    Table Cell Should Contain  css=div.participation-entite table  3  2  ${EMPTY}
    Table Cell Should Contain  css=div.participation-entite table  3  3  0,00%
    Table Cell Should Contain  css=div.participation-entite table  4  1  11:00:00
    Table Cell Should Contain  css=div.participation-entite table  4  2  100
    Table Cell Should Contain  css=div.participation-entite table  4  3  33,33%

    # Test de l'affichage du bureau_4 pour lequel la participation n'a pas été saisie
    Click On Link  css=li#unite-4 > a
    La page ne doit pas contenir d'erreur
    Element Should Contain  css=div.participation-entite div.panel-body  Inscrits : 300
    # Vérification des résultats dans le tableau
    Table Cell Should Contain  css=div.participation-entite table  2  1  09:00:00
    Table Cell Should Contain  css=div.participation-entite table  2  2  0
    Table Cell Should Contain  css=div.participation-entite table  2  3  0,00%
    Table Cell Should Contain  css=div.participation-entite table  3  1  10:00:00
    Table Cell Should Contain  css=div.participation-entite table  3  2  0
    Table Cell Should Contain  css=div.participation-entite table  3  3  0,00%
    Table Cell Should Contain  css=div.participation-entite table  4  1  11:00:00
    Table Cell Should Contain  css=div.participation-entite table  4  2  0
    Table Cell Should Contain  css=div.participation-entite table  4  3  0,00%

    # Modification des blocs choisit pour tester les 3 derniers blocs
    &{animation} =  BuiltIn.Create Dictionary
    ...  affichage=[bloc1]\ntype=sieges_communautaire\ndiagramme=donut\n[bloc2]\ntype=sieges_municipal\ndiagramme=donut\n[bloc3]\ntype=sieges_metropolitain\ndiagramme=donut
    Modifier animation de l'élection  ${animation}  ${id_election}  ${id_animation}

    Click On SubForm Portlet Action  animation  animation  new_window
    Go To  http://localhost/openresultat/aff/animation.php?idx=${id_election}&identifier=${id_animation}
    La page ne doit pas contenir d'erreur

    # Test du bloc : siège conseil municipal
    Table Cell Should Contain  css=div#repartition_sieges_cm table  2  1  sensei
    Table Cell Should Contain  css=div#repartition_sieges_cm table  2  2  24 sièges
    Table Cell Should Contain  css=div#repartition_sieges_cm table  1  1  kakarot
    Table Cell Should Contain  css=div#repartition_sieges_cm table  1  2  5 sièges
    # Vérifie la présence de l'emplacement du diagramme
    Page Should Contain Element  css=div#repartition_sieges_cm canvas#chartSiegesMun

    # Test du bloc : siège conseil communautaire
    Table Cell Should Contain  css=div#repartition_sieges_cc table  2  1  sensei
    Table Cell Should Contain  css=div#repartition_sieges_cc table  2  2  1 sièges
    Table Cell Should Contain  css=div#repartition_sieges_cc table  1  1  kakarot
    Table Cell Should Contain  css=div#repartition_sieges_cc table  1  2  0 sièges
    # Vérifie la présence de l'emplacement du diagramme
    Page Should Contain Element  css=div#repartition_sieges_cc canvas#chartSiegesCom

    # Test du bloc : siège conseil metropolitain
    Table Cell Should Contain  css=div#repartition_sieges_cmep table  2  1  sensei
    Table Cell Should Contain  css=div#repartition_sieges_cmep table  2  2  16 sièges
    Table Cell Should Contain  css=div#repartition_sieges_cmep table  1  1  kakarot
    Table Cell Should Contain  css=div#repartition_sieges_cmep table  1  2  4 sièges
    # Vérifie la présence de l'emplacement du diagramme
    Page Should Contain Element  css=div#repartition_sieges_cmep canvas#chartSiegesMep

    # Modification des blocs pour saisir un nom de bloc qui n'existe pas
    # Permet de vérifier qu'il n'y a pas d'erreur même en cas d'erreur sur le nom d'un bloc
    &{animation} =  BuiltIn.Create Dictionary
    ...  affichage=[bloc1]\ntype=type_non_existant\n[bloc2]\ntype=sieges_municipal
    Modifier animation de l'élection  ${animation}  ${id_election}  ${id_animation}

    Click On SubForm Portlet Action  animation  animation  new_window
    Go To  http://localhost/openresultat/aff/animation.php?idx=${id_election}&identifier=${id_animation}
    La page ne doit pas contenir d'erreur
    Page Should Contain Element  css=div#repartition_sieges_cm
    # Seul le panel de repartition des sièges doit être visible
    Page Should Contain Element  css=div.panel  None  INFO  1

Défilement
    [Documentation]  Test servant à vérifier que le défilement des unités
    ...  de l'élection fonctionne. C'est à dire que pour l'affichage des
    ...  résultats ou de la participation par unité, à chaque fois que la
    ...  page est réchargée on passe à l'unité suivante

    # Modification des blocs choisit pour avoir les blocs resultat/participation par unite
    # et la liste. La liste permet de sélectionner le bloc de départ pour le test
    &{animation} =  BuiltIn.Create Dictionary
    ...  affichage=[bloc1]\ntype=participation_unite\n[bloc2]\ntype=resultat_unite\n[sidebar]
    Modifier animation de l'élection  ${animation}  ${id_election}  ${id_animation}
    Click On SubForm Portlet Action  animation  animation  new_window
    Go To  http://localhost/openresultat/aff/animation.php?idx=${id_election}&identifier=${id_animation}
    La page ne doit pas contenir d'erreur

    # Sélectionne le premier élément, vérifie le titre des blocs, car il porte le nom de l'unité,
    # puis recharge la page et vérifie que l'unité suivante est affichée
    Click Element  css=div.sidebar li#unite-1
    :FOR  ${index}  IN RANGE  1  4
    \   Element Should Contain  css=div.resultats-entite div.panel-heading      00${index} bureau_${index}
    \   Element Should Contain  css=div.participation-entite div.panel-heading  00${index} bureau_${index}
    \   Reload Page
    \   La page ne doit pas contenir d'erreur

Utilisation de modèle
    [Documentation]  Test servant à vérifier que l'utilisation des modèles fonctionne.
    ...  Le principe est de pouvoir enregistrer une animation comme modèle. Ensuite
    ...  dans les autres animations si ce modèle est sélectionné, le même paramétrage
    ...  est donné dans le formulaire.

    # Création d'un modèle depuis le paramétrage métier
    &{animation} =  BuiltIn.Create Dictionary
    ...  libelle=Modele
    ...  titre=Test Modele
    ...  sous_titre=Sous test
    ...  couleur_titre=#ff0000
    ...  couleur_bandeau=#000000
    ...  logo=openresultat
    ...  affichage=[sidebar]\nposition=droite\n[bloc1]\ntype=resultat_global\n[bloc2]\ntype=resultat_unite
    ...  script=script
    ...  feuille_style=css
    ${id_modele} =  Ajouter animation  ${animation}
    La page ne doit pas contenir d'erreur
    # Le portlet ne doit pas contenir l'action permettant de créer des modèle
    Depuis le contexte animation  ${id_modele}
    Portlet Action Should Be In Form  animation  activer
    Activer animation  ${id_modele}
    Depuis le contexte animation  ${id_modele}
    Portlet Action Should Be In Form  animation  desactiver
    Portlet Action Should Not Be In Form  animation  creer_modele

    # Création d'un modèle depuis l'élection
    &{animation} =  BuiltIn.Create Dictionary
    ...  libelle=Mod election
    ...  titre=Test Modele election
    ...  sous_titre=Sous test modele election
    ...  couleur_titre=#000000
    ...  couleur_bandeau=#0000ff
    ...  affichage=
    ...  script=test
    ...  feuille_style=feuille_style
    ${id_animation_election} =  Ajouter animation à l'élection  ${animation}  ${id_election}
    La page ne doit pas contenir d'erreur
    # Le portlet ne doit pas contenir l'action permettant d'activer/desactiver des modeles
    Depuis le contexte animation de l'election  ${id_election}  ${id_animation_election}
    Portlet Action Should Be In SubForm  animation  creer_modele
    Portlet Action Should Not Be In SubForm  animation  activer
    Portlet Action Should Not Be In SubForm  animation  desactiver
    # Copie de l'animation en tant que modèle
    Click On SubForm Portlet Action  animation  creer_modele  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    La page ne doit pas contenir d'erreur
    # Activation du modèle pour pouvoir l'utiliser
    Depuis le listing  animation
    Use Simple Search  libellé  Modèle - Mod election
    ${id_modele_election} =  Get Text  css=div#tab-animation table tbody tr:nth-child(1) td:nth-child(2)
    Activer animation  ${id_modele_election}

    # Seul les modèles doivent être visible dans le listing des animationd paramétrage
    Depuis le listing  animation
    Table Should Contain  css=div#tab-animation table  Modèle - Mod election
    Table Should Contain  css=div#tab-animation table  Modele
    Element Should Not Contain  css=div#tab-animation table  test affichage
    # Les modèles ne doivent pas apparaître dans le listing des animation de l'élection
    Depuis le contexte election  ${id_election}
    Click On Tab  animation  animation(s)
    Element Should Not Contain  css=div#sousform-animation table  Modèle - Mod election
    Element Should Not Contain  css=div#sousform-animation table  Modele

    # Animation utilisant les modèles
    &{animation} =  BuiltIn.Create Dictionary
    ...  libelle=animation test modele
    ...  titre=Test Modele election
    ...  sous_titre=Sous test modele election
    ...  couleur_titre=#000000
    ...  couleur_bandeau=#000000
    ...  modele=Modele
    Depuis le contexte election  ${id_election}
    Click On Tab  animation  animation(s)
    Click On Add Button In SubForm
    Saisir animation de l'élection  ${animation}

    # Vérification que les valeurs du formulaire correspondent à celle du modele
    Page Should Contain Element  css=input#titre[value="Test Modele"]
    Page Should Contain Element  css=input#sous_titre[value="Sous test"]
    Page Should Contain Element  css=input#couleur_titre[value="#ff0000"]
    Page Should Contain Element  css=input#couleur_bandeau[value="#000000"]
    Element Should Contain  css=textarea#affichage    [sidebar]\nposition=droite\n[bloc1]\ntype=resultat_global\n[bloc2]\ntype=resultat_unite
    Element Should Contain  css=textarea#script    script
    Element Should Contain  css=textarea#feuille_style    css
    Element Should Contain  css=select#logo option[selected="selected"]    openresultat

    # Test du deuxième modèle
    &{animation} =  BuiltIn.Create Dictionary
    ...  modele=Modèle - Mod election
    Depuis le contexte election  ${id_election}
    Click On Tab  animation  animation(s)
    Click On Add Button In SubForm
    Saisir animation de l'élection  ${animation}
    Page Should Contain Element  css=input#titre[value="Test Modele election"]
    Page Should Contain Element  css=input#sous_titre[value="Sous test modele election"]
    Page Should Contain Element  css=input#couleur_titre[value="#000000"]
    Page Should Contain Element  css=input#couleur_bandeau[value="#0000ff"]
    Element Should Contain  css=textarea#affichage    ${EMPTY}
    Element Should Contain  css=textarea#script    test
    Element Should Contain  css=textarea#feuille_style    feuille_style
    Element Should Contain  css=select#logo option[selected="selected"]    ${EMPTY}

Désaffichage
    [Documentation]  Test servant à vérifier que l'annulation des résultats
    ...  et de la participation retire bien les informations de l'affichage.
    ...  Teste les deux cas de désaffichage : en décochant la case d'affichage
    ...  et en utilisant l'action de suppression

    # Suppression de la participation à 10:00:00 sur l'animation
    Depuis le contexte participation_election  ${id_election}  10:00:00
    Click On SubForm Portlet Action  participation_election  desafficher  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer

    # Animation avec les blocs servant à vérifier la participation par unité et globale
    &{animation} =  BuiltIn.Create Dictionary
    ...  libelle=Test Annulation Participation
    ...  titre=Test Annulation Participation
    ...  perimetre_aff=arrondissement_123
    ...  type_aff=Bureau de vote
    ...  affichage=[bloc1]\ntype=participation_unite\n[bloc2]\ntype=participation_globale\n[sidebar]\ncliquable=oui
    ${id_animation} =  Ajouter animation à l'élection  ${animation}  ${id_election}
    Click On SubForm Portlet Action  animation  animation  new_window
    Go To  http://localhost/openresultat/aff/animation.php?idx=${id_election}&identifier=${id_animation}
    La page ne doit pas contenir d'erreur

    # Vérification de la participation globale a 10:00:00
    Element Should Not Contain  css=div.participation-du-perimetre table  10:00:00
    # Vérification de la participation d'une unité a 10:00:00 on est à 0
    Click Element  css=div.sidebar li#unite-2
    Element Should Not Contain  css=div.participation-entite table  010:00:00

    # Suppression des résultats du bureau_1 sur l'animation
    Depuis le contexte election_unite  ${id_election}  001 bureau_1
    Click On SubForm Portlet Action  election_unite  depublier_aff  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    Message Should Contain   ../aff/res/${id_election}/bres/b${unite1}.json supprimé
    Message Should Contain   ../aff/res/${id_election}/unites.json envoyé

    # Pas d'envoi automatique des résultats afin de pouvoir accéder au champ de pubication
    &{election} =  BuiltIn.Create Dictionary
    ...  publication_auto=false
    Modifier election  ${id_election}  ${election}

    Depuis le contexte election_unite  ${id_election}  002 bureau_2
    Click On SubForm Portlet Action  election_unite  modifier
    Set Checkbox  envoi_aff  false
    Click On Submit Button In Subform
    La page ne doit pas contenir d'erreur
    Message Should Contain   ../aff/res/${id_election}/bres/b${unite2}.json supprimé
    Message Should Contain   ../aff/res/${id_election}/unites.json envoyé

    # Animation avec les blocs servant à vérifier les resultats par unité et globaux
    &{animation} =  BuiltIn.Create Dictionary
    ...  titre=Test Annulation Resultat
    ...  libelle=Test Annulation Resultat
    ...  perimetre_aff=arrondissement_123
    ...  type_aff=Bureau de vote
    ...  affichage=[bloc1]\ntype=resultat_unite\n[bloc2]\ntype=resultat_global\n[sidebar]
    ${id_animation} =  Ajouter animation à l'élection  ${animation}  ${id_election}
    Click On SubForm Portlet Action  animation  animation  new_window
    Go To  http://localhost/openresultat/aff/animation.php?idx=${id_election}&identifier=${id_animation}
    La page ne doit pas contenir d'erreur

    # Vérification des résultats affichés dans le bloc des résultats du périmètre
    Element Should Contain  css=div.resultats-du-perimetre div.panel-body  Inscrits : 300 / Votants : 287 (95 %)
    Element Should Contain  css=div.resultats-du-perimetre div.panel-body  Blancs : 47 / Nuls : 39 / Exprimés : 201
    # Vérification des résultats dans le tableau
    Table Cell Should Contain  css=div.resultats-du-perimetre table.resultats-resultats  3  1  koro
    Table Cell Should Contain  css=div.resultats-du-perimetre table.resultats-resultats  3  2  136
    Table Cell Should Contain  css=div.resultats-du-perimetre table.resultats-resultats  3  3  67,66%
    Table Cell Should Contain  css=div.resultats-du-perimetre table.resultats-resultats  2  1  goku
    Table Cell Should Contain  css=div.resultats-du-perimetre table.resultats-resultats  2  2  65
    Table Cell Should Contain  css=div.resultats-du-perimetre table.resultats-resultats  2  3  32,34%

Comparaison de la participation de deux élections
    [Documentation]  Test servant à vérifier que la comparaison de la participation
    ...  entre deux élections fonctionnent. C'est à dire que le tableau de la
    ...  participation globale et celui de la participation par unité contiennent
    ...  bien les lignes de la participation de l'élection de comparaison

    # Création d'une nouvelle élection et remplissage de sa participation
    &{election} =  BuiltIn.Create Dictionary
    ...  libelle=Election test comparaison
    ...  type_election=Municipales
    ...  date=09/06/1995
    ...  perimetre=arrondissement_123
    ...  heure_ouverture=08:00:00
    ...  heure_fermeture=10:00:00
    ${election_comparaison} =  Ajouter election  ${election}
    Set Suite Variable  ${election_comparaison}

    Element Should Contain In Subform  css=div.message  enregistrées

    # Creation de deux candidats
    &{candidat} =  BuiltIn.Create Dictionary
    ...  candidat=koro
    ...  ordre=1
    ...  photo=candidat1.png
    ...  couleur=#e1ff00
    ${id_candidat1} =  Ajouter election_candidat  ${candidat}  ${election_comparaison}

    &{candidat} =  BuiltIn.Create Dictionary
    ...  candidat=goku
    ...  ordre=2
    ...  photo=candidat2.png
    ...  couleur=#ff5900
    ${id_candidat2} =  Ajouter election_candidat  ${candidat}  ${election_comparaison}

    # Durant l'étape de simulation, la saisie des résultats, l'accés aux animations
    # et le paramétrage de l'élection sont possible, c'est donc l'étape la plus pratique
    # pour ce test
    Depuis le contexte election  ${election_comparaison}
    Passer à l'étape suivante  simulation  Simulation

    # Saisie du nombre d'inscrit et de votant
    Click On Tab  election_unite  unité(s)
    # Acces au bureau_1 et saisie de ses résultats
    Click On Link   001 bureau_1
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  150
    Input Text  votant  147
    Click On Submit Button In Subform
    La page ne doit pas contenir d'erreur
    # Acces au bureau_2 et saisie de ses résultats
    Click On Back Button In Subform
    Click On Link   002 bureau_2
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  150
    Input Text  votant  150
    Click On Submit Button In Subform
    La page ne doit pas contenir d'erreur
    # Acces au bureau_3 et saisie de ses résultats
    Click On Back Button In Subform
    Click On Link    003 bureau_3
    Click On SubForm Portlet Action  election_unite  modifier
    Input Text  inscrit  300
    Input Text  votant  297
    Click On Submit Button In Subform
    La page ne doit pas contenir d'erreur

    # Saisie de la participation
    Click On Tab  participation_election  participation(s)
    Click On Link  09:00:00
    Click On SubForm Portlet Action  participation_election  modifier
    Input Text  unite${unite1}  45
    Input Text  unite${unite2}  50
    Input Text  unite${unite3}  147
    Click On Submit Button In Subform
    La page ne doit pas contenir d'erreur

    Click On Back Button In SubForm
    Click On Link  10:00:00
    Click On SubForm Portlet Action  participation_election  modifier
    Input Text  unite${unite1}  55
    Input Text  unite${unite2}  50
    Input Text  unite${unite3}  50
    Click On Submit Button In Subform
    La page ne doit pas contenir d'erreur

    # Envoi des resultats de chaque unités à l'affichage ce qui permet d'avoir
    # le nombre d'inscrit et de votant
	@{unites}    Create List    001 bureau_1  004 bureau_4  003 bureau_3  002 bureau_2  mairie_de_12  mairie_de_34  arrondissement_123
    :FOR  ${unite}  IN  @{unites}
    \    Envoyer les résultats à l'affichage  ${election_comparaison}  ${unite}
    # Envoi des resultats de la participation à l'affichage
	@{horaires}    Create List    09:00:00  10:00:00
    :FOR  ${horaire}  IN  @{horaires}
    \    Envoyer la participation à l'affichage  ${election_comparaison}  ${horaire}

    # Création d'une animation à comparer avec l'animation de l'autre élection
	@{election_a_comparer}    Create List    Election test animation
    &{animation} =  BuiltIn.Create Dictionary
    ...  libelle=test comparaison
    ...  titre=Comparaison test
    ...  sous_titre=Résultats provisoires - nb_unite_arrivees unités arrivées sur nb_unite
    ...  couleur_titre=#ffffff
    ...  couleur_bandeau=#000000
    ...  perimetre_aff=arrondissement_123
    ...  type_aff=Bureau de vote
    ...  logo=openresultat
    ...  affichage=[bloc1]\ntype=comparaison_participation\ntaille=8
    ...  election_comparaison=${election_a_comparer}
    ${id_animation_comp} =  Ajouter animation à l'élection  ${animation}  ${election_comparaison}
    Set Suite Variable  ${id_animation}
    Element Should Contain In Subform  css=div.message  enregistrées

    Go To  http://localhost/openresultat/aff/animation.php?idx=${election_comparaison}&identifier=${id_animation_comp}
    Table Cell Should Contain  css=table#tab-comparaison  2  1  09:00:00
    Table Cell Should Contain  css=table#tab-comparaison  2  2  40.33% ( 242 )
    Table Cell Should Contain  css=table#tab-comparaison  2  3  27.18% ( 231 )
    Table Cell Should Contain  css=table#tab-comparaison  3  1  10:00:00
    Table Cell Should Contain  css=table#tab-comparaison  3  2  25.83% ( 155 )
    Table Cell Should Contain  css=table#tab-comparaison  3  3  -

Reset des résultats de simulation
    [Documentation]  Test vérifiant que les unités sont bien mises à jour
    ...  sur les animations suite au reset en sortie de l'étape de simulation

    # Animation des résultats et de la participation
    &{animation} =  BuiltIn.Create Dictionary
    ...  libelle=reset resultat
    ...  titre=Animation test
    ...  refresh=300
    ...  perimetre_aff=arrondissement_123
    ...  type_aff=Bureau de vote
    ...  modele=Résultat
    ${id_animation_res} =  Ajouter animation à l'élection  ${animation}  ${id_election}

    Depuis le contexte election  ${id_election}
    Passer à l'étape suivante  saisie  Saisie
    
    Go To  http://localhost/openresultat/aff/animation.php?idx=${id_election}&identifier=${id_animation_res}
    La page ne doit pas contenir d'erreur

    # Vérification que les unités sont non envoyées
    Locator Should Match X Times  css=div.sidebar span.glyphicon-time  4

    # Vérification des résultats affichés dans le bloc des résultats du périmètre
    Element Should Contain  css=div.resultats-du-perimetre div.panel-body  Total inscrits : 0
    Element Should Contain  css=div.resultats-du-perimetre div.panel-body  Inscrits : 0 / Votants : 0 (0 %)
    Element Should Contain  css=div.resultats-du-perimetre div.panel-body  Blancs : 0 / Nuls : 0 / Exprimés : 0

Suppression des animations suite à la suppression de l'élection
    [Documentation]  Test servant à vérifier que la suppression d'une élection entraine
    ...  la suppression de son répertoire aff et de celui de ses centaines

    # Les répertoires ne doivent plus exister
    Depuis le contexte election  ${id_election}
    Retourner à l'étape précédente  simulation  Simulation
    Depuis le contexte election  ${id_election}
    Retourner à l'étape précédente  parametrage  Paramétrage
    Supprimer election  ${id_election}
    File Should Not Exist  ../aff/res/${id_election}
    #File Should Not Exist  ../aff/res/${id_centaine}
    Depuis le contexte election  ${election_comparaison}
    Retourner à l'étape précédente  parametrage  Paramétrage
    Supprimer election  ${election_comparaison}
    File Should Not Exist  ../aff/res/${election_comparaison}
