
--
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) 
SELECT nextval('om_parametre_seq'), 'prefixe_edition_substitution_vars', 'edi_', 1
WHERE
    NOT EXISTS ( 
       SELECT om_parametre FROM om_parametre WHERE libelle = 'prefixe_edition_substitution_vars' AND om_collectivite = 1
    );
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) 
SELECT nextval('om_parametre_seq'), 'edi_entete_resultats', 'LIBREVILLE', 1
WHERE
    NOT EXISTS ( 
       SELECT om_parametre FROM om_parametre WHERE libelle = 'edi_entete_resultats' AND om_collectivite = 1
    );
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) 
SELECT nextval('om_parametre_seq'), 'edi_ville', 'LIBREVILLE', 1
WHERE
    NOT EXISTS ( 
       SELECT om_parametre FROM om_parametre WHERE libelle = 'edi_ville' AND om_collectivite = 1
    );

--
INSERT INTO om_requete (om_requete, code, libelle, description, requete, merge_fields, type, classe, methode) VALUES
(
    nextval('om_requete_seq'),
    'election', 'résultat de l''élection', NULL,
    NULL, '',
    'objet',
    'election', NULL
)
,
(
    nextval('om_requete_seq'),
    'election_unite', 'résultat d''une unité pour une élection', NULL,
    NULL, '',
    'objet',
    'election_unite', 'get_merge_fields_and_co'
)
;

--
INSERT INTO om_etat (om_etat, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, se_font, se_couleurtexte, margeleft, margetop, margeright, margebottom, header_om_htmletat, header_offset, footer_om_htmletat, footer_offset) VALUES
(
    nextval('om_etat_seq'),
    1,
    'participation', 'affichage de la participation', true,
    'L', 'A3',
    NULL,
    10, 15, 
    '<p style=''text-align: center;''><span style=''font-size: 15pt; font-weight: bold;''>[election.type_election] [election.annee] - Tour [election.tour] - [election.date]</span></p>
<p style=''text-align: center;''><span style=''font-size: 14pt;''>ÉTAT DE LA PARTICIPATION</span></p>
<p style=''text-align: center;''> </p>',
    10, 15, 0, 0, 0,
    '<p><span style=''font-size: 11pt;''>[election.etat_participation]</span></p>',
    (SELECT om_requete FROM om_requete WHERE code='election'),
    'helvetica', '0-0-0',
    10, 15, 10, 15,
    '<table>
<tbody>
<tr>
<td style=''text-align: left;''><span style=''font-size: 11pt;''>&amp;entete_resultats</span></td>
<td>
<p style=''text-align: right;''><span style=''font-size: 11pt;''>Édition du &amp;aujourdhui à &amp;maintenant</span></p>
</td>
</tr>
</tbody>
</table>',
    10,
    '<p style=''text-align: center;''><span style=''font-size: 8pt;''><em>Page &amp;numpage/&amp;nbpages</em></span></p>',
    12
)
,
(
    nextval('om_etat_seq'),
    1,
    'election', 'affichage des résultats', true,
    'L', 'A3',
    NULL,
    10, 15,
    '<p style=''text-align: center;''><span style=''font-size: 15pt; font-weight: bold;''>[election.type_election] [election.annee] - Tour [election.tour] - [election.date]</span></p>
<p style=''text-align: center;''><span style=''font-size: 14pt;''>RÉSULTATS</span></p>
<p style=''text-align: center;''> </p>',
    10, 15, 0, 0, 0,
    '<p>[election.etat_resultat]</p>',
    (SELECT om_requete FROM om_requete WHERE code='election'),
    'helvetica', '0-0-0',
    10, 15, 10, 15,
    '<table>
<tbody>
<tr>
<td style=''text-align: left;''><span style=''font-size: 11pt;''>&amp;entete_resultats</span></td>
<td>
<p style=''text-align: right;''><span style=''font-size: 11pt;''>Édition du &amp;aujourdhui à &amp;maintenant</span></p>
</td>
</tr>
</tbody>
</table>',
    10,
    '<p style=''text-align: center;''><span style=''font-size: 8pt;''><em>Page &amp;numpage/&amp;nbpages</em></span></p>',
    12
)
,
(
    nextval('om_etat_seq'),
    1,
    'prefecture', 'prefecture', true,
    'L', 'A3',
    NULL,
    10, 15,
    '<p style=''text-align: center;''><span style=''font-size: 15pt; font-weight: bold;''>[election.type_election] [election.annee] - Tour [election.tour] - [election.date]</span></p>
<p style=''text-align: center;''><span style=''font-size: 14pt;''>RÉSULTATS</span></p>
<p style=''text-align: center;''> </p>',
    10, 15, 0, 0, 0,
    '<p>[election.etat_resultat_prefecture]</p>',
    (SELECT om_requete FROM om_requete WHERE code='election'),
    'helvetica', '0-0-0',
    10, 15, 10, 15,
    '<table>
<tbody>
<tr>
<td style=''text-align: left;''><span style=''font-size: 11pt;''>&amp;entete_resultats</span></td>
<td>
<p style=''text-align: right;''><span style=''font-size: 11pt;''>Édition du &amp;aujourdhui à &amp;maintenant</span></p>
</td>
</tr>
</tbody>
</table>',
    10,
    '<p style=''text-align: center;''><span style=''font-size: 8pt;''><em>Page &amp;numpage/&amp;nbpages</em></span></p>',
    12
)
,
(
    nextval('om_etat_seq'),
    1,
    'resultats_globaux', 'resultats_globaux', true,
    'L', 'A3',
    NULL,
    10, 15,
    '<p style=''text-align: center;''><span style=''font-size: 15pt; font-weight: bold;''>[election.type_election] [election.annee] - Tour [election.tour] - [election.date]</span></p>
<p style=''text-align: center;''><span style=''font-size: 14pt;''>RÉSULTATS</span></p>
<p style=''text-align: center;''> </p>',
    10, 15, 0, 0, 0,
    '<p>[election.etat_resultat_globaux_opt2]</p>',
    (SELECT om_requete FROM om_requete WHERE code='election'),
    'helvetica', '0-0-0',
    10, 15, 10, 15,
    '<table>
<tbody>
<tr>
<td style=''text-align: left;''><span style=''font-size: 11pt;''>&amp;entete_resultats</span></td>
<td>
<p style=''text-align: right;''><span style=''font-size: 11pt;''>Édition du &amp;aujourdhui à &amp;maintenant</span></p>
</td>
</tr>
</tbody>
</table>',
    10,
    '<p style=''text-align: center;''><span style=''font-size: 8pt;''><em>Page &amp;numpage/&amp;nbpages</em></span></p>',
    12
)
,
(
    nextval('om_etat_seq'),
    1,
    'resultat_perimetre', 'résultat par périmètre', true,
    'L', 'A3',
    NULL,
    10, 15,
    '<p style=''text-align: center;''><span style=''font-size: 15pt; font-weight: bold;''>[election.type_election] [election.annee] - Tour [election.tour] - [election.date]</span></p>
<p style=''text-align: center;''><span style=''font-size: 14pt;''>RÉSULTATS</span></p>
<p style=''text-align: center;''> </p>',
    10, 15, 0, 0, 0,
    '<p>[election.etat_resultat_perimetre_opt1]</p>',
    (SELECT om_requete FROM om_requete WHERE code='election'),
    'helvetica', '0-0-0',
    10, 15, 10, 15,
    '<table>
<tbody>
<tr>
<td style=''text-align: left;''><span style=''font-size: 11pt;''>&amp;entete_resultats</span></td>
<td>
<p style=''text-align: right;''><span style=''font-size: 11pt;''>Édition du &amp;aujourdhui à &amp;maintenant</span></p>
</td>
</tr>
</tbody>
</table>',
    10,
    '<p style=''text-align: center;''><span style=''font-size: 8pt;''><em>Page &amp;numpage/&amp;nbpages</em></span></p>',
    12
)
,
(
    nextval('om_etat_seq'),
    1,
    'proclamation', 'proclamation des resultats', true,
    'P', 'A4',
    NULL,
    10, 25,
    '<p style=''text-align: center;''><span style=''font-weight: bold; font-size: 18pt;''>RÉSULTATS DÉFINITIFS</span></p>
<p style=''text-align: center;''><span style=''font-weight: bold; font-size: 12pt;''>Commune : &amp;ville</span></p>
<p style=''text-align: center;''> </p>
<p style=''text-align: center;''> </p>',
    10, 15, 190, 50, '0',
    '<table border=''0'' cellspacing=''2''>
<tbody>
<tr>
<td style=''font-weight: bold; width: 20%;''><span style=''font-size: 12pt;''>SCRUTIN</span></td>
<td style=''width: 20%;''><br /><span style=''font-size: 12pt;''>[election.type_election] [election.annee]</span></td>
<td style=''font-weight: bold; width: 20%;''> </td>
<td style=''font-weight: bold; width: 25%;''><span style=''font-weight: bold; font-size: 12pt;''>CIRCONSCRIPTION</span></td>
<td style=''width: 15%;''><span style=''font-size: 12pt;''>[perimetre.circonscription.code]</span></td>
</tr>
<tr>
<td style=''font-weight: bold; width: 20%;''><span style=''font-size: 12pt;''>TOUR</span></td>
<td style=''width: 20%;''><span style=''font-size: 12pt;''>[election.tour]</span></td>
<td style=''font-weight: bold; width: 20%;''> </td>
<td style=''font-weight: bold; width: 25%;''><span style=''font-weight: bold; font-size: 12pt;''>DÉPARTEMENT</span></td>
<td style=''width: 15%;''><span style=''font-size: 12pt;''>[perimetre.departement.code]</span></td>
</tr>
<tr>
<td style=''font-weight: bold; width: 20%;''><span style=''font-size: 12pt;''>DATE</span></td>
<td style=''width: 20%;''><span style=''font-size: 12pt;''>[election.date]</span></td>
<td style=''font-weight: bold; width: 20%;''> </td>
<td style=''font-weight: bold; width: 25%;''><span style=''font-weight: bold; font-size: 12pt;''>COMMUNE</span></td>
<td style=''width: 15%;''><span style=''font-size: 12pt;''>[perimetre.commune.code]</span></td>
</tr>
</tbody>
</table>
<p> </p>
<table border=''0'' style=''width: 100%;'' cellspacing=''0'' cellpadding=''2''>
<tbody>
<tr>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''>Inscrits :</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[perimetre.resultat.inscrit]</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''> </span></th>
<th style=''width: 10%; text-align: right;''> </th>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''> </span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''> </span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''> </span></th>
</tr>
<tr>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''>Abstentions :</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[perimetre.resultat.abstention]</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[perimetre.resultat.pourcentage_abstention]</span></th>
<th style=''width: 10%; text-align: right;''> </th>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''>Blancs :</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[perimetre.resultat.blanc]</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[perimetre.resultat.pourcentage_blanc]</span></th>
</tr>
<tr>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''>Votants :</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[perimetre.resultat.votant]</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[perimetre.resultat.pourcentage_votant]</span></th>
<th style=''width: 10%; text-align: right;''> </th>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''>Nuls :</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[perimetre.resultat.nul]</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[perimetre.resultat.pourcentage_nul]</span></th>
</tr>
<tr>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''>Émargements :</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[perimetre.resultat.emargement]</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[perimetre.resultat.pourcentage_emargement]</span></th>
<th style=''width: 10%; text-align: right;''> </th>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''>Exprimés :</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[perimetre.resultat.exprime]</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[perimetre.resultat.pourcentage_exprime]</span></th>
</tr>
</tbody>
</table>
<p>[election.etat_proclamation]</p>
<p> </p>',
    (SELECT om_requete FROM om_requete WHERE code='election'),
    'helvetica', '0-0-0',
    10, 25, 10, 25,
    '<p style=''text-align: right;''><span style=''font-size: 7pt;''>Édition du &amp;aujourdhui à &amp;maintenant</span></p>',
    10,
    '<p style=''text-align: center; font-size: 8pt;''><em>Page &amp;numpage/&amp;nbpages</em></p>',
    12
)
,
(
    nextval('om_etat_seq'),
    1,
    'proclamation_siege', 'proclamation_siege', true,
    'P', 'A4',
    NULL,
    10, 25,
    '<p style=''text-align: center;''><span style=''font-weight: bold; font-size: 18pt;''>RÉSULTATS DÉFINITIFS</span></p>
<p style=''text-align: center;''><span style=''font-weight: bold; font-size: 12pt;''>Commune : &amp;ville</span></p>
<p style=''text-align: center;''> </p>
<p style=''text-align: center;''> </p>',
    10, 15, 190, 50, '0',
    '<table border=''0'' cellspacing=''2''>
<tbody>
<tr>
<td style=''font-weight: bold; width: 20%;''><span style=''font-size: 12pt;''>SCRUTIN</span></td>
<td style=''width: 20%;''><br /><span style=''font-size: 12pt;''>[election.type_election] [election.annee]</span></td>
<td style=''font-weight: bold; width: 20%;''> </td>
<td style=''font-weight: bold; width: 25%;''><span style=''font-weight: bold; font-size: 12pt;''>CIRCONSCRIPTION</span></td>
<td style=''width: 15%;''><span style=''font-size: 12pt;''>[perimetre.circonscription.code]</span></td>
</tr>
<tr>
<td style=''font-weight: bold; width: 20%;''><span style=''font-size: 12pt;''>TOUR</span></td>
<td style=''width: 20%;''><span style=''font-size: 12pt;''>[election.tour]</span></td>
<td style=''font-weight: bold; width: 20%;''> </td>
<td style=''font-weight: bold; width: 25%;''><span style=''font-weight: bold; font-size: 12pt;''>DÉPARTEMENT</span></td>
<td style=''width: 15%;''><span style=''font-size: 12pt;''>[perimetre.departement.code]</span></td>
</tr>
<tr>
<td style=''font-weight: bold; width: 20%;''><span style=''font-size: 12pt;''>DATE</span></td>
<td style=''width: 20%;''><span style=''font-size: 12pt;''>[election.date]</span></td>
<td style=''font-weight: bold; width: 20%;''> </td>
<td style=''font-weight: bold; width: 25%;''><span style=''font-weight: bold; font-size: 12pt;''>COMMUNE</span></td>
<td style=''width: 15%;''><span style=''font-size: 12pt;''>[perimetre.commune.code]</span></td>
</tr>
</tbody>
</table>
<p> </p>
<table border=''0'' style=''width: 100%;'' cellspacing=''0'' cellpadding=''2''>
<tbody>
<tr>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''>Inscrits :</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[perimetre.resultat.inscrit]</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''> </span></th>
<th style=''width: 10%; text-align: right;''> </th>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''> </span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''> </span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''> </span></th>
</tr>
<tr>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''>Abstentions :</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[perimetre.resultat.abstention]</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[perimetre.resultat.pourcentage_abstention]</span></th>
<th style=''width: 10%; text-align: right;''> </th>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''>Blancs :</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[perimetre.resultat.blanc]</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[perimetre.resultat.pourcentage_blanc]</span></th>
</tr>
<tr>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''>Votants :</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[perimetre.resultat.votant]</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[perimetre.resultat.pourcentage_votant]</span></th>
<th style=''width: 10%; text-align: right;''> </th>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''>Nuls :</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[perimetre.resultat.nul]</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[perimetre.resultat.pourcentage_nul]</span></th>
</tr>
<tr>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''>Émargements :</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[perimetre.resultat.emargement]</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[perimetre.resultat.pourcentage_emargement]</span></th>
<th style=''width: 10%; text-align: right;''> </th>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''>Exprimés :</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[perimetre.resultat.exprime]</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[perimetre.resultat.pourcentage_exprime]</span></th>
</tr>
</tbody>
</table>
<p>[election.etat_proclamation_sieges_cm_cc]</p>
<p> </p>',
    (SELECT om_requete FROM om_requete WHERE code='election'),
    'helvetica', '0-0-0',
    10, 25, 10, 25,
    '<p style=''text-align: right;''><span style=''font-size: 7pt;''>Édition du &amp;aujourdhui à &amp;maintenant</span></p>',
    10,
    '<p style=''text-align: center; font-size: 8pt;''><em>Page &amp;numpage/&amp;nbpages</em></p>',
    12
)
,
(
    nextval('om_etat_seq'),
    1,
    'proclamation_perimetre', 'proclamation des résultats par périmètres', true,
'P', 'A4', NULL, 10, 25,
'<p style=''text-align: center;''><span style=''font-weight: bold; font-size: 18pt;''>RÉSULTATS DÉFINITIFS</span></p>
<p style=''text-align: center;''><span style=''font-weight: bold; font-size: 12pt;''>Commune : &amp;ville</span></p>
<p style=''text-align: center;''> </p>
<p style=''text-align: center;''> </p>',
10, 15, 190, 50, '0',
'<table border=''0'' cellspacing=''2''>
<tbody>
<tr>
<td style=''font-weight: bold; width: 20%;''><span style=''font-size: 12pt;''>SCRUTIN</span></td>
<td style=''width: 20%;''><br /><span style=''font-size: 12pt;''>[election.type_election] [election.annee]</span></td>
<td style=''font-weight: bold; width: 20%;''> </td>
<td style=''font-weight: bold; width: 25%;''><span style=''font-weight: bold; font-size: 12pt;''>CIRCONSCRIPTION</span></td>
<td style=''width: 15%;''><span style=''font-size: 12pt;''>[circonscription.code]</span></td>
</tr>
<tr>
<td style=''font-weight: bold; width: 20%;''><span style=''font-size: 12pt;''>TOUR</span></td>
<td style=''width: 20%;''><span style=''font-size: 12pt;''>[election.tour]</span></td>
<td style=''font-weight: bold; width: 20%;''> </td>
<td style=''font-weight: bold; width: 25%;''><span style=''font-weight: bold; font-size: 12pt;''>DÉPARTEMENT</span></td>
<td style=''width: 15%;''><span style=''font-size: 12pt;''>[departement.code]</span></td>
</tr>
<tr>
<td style=''font-weight: bold; width: 20%;''><span style=''font-size: 12pt;''>DATE</span></td>
<td style=''width: 20%;''><span style=''font-size: 12pt;''>[election.date]</span></td>
<td style=''font-weight: bold; width: 20%;''> </td>
<td style=''font-weight: bold; width: 25%;''><span style=''font-weight: bold; font-size: 12pt;''>COMMUNE</span></td>
<td style=''width: 15%;''><span style=''font-size: 12pt;''>[commune.code]</span></td>
</tr>
</tbody>
</table>
<p style=''text-align: center;''> </p>
<p style=''text-align: center;''><span style=''font-size: 12pt; font-weight: bold;''>PÉRIMÈTRE : [unite.libelle] </span></p>
<p> </p>
<table border=''0'' style=''width: 100%;'' cellspacing=''0'' cellpadding=''2''>
<tbody>
<tr>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''>Inscrits :</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[election_unite.inscrit]</span></th>
<th style=''width: 10%; text-align: right;''> </th>
<th style=''width: 10%; text-align: right;''> </th>
<th style=''width: 25%; text-align: left;''> </th>
<th style=''width: 10%; text-align: right;''> </th>
<th style=''width: 10%; text-align: right;''> </th>
</tr>
<tr>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''>Abstentions :</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[election_unite.abstention]</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[election_unite.pourcentage_abstention]</span></th>
<th style=''width: 10%; text-align: right;''> </th>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''>Blancs :</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[election_unite.blanc]</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[election_unite.pourcentage_blanc]</span></th>
</tr>
<tr>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''>Votants :</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[election_unite.votant]</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[election_unite.pourcentage_votant]</span></th>
<th style=''width: 10%; text-align: right;''> </th>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''>Nuls :</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[election_unite.nul]</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[election_unite.pourcentage_nul]</span></th>
</tr>
<tr>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''>Émargements :</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[election_unite.emargement]</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[election_unite.pourcentage_emargement]</span></th>
<th style=''width: 10%; text-align: right;''> </th>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''>Exprimés :</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[election_unite.exprime]</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[election_unite.pourcentage_exprime]</span></th>
</tr>
</tbody>
</table>
<p>[election_unite.etat_proclamation]</p>',
    (SELECT om_requete FROM om_requete WHERE code='election_unite'),
    'helvetica', '0-0-0',
    10, 25, 10, 25,
    '<p style=''text-align: right;''><span style=''font-size: 7pt;''>Édition du &amp;aujourdhui à &amp;maintenant</span></p>',
    10,
    '<p style=''text-align: center; font-size: 8pt;''><em>Page &amp;numpage/&amp;nbpages</em></p>',
    12
)
,
(
    nextval('om_etat_seq'),
    1,
    'proclamation_bureau', 'proclamation resultat par BV', true,
    'P', 'A4',
    NULL,
    10, 25,
    '<p style=''text-align: center;''><span style=''font-weight: bold; font-size: 18pt;''>RÉSULTATS DÉFINITIFS</span></p>
<p style=''text-align: center;''><span style=''font-weight: bold; font-size: 12pt;''>Commune : &amp;ville</span></p>
<p style=''text-align: center;''> </p>
<p style=''text-align: center;''> </p>',
    10, 15, 190, 50, '0',
    '<table border=''0'' cellspacing=''2''>
<tbody>
<tr>
<td style=''font-weight: bold; width: 20%;''><span style=''font-size: 12pt;''>SCRUTIN</span></td>
<td style=''width: 20%;''><br /><span style=''font-size: 12pt;''>[election.type_election] [election.annee]</span></td>
<td style=''font-weight: bold; width: 20%;''> </td>
<td style=''font-weight: bold; width: 25%;''><span style=''font-weight: bold; font-size: 12pt;''>CIRCONSCRIPTION</span></td>
<td style=''width: 15%;''><span style=''font-size: 12pt;''>[circonscription.code]</span></td>
</tr>
<tr>
<td style=''font-weight: bold; width: 20%;''><span style=''font-size: 12pt;''>TOUR</span></td>
<td style=''width: 20%;''><span style=''font-size: 12pt;''>[election.tour]</span></td>
<td style=''font-weight: bold; width: 20%;''> </td>
<td style=''font-weight: bold; width: 25%;''><span style=''font-weight: bold; font-size: 12pt;''>DÉPARTEMENT</span></td>
<td style=''width: 15%;''><span style=''font-size: 12pt;''>[departement.code]</span></td>
</tr>
<tr>
<td style=''font-weight: bold; width: 20%;''><span style=''font-size: 12pt;''>DATE</span></td>
<td style=''width: 20%;''><span style=''font-size: 12pt;''>[election.date]</span></td>
<td style=''font-weight: bold; width: 20%;''> </td>
<td style=''font-weight: bold; width: 25%;''><span style=''font-weight: bold; font-size: 12pt;''>COMMUNE</span></td>
<td style=''width: 15%;''><span style=''font-size: 12pt;''>[commune.code]</span></td>
</tr>
</tbody>
</table>
<p style=''text-align: center;''> </p>
<p style=''text-align: center;''><span style=''font-size: 12pt; font-weight: bold;''> CANTON : [canton.code] [canton.libelle]</span></p>
<p style=''text-align: center;''><span style=''font-weight: bold;''> BUREAU : [unite.code_unite] [unite.libelle]</span></p>
<p> </p>
<table border=''0'' style=''width: 100%;'' cellspacing=''0'' cellpadding=''2''>
<tbody>
<tr>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''>Inscrits :</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[election_unite.inscrit]</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''> </span></th>
<th style=''width: 10%; text-align: right;''> </th>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''> </span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''> </span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''> </span></th>
</tr>
<tr>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''>Abstentions :</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[election_unite.abstention]</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[election_unite.pourcentage_abstention]</span></th>
<th style=''width: 10%; text-align: right;''> </th>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''>Blancs :</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[election_unite.blanc]</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[election_unite.pourcentage_blanc]</span></th>
</tr>
<tr>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''>Votants :</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[election_unite.votant]</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[election_unite.pourcentage_votant]</span></th>
<th style=''width: 10%; text-align: right;''> </th>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''>Nuls :</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[election_unite.nul]</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[election_unite.pourcentage_nul]</span></th>
</tr>
<tr>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''>Émargements :</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[election_unite.emargement]</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[election_unite.pourcentage_emargement]</span></th>
<th style=''width: 10%; text-align: right;''> </th>
<th style=''width: 25%; text-align: left;''><span style=''font-size: 10pt; font-weight: bold;''>Exprimés :</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[election_unite.exprime]</span></th>
<th style=''width: 10%; text-align: right;''><span style=''font-size: 10pt;''>[election_unite.pourcentage_exprime]</span></th>
</tr>
</tbody>
</table>
<p>[election_unite.etat_proclamation]</p>',
    (SELECT om_requete FROM om_requete WHERE code='election_unite'),
    'helvetica', '0-0-0', 
    10, 25, 10, 25,
    '<p style=''text-align: right;''><span style=''font-size: 7pt;''>Édition du &amp;aujourdhui à &amp;maintenant</span></p>',
    10,
    '<p style=''text-align: center; font-size: 8pt;''><em>Page &amp;numpage/&amp;nbpages</em></p>',
    12
),
(
    nextval('om_etat_seq'),
    1,
    'prefecture', 'prefecture_par_perimetre', false,
    'L', 'A3',
    NULL,
    10, 15,
    '<p style=''text-align: center;''><span style=''font-size: 15pt; font-weight: bold;''>[election.type_election] [election.annee] - Tour [election.tour] - [election.date]</span></p>
<p style=''text-align: center;''><span style=''font-size: 14pt;''>RÉSULTATS</span></p>
<p style=''text-align: center;''> </p>',
    10, 15, 0, 0, 0,
    '<p>[election.etat_resultat_prefecture_par_perimetre]</p>',
    (SELECT om_requete FROM om_requete WHERE code='election'),
    'helvetica', '0-0-0',
    10, 15, 10, 15,
    '<table>
<tbody>
<tr>
<td style=''text-align: left;''><span style=''font-size: 11pt;''>&amp;entete_resultats</span></td>
<td>
<p style=''text-align: right;''><span style=''font-size: 11pt;''>Édition du &amp;aujourdhui à &amp;maintenant</span></p>
</td>
</tr>
</tbody>
</table>',
    10,
    '<p style=''text-align: center;''><span style=''font-size: 8pt;''><em>Page &amp;numpage/&amp;nbpages</em></span></p>',
    12
),
(
    nextval('om_etat_seq'),
    1,
    'resultats_globaux', 'resultats_globaux_opt1', false,
    'L', 'A3',
    NULL,
    10, 15,
    '<p style=''text-align: center;''><span style=''font-size: 15pt; font-weight: bold;''>[election.type_election] [election.annee] - Tour [election.tour] - [election.date]</span></p>
<p style=''text-align: center;''><span style=''font-size: 14pt;''>RÉSULTATS</span></p>
<p style=''text-align: center;''> </p>',
    10, 15, 0, 0, 0,
    '<p>[election.etat_resultat_globaux_opt1]</p>',
    (SELECT om_requete FROM om_requete WHERE code='election'),
    'helvetica', '0-0-0',
    10, 15, 10, 15,
    '<table>
<tbody>
<tr>
<td style=''text-align: left;''><span style=''font-size: 11pt;''>&amp;entete_resultats</span></td>
<td>
<p style=''text-align: right;''><span style=''font-size: 11pt;''>Édition du &amp;aujourdhui à &amp;maintenant</span></p>
</td>
</tr>
</tbody>
</table>',
    10,
    '<p style=''text-align: center;''><span style=''font-size: 8pt;''><em>Page &amp;numpage/&amp;nbpages</em></span></p>',
    12
)
;

