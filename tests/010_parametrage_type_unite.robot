*** Settings ***
Documentation     Test du parametrage du type d'unite
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Constitution du jeu de données
    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.
    Depuis la page d'accueil  admin  admin


Listing des types d'unité
    [Documentation]  Ce 'Test Case' vérifie l'intégration du listing des
    ...  enregistrements de type 'type_unite'.
    # le lien est présent dans le menu
    Depuis l'écran 'Administration & Paramétrage'
    Element Should Contain  css=#settings a.type_unite  type d'unité
    # le lien amène sur la page correcte
    Click Element  css=#settings a.type_unite
    Page Title Should Be  Administration & Paramétrage > Élection > Type D'unité
    La page ne doit pas contenir d'erreur
    # le mot clé raccourci amène sur la page correcte
    Depuis le listing des types d'unité
    Page Title Should Be  Administration & Paramétrage > Élection > Type D'unité


Ajout d'un type d'unité
    [Documentation]  Ce 'Test Case' vérifie l'intégration de l'action 'ajouter'
    ...  depuis le contexte 'type_unite'.

    Depuis le listing des types d'unité
    Click On Add Button
    Page Title Should Be  Administration & Paramétrage > Élection > Type D'unité
    La page ne doit pas contenir d'erreur

    &{type_unite} =  BuiltIn.Create Dictionary
    ...  libelle=maSuperUnite
    ...  hierarchie=1
    ...  bureau_vote=true
    Ajouter type_unite  ${type_unite}
    Element Should Contain In Subform  css=div.message  enregistrées


Blocage du type
    [Documentation]  L'objet de ce test case est de tester la verification
    ...  de la coherence de la hierarchie des unites lors d'une modification

    # creation de trois nouveau type
    &{type_superieur} =  BuiltIn.Create Dictionary
    ...  libelle=haut
    ...  hierarchie=10
    Ajouter type_unite  ${type_superieur}

    &{type_milieu} =  BuiltIn.Create Dictionary
    ...  libelle=milieu
    ...  hierarchie=5
    ${id_milieu} =  Ajouter type_unite  ${type_milieu}

    &{type_inferieur} =  BuiltIn.Create Dictionary
    ...  libelle=bas
    ...  hierarchie=3
    Ajouter type_unite  ${type_inferieur}

    # creation des unités ayant le type créé et contenant le type "inferieur"
    &{unite_sup} =  BuiltIn.Create Dictionary
    ...  libelle=haut
    ...  type_unite=haut
    ...  ordre=1
    ...  type_unite_contenu=milieu
    Ajouter unite  ${unite_sup}

    &{unite_milieu} =  BuiltIn.Create Dictionary
    ...  libelle=milieu
    ...  type_unite=milieu
    ...  ordre=2
    ...  type_unite_contenu=bas
    Ajouter unite  ${unite_milieu}

    # la hierarchie du type milieu est maintenant contenu entre haut et bas
    Depuis le contexte type_unite  ${id_milieu}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  type_unite  modifier
    # elle ne peut pas être égale à 3 ou moins
    Input Text  hierarchie  2
    # on valide le formulaire
    Click On Submit Button
    # on verifie le message d'erreur
    Error Message Should Contain  la hiérarchie ne peut pas être inférieure ou égale à 3

    # elle ne peut pas être égale à 10 ou plus
    Input Text  hierarchie  10
    # on valide le formulaire
    Click On Submit Button
    # on verifie le message d'erreur
    Error Message Should Contain  la hiérarchie ne peut pas être supérieure ou égale à 10


