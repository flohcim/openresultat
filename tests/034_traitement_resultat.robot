*** Settings ***
Documentation     Test des actions effectuant un traitement sur les résultats
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Jeu de données
    [Documentation]  L'objet de ce test case est de creer le jeu de données
    ...  qui servira ensuite à tester le fonctionnement du calcul des sièges

    Depuis la page d'accueil    admin  admin

    # Nombre de siège non défini pour tester le remplissage avec les valeurs par défauts
    # issus du paramétrage
    &{election} =  BuiltIn.Create Dictionary
    ...  libelle=Municipales 2020 1er tour
    ...  code=MUN20-1
    ...  tour=1
    ...  principal=true
    ...  type_election=Municipales
    ...  date=15/03/2020
    ...  perimetre=COMMUNE
    ...  heure_ouverture=08:00:00
    ...  heure_fermeture=18:00:00
    ...  calcul_auto_exprime=true
    ...  sieges_mep=20
    ${id_election} =  Ajouter election  ${election}
    Set Suite Variable  ${id_election}
    Element Should Contain In Subform  css=div.message  enregistrées

    # Ajout des candidats
    &{candidat} =  BuiltIn.Create Dictionary
    ...  candidat=David G.
    ...  ordre=1
    ...  prefecture=100
    ...  age_moyen=45
    ...  age_moyen_com=46
    ${id_candidat1} =  Ajouter election_candidat  ${candidat}  ${id_election}
    &{candidat} =  BuiltIn.Create Dictionary
    ...  candidat=Jean Michel B.
    ...  ordre=2
    ...  prefecture=101
    ...  age_moyen=38
    ...  age_moyen_com=41
    ${id_candidat2} =  Ajouter election_candidat  ${candidat}  ${id_election}

    # Durant l'étape de simulation, la saisie des résultats, le calcul des sièges
    # et le paramétrage de l'élection sont possible, c'est donc l'étape la plus pratique
    # pour ce test
    Depuis le contexte election  ${id_election}
    Passer à l'étape suivante  simulation  Simulation

    # Saisie des résultats
    Depuis le contexte election  ${id_election}
    Click On Tab  election_unite  unité(s)
    Click On Link   1 Salle des Mariages
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  760
    Input Text  votant  438
    Input Text  emargement  438
    Input Text  procuration  0
    Input Text  blanc  8
    Input Text  nul  1
    Input Text  candidat1  251
    Input Text  candidat2  178
    Click On Submit Button In Subform
    Click On Back Button In Subform

    Click On Link   2 Salle des fetes 1
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  742
    Input Text  votant  385
    Input Text  emargement  385
    Input Text  procuration  0
    Input Text  blanc  5
    Input Text  nul  6
    Input Text  candidat1  234
    Input Text  candidat2  140
    Click On Submit Button In Subform
    Click On Back Button In Subform

    Click On Link   3 Ecole A
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  919
    Input Text  votant  471
    Input Text  emargement  471
    Input Text  procuration  0
    Input Text  blanc  12
    Input Text  nul  8
    Input Text  candidat1  264
    Input Text  candidat2  187
    Click On Submit Button In SubForm
    Click On Back Button In Subform

    Click On Link   4 Salle des fêtes 2
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  915
    Input Text  votant  462
    Input Text  emargement  462
    Input Text  procuration  0
    Input Text  blanc  7
    Input Text  nul  10
    Input Text  candidat1  300
    Input Text  candidat2  145
    Click On Submit Button In Subform
    Click On Back Button In Subform

    Click On Link   5 Gymnase
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  761
    Input Text  votant  379
    Input Text  emargement  379
    Input Text  procuration  0
    Input Text  blanc  4
    Input Text  nul  5
    Input Text  candidat1  207
    Input Text  candidat2  163
    Click On Submit Button In Subform
    Click On Back Button In Subform

    Click On Link   6 Ecole B
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  680
    Input Text  votant  297
    Input Text  emargement  297
    Input Text  procuration  0
    Input Text  blanc  1
    Input Text  nul  3
    Input Text  candidat1  182
    Input Text  candidat2  111
    Click On Submit Button In Subform

Cas ou le calcul ne peut pas fonctionner
    [Documentation]  L'objet de ce test case est de tester les cas où le calcul échoue
    ...  pour s'assurer que ces cas sont correctement gérés. Les cas testés sont les suivants :
    ...     - calcul sans candidats
    ...     - calcul sans vote exprimés
    ...     - calcul sans résultat de candidat
    ...     - calcul sans renseigner le nombre de siège à attribuer
    ...     - calcul ou tous les candidats sont éliminés (moins de 5% des voix)

    &{election} =  BuiltIn.Create Dictionary
    ...  libelle=Municipales 2020 2eme tour
    ...  code=MUN20-2
    ...  tour=2
    ...  principal=true
    ...  type_election=Municipales
    ...  date=22/03/2020
    ...  perimetre=COMMUNE
    ...  heure_ouverture=08:00:00
    ...  heure_fermeture=18:00:00
    ...  siege=29
    ...  siege_com=1
    ${id_election_2} =  Ajouter election  ${election}

    # Durant l'étape de simulation, la saisie des résultats, le calcul des sièges
    # et le paramétrage de l'élection sont possible, c'est donc l'étape la plus pratique
    # pour ce test
    Depuis le contexte election  ${id_election_2}
    Passer à l'étape suivante  simulation  Simulation

    # Cas 1 : election sans candidat
    Depuis le contexte election  ${id_election_2}
    Click On Form Portlet Action  election  siege  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    La page ne doit pas contenir d'erreur
    Message Should Contain  Erreur : il n'y a pas de candidats pour cette élection
    Message Should Contain  Le nombre de sièges n'a pas pu être calculé !

    # Ajout des candidats
    &{candidat} =  BuiltIn.Create Dictionary
    ...  candidat=David G.
    ...  ordre=1
    ...  prefecture=100
    ...  age_moyen=45
    ...  age_moyen_com=46
    ${id_candidat1} =  Ajouter election_candidat  ${candidat}  ${id_election_2}

    &{candidat} =  BuiltIn.Create Dictionary
    ...  candidat=Jean Michel B.
    ...  ordre=2
    ...  prefecture=101
    ...  age_moyen=38
    ...  age_moyen_com=41
    ${id_candidat2} =  Ajouter election_candidat  ${candidat}  ${id_election_2}

    # Cas 2 : election sans vote exprimé
    Depuis le contexte election_unite  ${id_election_2}  1 Salle des Mariages
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  760
    Input Text  votant  438
    Input Text  emargement  438
    Input Text  procuration  0
    Input Text  blanc  8
    Input Text  nul  1
    Input Text  candidat1  251
    Input Text  candidat2  178
    Click On Submit Button In Subform

    Depuis le contexte election  ${id_election_2}
    Click On Form Portlet Action  election  siege  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    La page ne doit pas contenir d'erreur
    Message Should Contain  Erreur : il n'y a pas de vote exprimé
    Message Should Contain  Le nombre de sièges n'a pas pu être calculé !

    # Cas 3 : calcul avec résultat manquant pour un des candidats
    Depuis le contexte election_unite  ${id_election_2}  1 Salle des Mariages
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  exprime  430
    Input Text  candidat1  400
    Input Text  candidat2  30
    Click On Submit Button In Subform

    &{candidat} =  BuiltIn.Create Dictionary
    ...  candidat=Hervé SCHIAVETTI
    ...  ordre=3
    ...  prefecture=101
    ...  age_moyen=38
    ...  age_moyen_com=41
    ${id_candidat2} =  Ajouter election_candidat  ${candidat}  ${id_election_2}

    Depuis le contexte election  ${id_election_2}
    Click On Form Portlet Action  election  siege  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    La page ne doit pas contenir d'erreur
    Message Should Contain  Erreur : aucun résultat enregistré pour la liste Pour Arles
    Message Should Contain  Le nombre de sièges n'a pas pu être calculé !

    # Cas 4 : calcul sans sièges à attribuer et avec un nombre de siège nul
    &{election} =  BuiltIn.Create Dictionary
    ...  sieges=${EMPTY}
    ...  sieges_com=0
    Modifier election  ${id_election_2}  ${election}

    Depuis le contexte election  ${id_election_2}
    Click On Form Portlet Action  election  siege  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    La page ne doit pas contenir d'erreur
    # sert à vérifier que le message d'erreur est bien présent dans les deux cas :
    # 0 siège à répartir et nombre de siège non renseigné
    Message Should Contain  C O N S E I L M U N I C I P A L\n\nErreur : le nombre de siège à répartir n'est pas renseigné ou nul\nLe nombre de sièges n'a pas pu être calculé !
    Message Should Contain  C O N S E I L C O M M U N A U T A I R E\n\nErreur : le nombre de siège à répartir n'est pas renseigné ou nul\nLe nombre de sièges n'a pas pu être calculé !
    Message Should Contain  C O N S E I L M E T R O P O L I T A I N\n\nErreur : le nombre de siège à répartir n'est pas renseigné ou nul\nLe nombre de sièges n'a pas pu être calculé !

    # Cas 5 : calcul avec tous les candidats ayant moins de 5% des voix
    &{election} =  BuiltIn.Create Dictionary
    ...  sieges=29
    ...  sieges_com=1
    ...  sieges_mep=17
    Modifier election  ${id_election_2}  ${election}

    Depuis le contexte election_unite  ${id_election_2}  1 Salle des Mariages
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  exprime  430
    Input Text  candidat1  5
    Input Text  candidat2  5
    Input Text  candidat3  5
    Click On Submit Button In Subform

    Depuis le contexte election  ${id_election_2}
    Click On Form Portlet Action  election  siege  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    La page ne doit pas contenir d'erreur
    Message Should Contain  Erreur : Toutes les listes ont été éliminées et ont donc moins de 5% des voix
    Message Should Contain  Le nombre de sièges n'a pas pu être calculé !

Calcul du nombre de sièges
    [Documentation]  L'objet de ce test case est de vérifier le fonctionnement
    ...  de la fonction de calcul de siège

    Depuis le contexte election  ${id_election}
    # Calcul des sièges
    Click On Form Portlet Action  election  siege  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    # Vérification des resultats de la liste municipale
    Message Should Contain  -> la Liste "C'est pas si mal obtient 24 siège(s) pour 1438 voix
    Message Should Contain  -> la Liste "Liste 2 obtient 5 siège(s) pour 924 voix
    # Vérification des resultats de la liste commuautaire
    Message Should Contain  -> la Liste "C'est pas si mal obtient 1 siège(s) pour 1438 voix
    Message Should Contain  -> la Liste "Liste 2 obtient 0 siège(s) pour 924 voix
    # Vérification des resultats de la liste metropolitaine
    Message Should Contain  -> la Liste "C'est pas si mal obtient 16 siège(s) pour 1438 voix
    Message Should Contain  -> la Liste "Liste 2 obtient 4 siège(s) pour 924 voix

Calcul du nombre de sièges en cas d'égalité
    [Documentation]  L'objet de ce test case est de vérifier le fonctionnement
    ...  de la fonction de calcul de siège

    # Modif des résultats pour avoir une égalité
    # Saisie des résultats
    Depuis le contexte election  ${id_election}
    Click On Tab  election_unite  unité(s)
    Click On Link   1 Salle des Mariages
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  760
    Input Text  votant  438
    Input Text  emargement  438
    Input Text  procuration  0
    Input Text  blanc  8
    Input Text  nul  1
    Input Text  candidat1  214
    Input Text  candidat2  215
    Click On Submit Button In Subform
    Click On Back Button In Subform

    Click On Link   2 Salle des fetes 1
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  742
    Input Text  votant  385
    Input Text  emargement  385
    Input Text  procuration  0
    Input Text  blanc  15
    Input Text  nul  16
    Input Text  candidat1  177
    Input Text  candidat2  177
    Click On Submit Button In Subform
    Click On Back Button In Subform

    Click On Link   3 Ecole A
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  919
    Input Text  votant  471
    Input Text  emargement  471
    Input Text  procuration  0
    Input Text  blanc  12
    Input Text  nul  8
    Input Text  candidat1  226
    Input Text  candidat2  225
    Click On Submit Button In SubForm
    Click On Back Button In Subform

    Click On Link   4 Salle des fêtes 2
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  915
    Input Text  votant  462
    Input Text  emargement  462
    Input Text  procuration  0
    Input Text  blanc  7
    Input Text  nul  10
    Input Text  candidat1  222
    Input Text  candidat2  223
    Click On Submit Button In Subform
    Click On Back Button In Subform

    Click On Link   5 Gymnase
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  761
    Input Text  votant  379
    Input Text  emargement  379
    Input Text  procuration  0
    Input Text  blanc  4
    Input Text  nul  5
    Input Text  candidat1  185
    Input Text  candidat2  185
    Click On Submit Button In Subform
    Click On Back Button In Subform

    Click On Link   6 Ecole B
    Click On SubForm Portlet Action  election_unite  modifier
    # Remplissage du formulaire
    Input Text  inscrit  680
    Input Text  votant  297
    Input Text  emargement  297
    Input Text  procuration  0
    Input Text  blanc  1
    Input Text  nul  3
    Input Text  candidat1  147
    Input Text  candidat2  146
    Click On Submit Button In Subform

    # Calcul siege elu
    Depuis le contexte election  ${id_election}
    Click On Form Portlet Action  election  siege  modale
    Cliquer sur le bouton de la fenêtre modale  Confirmer
    # Vérification des resultats de la liste municipale
    Message Should Contain  Il y a égalite de moyenne et de suffrage
    Message Should Contain  LE SIEGE DOIT ETRE ATTRIBUE AU PLUS AGEE DES CANDIDATS SUCEPTIBLE D'ETRE ELU
    Message Should Contain  -> la Liste "C'est pas si mal obtient 22 siège(s) pour 1171 voix
    Message Should Contain  -> la Liste "Liste 2 obtient 7 siège(s) pour 1171 voix
