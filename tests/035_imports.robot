*** Settings ***
Documentation     Test des imports de l'application
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Import des inscrits depuis un fichier csv d'Elire
    [Documentation]  L'objet de ce test case est de tester que l'import
    ...  des inscrits à l'aide d'un fichier csv, issus d'Elire,
    ...  récupère et enregistre correctement les données dans la base

    Depuis la page d'accueil    admin  admin

    Depuis le formulaire d'import des unités
    Add File    fic1    ListeBureauVoteElire_T035.csv
    Select From List By Label   separateur   , (virgule)
    Click On Submit Button In Import CSV

    Valid Message Should Contain  5 ligne(s) dans le fichier dont :
    Valid Message Should Contain  - 1 ligne(s) d'entête
    Valid Message Should Contain  - 4 ligne(s) importée(s)

    # Création du périmètre de l'élection et lien avec les unités
    &{unite} =  BuiltIn.Create Dictionary
    ...  libelle=Commune Elire
    ...  type_unite=Commune
    ...  type_unite_contenu=Bureau de vote
    ...  perimetre=true
    ...  ordre=99
    ${idUnite} =  Ajouter unite  ${unite}

    Ajouter lien_unite  Commune Elire  008 T035 Mairie Import inscrits
    Ajouter lien_unite  Commune Elire  009 T035 Ecole Import inscrits
    Ajouter lien_unite  Commune Elire  010 T035 Gymnase Import inscrits
    Ajouter lien_unite  Commune Elire  011 T035 Salle des Fêtes Import inscrits

    # Création de l'élection et import des inscrits
    &{election} =  BuiltIn.Create Dictionary
    ...  libelle=Election test import ELIRE
    ...  code=lyoko
    ...  principal=true
    ...  type_election=Municipales
    ...  date=09/06/1995
    ...  perimetre=Commune Elire
    ...  heure_ouverture=08:00:00
    ...  heure_fermeture=11:00:00
    ${idElection} =  Ajouter election  ${election}

    # import de la liste principale et vérification
    Depuis le contexte election  ${idElection}
    Click On Form Portlet Action  election  import_inscrits
    La page ne doit pas contenir d'erreur
    Add File    fic1    ListeBureauVoteElire_T035.csv
    Select From List By Label   separateur   , (virgule)
    Select From List By Label   inscrit   LP
    Click On Submit Button In Import CSV
    La page ne doit pas contenir d'erreur

    Valid Message Should Contain  5 ligne(s) dans le fichier dont :
    Valid Message Should Contain  - 1 ligne(s) d'entête
    Valid Message Should Contain  - 4 ligne(s) importée(s)

    Click On Back Button In Import CSV
    Click On Tab  election_unite  unité(s)

    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  2  3  08 T035 Mairie Import inscrits
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  2  5  127
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  3  3  09 T035 Ecole Import inscrits
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  3  5  184
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  4  3  10 T035 Gymnase Import inscrits
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  4  5  143
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  5  3  11 T035 Salle des Fêtes Import inscrits
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  5  5  ${EMPTY}
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  6  3  Commune Elire
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  6  5  454

    # import de la liste principale + liste complémentaire municipale et vérification
    Depuis le contexte election  ${idElection}
    Click On Form Portlet Action  election  import_inscrits
    La page ne doit pas contenir d'erreur
    Add File    fic1    ListeBureauVoteElire_T035.csv
    Select From List By Label   separateur   , (virgule)
    Select From List By Label   inscrit   LP + LCM
    Click On Submit Button In Import CSV

    Valid Message Should Contain  5 ligne(s) dans le fichier dont :
    Valid Message Should Contain  - 1 ligne(s) d'entête
    Valid Message Should Contain  - 4 ligne(s) importée(s)

    Click On Back Button In Import CSV
    Click On Tab  election_unite  unité(s)

    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  2  3  08 T035 Mairie Import inscrits
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  2  5  133
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  3  3  09 T035 Ecole Import inscrits
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  3  5  191
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  4  3  10 T035 Gymnase Import inscrits
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  4  5  148
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  5  3  11 T035 Salle des Fêtes Import inscrits
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  5  5  ${EMPTY}
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  6  3  Commune Elire
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  6  5  472

    # import de la liste principale + liste complémentaire européenne et vérification
    Depuis le contexte election  ${idElection}
    Click On Form Portlet Action  election  import_inscrits
    La page ne doit pas contenir d'erreur
    Add File    fic1    ListeBureauVoteElire_T035.csv
    Select From List By Label   separateur   , (virgule)
    Select From List By Label   inscrit   LP + LCE
    Click On Submit Button In Import CSV

    Valid Message Should Contain  5 ligne(s) dans le fichier dont :
    Valid Message Should Contain  - 1 ligne(s) d'entête
    Valid Message Should Contain  - 4 ligne(s) importée(s)

    Click On Back Button In Import CSV
    Click On Tab  election_unite  unité(s)

    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  2  3  08 T035 Mairie Import inscrits
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  2  5  132
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  3  3  09 T035 Ecole Import inscrits
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  3  5  190
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  4  3  10 T035 Gymnase Import inscrits
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  4  5  147
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  5  3  11 T035 Salle des Fêtes Import inscrits
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  5  5  ${EMPTY}
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  6  3  Commune Elire
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  6  5  469

    #
    ## Test l'import des inscrits lorsque l'id n'est pas renseigné dans le csv
    ## et donc que l'import utilise les codes unités
    #

    # Création de l'élection et import des inscrits
    &{election} =  BuiltIn.Create Dictionary
    ...  libelle=Election test import sans id
    ...  code=lyoko
    ...  principal=true
    ...  type_election=Municipales
    ...  date=09/06/1995
    ...  perimetre=Commune Elire
    ...  heure_ouverture=08:00:00
    ...  heure_fermeture=11:00:00
    ${idElection} =  Ajouter election  ${election}

    # import de la liste principale et vérification
    Depuis le contexte election  ${idElection}
    Click On Form Portlet Action  election  import_inscrits
    La page ne doit pas contenir d'erreur
    Add File    fic1    import_inscrit_sans_id.csv
    Select From List By Label   separateur   , (virgule)
    Select From List By Label   inscrit   LP
    Click On Submit Button In Import CSV
    La page ne doit pas contenir d'erreur

    Valid Message Should Contain  5 ligne(s) dans le fichier dont :
    Valid Message Should Contain  - 1 ligne(s) d'entête
    Valid Message Should Contain  - 4 ligne(s) importée(s)

    Click On Back Button In Import CSV
    Click On Tab  election_unite  unité(s)

    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  2  3  08 T035 Mairie Import inscrits
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  2  5  127
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  3  3  09 T035 Ecole Import inscrits
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  3  5  184
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  4  3  10 T035 Gymnase Import inscrits
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  4  5  143
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  5  3  11 T035 Salle des Fêtes Import inscrits
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  5  5  ${EMPTY}
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  6  3  Commune Elire
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  6  5  454

    # import de la liste principale + liste complémentaire municipale et vérification
    Depuis le contexte election  ${idElection}
    Click On Form Portlet Action  election  import_inscrits
    La page ne doit pas contenir d'erreur
    Add File    fic1    import_inscrit_sans_id.csv
    Select From List By Label   separateur   , (virgule)
    Select From List By Label   inscrit   LP + LCM
    Click On Submit Button In Import CSV

    Valid Message Should Contain  5 ligne(s) dans le fichier dont :
    Valid Message Should Contain  - 1 ligne(s) d'entête
    Valid Message Should Contain  - 4 ligne(s) importée(s)

    Click On Back Button In Import CSV
    Click On Tab  election_unite  unité(s)

    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  2  3  08 T035 Mairie Import inscrits
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  2  5  133
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  3  3  09 T035 Ecole Import inscrits
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  3  5  191
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  4  3  10 T035 Gymnase Import inscrits
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  4  5  148
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  5  3  11 T035 Salle des Fêtes Import inscrits
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  5  5  ${EMPTY}
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  6  3  Commune Elire
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  6  5  472

    # import de la liste principale + liste complémentaire européenne et vérification
    Depuis le contexte election  ${idElection}
    Click On Form Portlet Action  election  import_inscrits
    La page ne doit pas contenir d'erreur
    Add File    fic1    import_inscrit_sans_id.csv
    Select From List By Label   separateur   , (virgule)
    Select From List By Label   inscrit   LP + LCE
    Click On Submit Button In Import CSV

    Valid Message Should Contain  5 ligne(s) dans le fichier dont :
    Valid Message Should Contain  - 1 ligne(s) d'entête
    Valid Message Should Contain  - 4 ligne(s) importée(s)

    Click On Back Button In Import CSV
    Click On Tab  election_unite  unité(s)

    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  2  3  08 T035 Mairie Import inscrits
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  2  5  132
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  3  3  09 T035 Ecole Import inscrits
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  3  5  190
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  4  3  10 T035 Gymnase Import inscrits
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  4  5  147
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  5  3  11 T035 Salle des Fêtes Import inscrits
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  5  5  ${EMPTY}
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  6  3  Commune Elire
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  6  5  469

Import des inscrits depuis un fichier csv d'openElec
    [Documentation]  L'objet de ce test case est de tester que l'import
    ...  des inscrits à l'aide d'un fichier csv, issus d'openElec,
    ...  récupère et enregistre correctement les données dans la base

    Depuis le formulaire d'import des unités
    Add File    fic1    ListeBureauVoteOpenelec_T035.csv
    Select From List By Label   separateur   ; (point-virgule)
    Click On Submit Button In Import CSV

    Valid Message Should Contain  8 ligne(s) dans le fichier dont :
    Valid Message Should Contain  - 1 ligne(s) d'entête
    Valid Message Should Contain  - 6 ligne(s) importée(s)

    # Création du périmètre de l'élection et lien avec les unités
    &{unite} =  BuiltIn.Create Dictionary
    ...  libelle=Commune openElec
    ...  type_unite=Commune
    ...  type_unite_contenu=Bureau de vote
    ...  perimetre=true
    ...  ordre=200
    ${idUnite} =  Ajouter unite  ${unite}

    Ajouter lien_unite  Commune openElec  101 T035 SALLE DES MARIAGES
    Ajouter lien_unite  Commune openElec  102 T035 SALLE DES FETES JEAN MONNET
    Ajouter lien_unite  Commune openElec  103 T035 ECOLE DU CANET
    Ajouter lien_unite  Commune openElec  104 T035 SALLE DES FETES JEAN MONNET
    Ajouter lien_unite  Commune openElec  105 T035 LE GYMNASE
    Ajouter lien_unite  Commune openElec  106 T035 ECOLE DU VILLAGE

    # Création de l'élection
    &{election} =  BuiltIn.Create Dictionary
    ...  libelle=Election test import openElec
    ...  code=lyoko
    ...  principal=true
    ...  type_election=Municipales
    ...  date=09/06/1995
    ...  perimetre=Commune openElec
    ...  heure_ouverture=08:00:00
    ...  heure_fermeture=11:00:00
    ${idElection} =  Ajouter election  ${election}

    # import de la liste principale et vérification
    Depuis le contexte election  ${idElection}
    Click On Form Portlet Action  election  import_inscrits
    La page ne doit pas contenir d'erreur
    Add File    fic1    inscritOpenElec_T035.csv
    Select From List By Label   separateur   ; (point-virgule)
    Select From List By Label   inscrit   LP
    Click On Submit Button In Import CSV
    La page ne doit pas contenir d'erreur

    Valid Message Should Contain  8 ligne(s) dans le fichier dont :
    Valid Message Should Contain  - 1 ligne(s) d'entête
    Valid Message Should Contain  - 6 ligne(s) importée(s)

    Click On Back Button In Import CSV
    Click On Tab  election_unite  unité(s)

    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  2  3  101 T035 SALLE DES MARIAGES
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  2  5  767
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  3  3  102 T035 SALLE DES FETES JEAN MONNET
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  3  5  739
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  4  3  103 T035 ECOLE DU CANET
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  4  5  975
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  5  3  104 T035 SALLE DES FETES JEAN MONNET
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  5  5  917
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  6  3  105 T035 LE GYMNASE
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  6  5  772
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  7  3  106 T035 ECOLE DU VILLAGE
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  7  5  671
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  8  3  Commune openElec
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  8  5  4841

    # import de la liste principale + liste complémentaire municipale et vérification
    Depuis le contexte election  ${idElection}
    Click On Form Portlet Action  election  import_inscrits
    La page ne doit pas contenir d'erreur
    Add File    fic1    inscritOpenElec_T035.csv
    Select From List By Label   separateur   ; (point-virgule)
    Select From List By Label   inscrit   LP + LCM
    Click On Submit Button In Import CSV

    Valid Message Should Contain  8 ligne(s) dans le fichier dont :
    Valid Message Should Contain  - 1 ligne(s) d'entête
    Valid Message Should Contain  - 6 ligne(s) importée(s)


    Click On Back Button In Import CSV
    Click On Tab  election_unite  unité(s)

    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  2  3  101 T035 SALLE DES MARIAGES
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  2  5  770
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  3  3  102 T035 SALLE DES FETES JEAN MONNET
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  3  5  742
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  4  3  103 T035 ECOLE DU CANET
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  4  5  976
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  5  3  104 T035 SALLE DES FETES JEAN MONNET
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  5  5  919
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  6  3  105 T035 LE GYMNASE
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  6  5  772
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  7  3  106 T035 ECOLE DU VILLAGE
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  7  5  685
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  8  3  Commune openElec
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  8  5  4864

    # import de la liste principale + liste complémentaire européenne et vérification
    Depuis le contexte election  ${idElection}
    Click On Form Portlet Action  election  import_inscrits
    La page ne doit pas contenir d'erreur
    Add File    fic1    inscritOpenElec_T035.csv
    Select From List By Label   separateur   ; (point-virgule)
    Select From List By Label   inscrit   LP + LCE
    Click On Submit Button In Import CSV

    Valid Message Should Contain  8 ligne(s) dans le fichier dont :
    Valid Message Should Contain  - 1 ligne(s) d'entête
    Valid Message Should Contain  - 6 ligne(s) importée(s)

    Click On Back Button In Import CSV
    Click On Tab  election_unite  unité(s)

    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  2  3  101 T035 SALLE DES MARIAGES
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  2  5  770
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  3  3  102 T035 SALLE DES FETES JEAN MONNET
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  3  5  742
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  4  3  103 T035 ECOLE DU CANET
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  4  5  976
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  5  3  104 T035 SALLE DES FETES JEAN MONNET
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  5  5  919
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  6  3  105 T035 LE GYMNASE
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  6  5  772
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  7  3  106 T035 ECOLE DU VILLAGE
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  7  5  685
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  8  3  Commune openElec
    Table Cell Should Contain  css=div#sousform-election_unite table.tab-tab  8  5  4864
