<?php
/**
 * Ce script permet d'interfacer l'application.
 *
 * @package openresultat
 * @version SVN : $Id$
 */

require_once "../obj/openresultat.class.php";
$flag = filter_input(INPUT_GET, 'module');
if (in_array($flag, array("login", "logout", )) === false) {
    $flag = "nohtml";
}
$f = new openresultat($flag);
$f->view_main();
