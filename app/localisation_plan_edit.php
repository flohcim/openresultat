<?php
/**
 * Ce script permet de ...
 *
 * @package openresultat
 * @version SVN : $Id$
 */

require_once "../obj/openresultat.class.php";
$f = new openresultat("nohtml");

/**
 * Initialisation des parametres
 */
//
(isset($_GET['plan']) ? $plan = $_GET['plan'] : $plan = "");
//
(isset($_GET['positionx']) ? $positionx = $_GET['positionx'] : $positionx = "");
//
(isset($_GET['positiony']) ? $positiony = $_GET['positiony'] : $positiony = ""); 
//
(isset($_GET['x']) ? $x = $_GET['x'] : $x = 0);
//
(isset($_GET['y']) ? $y = $_GET['y'] : $y = 0);
//
(isset($_GET['form']) ? $form = $_GET['form'] : $form = 'f2');
$form = 'f2';
/**
 *
 */
//
require_once "../obj/plan.class.php";
$plan = new plan($plan, $f->db, NULL);
//
$imageplan = $f->storage->getPath($plan->getVal("image_plan"));

/**
 * Verification des parametres
 */
if ($plan == ""
    or $positionx == ""
    or $positiony == ""
    or !file_exists($imageplan)) {
    //
    if ($f->isAjaxRequest() == false) {
        $f->setFlag(NULL);
        $f->display();
    }
    $class = "error";
    $message = __("L'objet est invalide.");
    $f->displayMessage($class, $message);
    die();
}


/**
 * Affichage de la structure HTML
 */
if ($f->isAjaxRequest()) {
    header("Content-type: text/html; charset=".HTTPCHARSET."");
} else {
    //
    $f->setFlag("htmlonly");
    $f->display();
}
//
$f->displayStartContent();
//
$f->setTitle(__("Localisation"));
$f->displayTitle();

/**
 *
 */
//
$f->displayLinkJsCloseWindow();

/**
 *
 */
//
$size = getimagesize($imageplan);
//
echo "<div";
echo " id=\"localisation-wrapper\"";
echo " style=\"";
//
echo "border: 1px solid #cdcdcd; ";
echo "position:relative; ";
echo "width:".$size[0]."px; ";
echo "height:".$size[1]."px; ";
echo "background-image: url('".OM_ROUTE_FORM."&snippet=file&obj=plan&champ=image_plan&id=".$plan->getVal('plan')."'); ";
//
echo "\"";
echo ">\n";
//
echo "<img";
echo " id=\"plan-draggable\"";
echo " class=\"".$form.";".$positionx.";".$positiony.";\"";
echo " src=\"../app/img/img_unite_arrivee.gif\"";
echo " style=\"position:absolute; margin:0; padding:0; left:".($x)."px; top:".($y)."px; cursor: pointer;\"";
echo " />";
//
echo "\n</div>\n";
//
echo "<div class=\"visualClear\"><!-- --></div>\n";

/**
 *
 */
//
$f->displayLinkJsCloseWindow();

/**
 *
 */
//
$f->displayEndContent();
