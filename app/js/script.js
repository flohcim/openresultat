/**
 * Script JS spécifique à l'applicatif, ce script est destiné à être
 * appelé en dernier dans la pile des fichiers JS.
 *
 * @package openresultat
 * @version SVN : $Id: script.js 2202 2013-03-28 17:30:48Z fmichon $
 */

function handle__widget_form__select_multiple_permissions() {
    //
	$('.field-type-select_multiple select#permissions"').DualListBox({
		'json': false,
		'title': "éléments",
		'warning': "Cette action va déplacer beaucoup d'éléments et peut ralentir votre navigateur."
	});
}

function app_initialize_content(tinymce_load) {
    //
    handle__widget_form__select_multiple_permissions();
	adresse_postale(); //
	bind_form_field_hexa();
	// Méthode permettant dans les formulaires de saisie des résultats de l'élection et de ces
	// centaine de surligné le champs de saisie sélectionné.
	highlight_focused_result_field();
}

/**
 * Cette méthode permet si on est dans le contexte du formulaire de saisie
 * des résultats par unité d'une élection ou d'une centaine de changer la
 * classe du label du champs sélection (uniquement les champs de saisie des
 * résultat).
 * En changeant la classe cela permet d'afficher le label du champ saisi en bleu.
 * De la même manière si le champs est déselectionné le label ne sera plus surligné.
 */
function highlight_focused_result_field() {
	// Vérifie si l'on se trouve bien dans le contexte des formulaire de saisie des résultats
	if($('#fieldset-sousform-election_unite-resultats .field div.form-content input').length > 0
		|| $('#fieldset-sousform-election_unite_centaine-resultats  .field div.form-content input').length > 0) {

		$(document).ready(function() {
			// Surligne le label du champs sélectionné en lui ajoutant la classe highlight-blue
			// Pour le formulaire de saisie des résultats de l'unité
			$('#fieldset-sousform-election_unite-resultats .field div.form-content input').focus(function(){
				$(this).parents('.field').find('div.form-libelle label').addClass('highlight-blue');
			});
			// Pour le formulaire de saisie des résultats de l'unité pour une centaine
			$('#fieldset-sousform-election_unite_centaine-resultats .field div.form-content input').focus(function(){
				$(this).parents('.field').find('div.form-libelle label').addClass('highlight-blue');
			});
			// Lorsque l'on déselectionne le champ le label n'est plus surligné car on lui supprime la classe highlight-blue
			// Pour le formulaire de saisie des résultats de l'unité
			$('#fieldset-sousform-election_unite-resultats .field div.form-content input').focusout(function(){
				$(this).parents('.field').find('div.form-libelle label').removeClass('highlight-blue');	
			});
			// Pour le formulaire de saisie des résultats de l'unité pour une centaine
			$('#fieldset-sousform-election_unite_centaine-resultats .field div.form-content input').focusout(function(){
				$(this).parents('.field').find('div.form-libelle label').removeClass('highlight-blue');	
			});
		});
	}
}

// adresse postale via la BAN 
function adresse_postale() {
// la fonction necessite cp + ville + adresse + 
// latitude arles -> l'ordre des donnees pour adresse est fonction latitude et longitude
// a mettre dans les om_parametre
var lat  =$("input[name='lat']").val();
var lon  = $("input[name='lon']").val();

$("#cp").autocomplete({
	source: function (request, response) {
		$.ajax({
			url: "https://api-adresse.data.gouv.fr/search/?postcode="+$("input[name='cp']").val(),
			data: { q: request.term },
			dataType: "json",
			success: function (data) {
				var postcodes = [];
				response($.map(data.features, function (item) {
					// Ici on est obligé d'ajouter les CP dans un array pour ne pas avoir plusieurs fois le même
					if ($.inArray(item.properties.postcode, postcodes) == -1) {
						postcodes.push(item.properties.postcode);
						return { label: item.properties.postcode + " - " + item.properties.city, 
								 city: item.properties.city,
								 value: item.properties.postcode
						};
					}
				}));
			}
		});
	},
	// On remplit aussi la ville
	select: function(event, ui) {
		$('#ville').val(ui.item.city);
	}
});
$("#adresse1").autocomplete({
	source: function (request, response) {
		$.ajax({
			url: "https://api-adresse.data.gouv.fr/search/?postcode="+$("input[name='cp']").val()+"&lat="+lat+"&lon="+lon,
			data: { q: request.term },
			dataType: "json",
			success: function (data) {
				response($.map(data.features, function (item) {
					return { label: item.properties.name + " - " + item.properties.city,
						     city: item.properties.city,
						     postcode: item.properties.postcode, 
						     coordinates : item.geometry.coordinates,
						     value: item.properties.name};
				}));
			}
		});
	},
	// On remplit ville cp coordonnees
	//var temp = val(ui.item.coordinates).split(',');
	//alert(temp[1]);
	select: function(event, ui) {
		$('#ville').val(ui.item.city);
		$('#cp').val(ui.item.postcode);
		$('#coordinates').val(ui.item.coordinates);
	}
	
});
$("#ville").autocomplete({
	source: function (request, response) {
		$.ajax({
			url: "https://api-adresse.data.gouv.fr/search/?city="+$("input[name='ville']").val(),
			data: { q: request.term },
			dataType: "json",
			success: function (data) {
				var cities = [];
				response($.map(data.features, function (item) {
					// Ici on est obligé d'ajouter les villes dans un array pour ne pas avoir plusieurs fois la même
					if ($.inArray(item.properties.postcode, cities) == -1) {
						cities.push(item.properties.postcode);
						return { label: item.properties.postcode + " - " + item.properties.city, 
								 postcode: item.properties.postcode,
								 value: item.properties.city
						};
					}
				}));
			}
		});
	},
	// On remplit aussi le CP
	select: function(event, ui) {
		$('#cp').val(ui.item.postcode);
	}
});	
}

/**
* Cette fonction permet d'afficher les options d'un select par rapport
* à un autre champ
* 
* @param  int id               Données (identifiant) du champ visé
* @param  string tableName     Nom de la table
* @param  string linkedField   Champ visé
* @param  string formCible     Formulaire visé
*/
function filterSelect(id, tableName, linkedField, formCible) {
    //lien vers script php
    link = "../app/index.php?module=form&snippet=filterselect&idx=" + id + "&tableName=" + tableName +
            "&linkedField=" + linkedField + "&formCible=" + formCible;

    $.ajax({
        type: "GET",
        url: link,
        cache: false,
        dataType: "json",
        success: function(res) {
            $('#'+tableName).empty();

            for ( j=0 ; j < res[0].length ; j++ ){

                $('#'+tableName).append(
                    '<option value="'+res[0][j]+'" >'+res[1][j]+'</option>'
                );
                
            }

		},
		error: function(jqXHR, textStatus, errorThrown) {
            $('#'+tableName).empty();
		},
        async: false
    });
}

/**
* Cette fonction permet d'afficher le champ code_unite comme obligatoire
* selon le type d'unité
*
* @param  integer  champ    Champ visé
*/
function obligatoireSelonType(typeUnite) {
    // lien script php
	var link = "../app/index.php?module=form&obj=unite&action=10&type_unite=" + typeUnite

    $.ajax({
        type: "GET",
        url: link,
        cache: false,
        dataType: "json",
        success: function(res) {
			if ("libelle" in res) {
				$('#lib-code_unite').text(res['libelle']);
			}
		},
        async: false
    });
}

// Sur l'événement "change" du champ "dossier_coordination_type"
function recuperer_perimetre(selected) {
    // lien script php
    link = "../app/index.php?module=form&obj=unite&action=7&idx=0";
    //
    $.ajax({
        type: "POST",
        url: link,
        cache: false,
        data: "selected="+selected+"&",
        dataType: "json",
        success: function(json){
            // Change la valeur du champ "perimetre"
            $("#perimetre").prop('checked', false);
            $("#perimetre").prop('value', '');
            //
            if (json.type_unite_contenu !== "") {
                $("#perimetre").prop('checked', true);
                $("#perimetre").prop('value', 'Oui');
            }
        }
    });
}

/**
 * Calcul et met à jour la valeur des champs correspondant aux périmètres.
 * Récupère un tableau json de cette forme :
 * 		[indice_champ_parent] = [indice_champ_enfant1, ..., indice_champ_enfant_n]
 * A partir des indices fournis dans ce tableau, récupère les valeurs des champs
 * enfants et les additionne si ce sont bien des valeurs numériques. Donne ensuite
 * cette valeur au champ parent. Cette action est réalisé pour chaque champs parents
 * du tableau json.
 * 
 * @param integer election identifiant de l'élection en cours qui permet de récupérer
 * les périmètres de l'élection pour remplir le json
 */
function update_participation_perimetre(election) {
    // lien script php
	var link = "../app/index.php?module=form&obj=participation_election&action=20&election="
		+ election

	$.ajax({
		type: "GET",
		url: link,
		cache: false,
        dataType: "json",
		success: function(res){
			//Parcours le tableau json et donc chaque indice des unites_parents
			for (parent in res) {
				//Si un parent n'a pas d'enfant alors on passe au suivant
				if (res[parent]['enfants'].length != 0){
					newValue = 0;
					//Pour chaque enfant de l'unité parent, récupère la valeur du champ en
					//utilisant son indice et si, c'est un nombre, l'ajoute au total des participations
					for (enfant in res[parent]['enfants']) {
						childValue = parseInt($('#unite'+ res[parent]['enfants'][enfant]).attr('value'))
						if ($.isNumeric(childValue)) {
							newValue += childValue;
						}
					}
					//Attribution de la valeur calculé à l'unité parent
					$('#unite'+ res[parent]['parent']).attr('value', newValue);
					//Affichage de l'unité calculé pour que l'utilisateur puisse voir l'évolution
					$('#unite'+ res[parent]['parent'] + ' + p').text(newValue);
				}
			}
		},
		async: false
	});
}


/**
 * A partir des valeurs du modèle récupéré en json, rempli le formulaire
 * de l'animation avec les mêmes valeurs.
 *  
 * @param integer modele identifiant de l'animation servant de modele
 */
function applique_parametrage_modele(modele) {
    // lien script php
	var link = "../app/index.php?module=form&obj=animation&action=10&modele="
		+ modele

	$.ajax({
		type: "GET",
		url: link,
		cache: false,
        dataType: "json",
		success: function(res){
			for (parametre in res) {
				//Pour éviter que le selecteur du champ du logo ne récupére
				//la position du logo de l'application par erreur, il est
				//nécessaire de rendre le selecteur plus précis.
				//Ainsi '#content' permet de ne sélectionner que
				//les élements se trouvant dans le formulaire
				var position = $('#content #'+ parametre);
				//Une fois la position obtenue, il faut savoir de quel type
				//champs il s'agit (select, input, ...) pour pouvoir le modifier
				if (position.prop('tagName') == 'SELECT') {
					//Si c'est un select, il faut déselectionner l'option sélectionnée
					//et sélectionner l'option du paramétrage
					var posOptionSelectionnee = $('#content #'+ parametre + ' option[selected]');
					posOptionSelectionnee.removeAttr('selected');
					var posOptionVoulue = $('#content #'+ parametre + ' option[value=' + res[parametre] + ']');
					posOptionVoulue.attr('selected', 'selected');
				} else if (position.prop('tagName') == 'INPUT') {
					//Si c'est un input, il suffit de remplacer le texte par le texte
					//issu du paramétrage
					position.attr('value', res[parametre]);
				} else if (position.prop('tagName') == 'TEXTAREA') {
					//Si c'est un textarea, il suffit de remplacer le texte par le texte
					//issu du paramétrage
					position.text(res[parametre]);
				}
			}
		},
		async: false
	});
}

/**
 * FORM WIDGET - HEX
 * @requires lib miniColors
 */
//
function bind_form_field_hexa() {
    $('input.hexa').not('[disabled="disabled"]').miniColors({});
}

/**
 * Les fonctions suivantes sont similaires aux fonctions qui permettent de gérer
 * la localisation dans les formulaires de paramétrage des éditions du framework
 * mais modifiées pour coller au besoin de la localisation sur plan
 * d'opencimetiere.
 */


$(function() {
   localisation_plan_bind_draggable();
});

function localisation_plan_bind_draggable() {
    $("#plan-draggable").draggable({ containment: "parent" });
    $("#plan-draggable").dblclick(function() {
        infos = $(this).attr("class");
        infos = infos.split(" ");
        infos = infos[0].split(";");
        // on enlève un à l'abscisse et à l'ordonnée pour la bordure de l'image
        // qui décale d'un pixel
        position = $(this).position();
        x = parseInt(position.left);
        y = parseInt(position.top);
        localisation_plan_return(infos[0], infos[1], (x-1), infos[2], (y-1));
        return true;
    });
}
	
// Cette fonction permet de retourner les informations sur le fichier téléchargé
// du formulaire d'upload vers le formulaire d'origine
function localisation_plan_return(form, champ_x, value_x, champ_y, value_y) {
    $("form[name|="+form+"] #"+champ_x, window.opener.document).attr('value', value_x);
    $("form[name|="+form+"] #"+champ_y, window.opener.document).attr('value', value_y);
    window.close();
}

//
function localisation_plan(champ, chplan, positionx) {
    //
    if (fenetreouverte == true) {
        pfenetre.close();
    }
    //
    var plan = document.f2.elements[chplan].value;
    var x = document.f2.elements[positionx].value;
    var y = document.f2.elements[champ].value;
    //
    if (plan == "") {
        //
        alert("Vous devez d'abord selectionner un plan pour pouvoir localiser l'emplacement.");
    } else {
        //
        link = "../app/localisation_plan_edit.php?positiony="+champ+"&positionx="+positionx+"&plan="+plan+"&form=f2"+"&x="+x+"&y="+y+"#draggable";
        //
        pfenetre = window.open(link,"localisation","toolbar=no,scrollbars=yes,width="+screen.availWidth+",height="+screen.availHeight+",top=0,left=0");
        //
        fenetreouverte = true;
    }
}

/**
 * Gère le comportement de la page settings (administration et paramétrage) :
 *  - autofocus sur le champ de recherche
 *  - comportement du livesearch
 */
 function handle_settings_page() {
    // Auto focus sur le champ de recherche au chargement de la page
    $('#settings-live-search #filter').focus();
    // Live search dans le contenu de la page settings
    $("#settings-live-search #filter").keyup(function(){
        var filter = $(this).val(), count = 0;
        $("div.list-group a").each(function(){
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).hide();
            } else {
                $(this).show();
                count++;
            }
        });
    });
}

/**
 *
 */
$(function(){
    /**
     * Gère le comportement de la page settings (administration et paramétrage).
     */
    handle_settings_page();
});

