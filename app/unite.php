<?php
/*
$Id: dossier.php,v 1.14 2009-12-10 15:24:23 fraynaud Exp $
*/
// INITIALISATION DES VARIABLES
$DEBUG = 0;
$existe = false;
$election = false;
$idx="";
$menu=0;
if (isset($_GET['idx'])) {
    $idx=$_GET['idx'];
}
if (isset($_GET['menu'])) {
    $menu=$_GET['menu'];
}

// utils
require_once "../obj/openresultat.class.php";
if ($menu==1) {
    $f = new openresultat(NULL, "unite", _("unite"), "ico_famille.png", "unite");
} else { // sans menu
    $f = new openresultat('nohtml', "unite", _("unite"), "ico_famille.png", "unite");
    $f->setFlag("htmlonly");
}
$f->display();
if ($menu==0) {// html only
    $f->displayStartContent();
}// db

$sql= "select *
    from
        ".DB_PREFIXE."election
    where
        workflow LIKE 'Simulation' OR
        workflow LIKE 'Saisie' OR
        workflow LIKE 'Finalisation'";
$res=$f->db->query($sql);
$f->isDatabaseError($res);

while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
    $election = true;
    // election_unite
    $sql= "select candidat.libelle as candidat, election_resultat.resultat,election_unite.exprime as exprime, ";
    $sql .= " election_unite.votant as votant,election_unite.nul as nul,election_unite.blanc as blanc,election_unite.inscrit as inscrit ";
    $sql .= " from ".DB_PREFIXE."election_resultat ";
    $sql .= " left join ".DB_PREFIXE."election_unite on election_unite.election_unite=election_resultat.election_unite ";
    $sql .= " left join ".DB_PREFIXE."election_candidat on election_candidat.election_candidat=election_resultat.election_candidat ";
    $sql .= " inner join ".DB_PREFIXE."candidat on election_candidat.candidat=candidat.candidat ";
    $sql .= " where election_unite.election =".$row['election'];
    $sql .= " and election_unite.unite =".$idx;
    $res1=$f->db->query($sql);
    $f->isDatabaseError($res1);

    $i=0;
    while ($row1=& $res1->fetchRow(DB_FETCHMODE_ASSOC)) {
        $existe = true;
        $i++;
        if ($i==1) {
            echo "<fieldset class=\"cadre ui-corner-all ui-widget-content collapsible\">\n";
            echo "\t<legend class=\"ui-corner-all ui-widget-content ui-state-active\">";
            echo "".$row['libelle']." "._('tour').' '.$row['tour']." "._('étape').' '.$row['workflow']."</legend>";
            $exprime = $row1 ['exprime'];
            echo "<b>  "._("inscrit")." : ".$row1 ['inscrit']." ";
            echo " - "._("votant")." : ".$row1 ['votant']." ";
            echo " - "._("nul")." : ".$row1 ['nul']." ";
            echo " - "._("blanc")." : ".$row1 ['blanc']." ";
            echo " - "._("exprime")." : ".$exprime."<br><br></b>";
            echo "<table  width='95%' border =1>";
            echo "<tr><td><b>"._("Candidat")."</b></td><td><b>".
                   _("resultat")."</b></td><td align='right'><b>"._("taux").
            "</b></td><tr>";
        }
        echo "<tr>";
        echo "<td>".$row1['candidat']."</td>";
        echo "<td align='right'>".$row1['resultat']."</td>";
        if ($exprime != 0) {
            echo "<td align='right'>".
                number_format(round($row1 ['resultat'] * 100 / $exprime, 2), 2).
                "%"."</td>";
        } else {
            echo "<td align='right'>pas de vote</td>";
        }
        echo "</tr>";
    }
    echo "</table>";
    echo "</fieldset>";
}

if (! $election) {
    $msg = "Aucune élection en cours";
    $f->displayMessage("Valid", $msg);
} elseif (! $existe) {
    $msg = "Aucun résultat enregistré pour l'unite ".$idx;
    $f->displayMessage("Valid", $msg);
}
?>
