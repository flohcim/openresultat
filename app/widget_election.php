<?php
/**
 * WIDGET DASHBOARD - widget_election.
 *
 * @package openresultat
 * @version SVN : $Id$
 */

// Instanciation optionnelle de la classe *om_application*
require_once "../obj/openresultat.class.php";
if (!isset($f)) {
    $f = new openresultat(null);
}

// Affichage du widget
$inst_om_widget = $f->get_inst__om_dbform(array(
    "obj" => "om_widget",
));
if (isset($content) !== true) {
    $content = null;
}
$widget_is_empty = $inst_om_widget->view_widget__election($content);
