<?php
/**
 * Ce fichier permet de ...
 *
 * @package openresultat
 * @version SVN : $Id$
 */

/**
 * Définition du répertoire de stockage des résultats
 */
// Récupère le path courant
$current_path = getcwd();
// Récupère le path du répertoire parent
$parent_path = dirname($current_path);
// Compose le path du répertoire qui contient les résultats
$web_results_path = $parent_path."/web/res/";
$web_results_url = "../web/res/";
// On stocke le répertoire dans une constante pour l'utiliser
// de manière plus lisible dans les scripts inclus
define("WEB_RESULTS_PATH", $web_results_path);
define("WEB_RESULTS_URL", $web_results_url);

/**
 *
 */
//
$election = (isset($_GET["election"]) ? $_GET["election"] : NULL);

/**
 * 
 */
//
require "web.class.php";
//
$t = new or_web($election);
