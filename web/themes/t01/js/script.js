$(function() {

    /* Gestion de l'accès direct aux onglets et sous onglets */
    var hash = window.location.hash;
    hash && $('ul.nav a[href="' + hash + '"]').tab('show');
    $('.nav a').click(function (e) {
        //
        $(this).tab('show');
        //
        resizeplan();
        //
        var scrollmem = $('html,body').scrollTop();
        window.location.hash = this.hash;
        $('html,body').scrollTop(scrollmem);
    });

    /* Gestion de l'accès aux onglets en cas de rechargement de page */
    if (hash.indexOf("resultats-") === 1) {
        //
        $("a[href$='#participation']").parent().removeClass("active");
        //
        $("a[href$='#resultats']").parent().addClass("active");
        //
        $("a[href$='"+hash+"']").parent().addClass("active");
        //
        $("#participation").removeClass("active");
        //
        $("#resultats").addClass("active");
    }
    
    /* Gestion du redimensionnement des liens vers les résultats du bureau sur le plan */
    var rapports = [];
    resizeplan();
    $(window).on('resize', resizeplan);
    function resizeplan() {
        //
        $('img.plan').each(function() {
            rapports[$(this).parent().attr('id')] = Math.min($(this).width() / $(this).data('originalwidth'), 1);
        });
        //
        $('.bureau').each(function() {
            rapport = rapports[$(this).parent().attr('id')];
            $(this).css('top', $(this).data('postop') * rapport);
            $(this).css('left', $(this).data('posleft') * rapport);
            $(this).css('width', $(this).data('originalwidth') * rapport);
        });
    }

});
