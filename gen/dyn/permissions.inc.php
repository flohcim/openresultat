<?php
/**
 * Ce fichier permet de paramétrer le générateur.
 *
 * @package openresultat
 * @version SVN : $Id$
 */

$files_to_avoid = array(
    "import_specific.class.php",
    "om_application_override.class.php",
    "om_dbform.class.php",
    "om_formulaire.class.php",
    "openresultat.class.php",
);

$permissions = array(
	"election_unite_saisir_centaine",
);
