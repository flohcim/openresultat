<?php
//$Id$ 
//gen openMairie le 06/05/2021 21:39

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("election")." -> ".__("plan_election");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."plan_election
    LEFT JOIN ".DB_PREFIXE."election 
        ON plan_election.election=election.election 
    LEFT JOIN ".DB_PREFIXE."plan 
        ON plan_election.plan=plan.plan ";
// SELECT 
$champAffiche = array(
    'plan_election.plan_election as "'.__("plan_election").'"',
    'plan.libelle as "'.__("plan").'"',
    'election.libelle as "'.__("election").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'plan_election.plan_election as "'.__("plan_election").'"',
    'plan.libelle as "'.__("plan").'"',
    'election.libelle as "'.__("election").'"',
    );
$tri="ORDER BY plan.libelle ASC NULLS LAST";
$edition="plan_election";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "election" => array("election", ),
    "plan" => array("plan", ),
);
// Filtre listing sous formulaire - election
if (in_array($retourformulaire, $foreign_keys_extended["election"])) {
    $selection = " WHERE (plan_election.election = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - plan
if (in_array($retourformulaire, $foreign_keys_extended["plan"])) {
    $selection = " WHERE (plan_election.plan = ".intval($idxformulaire).") ";
}

