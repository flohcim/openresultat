<?php
//$Id$ 
//gen openMairie le 07/05/2021 01:38

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("élection")." -> ".__("election_unite");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."election_unite
    LEFT JOIN ".DB_PREFIXE."election 
        ON election_unite.election=election.election 
    LEFT JOIN ".DB_PREFIXE."unite 
        ON election_unite.unite=unite.unite ";
// SELECT 
$champAffiche = array(
    'election_unite.election_unite as "'.__("election_unite").'"',
    'election.libelle as "'.__("election").'"',
    'unite.libelle as "'.__("unite").'"',
    'election_unite.inscrit as "'.__("inscrit").'"',
    'election_unite.votant as "'.__("votant").'"',
    'election_unite.blanc as "'.__("blanc").'"',
    'election_unite.nul as "'.__("nul").'"',
    'election_unite.exprime as "'.__("exprime").'"',
    "case election_unite.envoi_aff when 't' then 'Oui' else 'Non' end as \"".__("envoi_aff")."\"",
    "case election_unite.envoi_web when 't' then 'Oui' else 'Non' end as \"".__("envoi_web")."\"",
    'election_unite.emargement as "'.__("emargement").'"',
    'election_unite.procuration as "'.__("procuration").'"',
    'election_unite.saisie as "'.__("saisie").'"',
    'election_unite.validation as "'.__("validation").'"',
    'election_unite.date_derniere_modif as "'.__("date_derniere_modif").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'election_unite.election_unite as "'.__("election_unite").'"',
    'election.libelle as "'.__("election").'"',
    'unite.libelle as "'.__("unite").'"',
    'election_unite.inscrit as "'.__("inscrit").'"',
    'election_unite.votant as "'.__("votant").'"',
    'election_unite.blanc as "'.__("blanc").'"',
    'election_unite.nul as "'.__("nul").'"',
    'election_unite.exprime as "'.__("exprime").'"',
    'election_unite.emargement as "'.__("emargement").'"',
    'election_unite.procuration as "'.__("procuration").'"',
    'election_unite.saisie as "'.__("saisie").'"',
    'election_unite.validation as "'.__("validation").'"',
    'election_unite.date_derniere_modif as "'.__("date_derniere_modif").'"',
    );
$tri="ORDER BY election.libelle ASC NULLS LAST";
$edition="election_unite";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "election" => array("election", ),
    "unite" => array("unite", ),
);
// Filtre listing sous formulaire - election
if (in_array($retourformulaire, $foreign_keys_extended["election"])) {
    $selection = " WHERE (election_unite.election = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - unite
if (in_array($retourformulaire, $foreign_keys_extended["unite"])) {
    $selection = " WHERE (election_unite.unite = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'election_resultat',
    'participation_unite',
);

