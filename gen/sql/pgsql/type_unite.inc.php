<?php
//$Id$ 
//gen openMairie le 30/04/2021 23:58

$DEBUG=0;
$serie=30;
$ent = __("administration & paramétrage")." -> ".__("élection")." -> ".__("type d'unité");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."type_unite";
// SELECT 
$champAffiche = array(
    'type_unite.type_unite as "'.__("type_unite").'"',
    'type_unite.libelle as "'.__("libelle").'"',
    'type_unite.hierarchie as "'.__("hierarchie").'"',
    "case type_unite.bureau_vote when 't' then 'Oui' else 'Non' end as \"".__("bureau_vote")."\"",
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'type_unite.type_unite as "'.__("type_unite").'"',
    'type_unite.libelle as "'.__("libelle").'"',
    'type_unite.hierarchie as "'.__("hierarchie").'"',
    );
$tri="ORDER BY type_unite.libelle ASC NULLS LAST";
$edition="type_unite";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'animation',
    'unite',
);

