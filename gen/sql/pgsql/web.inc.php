<?php
//$Id$ 
//gen openMairie le 29/04/2021 23:44

$DEBUG=0;
$serie=30;
$ent = __("administration & paramétrage")." -> ".__("affichage")." -> ".__("web");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."web
    LEFT JOIN ".DB_PREFIXE."om_logo 
        ON web.logo=om_logo.om_logo ";
// SELECT 
$champAffiche = array(
    'web.web as "'.__("web").'"',
    'web.libelle as "'.__("libelle").'"',
    'om_logo.libelle as "'.__("logo").'"',
    'web.entete as "'.__("entete").'"',
    'web.url_collectivite as "'.__("url_collectivite").'"',
    'web.libelle_url as "'.__("libelle_url").'"',
    "case web.display_simulation when 't' then 'Oui' else 'Non' end as \"".__("display_simulation")."\"",
    "case web.actif when 't' then 'Oui' else 'Non' end as \"".__("actif")."\"",
    );
//
$champNonAffiche = array(
    'web.feuille_style as "'.__("feuille_style").'"',
    'web.jscript_stats as "'.__("jscript_stats").'"',
    );
//
$champRecherche = array(
    'web.web as "'.__("web").'"',
    'web.libelle as "'.__("libelle").'"',
    'om_logo.libelle as "'.__("logo").'"',
    'web.entete as "'.__("entete").'"',
    'web.url_collectivite as "'.__("url_collectivite").'"',
    'web.libelle_url as "'.__("libelle_url").'"',
    );
$tri="ORDER BY web.libelle ASC NULLS LAST";
$edition="web";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "om_logo" => array("om_logo", ),
);
// Filtre listing sous formulaire - om_logo
if (in_array($retourformulaire, $foreign_keys_extended["om_logo"])) {
    $selection = " WHERE (web.logo = ".intval($idxformulaire).") ";
}

