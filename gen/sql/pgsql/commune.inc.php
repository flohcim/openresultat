<?php
//$Id$ 
//gen openMairie le 29/04/2021 23:04

$DEBUG=0;
$serie=30;
$ent = __("administration & paramétrage")." -> ".__("découpage administratif")." -> ".__("commune");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."commune";
// SELECT 
$champAffiche = array(
    'commune.commune as "'.__("commune").'"',
    'commune.libelle as "'.__("libelle").'"',
    'commune.code as "'.__("code").'"',
    'commune.prefecture as "'.__("prefecture").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'commune.commune as "'.__("commune").'"',
    'commune.libelle as "'.__("libelle").'"',
    'commune.code as "'.__("code").'"',
    'commune.prefecture as "'.__("prefecture").'"',
    );
$tri="ORDER BY commune.libelle ASC NULLS LAST";
$edition="commune";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'unite',
);

