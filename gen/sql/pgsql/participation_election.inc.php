<?php
//$Id$ 
//gen openMairie le 07/05/2021 01:38

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("élection")." -> ".__("participation_election");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."participation_election
    LEFT JOIN ".DB_PREFIXE."election 
        ON participation_election.election=election.election 
    LEFT JOIN ".DB_PREFIXE."tranche 
        ON participation_election.tranche=tranche.tranche ";
// SELECT 
$champAffiche = array(
    'participation_election.participation_election as "'.__("participation_election").'"',
    'election.libelle as "'.__("election").'"',
    'tranche.libelle as "'.__("tranche").'"',
    'participation_election.date_derniere_modif as "'.__("date_derniere_modif").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'participation_election.participation_election as "'.__("participation_election").'"',
    'election.libelle as "'.__("election").'"',
    'tranche.libelle as "'.__("tranche").'"',
    'participation_election.date_derniere_modif as "'.__("date_derniere_modif").'"',
    );
$tri="ORDER BY election.libelle ASC NULLS LAST";
$edition="participation_election";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "election" => array("election", ),
    "tranche" => array("tranche", ),
);
// Filtre listing sous formulaire - election
if (in_array($retourformulaire, $foreign_keys_extended["election"])) {
    $selection = " WHERE (participation_election.election = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - tranche
if (in_array($retourformulaire, $foreign_keys_extended["tranche"])) {
    $selection = " WHERE (participation_election.tranche = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'participation_unite',
);

