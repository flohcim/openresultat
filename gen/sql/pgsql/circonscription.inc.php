<?php
//$Id$ 
//gen openMairie le 29/04/2021 23:04

$DEBUG=0;
$serie=30;
$ent = __("administration & paramétrage")." -> ".__("découpage administratif")." -> ".__("circonscription");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."circonscription";
// SELECT 
$champAffiche = array(
    'circonscription.circonscription as "'.__("circonscription").'"',
    'circonscription.libelle as "'.__("libelle").'"',
    'circonscription.code as "'.__("code").'"',
    'circonscription.prefecture as "'.__("prefecture").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'circonscription.circonscription as "'.__("circonscription").'"',
    'circonscription.libelle as "'.__("libelle").'"',
    'circonscription.code as "'.__("code").'"',
    'circonscription.prefecture as "'.__("prefecture").'"',
    );
$tri="ORDER BY circonscription.libelle ASC NULLS LAST";
$edition="circonscription";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'unite',
);

