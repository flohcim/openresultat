<?php
//$Id$ 
//gen openMairie le 07/05/2021 01:38

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("élection")." -> ".__("détail des participations");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."participation_unite
    LEFT JOIN ".DB_PREFIXE."election_unite 
        ON participation_unite.election_unite=election_unite.election_unite 
    LEFT JOIN ".DB_PREFIXE."participation_election 
        ON participation_unite.participation_election=participation_election.participation_election ";
// SELECT 
$champAffiche = array(
    'participation_unite.participation_unite as "'.__("participation_unite").'"',
    'election_unite.election as "'.__("election_unite").'"',
    'participation_election.election as "'.__("participation_election").'"',
    'participation_unite.votant as "'.__("votant").'"',
    "case participation_unite.saisie when 't' then 'Oui' else 'Non' end as \"".__("saisie")."\"",
    "case participation_unite.envoi_aff when 't' then 'Oui' else 'Non' end as \"".__("envoi_aff")."\"",
    "case participation_unite.envoi_web when 't' then 'Oui' else 'Non' end as \"".__("envoi_web")."\"",
    'participation_unite.date_derniere_modif as "'.__("date_derniere_modif").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'participation_unite.participation_unite as "'.__("participation_unite").'"',
    'election_unite.election as "'.__("election_unite").'"',
    'participation_election.election as "'.__("participation_election").'"',
    'participation_unite.votant as "'.__("votant").'"',
    'participation_unite.date_derniere_modif as "'.__("date_derniere_modif").'"',
    );
$tri="ORDER BY election_unite.election ASC NULLS LAST";
$edition="participation_unite";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "election_unite" => array("election_unite", ),
    "participation_election" => array("participation_election", ),
);
// Filtre listing sous formulaire - election_unite
if (in_array($retourformulaire, $foreign_keys_extended["election_unite"])) {
    $selection = " WHERE (participation_unite.election_unite = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - participation_election
if (in_array($retourformulaire, $foreign_keys_extended["participation_election"])) {
    $selection = " WHERE (participation_unite.participation_election = ".intval($idxformulaire).") ";
}

