<?php
//$Id$ 
//gen openMairie le 01/05/2021 01:09

$DEBUG=0;
$serie=30;
$ent = __("administration & paramétrage")." -> ".__("élection")." -> ".__("lien entre unités");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."lien_unite
    LEFT JOIN ".DB_PREFIXE."unite as unite0 
        ON lien_unite.unite_enfant=unite0.unite 
    LEFT JOIN ".DB_PREFIXE."unite as unite1 
        ON lien_unite.unite_parent=unite1.unite ";
// SELECT 
$champAffiche = array(
    'lien_unite.lien_unite as "'.__("lien_unite").'"',
    'unite0.libelle as "'.__("unite_enfant").'"',
    'unite1.libelle as "'.__("unite_parent").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'lien_unite.lien_unite as "'.__("lien_unite").'"',
    'unite0.libelle as "'.__("unite_enfant").'"',
    'unite1.libelle as "'.__("unite_parent").'"',
    );
$tri="ORDER BY unite0.libelle ASC NULLS LAST";
$edition="lien_unite";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "unite" => array("unite", ),
);
// Filtre listing sous formulaire - unite
if (in_array($retourformulaire, $foreign_keys_extended["unite"])) {
    $selection = " WHERE (lien_unite.unite_enfant = ".intval($idxformulaire)." OR lien_unite.unite_parent = ".intval($idxformulaire).") ";
}

