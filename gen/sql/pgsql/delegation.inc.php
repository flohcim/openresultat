<?php
//$Id$ 
//gen openMairie le 07/05/2021 01:38

$DEBUG=0;
$serie=30;
$ent = __("application")." -> ".__("élection")." -> ".__("delegation");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."delegation
    LEFT JOIN ".DB_PREFIXE."acteur 
        ON delegation.acteur=acteur.acteur 
    LEFT JOIN ".DB_PREFIXE."election 
        ON delegation.election=election.election 
    LEFT JOIN ".DB_PREFIXE."unite 
        ON delegation.unite=unite.unite ";
// SELECT 
$champAffiche = array(
    'delegation.delegation as "'.__("delegation").'"',
    'election.libelle as "'.__("election").'"',
    'unite.libelle as "'.__("unite").'"',
    'acteur.nom as "'.__("acteur").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'delegation.delegation as "'.__("delegation").'"',
    'election.libelle as "'.__("election").'"',
    'unite.libelle as "'.__("unite").'"',
    'acteur.nom as "'.__("acteur").'"',
    );
$tri="ORDER BY election.libelle ASC NULLS LAST";
$edition="delegation";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "acteur" => array("acteur", ),
    "election" => array("election", ),
    "unite" => array("unite", ),
);
// Filtre listing sous formulaire - acteur
if (in_array($retourformulaire, $foreign_keys_extended["acteur"])) {
    $selection = " WHERE (delegation.acteur = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - election
if (in_array($retourformulaire, $foreign_keys_extended["election"])) {
    $selection = " WHERE (delegation.election = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - unite
if (in_array($retourformulaire, $foreign_keys_extended["unite"])) {
    $selection = " WHERE (delegation.unite = ".intval($idxformulaire).") ";
}

