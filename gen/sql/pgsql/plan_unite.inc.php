<?php
//$Id$ 
//gen openMairie le 03/05/2021 01:35

$DEBUG=0;
$serie=30;
$ent = __("administration & paramétrage")." -> ".__("affichage")." -> ".__("plan_unite");
if(!isset($premier)) $premier='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
// FROM 
$table = DB_PREFIXE."plan_unite
    LEFT JOIN ".DB_PREFIXE."plan 
        ON plan_unite.plan=plan.plan 
    LEFT JOIN ".DB_PREFIXE."unite 
        ON plan_unite.unite=unite.unite ";
// SELECT 
$champAffiche = array(
    'plan_unite.plan_unite as "'.__("plan_unite").'"',
    'plan.libelle as "'.__("plan").'"',
    'unite.libelle as "'.__("unite").'"',
    'plan_unite.position_x as "'.__("position_x").'"',
    'plan_unite.position_y as "'.__("position_y").'"',
    );
//
$champNonAffiche = array(
    'plan_unite.img_unite_arrivee as "'.__("img_unite_arrivee").'"',
    'plan_unite.img_unite_non_arrivee as "'.__("img_unite_non_arrivee").'"',
    'plan_unite.largeur_icone as "'.__("largeur_icone").'"',
    );
//
$champRecherche = array(
    'plan_unite.plan_unite as "'.__("plan_unite").'"',
    'plan.libelle as "'.__("plan").'"',
    'unite.libelle as "'.__("unite").'"',
    'plan_unite.position_x as "'.__("position_x").'"',
    'plan_unite.position_y as "'.__("position_y").'"',
    );
$tri="ORDER BY plan.libelle ASC NULLS LAST";
$edition="plan_unite";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "plan" => array("plan", ),
    "unite" => array("unite", ),
);
// Filtre listing sous formulaire - plan
if (in_array($retourformulaire, $foreign_keys_extended["plan"])) {
    $selection = " WHERE (plan_unite.plan = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - unite
if (in_array($retourformulaire, $foreign_keys_extended["unite"])) {
    $selection = " WHERE (plan_unite.unite = ".intval($idxformulaire).") ";
}

