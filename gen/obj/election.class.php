<?php
//$Id$ 
//gen openMairie le 11/05/2021 14:34

require_once "../obj/om_dbform.class.php";

class election_gen extends om_dbform {

    protected $_absolute_class_name = "election";

    var $table = "election";
    var $clePrimaire = "election";
    var $typeCle = "N";
    var $required_field = array(
        "date",
        "election",
        "heure_fermeture",
        "heure_ouverture",
        "libelle",
        "perimetre",
        "type_election"
    );
    
    var $foreign_keys_extended = array(
        "election" => array("election", ),
        "tranche" => array("tranche", ),
        "unite" => array("unite", ),
        "type_election" => array("type_election", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "election",
            "libelle",
            "code",
            "tour",
            "type_election",
            "date",
            "perimetre",
            "votant_defaut",
            "heure_ouverture",
            "heure_fermeture",
            "envoi_initial",
            "is_centaine",
            "election_reference",
            "workflow",
            "publication_auto",
            "publication_erreur",
            "calcul_auto_exprime",
            "garder_resultat_simulation",
            "delegation_saisie",
            "delegation_participation",
            "validation_avant_publication",
            "sieges",
            "sieges_com",
            "sieges_mep",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_election_reference() {
        return "SELECT election.election, election.libelle FROM ".DB_PREFIXE."election ORDER BY election.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_election_reference_by_id() {
        return "SELECT election.election, election.libelle FROM ".DB_PREFIXE."election WHERE election = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_heure_fermeture() {
        return "SELECT tranche.tranche, tranche.libelle FROM ".DB_PREFIXE."tranche ORDER BY tranche.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_heure_fermeture_by_id() {
        return "SELECT tranche.tranche, tranche.libelle FROM ".DB_PREFIXE."tranche WHERE tranche = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_heure_ouverture() {
        return "SELECT tranche.tranche, tranche.libelle FROM ".DB_PREFIXE."tranche ORDER BY tranche.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_heure_ouverture_by_id() {
        return "SELECT tranche.tranche, tranche.libelle FROM ".DB_PREFIXE."tranche WHERE tranche = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_perimetre() {
        return "SELECT unite.unite, unite.libelle FROM ".DB_PREFIXE."unite WHERE ((unite.om_validite_debut IS NULL AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)) OR (unite.om_validite_debut <= CURRENT_DATE AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE))) ORDER BY unite.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_perimetre_by_id() {
        return "SELECT unite.unite, unite.libelle FROM ".DB_PREFIXE."unite WHERE unite = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_type_election() {
        return "SELECT type_election.type_election, type_election.libelle FROM ".DB_PREFIXE."type_election ORDER BY type_election.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_type_election_by_id() {
        return "SELECT type_election.type_election, type_election.libelle FROM ".DB_PREFIXE."type_election WHERE type_election = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['election'])) {
            $this->valF['election'] = ""; // -> requis
        } else {
            $this->valF['election'] = $val['election'];
        }
        $this->valF['libelle'] = $val['libelle'];
        if ($val['code'] == "") {
            $this->valF['code'] = NULL;
        } else {
            $this->valF['code'] = $val['code'];
        }
        if (!is_numeric($val['tour'])) {
            $this->valF['tour'] = NULL;
        } else {
            $this->valF['tour'] = $val['tour'];
        }
        if (!is_numeric($val['type_election'])) {
            $this->valF['type_election'] = ""; // -> requis
        } else {
            $this->valF['type_election'] = $val['type_election'];
        }
        if ($val['date'] != "") {
            $this->valF['date'] = $this->dateDB($val['date']);
        }
        if (!is_numeric($val['perimetre'])) {
            $this->valF['perimetre'] = ""; // -> requis
        } else {
            $this->valF['perimetre'] = $val['perimetre'];
        }
        if (!is_numeric($val['votant_defaut'])) {
            $this->valF['votant_defaut'] = NULL;
        } else {
            $this->valF['votant_defaut'] = $val['votant_defaut'];
        }
        if (!is_numeric($val['heure_ouverture'])) {
            $this->valF['heure_ouverture'] = ""; // -> requis
        } else {
            $this->valF['heure_ouverture'] = $val['heure_ouverture'];
        }
        if (!is_numeric($val['heure_fermeture'])) {
            $this->valF['heure_fermeture'] = ""; // -> requis
        } else {
            $this->valF['heure_fermeture'] = $val['heure_fermeture'];
        }
        if ($val['envoi_initial'] == 1 || $val['envoi_initial'] == "t" || $val['envoi_initial'] == "Oui") {
            $this->valF['envoi_initial'] = true;
        } else {
            $this->valF['envoi_initial'] = false;
        }
        if ($val['is_centaine'] == 1 || $val['is_centaine'] == "t" || $val['is_centaine'] == "Oui") {
            $this->valF['is_centaine'] = true;
        } else {
            $this->valF['is_centaine'] = false;
        }
        if (!is_numeric($val['election_reference'])) {
            $this->valF['election_reference'] = NULL;
        } else {
            $this->valF['election_reference'] = $val['election_reference'];
        }
        if ($val['workflow'] == "") {
            $this->valF['workflow'] = NULL;
        } else {
            $this->valF['workflow'] = $val['workflow'];
        }
        if ($val['publication_auto'] == 1 || $val['publication_auto'] == "t" || $val['publication_auto'] == "Oui") {
            $this->valF['publication_auto'] = true;
        } else {
            $this->valF['publication_auto'] = false;
        }
        if ($val['publication_erreur'] == 1 || $val['publication_erreur'] == "t" || $val['publication_erreur'] == "Oui") {
            $this->valF['publication_erreur'] = true;
        } else {
            $this->valF['publication_erreur'] = false;
        }
        if ($val['calcul_auto_exprime'] == 1 || $val['calcul_auto_exprime'] == "t" || $val['calcul_auto_exprime'] == "Oui") {
            $this->valF['calcul_auto_exprime'] = true;
        } else {
            $this->valF['calcul_auto_exprime'] = false;
        }
        if ($val['garder_resultat_simulation'] == 1 || $val['garder_resultat_simulation'] == "t" || $val['garder_resultat_simulation'] == "Oui") {
            $this->valF['garder_resultat_simulation'] = true;
        } else {
            $this->valF['garder_resultat_simulation'] = false;
        }
        if ($val['delegation_saisie'] == 1 || $val['delegation_saisie'] == "t" || $val['delegation_saisie'] == "Oui") {
            $this->valF['delegation_saisie'] = true;
        } else {
            $this->valF['delegation_saisie'] = false;
        }
        if ($val['delegation_participation'] == 1 || $val['delegation_participation'] == "t" || $val['delegation_participation'] == "Oui") {
            $this->valF['delegation_participation'] = true;
        } else {
            $this->valF['delegation_participation'] = false;
        }
        if ($val['validation_avant_publication'] == 1 || $val['validation_avant_publication'] == "t" || $val['validation_avant_publication'] == "Oui") {
            $this->valF['validation_avant_publication'] = true;
        } else {
            $this->valF['validation_avant_publication'] = false;
        }
        if (!is_numeric($val['sieges'])) {
            $this->valF['sieges'] = NULL;
        } else {
            $this->valF['sieges'] = $val['sieges'];
        }
        if (!is_numeric($val['sieges_com'])) {
            $this->valF['sieges_com'] = NULL;
        } else {
            $this->valF['sieges_com'] = $val['sieges_com'];
        }
        if (!is_numeric($val['sieges_mep'])) {
            $this->valF['sieges_mep'] = 0; // -> default
        } else {
            $this->valF['sieges_mep'] = $val['sieges_mep'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("election", "hidden");
            $form->setType("libelle", "text");
            $form->setType("code", "text");
            $form->setType("tour", "text");
            if ($this->is_in_context_of_foreign_key("type_election", $this->retourformulaire)) {
                $form->setType("type_election", "selecthiddenstatic");
            } else {
                $form->setType("type_election", "select");
            }
            $form->setType("date", "date");
            if ($this->is_in_context_of_foreign_key("unite", $this->retourformulaire)) {
                $form->setType("perimetre", "selecthiddenstatic");
            } else {
                $form->setType("perimetre", "select");
            }
            $form->setType("votant_defaut", "text");
            if ($this->is_in_context_of_foreign_key("tranche", $this->retourformulaire)) {
                $form->setType("heure_ouverture", "selecthiddenstatic");
            } else {
                $form->setType("heure_ouverture", "select");
            }
            if ($this->is_in_context_of_foreign_key("tranche", $this->retourformulaire)) {
                $form->setType("heure_fermeture", "selecthiddenstatic");
            } else {
                $form->setType("heure_fermeture", "select");
            }
            $form->setType("envoi_initial", "checkbox");
            $form->setType("is_centaine", "checkbox");
            if ($this->is_in_context_of_foreign_key("election", $this->retourformulaire)) {
                $form->setType("election_reference", "selecthiddenstatic");
            } else {
                $form->setType("election_reference", "select");
            }
            $form->setType("workflow", "text");
            $form->setType("publication_auto", "checkbox");
            $form->setType("publication_erreur", "checkbox");
            $form->setType("calcul_auto_exprime", "checkbox");
            $form->setType("garder_resultat_simulation", "checkbox");
            $form->setType("delegation_saisie", "checkbox");
            $form->setType("delegation_participation", "checkbox");
            $form->setType("validation_avant_publication", "checkbox");
            $form->setType("sieges", "text");
            $form->setType("sieges_com", "text");
            $form->setType("sieges_mep", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("election", "hiddenstatic");
            $form->setType("libelle", "text");
            $form->setType("code", "text");
            $form->setType("tour", "text");
            if ($this->is_in_context_of_foreign_key("type_election", $this->retourformulaire)) {
                $form->setType("type_election", "selecthiddenstatic");
            } else {
                $form->setType("type_election", "select");
            }
            $form->setType("date", "date");
            if ($this->is_in_context_of_foreign_key("unite", $this->retourformulaire)) {
                $form->setType("perimetre", "selecthiddenstatic");
            } else {
                $form->setType("perimetre", "select");
            }
            $form->setType("votant_defaut", "text");
            if ($this->is_in_context_of_foreign_key("tranche", $this->retourformulaire)) {
                $form->setType("heure_ouverture", "selecthiddenstatic");
            } else {
                $form->setType("heure_ouverture", "select");
            }
            if ($this->is_in_context_of_foreign_key("tranche", $this->retourformulaire)) {
                $form->setType("heure_fermeture", "selecthiddenstatic");
            } else {
                $form->setType("heure_fermeture", "select");
            }
            $form->setType("envoi_initial", "checkbox");
            $form->setType("is_centaine", "checkbox");
            if ($this->is_in_context_of_foreign_key("election", $this->retourformulaire)) {
                $form->setType("election_reference", "selecthiddenstatic");
            } else {
                $form->setType("election_reference", "select");
            }
            $form->setType("workflow", "text");
            $form->setType("publication_auto", "checkbox");
            $form->setType("publication_erreur", "checkbox");
            $form->setType("calcul_auto_exprime", "checkbox");
            $form->setType("garder_resultat_simulation", "checkbox");
            $form->setType("delegation_saisie", "checkbox");
            $form->setType("delegation_participation", "checkbox");
            $form->setType("validation_avant_publication", "checkbox");
            $form->setType("sieges", "text");
            $form->setType("sieges_com", "text");
            $form->setType("sieges_mep", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("election", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("code", "hiddenstatic");
            $form->setType("tour", "hiddenstatic");
            $form->setType("type_election", "selectstatic");
            $form->setType("date", "hiddenstatic");
            $form->setType("perimetre", "selectstatic");
            $form->setType("votant_defaut", "hiddenstatic");
            $form->setType("heure_ouverture", "selectstatic");
            $form->setType("heure_fermeture", "selectstatic");
            $form->setType("envoi_initial", "hiddenstatic");
            $form->setType("is_centaine", "hiddenstatic");
            $form->setType("election_reference", "selectstatic");
            $form->setType("workflow", "hiddenstatic");
            $form->setType("publication_auto", "hiddenstatic");
            $form->setType("publication_erreur", "hiddenstatic");
            $form->setType("calcul_auto_exprime", "hiddenstatic");
            $form->setType("garder_resultat_simulation", "hiddenstatic");
            $form->setType("delegation_saisie", "hiddenstatic");
            $form->setType("delegation_participation", "hiddenstatic");
            $form->setType("validation_avant_publication", "hiddenstatic");
            $form->setType("sieges", "hiddenstatic");
            $form->setType("sieges_com", "hiddenstatic");
            $form->setType("sieges_mep", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("election", "static");
            $form->setType("libelle", "static");
            $form->setType("code", "static");
            $form->setType("tour", "static");
            $form->setType("type_election", "selectstatic");
            $form->setType("date", "datestatic");
            $form->setType("perimetre", "selectstatic");
            $form->setType("votant_defaut", "static");
            $form->setType("heure_ouverture", "selectstatic");
            $form->setType("heure_fermeture", "selectstatic");
            $form->setType("envoi_initial", "checkboxstatic");
            $form->setType("is_centaine", "checkboxstatic");
            $form->setType("election_reference", "selectstatic");
            $form->setType("workflow", "static");
            $form->setType("publication_auto", "checkboxstatic");
            $form->setType("publication_erreur", "checkboxstatic");
            $form->setType("calcul_auto_exprime", "checkboxstatic");
            $form->setType("garder_resultat_simulation", "checkboxstatic");
            $form->setType("delegation_saisie", "checkboxstatic");
            $form->setType("delegation_participation", "checkboxstatic");
            $form->setType("validation_avant_publication", "checkboxstatic");
            $form->setType("sieges", "static");
            $form->setType("sieges_com", "static");
            $form->setType("sieges_mep", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('election','VerifNum(this)');
        $form->setOnchange('tour','VerifNum(this)');
        $form->setOnchange('type_election','VerifNum(this)');
        $form->setOnchange('date','fdate(this)');
        $form->setOnchange('perimetre','VerifNum(this)');
        $form->setOnchange('votant_defaut','VerifNum(this)');
        $form->setOnchange('heure_ouverture','VerifNum(this)');
        $form->setOnchange('heure_fermeture','VerifNum(this)');
        $form->setOnchange('election_reference','VerifNum(this)');
        $form->setOnchange('sieges','VerifNum(this)');
        $form->setOnchange('sieges_com','VerifNum(this)');
        $form->setOnchange('sieges_mep','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("election", 11);
        $form->setTaille("libelle", 30);
        $form->setTaille("code", 10);
        $form->setTaille("tour", 11);
        $form->setTaille("type_election", 11);
        $form->setTaille("date", 12);
        $form->setTaille("perimetre", 11);
        $form->setTaille("votant_defaut", 11);
        $form->setTaille("heure_ouverture", 11);
        $form->setTaille("heure_fermeture", 11);
        $form->setTaille("envoi_initial", 1);
        $form->setTaille("is_centaine", 1);
        $form->setTaille("election_reference", 11);
        $form->setTaille("workflow", 15);
        $form->setTaille("publication_auto", 1);
        $form->setTaille("publication_erreur", 1);
        $form->setTaille("calcul_auto_exprime", 1);
        $form->setTaille("garder_resultat_simulation", 1);
        $form->setTaille("delegation_saisie", 1);
        $form->setTaille("delegation_participation", 1);
        $form->setTaille("validation_avant_publication", 1);
        $form->setTaille("sieges", 11);
        $form->setTaille("sieges_com", 11);
        $form->setTaille("sieges_mep", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("election", 11);
        $form->setMax("libelle", 100);
        $form->setMax("code", 10);
        $form->setMax("tour", 11);
        $form->setMax("type_election", 11);
        $form->setMax("date", 12);
        $form->setMax("perimetre", 11);
        $form->setMax("votant_defaut", 11);
        $form->setMax("heure_ouverture", 11);
        $form->setMax("heure_fermeture", 11);
        $form->setMax("envoi_initial", 1);
        $form->setMax("is_centaine", 1);
        $form->setMax("election_reference", 11);
        $form->setMax("workflow", 15);
        $form->setMax("publication_auto", 1);
        $form->setMax("publication_erreur", 1);
        $form->setMax("calcul_auto_exprime", 1);
        $form->setMax("garder_resultat_simulation", 1);
        $form->setMax("delegation_saisie", 1);
        $form->setMax("delegation_participation", 1);
        $form->setMax("validation_avant_publication", 1);
        $form->setMax("sieges", 11);
        $form->setMax("sieges_com", 11);
        $form->setMax("sieges_mep", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('election', __('election'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('code', __('code'));
        $form->setLib('tour', __('tour'));
        $form->setLib('type_election', __('type_election'));
        $form->setLib('date', __('date'));
        $form->setLib('perimetre', __('perimetre'));
        $form->setLib('votant_defaut', __('votant_defaut'));
        $form->setLib('heure_ouverture', __('heure_ouverture'));
        $form->setLib('heure_fermeture', __('heure_fermeture'));
        $form->setLib('envoi_initial', __('envoi_initial'));
        $form->setLib('is_centaine', __('is_centaine'));
        $form->setLib('election_reference', __('election_reference'));
        $form->setLib('workflow', __('workflow'));
        $form->setLib('publication_auto', __('publication_auto'));
        $form->setLib('publication_erreur', __('publication_erreur'));
        $form->setLib('calcul_auto_exprime', __('calcul_auto_exprime'));
        $form->setLib('garder_resultat_simulation', __('garder_resultat_simulation'));
        $form->setLib('delegation_saisie', __('delegation_saisie'));
        $form->setLib('delegation_participation', __('delegation_participation'));
        $form->setLib('validation_avant_publication', __('validation_avant_publication'));
        $form->setLib('sieges', __('sieges'));
        $form->setLib('sieges_com', __('sieges_com'));
        $form->setLib('sieges_mep', __('sieges_mep'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // election_reference
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "election_reference",
            $this->get_var_sql_forminc__sql("election_reference"),
            $this->get_var_sql_forminc__sql("election_reference_by_id"),
            false
        );
        // heure_fermeture
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "heure_fermeture",
            $this->get_var_sql_forminc__sql("heure_fermeture"),
            $this->get_var_sql_forminc__sql("heure_fermeture_by_id"),
            false
        );
        // heure_ouverture
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "heure_ouverture",
            $this->get_var_sql_forminc__sql("heure_ouverture"),
            $this->get_var_sql_forminc__sql("heure_ouverture_by_id"),
            false
        );
        // perimetre
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "perimetre",
            $this->get_var_sql_forminc__sql("perimetre"),
            $this->get_var_sql_forminc__sql("perimetre_by_id"),
            true
        );
        // type_election
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "type_election",
            $this->get_var_sql_forminc__sql("type_election"),
            $this->get_var_sql_forminc__sql("type_election_by_id"),
            false
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('election', $this->retourformulaire))
                $form->setVal('election_reference', $idxformulaire);
            if($this->is_in_context_of_foreign_key('unite', $this->retourformulaire))
                $form->setVal('perimetre', $idxformulaire);
            if($this->is_in_context_of_foreign_key('type_election', $this->retourformulaire))
                $form->setVal('type_election', $idxformulaire);
        }// fin validation
        if ($validation == 0 and $maj == 0) {
            if($this->is_in_context_of_foreign_key('tranche', $this->retourformulaire))
                $form->setVal('heure_fermeture', $idxformulaire);
            if($this->is_in_context_of_foreign_key('tranche', $this->retourformulaire))
                $form->setVal('heure_ouverture', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : animation
        $this->rechercheTable($this->f->db, "animation", "election", $id);
        // Verification de la cle secondaire : delegation
        $this->rechercheTable($this->f->db, "delegation", "election", $id);
        // Verification de la cle secondaire : election
        $this->rechercheTable($this->f->db, "election", "election_reference", $id);
        // Verification de la cle secondaire : election_candidat
        $this->rechercheTable($this->f->db, "election_candidat", "election", $id);
        // Verification de la cle secondaire : election_unite
        $this->rechercheTable($this->f->db, "election_unite", "election", $id);
        // Verification de la cle secondaire : participation_election
        $this->rechercheTable($this->f->db, "participation_election", "election", $id);
        // Verification de la cle secondaire : plan_election
        $this->rechercheTable($this->f->db, "plan_election", "election", $id);
    }


}
