<?php
//$Id$ 
//gen openMairie le 07/04/2021 16:36

require_once "../obj/om_dbform.class.php";

class web_gen extends om_dbform {

    protected $_absolute_class_name = "web";

    var $table = "web";
    var $clePrimaire = "web";
    var $typeCle = "N";
    var $required_field = array(
        "libelle",
        "web"
    );
    
    var $foreign_keys_extended = array(
        "om_logo" => array("om_logo", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "web",
            "libelle",
            "logo",
            "entete",
            "url_collectivite",
            "libelle_url",
            "feuille_style",
            "jscript_stats",
            "display_simulation",
            "actif",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_logo() {
        return "SELECT om_logo.om_logo, om_logo.libelle FROM ".DB_PREFIXE."om_logo ORDER BY om_logo.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_logo_by_id() {
        return "SELECT om_logo.om_logo, om_logo.libelle FROM ".DB_PREFIXE."om_logo WHERE om_logo = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['web'])) {
            $this->valF['web'] = ""; // -> requis
        } else {
            $this->valF['web'] = $val['web'];
        }
        $this->valF['libelle'] = $val['libelle'];
        if (!is_numeric($val['logo'])) {
            $this->valF['logo'] = NULL;
        } else {
            $this->valF['logo'] = $val['logo'];
        }
        if ($val['entete'] == "") {
            $this->valF['entete'] = NULL;
        } else {
            $this->valF['entete'] = $val['entete'];
        }
        if ($val['url_collectivite'] == "") {
            $this->valF['url_collectivite'] = NULL;
        } else {
            $this->valF['url_collectivite'] = $val['url_collectivite'];
        }
        if ($val['libelle_url'] == "") {
            $this->valF['libelle_url'] = NULL;
        } else {
            $this->valF['libelle_url'] = $val['libelle_url'];
        }
            $this->valF['feuille_style'] = $val['feuille_style'];
            $this->valF['jscript_stats'] = $val['jscript_stats'];
        if ($val['display_simulation'] == 1 || $val['display_simulation'] == "t" || $val['display_simulation'] == "Oui") {
            $this->valF['display_simulation'] = true;
        } else {
            $this->valF['display_simulation'] = false;
        }
        if ($val['actif'] == 1 || $val['actif'] == "t" || $val['actif'] == "Oui") {
            $this->valF['actif'] = true;
        } else {
            $this->valF['actif'] = false;
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("web", "hidden");
            $form->setType("libelle", "text");
            if ($this->is_in_context_of_foreign_key("om_logo", $this->retourformulaire)) {
                $form->setType("logo", "selecthiddenstatic");
            } else {
                $form->setType("logo", "select");
            }
            $form->setType("entete", "text");
            $form->setType("url_collectivite", "text");
            $form->setType("libelle_url", "text");
            $form->setType("feuille_style", "textarea");
            $form->setType("jscript_stats", "textarea");
            $form->setType("display_simulation", "checkbox");
            $form->setType("actif", "checkbox");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("web", "hiddenstatic");
            $form->setType("libelle", "text");
            if ($this->is_in_context_of_foreign_key("om_logo", $this->retourformulaire)) {
                $form->setType("logo", "selecthiddenstatic");
            } else {
                $form->setType("logo", "select");
            }
            $form->setType("entete", "text");
            $form->setType("url_collectivite", "text");
            $form->setType("libelle_url", "text");
            $form->setType("feuille_style", "textarea");
            $form->setType("jscript_stats", "textarea");
            $form->setType("display_simulation", "checkbox");
            $form->setType("actif", "checkbox");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("web", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("logo", "selectstatic");
            $form->setType("entete", "hiddenstatic");
            $form->setType("url_collectivite", "hiddenstatic");
            $form->setType("libelle_url", "hiddenstatic");
            $form->setType("feuille_style", "hiddenstatic");
            $form->setType("jscript_stats", "hiddenstatic");
            $form->setType("display_simulation", "hiddenstatic");
            $form->setType("actif", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("web", "static");
            $form->setType("libelle", "static");
            $form->setType("logo", "selectstatic");
            $form->setType("entete", "static");
            $form->setType("url_collectivite", "static");
            $form->setType("libelle_url", "static");
            $form->setType("feuille_style", "textareastatic");
            $form->setType("jscript_stats", "textareastatic");
            $form->setType("display_simulation", "checkboxstatic");
            $form->setType("actif", "checkboxstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('web','VerifNum(this)');
        $form->setOnchange('logo','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("web", 11);
        $form->setTaille("libelle", 30);
        $form->setTaille("logo", 11);
        $form->setTaille("entete", 30);
        $form->setTaille("url_collectivite", 30);
        $form->setTaille("libelle_url", 30);
        $form->setTaille("feuille_style", 80);
        $form->setTaille("jscript_stats", 80);
        $form->setTaille("display_simulation", 1);
        $form->setTaille("actif", 1);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("web", 11);
        $form->setMax("libelle", 30);
        $form->setMax("logo", 11);
        $form->setMax("entete", 100);
        $form->setMax("url_collectivite", 100);
        $form->setMax("libelle_url", 30);
        $form->setMax("feuille_style", 6);
        $form->setMax("jscript_stats", 6);
        $form->setMax("display_simulation", 1);
        $form->setMax("actif", 1);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('web', __('web'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('logo', __('logo'));
        $form->setLib('entete', __('entete'));
        $form->setLib('url_collectivite', __('url_collectivite'));
        $form->setLib('libelle_url', __('libelle_url'));
        $form->setLib('feuille_style', __('feuille_style'));
        $form->setLib('jscript_stats', __('jscript_stats'));
        $form->setLib('display_simulation', __('display_simulation'));
        $form->setLib('actif', __('actif'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // logo
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "logo",
            $this->get_var_sql_forminc__sql("logo"),
            $this->get_var_sql_forminc__sql("logo_by_id"),
            false
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('om_logo', $this->retourformulaire))
                $form->setVal('logo', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
