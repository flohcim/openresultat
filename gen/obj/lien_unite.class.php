<?php
//$Id$ 
//gen openMairie le 05/03/2021 14:09

require_once "../obj/om_dbform.class.php";

class lien_unite_gen extends om_dbform {

    protected $_absolute_class_name = "lien_unite";

    var $table = "lien_unite";
    var $clePrimaire = "lien_unite";
    var $typeCle = "N";
    var $required_field = array(
        "lien_unite",
        "unite_enfant",
        "unite_parent"
    );
    
    var $foreign_keys_extended = array(
        "unite" => array("unite", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("unite_enfant");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "lien_unite",
            "unite_enfant",
            "unite_parent",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_unite_enfant() {
        return "SELECT unite.unite, unite.libelle FROM ".DB_PREFIXE."unite WHERE ((unite.om_validite_debut IS NULL AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)) OR (unite.om_validite_debut <= CURRENT_DATE AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE))) ORDER BY unite.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_unite_enfant_by_id() {
        return "SELECT unite.unite, unite.libelle FROM ".DB_PREFIXE."unite WHERE unite = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_unite_parent() {
        return "SELECT unite.unite, unite.libelle FROM ".DB_PREFIXE."unite WHERE ((unite.om_validite_debut IS NULL AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)) OR (unite.om_validite_debut <= CURRENT_DATE AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE))) ORDER BY unite.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_unite_parent_by_id() {
        return "SELECT unite.unite, unite.libelle FROM ".DB_PREFIXE."unite WHERE unite = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['lien_unite'])) {
            $this->valF['lien_unite'] = ""; // -> requis
        } else {
            $this->valF['lien_unite'] = $val['lien_unite'];
        }
        if (!is_numeric($val['unite_enfant'])) {
            $this->valF['unite_enfant'] = ""; // -> requis
        } else {
            $this->valF['unite_enfant'] = $val['unite_enfant'];
        }
        if (!is_numeric($val['unite_parent'])) {
            $this->valF['unite_parent'] = ""; // -> requis
        } else {
            $this->valF['unite_parent'] = $val['unite_parent'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("lien_unite", "hidden");
            if ($this->is_in_context_of_foreign_key("unite", $this->retourformulaire)) {
                $form->setType("unite_enfant", "selecthiddenstatic");
            } else {
                $form->setType("unite_enfant", "select");
            }
            if ($this->is_in_context_of_foreign_key("unite", $this->retourformulaire)) {
                $form->setType("unite_parent", "selecthiddenstatic");
            } else {
                $form->setType("unite_parent", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("lien_unite", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("unite", $this->retourformulaire)) {
                $form->setType("unite_enfant", "selecthiddenstatic");
            } else {
                $form->setType("unite_enfant", "select");
            }
            if ($this->is_in_context_of_foreign_key("unite", $this->retourformulaire)) {
                $form->setType("unite_parent", "selecthiddenstatic");
            } else {
                $form->setType("unite_parent", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("lien_unite", "hiddenstatic");
            $form->setType("unite_enfant", "selectstatic");
            $form->setType("unite_parent", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("lien_unite", "static");
            $form->setType("unite_enfant", "selectstatic");
            $form->setType("unite_parent", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('lien_unite','VerifNum(this)');
        $form->setOnchange('unite_enfant','VerifNum(this)');
        $form->setOnchange('unite_parent','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("lien_unite", 11);
        $form->setTaille("unite_enfant", 11);
        $form->setTaille("unite_parent", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("lien_unite", 11);
        $form->setMax("unite_enfant", 11);
        $form->setMax("unite_parent", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('lien_unite', __('lien_unite'));
        $form->setLib('unite_enfant', __('unite_enfant'));
        $form->setLib('unite_parent', __('unite_parent'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // unite_enfant
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "unite_enfant",
            $this->get_var_sql_forminc__sql("unite_enfant"),
            $this->get_var_sql_forminc__sql("unite_enfant_by_id"),
            true
        );
        // unite_parent
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "unite_parent",
            $this->get_var_sql_forminc__sql("unite_parent"),
            $this->get_var_sql_forminc__sql("unite_parent_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if ($validation == 0 and $maj == 0) {
            if($this->is_in_context_of_foreign_key('unite', $this->retourformulaire))
                $form->setVal('unite_enfant', $idxformulaire);
            if($this->is_in_context_of_foreign_key('unite', $this->retourformulaire))
                $form->setVal('unite_parent', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
