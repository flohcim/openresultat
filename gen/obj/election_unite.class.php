<?php
//$Id$ 
//gen openMairie le 06/05/2021 21:39

require_once "../obj/om_dbform.class.php";

class election_unite_gen extends om_dbform {

    protected $_absolute_class_name = "election_unite";

    var $table = "election_unite";
    var $clePrimaire = "election_unite";
    var $typeCle = "N";
    var $required_field = array(
        "election",
        "election_unite",
        "unite"
    );
    var $unique_key = array(
      array("election","unite"),
    );
    var $foreign_keys_extended = array(
        "election" => array("election", ),
        "unite" => array("unite", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("election");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "election_unite",
            "election",
            "unite",
            "inscrit",
            "votant",
            "blanc",
            "nul",
            "exprime",
            "envoi_aff",
            "envoi_web",
            "emargement",
            "procuration",
            "saisie",
            "validation",
            "date_derniere_modif",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_election() {
        return "SELECT election.election, election.libelle FROM ".DB_PREFIXE."election ORDER BY election.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_election_by_id() {
        return "SELECT election.election, election.libelle FROM ".DB_PREFIXE."election WHERE election = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_unite() {
        return "SELECT unite.unite, unite.libelle FROM ".DB_PREFIXE."unite WHERE ((unite.om_validite_debut IS NULL AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE)) OR (unite.om_validite_debut <= CURRENT_DATE AND (unite.om_validite_fin IS NULL OR unite.om_validite_fin > CURRENT_DATE))) ORDER BY unite.libelle ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_unite_by_id() {
        return "SELECT unite.unite, unite.libelle FROM ".DB_PREFIXE."unite WHERE unite = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['election_unite'])) {
            $this->valF['election_unite'] = ""; // -> requis
        } else {
            $this->valF['election_unite'] = $val['election_unite'];
        }
        if (!is_numeric($val['election'])) {
            $this->valF['election'] = ""; // -> requis
        } else {
            $this->valF['election'] = $val['election'];
        }
        if (!is_numeric($val['unite'])) {
            $this->valF['unite'] = ""; // -> requis
        } else {
            $this->valF['unite'] = $val['unite'];
        }
        if (!is_numeric($val['inscrit'])) {
            $this->valF['inscrit'] = NULL;
        } else {
            $this->valF['inscrit'] = $val['inscrit'];
        }
        if (!is_numeric($val['votant'])) {
            $this->valF['votant'] = NULL;
        } else {
            $this->valF['votant'] = $val['votant'];
        }
        if (!is_numeric($val['blanc'])) {
            $this->valF['blanc'] = NULL;
        } else {
            $this->valF['blanc'] = $val['blanc'];
        }
        if (!is_numeric($val['nul'])) {
            $this->valF['nul'] = NULL;
        } else {
            $this->valF['nul'] = $val['nul'];
        }
        if (!is_numeric($val['exprime'])) {
            $this->valF['exprime'] = NULL;
        } else {
            $this->valF['exprime'] = $val['exprime'];
        }
        if ($val['envoi_aff'] == 1 || $val['envoi_aff'] == "t" || $val['envoi_aff'] == "Oui") {
            $this->valF['envoi_aff'] = true;
        } else {
            $this->valF['envoi_aff'] = false;
        }
        if ($val['envoi_web'] == 1 || $val['envoi_web'] == "t" || $val['envoi_web'] == "Oui") {
            $this->valF['envoi_web'] = true;
        } else {
            $this->valF['envoi_web'] = false;
        }
        if (!is_numeric($val['emargement'])) {
            $this->valF['emargement'] = NULL;
        } else {
            $this->valF['emargement'] = $val['emargement'];
        }
        if (!is_numeric($val['procuration'])) {
            $this->valF['procuration'] = NULL;
        } else {
            $this->valF['procuration'] = $val['procuration'];
        }
        if ($val['saisie'] == "") {
            $this->valF['saisie'] = NULL;
        } else {
            $this->valF['saisie'] = $val['saisie'];
        }
        if ($val['validation'] == "") {
            $this->valF['validation'] = NULL;
        } else {
            $this->valF['validation'] = $val['validation'];
        }
        if (!is_numeric($val['date_derniere_modif'])) {
            $this->valF['date_derniere_modif'] = NULL;
        } else {
            $this->valF['date_derniere_modif'] = $val['date_derniere_modif'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("election_unite", "hidden");
            if ($this->is_in_context_of_foreign_key("election", $this->retourformulaire)) {
                $form->setType("election", "selecthiddenstatic");
            } else {
                $form->setType("election", "select");
            }
            if ($this->is_in_context_of_foreign_key("unite", $this->retourformulaire)) {
                $form->setType("unite", "selecthiddenstatic");
            } else {
                $form->setType("unite", "select");
            }
            $form->setType("inscrit", "text");
            $form->setType("votant", "text");
            $form->setType("blanc", "text");
            $form->setType("nul", "text");
            $form->setType("exprime", "text");
            $form->setType("envoi_aff", "checkbox");
            $form->setType("envoi_web", "checkbox");
            $form->setType("emargement", "text");
            $form->setType("procuration", "text");
            $form->setType("saisie", "text");
            $form->setType("validation", "text");
            $form->setType("date_derniere_modif", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("election_unite", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("election", $this->retourformulaire)) {
                $form->setType("election", "selecthiddenstatic");
            } else {
                $form->setType("election", "select");
            }
            if ($this->is_in_context_of_foreign_key("unite", $this->retourformulaire)) {
                $form->setType("unite", "selecthiddenstatic");
            } else {
                $form->setType("unite", "select");
            }
            $form->setType("inscrit", "text");
            $form->setType("votant", "text");
            $form->setType("blanc", "text");
            $form->setType("nul", "text");
            $form->setType("exprime", "text");
            $form->setType("envoi_aff", "checkbox");
            $form->setType("envoi_web", "checkbox");
            $form->setType("emargement", "text");
            $form->setType("procuration", "text");
            $form->setType("saisie", "text");
            $form->setType("validation", "text");
            $form->setType("date_derniere_modif", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("election_unite", "hiddenstatic");
            $form->setType("election", "selectstatic");
            $form->setType("unite", "selectstatic");
            $form->setType("inscrit", "hiddenstatic");
            $form->setType("votant", "hiddenstatic");
            $form->setType("blanc", "hiddenstatic");
            $form->setType("nul", "hiddenstatic");
            $form->setType("exprime", "hiddenstatic");
            $form->setType("envoi_aff", "hiddenstatic");
            $form->setType("envoi_web", "hiddenstatic");
            $form->setType("emargement", "hiddenstatic");
            $form->setType("procuration", "hiddenstatic");
            $form->setType("saisie", "hiddenstatic");
            $form->setType("validation", "hiddenstatic");
            $form->setType("date_derniere_modif", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("election_unite", "static");
            $form->setType("election", "selectstatic");
            $form->setType("unite", "selectstatic");
            $form->setType("inscrit", "static");
            $form->setType("votant", "static");
            $form->setType("blanc", "static");
            $form->setType("nul", "static");
            $form->setType("exprime", "static");
            $form->setType("envoi_aff", "checkboxstatic");
            $form->setType("envoi_web", "checkboxstatic");
            $form->setType("emargement", "static");
            $form->setType("procuration", "static");
            $form->setType("saisie", "static");
            $form->setType("validation", "static");
            $form->setType("date_derniere_modif", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('election_unite','VerifNum(this)');
        $form->setOnchange('election','VerifNum(this)');
        $form->setOnchange('unite','VerifNum(this)');
        $form->setOnchange('inscrit','VerifNum(this)');
        $form->setOnchange('votant','VerifNum(this)');
        $form->setOnchange('blanc','VerifNum(this)');
        $form->setOnchange('nul','VerifNum(this)');
        $form->setOnchange('exprime','VerifNum(this)');
        $form->setOnchange('emargement','VerifNum(this)');
        $form->setOnchange('procuration','VerifNum(this)');
        $form->setOnchange('date_derniere_modif','VerifFloat(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("election_unite", 11);
        $form->setTaille("election", 11);
        $form->setTaille("unite", 11);
        $form->setTaille("inscrit", 11);
        $form->setTaille("votant", 11);
        $form->setTaille("blanc", 11);
        $form->setTaille("nul", 11);
        $form->setTaille("exprime", 11);
        $form->setTaille("envoi_aff", 1);
        $form->setTaille("envoi_web", 1);
        $form->setTaille("emargement", 11);
        $form->setTaille("procuration", 11);
        $form->setTaille("saisie", 10);
        $form->setTaille("validation", 30);
        $form->setTaille("date_derniere_modif", 20);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("election_unite", 11);
        $form->setMax("election", 11);
        $form->setMax("unite", 11);
        $form->setMax("inscrit", 11);
        $form->setMax("votant", 11);
        $form->setMax("blanc", 11);
        $form->setMax("nul", 11);
        $form->setMax("exprime", 11);
        $form->setMax("envoi_aff", 1);
        $form->setMax("envoi_web", 1);
        $form->setMax("emargement", 11);
        $form->setMax("procuration", 11);
        $form->setMax("saisie", 10);
        $form->setMax("validation", 30);
        $form->setMax("date_derniere_modif", 20);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('election_unite', __('election_unite'));
        $form->setLib('election', __('election'));
        $form->setLib('unite', __('unite'));
        $form->setLib('inscrit', __('inscrit'));
        $form->setLib('votant', __('votant'));
        $form->setLib('blanc', __('blanc'));
        $form->setLib('nul', __('nul'));
        $form->setLib('exprime', __('exprime'));
        $form->setLib('envoi_aff', __('envoi_aff'));
        $form->setLib('envoi_web', __('envoi_web'));
        $form->setLib('emargement', __('emargement'));
        $form->setLib('procuration', __('procuration'));
        $form->setLib('saisie', __('saisie'));
        $form->setLib('validation', __('validation'));
        $form->setLib('date_derniere_modif', __('date_derniere_modif'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // election
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "election",
            $this->get_var_sql_forminc__sql("election"),
            $this->get_var_sql_forminc__sql("election_by_id"),
            false
        );
        // unite
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "unite",
            $this->get_var_sql_forminc__sql("unite"),
            $this->get_var_sql_forminc__sql("unite_by_id"),
            true
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('election', $this->retourformulaire))
                $form->setVal('election', $idxformulaire);
            if($this->is_in_context_of_foreign_key('unite', $this->retourformulaire))
                $form->setVal('unite', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : election_resultat
        $this->rechercheTable($this->f->db, "election_resultat", "election_unite", $id);
        // Verification de la cle secondaire : participation_unite
        $this->rechercheTable($this->f->db, "participation_unite", "election_unite", $id);
    }


}
