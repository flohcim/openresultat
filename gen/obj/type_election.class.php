<?php
//$Id$ 
//gen openMairie le 05/03/2021 14:09

require_once "../obj/om_dbform.class.php";

class type_election_gen extends om_dbform {

    protected $_absolute_class_name = "type_election";

    var $table = "type_election";
    var $clePrimaire = "type_election";
    var $typeCle = "N";
    var $required_field = array(
        "code",
        "libelle",
        "prefecture",
        "type_election"
    );
    
    var $foreign_keys_extended = array(
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("libelle");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "type_election",
            "libelle",
            "code",
            "prefecture",
        );
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['type_election'])) {
            $this->valF['type_election'] = ""; // -> requis
        } else {
            $this->valF['type_election'] = $val['type_election'];
        }
        $this->valF['libelle'] = $val['libelle'];
        $this->valF['code'] = $val['code'];
        $this->valF['prefecture'] = $val['prefecture'];
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("type_election", "hidden");
            $form->setType("libelle", "text");
            $form->setType("code", "text");
            $form->setType("prefecture", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("type_election", "hiddenstatic");
            $form->setType("libelle", "text");
            $form->setType("code", "text");
            $form->setType("prefecture", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("type_election", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("code", "hiddenstatic");
            $form->setType("prefecture", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("type_election", "static");
            $form->setType("libelle", "static");
            $form->setType("code", "static");
            $form->setType("prefecture", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('type_election','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("type_election", 11);
        $form->setTaille("libelle", 30);
        $form->setTaille("code", 10);
        $form->setTaille("prefecture", 10);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("type_election", 11);
        $form->setMax("libelle", 30);
        $form->setMax("code", 8);
        $form->setMax("prefecture", 3);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('type_election', __('type_election'));
        $form->setLib('libelle', __('libelle'));
        $form->setLib('code', __('code'));
        $form->setLib('prefecture', __('prefecture'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : election
        $this->rechercheTable($this->f->db, "election", "type_election", $id);
    }


}
