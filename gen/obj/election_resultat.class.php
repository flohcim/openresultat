<?php
//$Id$ 
//gen openMairie le 05/03/2021 14:09

require_once "../obj/om_dbform.class.php";

class election_resultat_gen extends om_dbform {

    protected $_absolute_class_name = "election_resultat";

    var $table = "election_resultat";
    var $clePrimaire = "election_resultat";
    var $typeCle = "N";
    var $required_field = array(
        "election_candidat",
        "election_resultat",
        "election_unite"
    );
    var $unique_key = array(
      array("election_candidat","election_unite"),
    );
    var $foreign_keys_extended = array(
        "election_candidat" => array("election_candidat", ),
        "election_unite" => array("election_unite", ),
    );
    
    /**
     *
     * @return string
     */
    function get_default_libelle() {
        return $this->getVal($this->clePrimaire)."&nbsp;".$this->getVal("election_unite");
    }

    /**
     *
     * @return array
     */
    function get_var_sql_forminc__champs() {
        return array(
            "election_resultat",
            "election_unite",
            "election_candidat",
            "resultat",
        );
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_election_candidat() {
        return "SELECT election_candidat.election_candidat, election_candidat.election FROM ".DB_PREFIXE."election_candidat ORDER BY election_candidat.election ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_election_candidat_by_id() {
        return "SELECT election_candidat.election_candidat, election_candidat.election FROM ".DB_PREFIXE."election_candidat WHERE election_candidat = <idx>";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_election_unite() {
        return "SELECT election_unite.election_unite, election_unite.election FROM ".DB_PREFIXE."election_unite ORDER BY election_unite.election ASC";
    }

    /**
     *
     * @return string
     */
    function get_var_sql_forminc__sql_election_unite_by_id() {
        return "SELECT election_unite.election_unite, election_unite.election FROM ".DB_PREFIXE."election_unite WHERE election_unite = <idx>";
    }




    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['election_resultat'])) {
            $this->valF['election_resultat'] = ""; // -> requis
        } else {
            $this->valF['election_resultat'] = $val['election_resultat'];
        }
        if (!is_numeric($val['election_unite'])) {
            $this->valF['election_unite'] = ""; // -> requis
        } else {
            $this->valF['election_unite'] = $val['election_unite'];
        }
        if (!is_numeric($val['election_candidat'])) {
            $this->valF['election_candidat'] = ""; // -> requis
        } else {
            $this->valF['election_candidat'] = $val['election_candidat'];
        }
        if (!is_numeric($val['resultat'])) {
            $this->valF['resultat'] = NULL;
        } else {
            $this->valF['resultat'] = $val['resultat'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$dnu1 = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val = array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$dnu1 = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("election_resultat", "hidden");
            if ($this->is_in_context_of_foreign_key("election_unite", $this->retourformulaire)) {
                $form->setType("election_unite", "selecthiddenstatic");
            } else {
                $form->setType("election_unite", "select");
            }
            if ($this->is_in_context_of_foreign_key("election_candidat", $this->retourformulaire)) {
                $form->setType("election_candidat", "selecthiddenstatic");
            } else {
                $form->setType("election_candidat", "select");
            }
            $form->setType("resultat", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("election_resultat", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("election_unite", $this->retourformulaire)) {
                $form->setType("election_unite", "selecthiddenstatic");
            } else {
                $form->setType("election_unite", "select");
            }
            if ($this->is_in_context_of_foreign_key("election_candidat", $this->retourformulaire)) {
                $form->setType("election_candidat", "selecthiddenstatic");
            } else {
                $form->setType("election_candidat", "select");
            }
            $form->setType("resultat", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("election_resultat", "hiddenstatic");
            $form->setType("election_unite", "selectstatic");
            $form->setType("election_candidat", "selectstatic");
            $form->setType("resultat", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("election_resultat", "static");
            $form->setType("election_unite", "selectstatic");
            $form->setType("election_candidat", "selectstatic");
            $form->setType("resultat", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('election_resultat','VerifNum(this)');
        $form->setOnchange('election_unite','VerifNum(this)');
        $form->setOnchange('election_candidat','VerifNum(this)');
        $form->setOnchange('resultat','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("election_resultat", 11);
        $form->setTaille("election_unite", 11);
        $form->setTaille("election_candidat", 11);
        $form->setTaille("resultat", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("election_resultat", 11);
        $form->setMax("election_unite", 11);
        $form->setMax("election_candidat", 11);
        $form->setMax("resultat", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('election_resultat', __('election_resultat'));
        $form->setLib('election_unite', __('election_unite'));
        $form->setLib('election_candidat', __('election_candidat'));
        $form->setLib('resultat', __('resultat'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // election_candidat
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "election_candidat",
            $this->get_var_sql_forminc__sql("election_candidat"),
            $this->get_var_sql_forminc__sql("election_candidat_by_id"),
            false
        );
        // election_unite
        $this->init_select(
            $form, 
            $this->f->db,
            $maj,
            null,
            "election_unite",
            $this->get_var_sql_forminc__sql("election_unite"),
            $this->get_var_sql_forminc__sql("election_unite_by_id"),
            false
        );
    }


    //==================================
    // sous Formulaire
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('election_candidat', $this->retourformulaire))
                $form->setVal('election_candidat', $idxformulaire);
            if($this->is_in_context_of_foreign_key('election_unite', $this->retourformulaire))
                $form->setVal('election_unite', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire
    //==================================
    

}
