<?php
/**
 * Ce script permet d'afficher l'animation de l'élection passée en paramètre.
 *
 * @package openresultat
 * @version SVN : $Id$
 */

/**
 * Définition du répertoire de stockage des résultats
 */
// Récupère le path courant
$current_path = getcwd();
// Récupère le path du répertoire parent
$parent_path = dirname($current_path);
// Compose le path du répertoire qui contient les résultats
$aff_results_path = $parent_path."/aff/res/";
$aff_results_url = "../aff/res/";
// On stocke le répertoire dans une constante pour l'utiliser
// de manière plus lisible dans les scripts inclus
define("AFF_RESULTS_PATH", $aff_results_path);
define("AFF_RESULTS_URL", $aff_results_url);

/**
 *
 */
//
$idx = (isset($_GET["idx"]) ? $_GET["idx"] : NULL);
//
$identifier = (isset($_GET["identifier"]) ? $_GET["identifier"] : 0);

/**
 * 
 */
//
require_once "aff.class.php";
//
$animation = new or_aff($idx, $identifier);
