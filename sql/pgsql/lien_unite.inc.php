<?php
//$Id$
//gen openMairie le 10/08/2020 10:08

include "../gen/sql/pgsql/lien_unite.inc.php";

$tab_title = _("lien entre unités");

if ($retourformulaire === null || $retourformulaire === "") {
    // Lien dans le listing des lien_unite vers le formulaire d'ajout multiple
    $tab_actions['corner']['ajouter_multiple'] = array(
        'lien' => ''.OM_ROUTE_FORM.'&obj='.$obj.'&amp;action=5&amp;idx=0',
        'id' => '&amp;tri='.$tricolsf.'&amp;objsf='.$obj.'&amp;premiersf='.$premier.'&amp;retourformulaire='.$retourformulaire.'&amp;idxformulaire='.$idxformulaire.'&amp;trisf='.$tricolsf.'&amp;retour=tab',
        'lib' => '<span class="om-icon om-icon-16 om-icon-fix add-multiple-lien_unite-16" title="'._('Ajouter plusieurs').'">'._('Ajouter plusieurs').'</span>',
        'rights' => array('list' => array($obj, $obj.'_ajouter'), 'operator' => 'OR'),
        'ordre' => 20,
    );
} else {
    // Lien dans le listing des lien_unite vers le formulaire d'ajout multiple
    $tab_actions['corner']['ajouter_multiple'] = array(
        'lien' => OM_ROUTE_SOUSFORM.'&obj='.$obj.'&action=5&idx=0',
        'id' => '&amp;advs_id='.$advs_id.'&amp;premiersf='.$premier.'&amp;trisf='.$tricol.'&amp;valide='.$valide.'&amp;retourformulaire='.$retourformulaire.'&amp;idxformulaire='.$idxformulaire.'&amp;retour=tab',
        'lib' => '<span class="om-icon om-icon-16 om-icon-fix add-multiple-lien_unite-16" title="'._('Ajouter plusieurs').'">'._('Ajouter plusieurs').'</span>',
        'rights' => array('list' => array($obj, $obj.'_ajouter'), 'operator' => 'OR'),
        'ordre' => 20,
    );
}

$table = DB_PREFIXE."lien_unite
    LEFT JOIN ".DB_PREFIXE."unite as unite0
        ON lien_unite.unite_enfant = unite0.unite
    LEFT JOIN ".DB_PREFIXE."unite as unite1
        ON lien_unite.unite_parent = unite1.unite
    ";

$champAffiche = array(
    'lien_unite.lien_unite as "'.__("id").'"',
    'concat(unite0.code_unite, \' \', unite0.libelle) as "'.__("unité enfant").'"',
    'unite0.ordre as "'.__("ordre").'"',
    'concat(unite1.code_unite, \' \', unite1.libelle) as "'.__("unité parent").'"',
    );
