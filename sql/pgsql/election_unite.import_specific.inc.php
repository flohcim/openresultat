<?php
//$Id$ 
//gen openMairie le 20/01/2021 15:33

$import= "Mise à jour des inscrits d'une élection";
$table="";
$id='election_unite'; // numerotation automatique
$verrou=0;// =0 pas de mise a jour de la base / =1 mise a jour
$fic_rejet=1; // =0 pas de fichier pour relance / =1 fichier relance traitement
$ligne1=1;// = 1 : 1ere ligne contient nom des champs / o sinon
/**
 *
 */
$fields = array(
    "Id" => array( //code du bureau
        "notnull" => "",
        "type" => "int",
        "len" => "10",
        "colonnesName" => array(
            "﻿\"Id\"",
            "Id",
            'Identifiant du bureau'
        )
    ),
    "Code" => array( //code du bureau
        "notnull" => "",
        "type" => "int",
        "len" => "3",
        "colonnesName" => array(
            'Code',
            'Code du bureau'
        )
    ),
    "Inscrits" => array( // Libelle du Bureau
        "notnull" => "1",
        "type" => "integer",
        "len" => "30",
        "colonnesName" => array(
            "Nombre d'électeurs inscrits en LP",
            "Nombre d'électeurs inscrits en LCM",
            "Nombre d'électeurs inscrits en LCE",
            "Nombre d'électeurs inscrits en liste principale",
            "Nombre d'électeurs inscrits en liste complémentaire municipale",
            "Nombre d'électeurs inscrits en liste complémentaire européenne"
        )
    ),
);
