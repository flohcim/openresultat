<?php
//$Id$ 
//gen openMairie le 05/03/2021 17:08

include "../gen/sql/pgsql/delegation.inc.php";

$workflow = $this->get_workflow_election();

$tab_actions['corner']['ajouter_multiple'] =
array('lien' => ''.OM_ROUTE_SOUSFORM.'&obj='.$obj.'&amp;action=5&amp;idx=0',
      'id' => '&amp;tri='.$tricolsf.'&amp;objsf='.$obj.'&amp;premiersf='.$premier.'&amp;retourformulaire='.$retourformulaire.'&amp;idxformulaire='.$idxformulaire.'&amp;trisf='.$tricolsf.'&amp;retour=tab',
      'lib' => '<span class="om-icon om-icon-16 om-icon-fix add-multiple-delegation-16" title="'._('Ajouter plusieurs').'">'._('Ajouter plusieurs').'</span>',
      'rights' => array('list' => array($obj, $obj.'_ajouter'), 'operator' => 'OR'),
);

if ($workflow === false ||
    $workflow === 'Finalisation' ||
    $workflow === 'Archivage'
) {
    $tab_actions['corner']['ajouter'] = null;
    $tab_actions['corner']['ajouter_multiple'] = null;
}

// SELECT 
$champAffiche = array(
    'delegation.delegation as "'.__("delegation").'"',
    'election.libelle as "'.__("election").'"',
    'concat(unite.code_unite, \' \', unite.libelle) as "'.__("unité").'"',
    'acteur.nom as "'.__("acteur").'"',
    );
