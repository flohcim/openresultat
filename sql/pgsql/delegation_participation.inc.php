<?php
//$Id$
//gen openMairie le 30/05/2019 16:10

include "election_unite.inc.php";

if (in_array($retourformulaire, $foreign_keys_extended["election"])) {
    $tab_actions['corner']['ajouter'] = null;
}

$table = DB_PREFIXE."election_unite
    LEFT JOIN ".DB_PREFIXE."election
        ON election_unite.election = election.election
    LEFT JOIN ".DB_PREFIXE."unite
        ON election_unite.unite = unite.unite
    LEFT JOIN ".DB_PREFIXE."resultat_participation_unite
        ON election_unite.election_unite = resultat_participation_unite.election_unite
    -- Correction du nombre de votant. A terme cette correction devrait permettre de
    -- remplacer la vue resultat_participation_unite.
    -- Le nombre de votant est cumulé d'heure en heure. Du coup, en récupérant le max de
    -- votant on est sur de récupérer la dernière valeur saisie et donc le total de participation
    -- du bureau
    LEFT JOIN (
        SELECT
            election_unite.election_unite,
            MAX(participation_unite.votant) as votant
        FROM
            ".DB_PREFIXE."election_unite
            JOIN ".DB_PREFIXE."participation_unite
                ON election_unite.election_unite = participation_unite.election_unite
        GROUP BY
            election_unite.election_unite) AS total_participation_unite
        ON election_unite.election_unite = total_participation_unite.election_unite";

// SELECT
$displayed_field_election = 'election.libelle as "'.__("election").'"';
$champAffiche = array(
    'election_unite.election_unite as "'.__("id").'"',
    $displayed_field_election,
    'concat(unite.code_unite, \' \', unite.libelle) as "'.__("unité").'"',
    'total_participation_unite.votant as "'.__("votant").'"',
    "case
        resultat_participation_unite.saisie
    when
        't'
        then '<img src=../app/img/arrive.png>'
        else '<img src=../app/img/nonarrive.png>'
    end as \"".__("saisie")."\"",
    "case
        resultat_participation_unite.envoi_aff
    when
        't'
        then '<img src=../app/img/arrive.png>'
        else '<img src=../app/img/nonarrive.png>'
    end as \"".__("aff")."\"",
    "case
        resultat_participation_unite.envoi_web
    when
        't'
        then '<img src=../app/img/arrive.png>'
        else '<img src=../app/img/nonarrive.png>'
    end as \"".__("web")."\""
);
// La colonne élection n'est pas nécessaire dans le contexte d'une élection
if (in_array($retourformulaire, $foreign_keys_extended["election"])) {
    $champAffiche = array_diff($champAffiche, array($displayed_field_election, ));
    $champRecherche = array_diff($champRecherche, array($displayed_field_election, ));
}
$tab_title = _("delegation_participation");
$ent .= " -> "._("delegation_participation");
