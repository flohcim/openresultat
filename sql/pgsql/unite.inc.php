<?php
//$Id$
//gen openMairie le 10/08/2020 10:08

include "../gen/sql/pgsql/unite.inc.php";

// FROM 
$table = DB_PREFIXE."unite
    LEFT JOIN ".DB_PREFIXE."canton 
        ON unite.canton=canton.canton 
    LEFT JOIN ".DB_PREFIXE."circonscription 
        ON unite.circonscription=circonscription.circonscription 
    LEFT JOIN ".DB_PREFIXE."commune 
        ON unite.commune=commune.commune 
    LEFT JOIN ".DB_PREFIXE."departement 
        ON unite.departement=departement.departement 
    LEFT JOIN ".DB_PREFIXE."type_unite 
        ON unite.type_unite=type_unite.type_unite     
    ";

// SELECT
$displayed_field__perimetre__case = "CASE unite.perimetre WHEN 't' THEN 'Oui' ELSE '' END";
$displayed_field__perimetre = $displayed_field__perimetre__case." as \"".__("périmètre")."\"";
$champAffiche = array(
    'unite.unite as "'.__("id").'"',
    'unite.code_unite as "'.__("code unité").'"',
    'unite.libelle as "'.__("libelle").'"',
    'type_unite.libelle as "'.__("type").'"',
    'unite.ordre as "'.__("ordre").'"',
    "concat(canton.libelle, ' (',canton.code, ')[', canton.prefecture, ']') as \"".__("canton")."\"",
    "concat(circonscription.libelle, ' (',circonscription.code, ')[', circonscription.prefecture, ']') as \"".__("circonscription")."\"",
    'unite.adresse1 as "'.__("adresse 1").'"',
    'unite.adresse2 as "'.__("adresse 2").'"',
    'unite.cp as "'.__("cp").'"',
    'unite.ville as "'.__("ville").'"',
    $displayed_field__perimetre,
);

$sousformulaire = array(
    'lien_unite',
);

$sousformulaire_parameters = array(
    "lien_unite" => array(
        "title" => _("Lien entre les unités")
    )
);


if (isset($retourformulaire) === false || $retourformulaire === "") {
    // Action import-unites
    $tab_actions["corner"]["import-unites"] = array(
        "lien" => OM_ROUTE_FORM."&obj=".$obj."&action=9&idx=0",
        "id" => "&advs_id=".$advs_id."&premier=".$premier."&tricol=".$tricol."&valide=".$valide."&retour=tab",
        "lib" => "<span class=\"om-icon om-icon-16 om-icon-fix import-unites-16\" title=\""._("Importer")."\">"._("Importer")."</span>",
        "rights" => array("list" => array($obj, $obj."_importer"), "operator" => "OR"),
    );
} else {
    $tab_actions['corner']['ajouter'] = null;
}

// Gestion des options du listing
if (!isset($options)) {
    $options = array();
}
// Option condition périmètre : permet d'ajouter la classe css unite-perimetre
// sur chaque enregistrement qui est un périmètre.
$option_condition_perimetre = array(
    "type" => "condition",
    "field" => $displayed_field__perimetre__case,
    "case" => array(
        array(
            "values" => array("Oui", ),
            "style" => "unite-perimetre",
        ),
    ),
);
$options[] = $option_condition_perimetre;

$champRecherche = array(
    'unite.unite as "'.__("unite").'"',
    'unite.libelle as "'.__("libelle").'"',
    'type_unite.libelle as "'.__("type_unite").'"',
    'unite.ordre as "'.__("ordre").'"',
    'unite.adresse1 as "'.__("adresse1").'"',
    'unite.adresse2 as "'.__("adresse2").'"',
    'unite.cp as "'.__("cp").'"',
    'unite.ville as "'.__("ville").'"',
    'unite.coordinates as "'.__("coordinates").'"',
    'type_unite.libelle as "'.__("type_unite_contenu").'"',
    'canton.libelle as "'.__("canton").'"',
    'circonscription.libelle as "'.__("circonscription").'"',
    'commune.libelle as "'.__("commune").'"',
    'departement.libelle as "'.__("departement").'"',
    'unite.code_unite as "'.__("code_unite").'"',
    'unite.id_reu as "'.__("id_reu").'"',
    );
