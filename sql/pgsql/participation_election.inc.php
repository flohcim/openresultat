<?php
/**
 * Ce script contient la définition des variables de l'objet *participation_election*.
 *
 * @package openresultat
 * @version SVN : $Id$
 */

include "../gen/sql/pgsql/participation_election.inc.php";

// Impossible d'ajouter un enregistrement manuellement, il faut passer par les
// actions de l'élection, donc on cache l'action ajouter
$tab_actions['corner']['ajouter'] = null;

$tri=" order by tranche.ordre ";

$table = DB_PREFIXE."participation_election
    LEFT JOIN ".DB_PREFIXE."election
        ON participation_election.election=election.election
    LEFT JOIN ".DB_PREFIXE."tranche
        ON participation_election.tranche=tranche.tranche
    LEFT JOIN ".DB_PREFIXE."resultat_participation_tranche
        ON participation_election.participation_election =
            resultat_participation_tranche.participation_election";

// SELECT
$displayed_field__election = 'election.libelle as "'.__("election").'"';
$champAffiche = array(
    'participation_election.participation_election as "'.__("id").'"',
    $displayed_field__election,
    'tranche.libelle as "'.__("tranche").'"',
    'resultat_participation_tranche.votant as "'.__("votant").'"',
    "case
        resultat_participation_tranche.saisie
    when
        't'
        then '<img src=../app/img/arrive.png>'
        else '<img src=../app/img/nonarrive.png>'
    end as \"".__("saisie")."\"",
    "case
        resultat_participation_tranche.envoi_aff
    when
        't'
        then '<img src=../app/img/arrive.png>'
        else '<img src=../app/img/nonarrive.png>'
    end as \"".__("aff")."\"",
    "case
        resultat_participation_tranche.envoi_web
    when
        't'
        then '<img src=../app/img/arrive.png>'
        else '<img src=../app/img/nonarrive.png>'
    end as \"".__("web")."\""
);
$champRecherche = array(
    'participation_election.participation_election as "'.__("participation_election").'"',
    $displayed_field__election,
    'tranche.libelle as "'.__("tranche").'"',
    'participation_election.date_derniere_modif as "'.__("date_derniere_modif").'"',
);

// Dans le contexte d'une élection
if (in_array($retourformulaire, $foreign_keys_extended["election"])) {
    // La colonne élection n'est pas nécessaire
    $champAffiche = array_diff($champAffiche, array($displayed_field__election, ));
    $champRecherche = array_diff($champRecherche, array($displayed_field__election, ));
}
