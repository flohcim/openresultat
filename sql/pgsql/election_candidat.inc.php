<?php
/**
 * Ce script contient la définition des variables de l'objet *election_candidat*.
 *
 * @package openresultat
 * @version SVN : $Id$
 */

include "../gen/sql/pgsql/election_candidat.inc.php";

// FROM
// Jointure supplémentaire sur la VIEW candidat_resultat pour ajouter la
// colonne voix dans le lsiting
$table .= sprintf(
    '
        LEFT JOIN %1$scandidat_resultat
            ON election_candidat.election_candidat=candidat_resultat.election_candidat
    ',
    DB_PREFIXE
);

// SELECT
// Renommage de la clé primaire en id
// Ajout de la colonne représentant le nombre de voix depuis la VIEW
// candidat_resultat
$displayed_field__id = 'election_candidat.election_candidat as "'.__("id").'"';
$displayed_field__election = 'election.libelle as "'.__("election").'"';
$champAffiche = array(
    $displayed_field__id,
    $displayed_field__election,
    'election_candidat.ordre as "'.__("ordre").'"',
    'candidat.libelle as "'.__("candidat").'"',
    'candidat_resultat.resultat as "'.__("voix").'"',
    'election_candidat.prefecture as "'.__("prefecture").'"',
);
$champRecherche = array(
    $displayed_field__id,
    $displayed_field__election,
    'candidat.libelle as "'.__("candidat").'"',
    'election_candidat.ordre as "'.__("ordre").'"',
    'election_candidat.prefecture as "'.__("prefecture").'"',
);

// SORT
$tri = " ORDER BY election.date, election.libelle, election_candidat.ordre ";

// Dans le contexte d'une élection
if (in_array($retourformulaire, $foreign_keys_extended["election"])) {
    // La colonne élection n'est pas nécessaire
    $champAffiche = array_diff($champAffiche, array($displayed_field__election, ));
    $champRecherche = array_diff($champRecherche, array($displayed_field__election, ));
    // On cache l'action ajouter si l'étape de workflow de l'élection le nécessite
    $workflow = $this->get_workflow_election();
    if (in_array($workflow, array(false, "Saisie", "Finalisation", "Archivage"), true) === true) {
        $tab_actions["corner"]["ajouter"] = null;
    }
}
