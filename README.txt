openRésultat - ReadMe
=====================

openRésultat permet la gestion des résultats électoraux lors de soirées
électorales : export des résultats pour la préfecture, animation sur
grand écran, publication des résultats sur le web, gestion des centaines,
de la participation et bien plus encore.

Toutes les informations sur http://www.openmairie.org/


Documentation
-------------

Toutes les instructions pour l'installation, l'utilisation et la prise en main
du logiciel dans la documentation en ligne :
http://docs.openmairie.org/?project=openresultat


Licence
-------

Voir le fichier LICENSE.txt
